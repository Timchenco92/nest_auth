// Modules
import { FC } from 'react';
import { Box, CircularProgress } from '@mui/material';

export const LoaderComponent: FC = (): JSX.Element => {
  const minHeight = window.innerHeight;

  return (
    <Box sx={{
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center',
      minHeight,
      width: '100%',
    }}>
      <CircularProgress />
    </Box>
  );
};

export default LoaderComponent;
