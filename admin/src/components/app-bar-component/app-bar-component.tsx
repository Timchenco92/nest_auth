// Modules
import { FC } from 'react';
import { AppBar as MuiAppBar, Toolbar, IconButton, styled, Theme, Typography } from '@mui/material';
import { Menu } from '@mui/icons-material';

// Interfaces
import { IProps } from '@/components/app-bar-component/IProps';

interface RootProps {
  theme?: Theme;
  open?: boolean;
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<RootProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

export const AppBarComponent:FC<IProps> = ({ handleToggleDrawer, isOpen }): JSX.Element => {
  return (
    <AppBar  open={isOpen} position="absolute">
      <Toolbar sx={{ pr: '24px' }}>
        <IconButton
          size="large"
          edge="start"
          color="inherit"
          aria-label="menu"
          sx={{ mr: 2 }}
          onClick={handleToggleDrawer}
        >
          <Menu />
        </IconButton>
        <Typography
          component="h1"
          variant="h6"
          color="inherit"
          noWrap
          sx={{ flexGrow: 1 }}
        >
          Dashboard
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default AppBarComponent;
