// Types
import { MouseEventHandler } from 'react';

export interface IProps {
  handleToggleDrawer: MouseEventHandler;
  isOpen: boolean;
}
