// Modules
import { FC, forwardRef, SyntheticEvent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Snackbar, AlertProps, Alert as MuiAlert } from '@mui/material';

// Engine
import { hideToast, isShowingSelector, messageSelector, typeSelector } from '@/store/toast';

const Alert = forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export const CustomSnackbarComponent: FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const isOpen = useSelector(isShowingSelector);
  const message = useSelector(messageSelector);
  const severity = useSelector(typeSelector);

  const handleClose = (event?: SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    const action = hideToast({ isShowing: false, message: '', type: severity });

    dispatch(action);
  };

  return (
    <Snackbar
      anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
      autoHideDuration={3000}
      onClose={handleClose}
      open={isOpen}
    >
      <Alert onClose={handleClose} severity={severity} sx={{ color: 'white', width: '100%' }}>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default CustomSnackbarComponent;
