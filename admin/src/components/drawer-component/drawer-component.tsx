// Modules
import { FC } from 'react';
import {
  Divider,
  Drawer as MuiDrawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
  styled,
} from '@mui/material';
import { PowerSettingsNew, ChevronLeft } from '@mui/icons-material';
import { Link, useLocation } from 'react-router-dom';

// Constants
import routes from '@/constants/routes';

// Interfaces
import { IProps } from '@/components/drawer-component/IProps';

const drawerWidth = 240;

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      }),
    },
  }),
);

export const DrawerComponent: FC<IProps> = (props): JSX.Element => {
  const { handleToggleDrawer, handleLogout, isOpen } = props;
  const { pathname } = useLocation();
  const { listDrawerRoutes } = routes;

  const formattedPathname = pathname.split('/', 3).join('/');

  return (
    <Drawer variant="permanent" open={isOpen}>
      <Toolbar
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'flex-end',
          px: [1],
        }}
      >
        <IconButton onClick={handleToggleDrawer}>
          <ChevronLeft />
        </IconButton>
      </Toolbar>
      <Divider />
      <List>
        {
          listDrawerRoutes.map(({ name, path, icon: Icon }, idx) => {
            return (
              <ListItemButton key={idx} component={Link} to={path} selected={path === formattedPathname}>
                <ListItemIcon>
                  <Icon />
                </ListItemIcon>
                <ListItemText primary={name} />
              </ListItemButton>
            );
          })
        }
        <ListItem button onClick={handleLogout}>
          <ListItemIcon>
            <PowerSettingsNew color={'error'} />
          </ListItemIcon>
          <ListItemText primary={'Logout'} />
        </ListItem>
      </List>
    </Drawer>
  );
};

export default DrawerComponent;
