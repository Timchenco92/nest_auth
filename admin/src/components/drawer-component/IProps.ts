// Types
import { MouseEventHandler } from 'react';

export interface IProps {
  handleToggleDrawer: MouseEventHandler;
  handleLogout: MouseEventHandler;
  isOpen: boolean;
}
