// Modules
import { ChangeEvent, FC } from 'react';
import { Box, FormControl, Grid, InputLabel, MenuItem, Pagination, Select, SelectChangeEvent } from '@mui/material';

interface IProps {
  currentPage: string;
  handleChangeLimit(event: SelectChangeEvent): void;
  handleChangePage(event: ChangeEvent<unknown>, page: number): void;
  limit: string;
  totalPages: number;
}

export const PaginationComponent: FC<IProps> = (props): JSX.Element => {
  const { currentPage, handleChangeLimit, handleChangePage, limit, totalPages } = props;

  return (
    <Box>
      <Grid item paddingY={2}>
        <Grid
          container
          direction="row"
          justifyContent="flex-end"
          alignItems="center"
        >
          <Grid item>
            <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
              <InputLabel>Limit</InputLabel>
              <Select
                defaultValue={limit}
                label="Limit"
                onChange={handleChangeLimit}
              >
                <MenuItem value="15">15</MenuItem>
                <MenuItem value="30">30</MenuItem>
                <MenuItem value="100">100</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item>
            <Pagination
              count={totalPages}
              page={Number(currentPage)}
              onChange={handleChangePage}
              size="medium"
            />
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

PaginationComponent.defaultProps = {
  currentPage: '1',
  limit: '15',
};
