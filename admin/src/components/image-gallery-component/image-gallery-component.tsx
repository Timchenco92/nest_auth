// Modules
import { Dialog, DialogContent, Grid, IconButton } from '@mui/material';
import { ArrowBack, ArrowForward } from '@mui/icons-material';

// Modules Types
import { FC } from 'react';

// Interfaces
import type { IProps } from './IProps';

export const ImageGalleryComponent: FC<IProps> = (props): JSX.Element => {
  const {
    handleClose,
    handleNext,
    handlePrevious,
    image,
    open,
  } = props;

  return (
    <Dialog
      fullWidth
      maxWidth="md"
      open={open}
      onClose={handleClose}
    >
      <Grid container>
        <Grid display="flex" item xs={1}>
          <IconButton
            onClick={handlePrevious}
            sx={{ margin: 'auto' }}
            type="button"
          >
            <ArrowBack />
          </IconButton>
        </Grid>
        <Grid item xs={10}>
          <DialogContent>
            <img width="100%" height="450px" src={image} alt="" />
          </DialogContent>
        </Grid>
        <Grid display="flex" item xs={1}>
          <IconButton
            onClick={handleNext}
            sx={{ margin: 'auto' }}
            type="button"
          >
            <ArrowForward />
          </IconButton>
        </Grid>
      </Grid>
    </Dialog>
  );
};
