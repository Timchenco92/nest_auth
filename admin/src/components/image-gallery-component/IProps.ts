// Modules Types
import { KeyboardEvent } from 'react';

export interface IProps {
  handleClose: () => void;
  handleNext: () => void;
  handlePrevious: () => void;
  image: string;
  open: boolean;
}
