export interface IInitialValues {
  avatarUrl: string | undefined;
  email: string;
  firstName: string;
  id?: string;
  lastName: string;
  middleName: string;
  password: string | undefined;
  phone: string;
}
