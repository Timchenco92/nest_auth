// Modules
import { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, ISettingsValidationErrors } from '@/screens/settings/interfaces';

export interface ISettingsScreenProps {
  errors: ISettingsValidationErrors;
  formik: FormikProps<IInitialValues>;
  pending: boolean;
}
