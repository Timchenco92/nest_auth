// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  firstName: string().required(isRequired),
  middleName: mixed().required(isRequired),
  lastName: string().required(isRequired),
  email: string().required(isRequired),
  phone: string().required(isRequired),
  password: string().notRequired(),
});
