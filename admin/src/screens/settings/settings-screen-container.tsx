// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema } from '@/screens/settings/form-config';

// Engine
import { getMeRequest } from '@/store/auth';
import {
  resetUpdateAdminError,
  resetUpdateAdminSuccess,
  updateAdminSettingsErrorSelector,
  updateAdminSettingsPendingSelector,
  updateAdminSettingsRequest,
  updateAdminSettingsSuccessSelector,
} from '@/store/settings';

import { adminSelector } from '@/store/auth';

// Interfaces
import { IInitialValues } from '@/screens/settings/interfaces';

// Screens
import { SettingsScreen } from '@/screens/settings/settings-screen';

export const SettingsScreenContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const isUpdate = useSelector(updateAdminSettingsSuccessSelector);
  const { errors, hasError } = useSelector(updateAdminSettingsErrorSelector);
  const pending = useSelector(updateAdminSettingsPendingSelector);
  const initialValues = useSelector(adminSelector);
  const adminId = initialValues?.id;

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    const action = updateAdminSettingsRequest({ id: adminId, data: values });
    dispatch(action);
  }, [adminId, dispatch]);

  useEffect(() => {
    if (initialValues) return;

    const action = getMeRequest();
    dispatch(action);
  }, [dispatch, initialValues]);

  useEffect(() => {
    if (isUpdate) return;

    const action = resetUpdateAdminSuccess();
    dispatch(action);
  }, [dispatch, isUpdate]);

  useEffect(() => () => {
    const action = resetUpdateAdminError();
    dispatch(action);
  }, [dispatch]);

  return (
    <SettingsScreen errors={errors} formik={formik} pending={pending} />
  );
};

export default SettingsScreenContainer;
