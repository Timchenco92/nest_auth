// Modules
import { useEffect } from 'react';
import { Box, Button, Grid, TextField, Typography } from '@mui/material';

// Modules Types
import { FC } from 'react';

// Interfaces
import type { ISettingsScreenProps } from '@/screens/settings/interfaces';

export const SettingsScreen: FC<ISettingsScreenProps> = (props): JSX.Element => {
  const { formik, errors: serverValidationErrors, pending } = props;

  const {
    dirty,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    touched,
    values,
  } = formik;

  const isSubmitDisabled: boolean = !dirty || isSubmitting || isValid || pending

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2} maxWidth="100%" margin={0} justifyContent="center">
        <Grid item md={8} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px" textAlign="center">
            Updating Settings
          </Typography>
        </Grid>
        <Grid item md={8} xs={12} marginY="10px" paddingY="15px">
          <form onSubmit={handleSubmit}>
            <TextField
              error={touched.firstName && Boolean(errors.firstName)}
              fullWidth
              helperText={touched.firstName && errors.firstName}
              label="First Name"
              name="firstName"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="text"
              value={values.firstName}
              variant="standard"
              sx={{ marginY: '25px' }}
            />
            <TextField
              error={touched.middleName && Boolean(errors.middleName)}
              fullWidth
              helperText={touched.middleName && errors.middleName}
              label="Middle Name"
              name="middleName"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="text"
              value={values.middleName}
              variant="standard"
              sx={{ marginY: '25px' }}
            />
            <TextField
              error={touched.lastName && Boolean(errors.lastName)}
              fullWidth
              helperText={touched.lastName && errors.lastName}
              label="Last Name"
              name="lastName"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="text"
              value={values.lastName}
              variant="standard"
              sx={{ marginY: '25px' }}
            />
            <TextField
              error={touched.email && Boolean(errors.email)}
              fullWidth
              helperText={touched.email && errors.email}
              label="Email"
              name="email"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="email"
              value={values.email}
              variant="standard"
              sx={{ marginY: '25px' }}
            />
            <TextField
              error={touched.phone && Boolean(errors.phone)}
              fullWidth
              helperText={touched.phone && errors.phone}
              label="Phone"
              name="phone"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="tel"
              value={values.phone}
              variant="standard"
              sx={{ marginY: '25px' }}
            />
            <TextField
              error={touched.password && Boolean(errors.password)}
              fullWidth
              helperText={touched.password && errors.password}
              label="Password"
              name="password"
              onBlur={handleBlur}
              onChange={handleChange}
              type="password"
              value={values.password}
              variant="standard"
              sx={{ marginY: '25px' }}
            />
          </form>
        </Grid>
        <Grid item md={8} xs={12} marginY="10px" paddingY="15px">
          <Button color="success" disabled={isSubmitDisabled} type="submit" fullWidth>Update</Button>
        </Grid>
      </Grid>
    </Box>
  );
};
