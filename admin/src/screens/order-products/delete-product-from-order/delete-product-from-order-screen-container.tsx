// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Engine
import { deleteOrderProductSuccessSelector, resetDeleteOrderProductSuccess } from '@/store/order-products';

// Screens
import { DeleteProductFromOrderScreen } from '@/screens/order-products/delete-product-from-order/delete-product-from-order-screen';

export const DeleteProductFromOrderScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const isDeleted = useSelector(deleteOrderProductSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();

    return () => {
      const action = resetDeleteOrderProductSuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteProductFromOrderScreen handleClose={handleClose}/>
  );
};
