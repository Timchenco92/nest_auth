// Modules
import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Engine
import { deleteOrderProductRequest } from '@/store/order-products';

export interface IProps {
  handleClose(): void;
}

export const DeleteProductFromOrderScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;

  const { orderProductId, orderId } = useParams<{ orderProductId: 'orderProductId', orderId: 'orderId' }>();
  const dispatch: Dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!orderProductId || !orderId) return;

    const action = deleteOrderProductRequest(orderProductId, orderId);
    dispatch(action)
  }, [orderProductId, orderId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing product from order</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};
