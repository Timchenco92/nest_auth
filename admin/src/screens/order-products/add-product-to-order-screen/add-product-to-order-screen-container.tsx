// Modules
import { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/order-products/add-product-to-order-screen/form-config';

// Engine
import {
  addOrderProductRequest,
  addOrderProductErrorSelector,
  addOrderProductPendingSelector,
  addOrderProductSuccessSelector,
  resetAddOrderProductSuccess,
  orderProductsSelector,
} from '@/store/order-products';

// Interfaces
import type { IInitialValues } from '@/screens/order-products/add-product-to-order-screen/interfaces';

// Screens
import { AddProductToOrderScreen } from '@/screens/order-products/add-product-to-order-screen/add-product-to-order-screen';

export const AddProductToOrderScreenContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { orderId } = useParams<'orderId'>();
  const [orderTotal, setOrderTotal] = useState<number>(0);

  const { errors, hasError } = useSelector(addOrderProductErrorSelector);
  const isAdded = useSelector(addOrderProductSuccessSelector);
  const orderProducts = useSelector(orderProductsSelector);
  const addOrderProductPending = useSelector(addOrderProductPendingSelector);

  const handleClose = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      await handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback(async (values: IInitialValues) => {
    if (!orderId) return;

    const action = addOrderProductRequest({
      orderId,
      orderProducts: values.orderProducts,
      orderTotal,
    });
    dispatch(action);
  }, [dispatch, orderId, orderTotal]);

  useEffect(() => {
    if (!isAdded) return;
    handleClose();
    return() => {
      const action = resetAddOrderProductSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isAdded]);

  return (
    <AddProductToOrderScreen
      addOrderProductPending={addOrderProductPending}
      errors={errors}
      formik={formik}
      handleClose={handleClose}
      orderProducts={orderProducts}
      orderTotal={orderTotal}
      setOrderTotal={setOrderTotal}
    />
  );
};
