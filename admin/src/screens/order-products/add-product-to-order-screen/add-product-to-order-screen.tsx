// Modules
import { useCallback, useEffect, useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';

// Modules Types
import type { ChangeEvent, FC } from 'react';
import type { Dispatch } from 'redux';
import type { TextFieldProps } from '@mui/material';

// Components
import { AddedProductsComponent } from '@/screens/order-products/add-product-to-order-screen/components/added-products-component';
import { ProductsAutocompleteComponent } from '@/screens/order-products/add-product-to-order-screen/components/products-autocomplete-component';

// Engine
import {
  getProductsRequest,
  paginationAndQuerySearchSelector as productsPaginationAndQuerySearchSelector,
  productsPendingSelector,
  productsSelector,
  resetQuerySearch as resetProductsQuerySearch,
} from '@/store/products';

import { showToast } from '@/store/toast';

// Interfaces
import type { IAddProductToOrderScreenProps } from '@/screens/order-products/add-product-to-order-screen/interfaces';
import type { IAddedProduct } from '@/screens/order-products/add-product-to-order-screen/interfaces';

export const AddProductToOrderScreen: FC<IAddProductToOrderScreenProps> = (props): JSX.Element => {
  const {
    errors: serverValidationErrors,
    formik,
    handleClose,
    orderProducts,
    orderTotal,
    setOrderTotal,
  } = props;

  const dispatch: Dispatch = useDispatch();

  const [isProductVisible, setIsProductVisible] = useState<boolean>(false);
  const [addedProducts, setAddedProducts] = useState<IAddedProduct[]>([]);
  const productAutocompleteRef = useRef<TextFieldProps>(null);
  const { orderId } = useParams<'orderId'>();

  const { data: products } = useSelector(productsSelector);
  const productsPending = useSelector(productsPendingSelector);
  const { limit: productsLimit, page: productsPage } = useSelector(productsPaginationAndQuerySearchSelector);

  const {
    errors,
    handleBlur,
    handleSubmit,
    isSubmitting,
    setErrors,
    setFieldTouched,
    setFieldValue,
    touched,
  } = formik;

  const handleChangeProductAutocomplete = useCallback((event: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target;
    setFieldTouched('products', true);
    setIsProductVisible(true);
    const action = getProductsRequest(productsLimit, productsPage, value);
    dispatch(action);
  }, [dispatch, productsLimit, productsPage, setFieldTouched]);

  const handleSelectProduct = useCallback(async ({ price = '', productId = '', title = '' }) => {
    const isAdded = orderProducts.find((product) => product.productId === productId);
    if (!orderId) return;

    if (isAdded) {
      const toastAction = showToast({
        isShowing: true,
        message: 'The current product is already added.',
        type: 'error',
      });
      dispatch(toastAction);
    } else {
      addedProducts.push({
        price,
        productId,
        quantity: 1,
        title,
        total: price,
        orderId,
      });

      setOrderTotal((prevState) => {
        prevState += Number(price);
        return prevState;
      });

      await setFieldValue('orderProducts', addedProducts);
      setIsProductVisible(false);
      if (productAutocompleteRef.current) productAutocompleteRef.current.value = '';
      dispatch(showToast({ isShowing: true, message: 'The product successfully added.', type: 'success' }));
      dispatch(resetProductsQuerySearch());
    }
  }, [addedProducts, dispatch, orderId, orderProducts, setFieldValue, setOrderTotal]);

  const handleVisibleProductsItems = () => setIsProductVisible(false);

  const handleDecrementAddedProductCount = useCallback((productId: IAddedProduct['productId']) => {
    if (!addedProducts.length) return;

    setAddedProducts((prevState) => {
      return prevState.map((product) => {
        if (product.productId === productId && product.quantity >= 2) {
          product.quantity--;
          const total = product.quantity * Number(product.price);
          product.total = String(total);
          setOrderTotal((prevState) => {
            prevState -= Number(product.price);
            return prevState;
          });
        }
        return product;
      });
    });
  }, [addedProducts.length, setOrderTotal]);

  const handleIncrementAddedProductCount = useCallback((productId: IAddedProduct['productId']) => {
    if (!addedProducts.length) return;

    setAddedProducts((prevState) => {
      return prevState.map((product) => {
        if (product.productId === productId) {
          product.quantity++;
          const total = product.quantity * Number(product.price);
          product.total = String(total);
          setOrderTotal((prevState) => {
            prevState += Number(product.price);
            return prevState;
          });
        }
        return product;
      });
    });
  }, [addedProducts.length, setOrderTotal]);

  const handleRemoveProductFromOrder = useCallback((product: IAddedProduct) => {
    const idx = addedProducts.indexOf(product);

    if (idx > -1) {
      addedProducts.splice(idx, 1);

      const toastAction = showToast({
        isShowing: true,
        message: 'The product successfully removed from order.',
        type: 'success',
      });

      dispatch(toastAction);
    }
  }, [addedProducts, dispatch]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 100 } }}
    >
      <DialogTitle>Adding a product to order</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <ProductsAutocompleteComponent
            errors={errors}
            handleBlur={handleBlur}
            handleChangeProductAutocomplete={handleChangeProductAutocomplete}
            handleSelectProduct={handleSelectProduct}
            handleVisibleProductsItems={handleVisibleProductsItems}
            isProductVisible={isProductVisible}
            productAutocompleteRef={productAutocompleteRef}
            products={products}
            productsPending={productsPending}
            touched={touched}
          />
          <AddedProductsComponent
            handleDecrementAddedProductCount={handleDecrementAddedProductCount}
            handleIncrementAddedProductCount={handleIncrementAddedProductCount}
            handleRemoveProductFromOrder={handleRemoveProductFromOrder}
            products={addedProducts}
            total={orderTotal}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitting}
            color="success"
            fullWidth
            type="submit"
          >
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
