// Modules Types
import type { ChangeEvent, FocusEvent, Ref } from 'react';
import type { FormikErrors, FormikTouched } from 'formik';
import type { TextFieldProps } from '@mui/material';

// Interfaces
import type { IProduct } from '@/interfaces/models';
import type { IInitialValues } from '@/screens/orders/screens/add-order-screen/interfaces';

export interface IProductsAutocompleteComponentProps {
  errors: FormikErrors<IInitialValues>;
  handleBlur(event: FocusEvent<any, any>): void;
  handleChangeProductAutocomplete(event: ChangeEvent<HTMLTextAreaElement>): void;
  handleSelectProduct(
    {
      price = '',
      productId = '',
      title = '',
    }
  ): void;
  handleVisibleProductsItems(): void;
  isProductVisible: boolean;
  touched: FormikTouched<IInitialValues>;
  productAutocompleteRef: Ref<TextFieldProps>;
  products: IProduct[];
  productsPending: boolean;
}
