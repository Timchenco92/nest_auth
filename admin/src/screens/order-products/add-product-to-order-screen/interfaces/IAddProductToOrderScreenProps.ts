// Modules Types
import type { Dispatch, SetStateAction } from 'react';
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues } from '@/screens/order-products/add-product-to-order-screen/interfaces';
import type { IOrderProduct } from '@/interfaces/models';

export interface IAddProductToOrderScreenProps {
  addOrderProductPending: boolean;
  errors: any;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  orderProducts: IOrderProduct[];
  orderTotal: number;
  setOrderTotal: Dispatch<SetStateAction<number>>;
}
