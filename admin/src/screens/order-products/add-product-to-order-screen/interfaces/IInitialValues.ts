import { IAddedProduct } from '@/screens/order-products/add-product-to-order-screen/interfaces';

export interface IInitialValues {
  orderProducts: IAddedProduct[];
}
