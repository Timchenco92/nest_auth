export interface IAddedProduct {
  price: string;
  productId: string;
  quantity: number;
  title: string;
  total: string;
  orderId: string;
}
