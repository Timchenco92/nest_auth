// Modules
import { object, array } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/order-products/add-product-to-order-screen/interfaces';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  orderProducts: array().required(isRequired),
});

export const initialValues: IInitialValues = {
  orderProducts: [],
};
