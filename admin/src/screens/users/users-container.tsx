// Modules
import { Route, Routes } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Screens
import { AddUserScreenContainer } from '@/screens/users/screens/add-user-screen';
import { DeleteUserScreenContainer } from '@/screens/users/screens/delete-user-screen';
import { UpdateUserScreenContainer } from '@/screens/users/screens/update-user-screen';
import { UserScreenContainer } from '@/screens/users/screens/user-screen';
import { UsersScreenContainer } from '@/screens/users/screens/users-screen';

export const UsersContainer: FC = (): JSX.Element => {
  return (
    <>
      <CustomSnackbarComponent />
      <Routes>
        <Route element={<UsersScreenContainer />} path="*">
          <Route element={<AddUserScreenContainer />} path="add" />
          <Route element={<DeleteUserScreenContainer />} path=":userId/delete" />
          <Route element={<UpdateUserScreenContainer />} path=":userId/update" />
        </Route>
        <Route element={<UserScreenContainer />} path=":userId/details" />
      </Routes>
    </>
  );
};

export default UsersContainer;
