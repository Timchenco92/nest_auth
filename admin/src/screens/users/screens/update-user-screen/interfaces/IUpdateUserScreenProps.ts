// Modules
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, IUpdateUserValidationErrors } from '@/screens/users/screens/update-user-screen/interfaces';

export interface IUpdateUserScreenProps {
  errors: IUpdateUserValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  getUserError: boolean;
  getUserPending: boolean;
  updateUserPending: boolean;
}
