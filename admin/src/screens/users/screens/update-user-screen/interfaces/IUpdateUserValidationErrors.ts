export interface IUpdateUserValidationErrors {
  address: string;
  avatar: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  password: string;
  phone: string;
}
