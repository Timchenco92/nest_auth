// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

const { isEmail, isRequired } = staticValidationErrors;

export const validationSchema = object({
  address: string().notRequired(),
  avatar: mixed().notRequired(),
  email: string().email(isEmail).required(isRequired),
  firstName: string().required(isRequired),
  lastName: string().required(isRequired),
  middleName: string().required(isRequired),
  password: string().notRequired(),
  phone: string().required(isRequired),
});
