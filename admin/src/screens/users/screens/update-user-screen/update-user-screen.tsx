// Modules
import {
  useCallback,
  useRef,
  useState,
  useEffect,
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  IconButton,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import { BaseSyntheticEvent, FC } from 'react';

// Assets
import { CustomErrorOverlay } from '@/assets/svg'

// Components
import { LoaderComponent } from '@/components/loader-component';

// Interfaces
import type { IUpdateUserScreenProps } from '@/screens/users/screens/update-user-screen/interfaces';

export const UpdateUserScreen: FC<IUpdateUserScreenProps> = (props): JSX.Element => {
  const {
    getUserError,
    getUserPending,
    updateUserPending,
    errors: serverValidationErrors,
    handleClose,
    formik
  } = props;

  const {
    dirty,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    setErrors,
    setFieldValue,
    touched,
    values,
  } = formik;

  const [imageName, setImageName] = useState<string>('');
  const imageRef = useRef<HTMLInputElement>(null);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('avatar', files);
    setImageName(files?.name);
  }, [setFieldValue]);

  const handleClickImage = useCallback(() => {
    imageRef?.current?.click();
  }, []);

  const handleClearImage = useCallback(async () => {
    if (!imageRef.current) return;
    setImageName('');
    imageRef.current.value = '';
    await setFieldValue('avatar', '');
  }, [setFieldValue]);

  const isImageFieldFullWidth: string = Boolean(imageName) ? '95%' : '100%';

  useEffect(() => {
    setImageName(values.avatar);
  }, [values.avatar]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  if (getUserError) {
    return (
      <Dialog
        fullWidth
        keepMounted
        maxWidth="md"
        onClose={handleClose}
        open={true}
        PaperProps={{ sx: { position: 'fixed', top: 100 } }}
      >
        <CustomErrorOverlay />
      </Dialog>
    );
  }

  if (getUserPending) {
    return (
      <Dialog
        fullWidth
        keepMounted
        maxWidth="md"
        onClose={handleClose}
        open={true}
        PaperProps={{ sx: { position: 'fixed', top: 100 } }}
      >
        <LoaderComponent />
      </Dialog>
    );
  }

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 100 } }}
    >
      <DialogTitle>Updating a user</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <TextField
            error={touched.firstName && Boolean(errors.firstName)}
            fullWidth
            helperText={touched.firstName && errors.firstName}
            label="First Name"
            name="firstName"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.firstName}
            variant="standard"
          />
          <TextField
            error={touched.middleName && Boolean(errors.middleName)}
            fullWidth
            helperText={touched.middleName && errors.middleName}
            label="Middle Name"
            name="middleName"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.middleName}
            variant="standard"
          />
          <TextField
            error={touched.lastName && Boolean(errors.lastName)}
            fullWidth
            helperText={touched.lastName && errors.lastName}
            label="Last Name"
            name="lastName"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.lastName}
            variant="standard"
          />
          <TextField
            error={touched.email && Boolean(errors.email)}
            fullWidth
            helperText={touched.email && errors.email}
            label="Email"
            name="email"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="email"
            value={values.email}
            variant="standard"
          />
          <TextField
            error={touched.phone && Boolean(errors.phone)}
            fullWidth
            helperText={touched.phone && errors.phone}
            label="Phone"
            name="phone"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="tel"
            value={values.phone}
            variant="standard"
          />
          <TextField
            error={touched.address && Boolean(errors.address)}
            fullWidth
            helperText={touched.address && errors.address}
            label="Address"
            name="address"
            onBlur={handleBlur}
            onChange={handleChange}
            type="text"
            value={values.address}
            variant="standard"
          />
          <TextField
            error={touched.password && Boolean(errors.password)}
            fullWidth
            helperText={touched.password && errors.password}
            label="Password"
            name="password"
            onBlur={handleBlur}
            onChange={handleChange}
            type="password"
            value={values.password}
            variant="standard"
          />
          <FormGroup row>
            <FormControl sx={{ flexBasis: isImageFieldFullWidth }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={touched.avatar && Boolean(errors.avatar)}
                focused={Boolean(imageName)}
                helperText={touched.avatar && errors.avatar}
                label="Avatar"
                margin="dense"
                name="image"
                onClick={handleClickImage}
                type="text"
                value={imageName}
                variant="standard"
              />
            </FormControl>
            {Boolean(imageName) && (
              <FormControl sx={{ textAlign: 'center', justifyContent: 'flex-end', flexBasis: '5%' }}>
                <IconButton onClick={handleClearImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="avatar"
            onChange={handleChangeImage}
            type="file"
            ref={imageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitting || !dirty || updateUserPending}
            color="success"
            fullWidth
            type="submit"
          >
            Update
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
