// Modules
import { useCallback, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema } from '@/screens/users/screens/update-user-screen/form-config';

// Engine
import {
  getUserRequest,
  resetUpdateUserError,
  resetUpdateUserSuccess,
  updateUserErrorSelector,
  updateUserPendingSelector,
  updateUserRequest,
  updateUserSuccessSelector,
  userErrorSelector,
  userPendingSelector,
  userSelector,
} from '@/store/users';

// Interfaces
import type { IInitialValues } from '@/screens/users/screens/update-user-screen/interfaces';

// Screens
import { UpdateUserScreen } from '@/screens/users/screens/update-user-screen/update-user-screen';

export const UpdateUserScreenContainer: FC = (): JSX.Element => {
  const { userId } = useParams<'userId'>();
  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();

  const isUpdate = useSelector(updateUserSuccessSelector);
  const error = useSelector(userErrorSelector);
  const { errors, hasErrors } = useSelector(updateUserErrorSelector);
  const userPending = useSelector(userPendingSelector);
  const user = useSelector(userSelector);
  const userUpdatePending = useSelector(updateUserPendingSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/users${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues: {
      address: user.address || '',
      avatar: user.avatar || '',
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      middleName: user.middleName,
      password: '',
      phone: user.phone,
    },
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasErrors,
    validateOnBlur: !hasErrors,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    if (!userId) return;

    const data = new FormData();
    data.append('avatar', values.avatar);
    if (values.password.length) data.append('password', values.password);
    data.append('address', values.address);
    data.append('email', values.email);
    data.append('firstName', values.firstName);
    data.append('lastName', values.lastName);
    data.append('middleName', values.middleName);
    data.append('phone', values.phone);
    const action = updateUserRequest(userId, data);
    dispatch(action);
  }, [dispatch, userId]);

  useEffect(() => {
    if (!userId) return;

    const action = getUserRequest(userId, '15', '1');
    dispatch(action);
  }, [dispatch, userId]);

  useEffect(() => {
    if (!isUpdate) return;
    handleClose();

    return () => {
      const action = resetUpdateUserSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isUpdate]);

  useEffect(() => {
    return () => {
      const action = resetUpdateUserError();
      dispatch(action);
    };
  }, [dispatch]);

  return (
    <UpdateUserScreen
      errors={errors}
      formik={formik}
      getUserError={error}
      getUserPending={userPending}
      handleClose={handleClose}
      updateUserPending={userUpdatePending}
    />
  );
};
