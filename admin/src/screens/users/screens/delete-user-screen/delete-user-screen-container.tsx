// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Engine
import { deleteUserSuccessSelector, resetDeleteUserSuccess } from '@/store/users';

// Screens
import { DeleteUserScreen } from '@/screens/users/screens/delete-user-screen/delete-user-screen';

export const DeleteUserScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();
  const isDeleted = useSelector(deleteUserSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/users${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();

    return () => {
      const action = resetDeleteUserSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteUserScreen handleClose={handleClose} />
  );
};
