// Modules
import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Engine
import { deleteUserRequest } from '@/store/users';

export interface IProps {
  handleClose(): void;
}

export const DeleteUserScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;

  const { userId } = useParams<'userId'>();
  const dispatch: Dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!userId) return;

    const action = deleteUserRequest(userId);
    dispatch(action)
  }, [userId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing a user</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};
