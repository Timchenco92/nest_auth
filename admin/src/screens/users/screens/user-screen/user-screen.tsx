// Modules
import { FC } from 'react';
import {
  Avatar,
  Box,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
  Typography,
} from '@mui/material';

// Components
import { OrderComponent } from '@/screens/users/screens/user-screen/components/order-component';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IUserScreenProps } from '@/screens/users/screens/user-screen/interfaces';

export const UserScreen: FC<IUserScreenProps> = (props): JSX.Element => {
  const {
    handleChangeLimit,
    handleChangePage,
    limit,
    page,
    user,
  } = props;

  const {
    email,
    fullName,
    avatarUrl,
    updatedAt,
    phone,
    createdAt,
    orders,
    address,
  } = user;

  const { data, meta: { totalPages } } = orders!;

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2} maxWidth="100%" margin={0}>
        <Grid item md={5} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            User Details
          </Typography>
          <Paper elevation={3}>
            <Box sx={{ padding: '20px' }}>
              <Avatar src={avatarUrl} sx={{ height: 130, marginX: 'auto', width: 130 }} />
            </Box>
            <Divider />
            <List>
              <ListItem>
                <ListItemText primary={`Full Name`} />
                <ListItemSecondaryAction>{fullName}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Phone`} />
                <ListItemSecondaryAction>{phone}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Email`} />
                <ListItemSecondaryAction>{email}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Address`} />
                <ListItemSecondaryAction>{address || 'N/A'}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item md={7} xs={12} marginY="10px" paddingY="15px">
          <OrderComponent
            handleChangeLimit={handleChangeLimit}
            handleChangePage={handleChangePage}
            limit={limit}
            orders={data}
            page={page}
            totalPages={totalPages}
          />
        </Grid>
      </Grid>
    </Box>
  );
};
