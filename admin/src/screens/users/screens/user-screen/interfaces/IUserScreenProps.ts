// Modules Types
import type { SelectChangeEvent } from '@mui/material';
import type { ChangeEvent } from 'react';

// Interfaces
import type { IUser } from '@/interfaces/models';

export interface IUserScreenProps {
  user: IUser;
  handleChangeLimit(event: SelectChangeEvent): void;
  handleChangePage(event: ChangeEvent<unknown>, page: number): void;
  limit: string;
  page: string;
}
