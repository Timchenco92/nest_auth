// Modules
import { FC, useState } from 'react';
import {
  Box,
  Chip,
  Collapse,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material';
import { KeyboardArrowUp, KeyboardArrowDown } from '@mui/icons-material';

// Components
import { OrderProductItemComponent } from '@/screens/users/screens/user-screen/components/order-component/components/order-item-component/components/order-product-item-component';

// Enums
import { EOrderStatus } from '@/interfaces/models';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IOrderItemComponentProps } from '@/screens/users/screens/user-screen/components/order-component/components/order-item-component/interfaces';

export const OrderItemComponent: FC<IOrderItemComponentProps> = (props): JSX.Element => {
  const { order } = props;
  const [open, setOpen] = useState<boolean>(false);

  const formattedCreatedAt = ConvertDatetime({ dateTime: order.createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: order.updatedAt });

  const orderStatus = EOrderStatus[order.status];

  const handleOpen = (): void => setOpen(prevState => !prevState);

  return (
    <>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
        <TableCell align="center">
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={handleOpen}
          >
            {open ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
          </IconButton>
        </TableCell>
        <TableCell align="center">{order.orderNumber}</TableCell>
        <TableCell align="center">
          <Chip label={orderStatus} color="success" />
        </TableCell>
        <TableCell align="center">{order.total}</TableCell>
        <TableCell align="center">{formattedCreatedAt}</TableCell>
        <TableCell align="center">{formattedUpdatedAt}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box sx={{ margin: 1 }}>
              <Typography variant="h6" gutterBottom component="div">
                Products
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Price ($)</TableCell>
                    <TableCell align="right">Amount</TableCell>
                    <TableCell align="right">Total price ($)</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {order.orderProducts.map((product) => (
                    <OrderProductItemComponent product={product} key={product.id} />
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};
