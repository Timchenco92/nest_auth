// Modules
import { FC } from 'react';
import { TableCell, TableRow } from '@mui/material';

// Interfaces
import type { IOrderProductItemComponentProps } from '@/screens/users/screens/user-screen/components/order-component/components/order-item-component/components/order-product-item-component/interfaces/IOrderProductItemComponentProps';

export const OrderProductItemComponent: FC<IOrderProductItemComponentProps> = (props): JSX.Element => {
  const { product } = props;
  return (
    <TableRow>
      <TableCell component="th" scope="row">{product.product.title}</TableCell>
      <TableCell>{product.product.price}</TableCell>
      <TableCell align="right">{product.quantity}</TableCell>
      <TableCell align="right">{product.total}</TableCell>
    </TableRow>
  );
};
