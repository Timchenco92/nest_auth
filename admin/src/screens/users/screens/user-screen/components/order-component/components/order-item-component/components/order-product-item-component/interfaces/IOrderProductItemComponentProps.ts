// Interfaces
import type { IOrderProduct } from '@/interfaces/models';

export interface IOrderProductItemComponentProps {
  product: IOrderProduct;
}
