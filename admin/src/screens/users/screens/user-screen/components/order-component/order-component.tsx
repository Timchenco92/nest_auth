// Modules
import { FC } from 'react';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';

// Components
import { PaginationComponent } from '@/components/pagination-component';
import { OrderItemComponent } from '@/screens/users/screens/user-screen/components/order-component/components/order-item-component';

// Interfaces
import type { IOrderComponentProps } from '@/screens/users/screens/user-screen/components/order-component/interfaces';

export const OrderComponent: FC<IOrderComponentProps> = (props): JSX.Element => {
  const { orders, totalPages, page, limit, handleChangeLimit, handleChangePage } = props;

  return (
    <>
      <Typography variant="h5" marginBottom="15px">
        Orders
      </Typography>
      <TableContainer component={Paper} elevation={6}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell align="center"/>
              <TableCell align="center">Order Number</TableCell>
              <TableCell align="center">Status</TableCell>
              <TableCell align="center">Total ($)</TableCell>
              <TableCell align="center">Created</TableCell>
              <TableCell align="center">Updated</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {orders && orders.map((order) => (
              <OrderItemComponent
                key={order.id}
                order={order}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <PaginationComponent
        currentPage={page}
        handleChangeLimit={handleChangeLimit}
        handleChangePage={handleChangePage}
        limit={limit}
        totalPages={totalPages}
      />
    </>
  );
};
