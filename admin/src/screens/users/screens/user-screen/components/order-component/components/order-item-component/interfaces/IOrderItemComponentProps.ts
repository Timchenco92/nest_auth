// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IOrderItemComponentProps {
  order: IOrder;
}
