// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, Outlet, useSearchParams } from 'react-router-dom';

// Modules Types
import type { ChangeEvent, FC } from 'react';
import type { Dispatch } from 'redux';
import { SelectChangeEvent } from '@mui/material';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component';

// Engine
import {
  changeLimit,
  changePage,
  resetPaginationPage,
  resetPaginationLimit,
  getUserRequest,
  paginationAndQuerySearchSelector,
  userErrorSelector,
  userPendingSelector,
  userSelector,
} from '@/store/users';

// Screens
import { UserScreen } from '@/screens/users/screens/user-screen/user-screen';

const DEFAULT_PAGINATION_PAGE = '1';
const DEFAULT_LIMIT = '15';

export const UserScreenContainer: FC = (): JSX.Element => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch: Dispatch = useDispatch();
  const { userId } = useParams<'userId'>();

  const pending = useSelector(userPendingSelector);
  const error = useSelector(userErrorSelector);
  const user = useSelector(userSelector);
  const { limit, page } = useSelector(paginationAndQuerySearchSelector);

  const handleResetPage = useCallback(() => {
    const action = resetPaginationPage();
    dispatch(action);

    setSearchParams((searchParams) => {
      searchParams.delete('page');
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleChangeLimit = useCallback(async (event: SelectChangeEvent) => {
    const { target: { value } } = event;
    handleResetPage();

    const action = changeLimit(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(value) !== DEFAULT_LIMIT) {
        searchParams.set('limit', String(value));
      } else {
        searchParams.delete('limit');
      }
      return searchParams;
    });
  }, [dispatch, handleResetPage, setSearchParams]);

  const handleChangePage = useCallback((event: ChangeEvent<unknown>, page: number) => {
    const action = changePage(String(page));
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(page) !== DEFAULT_PAGINATION_PAGE) {
        searchParams.set('page', String(page));
      } else {
        searchParams.delete('page');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  useEffect(() => {
    if (!searchParams.has('page')) return;
    const qsPage = searchParams.get('page') as string;

    const action = changePage(qsPage);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('limit')) return;
    const qsLimit = searchParams.get('limit') as string;

    const action = changeLimit(qsLimit);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!userId) return;

    const action = getUserRequest(userId, limit, page);
    dispatch(action);
  }, [dispatch, limit, page, userId]);

  useEffect(() => {
    return () => {
      dispatch(resetPaginationLimit());
      dispatch(resetPaginationLimit());
    };
  }, [dispatch]);

  if (error) return <CustomErrorOverlay />;

  if (pending) return <LoaderComponent />;

  return (
    <>
      <UserScreen
        handleChangeLimit={handleChangeLimit}
        handleChangePage={handleChangePage}
        limit={limit}
        page={page}
        user={user}
      />
      <Outlet />
    </>
  );
};
