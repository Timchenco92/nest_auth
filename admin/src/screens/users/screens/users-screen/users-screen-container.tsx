// Modules
import { useCallback, useEffect } from 'react';
import { Link, Outlet, useLocation, useSearchParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Button, FormControl, Grid, IconButton, TextField } from '@mui/material';
import { Clear, Search } from '@mui/icons-material';

// Modules Types
import { ChangeEvent, FC } from 'react';
import { Dispatch } from 'redux';
import { SelectChangeEvent } from '@mui/material';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Engine
import {
  changeLimit,
  changePage,
  getUsersRequest,
  paginationAndQuerySearchSelector,
  resetPaginationLimit,
  resetPaginationPage,
  resetQuerySearch,
  setQuerySearch,
  usersErrorSelector,
  usersPendingSelector,
  usersSelector,
} from '@/store/users';

// Screens
import { UsersScreen } from '@/screens/users/screens/users-screen/users-screen';

const DEFAULT_PAGINATION_PAGE = '1';
const DEFAULT_LIMIT = '15';
const DEFAULT_QUERY_SEARCH = '';

export const UsersScreenContainer: FC = (): JSX.Element => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();

  const { data: users, meta: { totalPages } } = useSelector(usersSelector);
  const { limit, page, query } = useSelector(paginationAndQuerySearchSelector);
  const error = useSelector(usersErrorSelector);
  const pending = useSelector(usersPendingSelector);

  const usersCount = users.length;

  const addPath = search.length ? `add${search}` : 'add';

  const handleResetPage = useCallback(() => {
    const action = resetPaginationPage();
    dispatch(action);

    setSearchParams((searchParams) => {
      searchParams.delete('page');
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleResetSearch = useCallback(() => {
    const action = resetQuerySearch();
    dispatch(action);

    setSearchParams((searchParams) => {
      searchParams.delete('search');
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleChangeLimit = useCallback(async (event: SelectChangeEvent) => {
    const { target: { value } } = event;
    handleResetPage();
    handleResetSearch();

    const action = changeLimit(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(value) !== DEFAULT_LIMIT) {
        searchParams.set('limit', String(value));
      } else {
        searchParams.delete('limit');
      }
      return searchParams;
    });
  }, [dispatch, handleResetPage, handleResetSearch, setSearchParams]);

  const handleChangePage = useCallback((event: ChangeEvent<unknown>, page: number) => {
    const action = changePage(String(page));
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(page) !== DEFAULT_PAGINATION_PAGE) {
        searchParams.set('page', String(page));
      } else {
        searchParams.delete('page');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleSearch = useCallback(({ target }: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const { value } = target;

    const action = setQuerySearch(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (value !== DEFAULT_QUERY_SEARCH) {
        searchParams.set('search', value);
      } else {
        searchParams.delete('search');
      }

      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleClearSearch = useCallback(() => {
    const action = setQuerySearch('');
    dispatch(action);

    setSearchParams((searchParams) => {
      searchParams.delete('search');
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  useEffect(() => {
    if (!searchParams.has('page')) return;
    const qsPage = searchParams.get('page') as string;

    const action = changePage(qsPage);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('limit')) return;
    const qsLimit = searchParams.get('limit') as string;

    const action = changeLimit(qsLimit);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('search')) return;
    const qsLimit = searchParams.get('search') as string;

    const action = setQuerySearch(qsLimit);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    const action = getUsersRequest(limit, page, query);
    dispatch(action);
  }, [dispatch, limit, page, query]);

  useEffect(() => {
    return () => {
      dispatch(resetPaginationLimit());
      dispatch(resetPaginationLimit());
      dispatch(resetQuerySearch());
    };
  }, [dispatch]);

  if (error) return <CustomErrorOverlay />;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={6} marginY="10px" paddingY="15px">
          {!!usersCount && (
            <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
              <FormControl fullWidth>
                <TextField
                  disabled
                  label="Search"
                  onChange={handleSearch}
                  placeholder="Put a phone number"
                  type="tel"
                  value={query}
                  variant="standard"
                />
              </FormControl>
              {!query ? (
                <Search sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
              ) : (
                <IconButton onClick={handleClearSearch}>
                  <Clear />
                </IconButton>
              )}
            </Box>
          )}
        </Grid>
        <Grid item xs={6}>
          <Grid
            alignItems="center"
            container
            direction="row"
            height="100%"
            justifyContent="flex-end"
          >
            <Button
              color="success"
              component={Link}
              size="large"
              to={addPath}
              type="button"
              variant="text"
            >
              Create
            </Button>
          </Grid>
        </Grid>
        <UsersScreen
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          page={page}
          pending={pending}
          totalPages={totalPages}
          users={users}
        />
      </Grid>
      <Outlet />
    </Box>
  );
};
