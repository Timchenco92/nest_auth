// Modules
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Assets
import { CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component'
import { PaginationComponent } from '@/components/pagination-component';
import { UserItemComponent } from '@/screens/users/screens/users-screen/components/user-item-component';

// Constants
import { COLORS } from '@/constants';

// Interfaces
import type { IUsersScreenProps } from '@/screens/users/screens/users-screen/interfaces';

export const UsersScreen: FC<IUsersScreenProps> = (props): JSX.Element => {
  const { handleChangeLimit, handleChangePage, limit, page, pending, users, totalPages } = props;

  const usersCount = users.length;

  if (!usersCount) return <CustomNoRowsOverlay />

  if (pending) return <LoaderComponent />;

  return (
    <Grid item xs={12}>
      <Paper elevation={6}>
        <TableContainer>
          <Table>
            <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
              <TableRow>
                <TableCell align="center">#</TableCell>
                <TableCell />
                <TableCell align="center">Full Name</TableCell>
                <TableCell align="center">Email</TableCell>
                <TableCell align="center">Phone</TableCell>
                <TableCell align="center">Count Of Orders</TableCell>
                <TableCell align="center">Created</TableCell>
                <TableCell align="center">Updated</TableCell>
                <TableCell align="center"/>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((user, key) => (
                <UserItemComponent key={user.id} rowIdx={key} user={user} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <PaginationComponent
          currentPage={page}
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          totalPages={totalPages}
        />
      </Paper>
    </Grid>
  );
};

UsersScreen.defaultProps = {
  page: '1',
  limit: '15',
}
