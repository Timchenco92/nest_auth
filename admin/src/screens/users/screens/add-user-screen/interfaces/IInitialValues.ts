export interface IInitialValues {
  address: string;
  avatar: File | string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  password: string;
  phone: string;
}
