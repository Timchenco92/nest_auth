// Modules
import type { FormikProps } from 'formik';

// Interfaces
import type { IAddUserValidationErrors, IInitialValues } from '@/screens/users/screens/add-user-screen/interfaces';

export interface IAddUserScreenProps {
  errors: IAddUserValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  pending: boolean;
}
