// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/users/screens/add-user-screen/interfaces';

const { isEmail, isRequired } = staticValidationErrors;

export const validationSchema = object({
  address: string().notRequired(),
  avatar: mixed().notRequired(),
  email: string().email(isEmail).required(isRequired),
  firstName: string().required(isRequired),
  lastName: string().required(isRequired),
  middleName: string().required(isRequired),
  password: string().required(isRequired),
  phone: string().required(isRequired),
});

export const initialValues: IInitialValues = {
  address: '',
  avatar: '',
  email: '',
  firstName: '',
  lastName: '',
  middleName: '',
  password: '',
  phone: '',
};
