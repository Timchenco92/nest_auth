// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/users/screens/add-user-screen/form-config';

// Engine
import {
  addUserErrorSelector,
  addUserPendingSelector,
  addUserRequest,
  addUserSuccessSelector,
  resetAddUserError,
  resetAddUserSuccess,
} from '@/store/users';

// Interfaces
import type { IInitialValues } from '@/screens/users/screens/add-user-screen/interfaces';

// Screens
import { AddUserScreen } from '@/screens/users/screens/add-user-screen/add-user-screen';

export const AddUserScreenContainer: FC = (): JSX.Element => {
  const { errors, hasError } = useSelector(addUserErrorSelector);
  const isUserAdded: boolean = useSelector(addUserSuccessSelector);
  const pending: boolean = useSelector(addUserPendingSelector);

  const dispatch: Dispatch = useDispatch();

  const navigate = useNavigate();
  const { search } = useLocation();

  const handleClose = useCallback(() => {
    navigate(`/dashboard/users${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    const data = new FormData();
    if (values.avatar instanceof File) data.append('avatar', values.avatar);
    if (values.address.length) data.append('address', values.address);
    data.append('email', values.email);
    data.append('firstName', values.firstName);
    data.append('lastName', values.lastName);
    data.append('middleName', values.middleName);
    data.append('password', values.password);
    data.append('phone', values.phone);
    const action = addUserRequest(data);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isUserAdded) return;
    handleClose();
    return () => {
      const action = resetAddUserSuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isUserAdded]);

  useEffect(() => {
    return () => {
      const action = resetAddUserError();
      dispatch(action);
    }
  }, [dispatch]);

  return (
    <AddUserScreen
      errors={errors}
      handleClose={handleClose}
      formik={formik}
      pending={pending}
    />
  );
};
