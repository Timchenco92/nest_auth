// Modules
import { Grid } from '@mui/material';
import { Apps, Person, ShoppingCart } from '@mui/icons-material';

// Modules Types
import type { FC } from 'react';

// Components
import { OrderCardComponent } from '@/screens/home/components/orders-card-component';
import { OrdersTableComponent } from '@/screens/home/components/orders-table-component';
import { ProductsCardComponent } from '@/screens/home/components/products-card-component';
import { UsersCardComponent } from '@/screens/home/components/users-card-component';
import { UsersTableComponent } from '@/screens/home/components/users-table-component';

// Interfaces
import type { IHomeScreenProps } from '@/screens/home/interfaces';

export const HomeScreen: FC<IHomeScreenProps> = (props): JSX.Element => {
  const {
    orderCountError,
    orders,
    ordersCount,
    ordersCountPending,
    ordersError,
    ordersPagination,
    ordersPending,
    productsCount,
    productsCountError,
    productsCountPending,
    users,
    usersCount,
    usersCountError,
    usersCountPending,
    usersError,
    usersPagination,
    usersPending,
  } = props;

  console.log(usersPagination);

  return (
    <Grid
      alignItems="center"
      container
      justifyContent="center"
      spacing={4}
    >
      <UsersCardComponent
        count={usersCount}
        error={usersCountError}
        iconPerformance={Person}
        pending={usersCountPending}
        title="Users"
      />

      <OrderCardComponent
        count={ordersCount}
        error={orderCountError}
        iconPerformance={ShoppingCart}
        pending={ordersCountPending}
        title="Orders"
      />

      <ProductsCardComponent
        count={productsCount}
        error={productsCountError}
        iconPerformance={Apps}
        pending={productsCountPending}
        title="Products"
      />

      <OrdersTableComponent
        count={ordersPagination.totalItems}
        currentPage={ordersPagination.currentPage}
        error={ordersError}
        orders={orders}
        pending={ordersPending}
        rowsPerPage={ordersPagination.itemsPerPage}
      />

      <UsersTableComponent
        count={usersPagination.totalItems}
        currentPage={usersPagination.currentPage}
        error={usersError}
        pending={usersPending}
        rowsPerPage={usersPagination.itemsPerPage}
        users={users}
      />
    </Grid>
  );
};

export default HomeScreen;
