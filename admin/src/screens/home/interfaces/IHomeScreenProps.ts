// Interfaces
import type { IOrder, IUser } from '@/interfaces/models';

export interface IHomeScreenProps {
  orderCountError: boolean;
  orders: IOrder[];
  ordersCount: number;
  ordersCountPending: boolean;
  ordersError: boolean;
  ordersPending: boolean;
  productsCount: number;
  productsCountError: boolean;
  productsCountPending: boolean;
  users: IUser[];
  usersCount: number;
  usersCountError: boolean;
  usersCountPending: boolean;
  usersError: boolean;
  usersPending: boolean;
  usersPagination: IPagination;
  ordersPagination: IPagination;
}

interface IPagination {
  currentPage: number;
  itemsPerPage: number;
  totalItems: number;
  totalPages: number;
}
