// Modules Types
import type { SvgIconComponent } from '@mui/icons-material';

export interface IOrderCardComponentProps {
  count: number;
  error: boolean;
  iconPerformance: SvgIconComponent;
  pending: boolean;
  title: string;
}
