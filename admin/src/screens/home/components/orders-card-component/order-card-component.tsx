// Modules
import { Grid } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Components
import { CardComponent } from '@/screens/home/components/card-component';

// Interfaces
import type { IOrderCardComponentProps } from '@/screens/home/components/orders-card-component/interfaces';

export const OrderCardComponent: FC<IOrderCardComponentProps> = (props): JSX.Element => {
  const { count, iconPerformance, title } = props;

  return (
    <Grid item lg={4} md={6} xs={12}>
      <CardComponent
        color="linear-gradient(60deg, rgba(251,140,0,1) 0%, rgba(255,202,41,1) 100%)"
        iconPerformance={iconPerformance}
        count={count}
        title={title}
      />
    </Grid>
  );
};
