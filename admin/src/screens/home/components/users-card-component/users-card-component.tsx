// Modules
import { Grid } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Components
import { CardComponent } from '@/screens/home/components/card-component';

// Interfaces
import type { IUsersCardComponentProps } from '@/screens/home/components/users-card-component/interfaces';

export const UsersCardComponent: FC<IUsersCardComponentProps> = (props): JSX.Element => {
  const { count, iconPerformance, title } = props;

  return (
    <Grid item lg={4} md={6} xs={12}>
      <CardComponent
        color="linear-gradient(60deg, rgba(245,0,87,1) 0%, rgba(255,138,128,1) 100%)"
        iconPerformance={iconPerformance}
        count={count}
        title={title}
      />
    </Grid>
  );
};
