// Modules Types
import type { SvgIconComponent } from '@mui/icons-material';

export interface IProductsCardComponentProps {
  count: number;
  error: boolean;
  iconPerformance: SvgIconComponent;
  pending: boolean;
  title: string;
}
