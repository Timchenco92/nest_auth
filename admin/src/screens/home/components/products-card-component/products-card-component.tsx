// Modules
import { Grid } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Components
import { CardComponent } from '@/screens/home/components/card-component';

// Interfaces
import type { IProductsCardComponentProps } from '@/screens/home/components/products-card-component/interfaces';

export const ProductsCardComponent: FC<IProductsCardComponentProps> = (props): JSX.Element => {
  const { count, iconPerformance, title } = props;

  return (
    <Grid item lg={4} md={6} xs={12}>
      <CardComponent
        color="linear-gradient(60deg, rgba(67,160,71,1) 0%, rgba(255,235,59,1) 100%)"
        iconPerformance={iconPerformance}
        count={count}
        title={title}
      />
    </Grid>
  );
};
