// Interfaces
import type { IUser } from '@/interfaces/models';

export interface IUsersTableComponentProps {
  count: number;
  currentPage: number;
  rowsPerPage: number;
  users: IUser[];
  error: boolean;
  pending: boolean;
}
