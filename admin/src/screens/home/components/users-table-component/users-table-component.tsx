// Modules
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import {
  Avatar, Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@mui/material';

// Modules Types
import type { FC, MouseEvent } from 'react';
import type { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay, CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component';

// Engine
import { getUsersPerMonthRequest } from '@/store/home';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IUsersTableComponentProps } from '@/screens/home/components/users-table-component/interfaces';

export const UsersTableComponent: FC<IUsersTableComponentProps> = (props): JSX.Element => {
  const { count, currentPage, error, pending, rowsPerPage, users, } = props;
  const dispatch: Dispatch = useDispatch();

  const handleChangePage = useCallback((event: MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    const action = getUsersPerMonthRequest(newPage.toString());
    dispatch(action);
  }, [dispatch]);

  if (pending) return <LoaderComponent />;

  if (error) return <CustomErrorOverlay />;

  if (!users.length) return <CustomNoRowsOverlay />;

  return (
    <Grid item lg={6} md={6} xs={12}>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell />
                <TableCell>Full Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Created</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users.map((user, idx) => {
                const { avatarUrl, fullName, email, createdAt } = user;
                const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });

                return (
                  <TableRow>
                    <TableCell>{idx + 1}</TableCell>
                    <TableCell>
                      <Avatar src={avatarUrl} sx={{ marginX: 'auto' }} />
                    </TableCell>
                    <TableCell>{fullName}</TableCell>
                    <TableCell>{email}</TableCell>
                    <TableCell>{formattedCreatedAt}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[]}
          component="div"
          count={count}
          rowsPerPage={rowsPerPage}
          page={currentPage}
          onPageChange={handleChangePage}
        />
      </Paper>
    </Grid>
  );
};
