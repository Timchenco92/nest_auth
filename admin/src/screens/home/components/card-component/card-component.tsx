// Modules
import { Divider, Grid, Hidden, Paper, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles'

// Modules Types
import { FC } from 'react';
import { SvgIconComponent } from '@mui/icons-material';


const useStyles = makeStyles((theme: any) => ({
  card: {
    position: "relative",
    padding: theme.spacing(1, 2),
    width: "100%",
    margin: theme.spacing(2)
  },
  title: {
    padding: theme.spacing(1),
    margin: theme.spacing(1, 0)
  },
  persentage: {
    padding: theme.spacing(1),
    margin: theme.spacing(1)
  },
  detail: {
    padding: theme.spacing(1),
    margin: theme.spacing(2, 0, 2, 0),
  },
  boxicon: {
    height: "60px",
    width: "60px",
    display: "flex",
    position: "absolute",
    zIndex: 1,
    top: 10,
    left: 12,
    color: "#ff0000",
    borderRadius: "25%",
    justifyContent: "center",
    alignItems: "center"
  },
  icons: {
    minWidth: 50,
    minHeight: 50,
    color: "#ffffff"
  }
}));

interface IProps {
  color: string;
  count: number;
  iconPerformance: SvgIconComponent;
  title: string;
}

export const CardComponent: FC<IProps> = (props): JSX.Element => {
  const { color, count, iconPerformance: IconPerformance, title } = props;
  const classes = useStyles();

  return (
    <Paper className={classes.card}>
      <Grid container>
        <Grid className={classes.boxicon} style={{ background: color }}>
          <IconPerformance className={classes.icons} />
        </Grid>
        <Grid
          container
          className={classes.persentage}
          justifyContent="flex-end"
        >
          <Typography variant="h4"> {count}</Typography>
        </Grid>
      </Grid>
      <Divider style={{ color: "#000" }} />
      <Grid container justifyContent={'flex-start'} className={classes.title}>
        <Typography>
          <strong>{title}</strong>
        </Typography>
      </Grid>

    </Paper>
  );
};
