// Modules
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
} from '@mui/material';

// Modules Types
import type { FC, MouseEvent } from 'react';
import type { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay, CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { OrderStatusComponent } from '@/screens/orders/screens/orders-screen/components/order-status-component';
import { LoaderComponent } from '@/components/loader-component';

// Engine
import { getOrdersPerMonthRequest } from '@/store/home';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IOrdersTableComponentProps } from '@/screens/home/components/orders-table-component/interfaces';

export const OrdersTableComponent: FC<IOrdersTableComponentProps> = (props): JSX.Element => {
  const {
    count,
    currentPage,
    error,
    orders,
    pending,
    rowsPerPage,
  } = props;
  const dispatch: Dispatch = useDispatch();

  const handleChangePage = useCallback((event: MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    const action = getOrdersPerMonthRequest(newPage.toString());
    dispatch(action);
  }, [dispatch]);

  if (pending) return <LoaderComponent />;

  if (error) return <CustomErrorOverlay />;

  if (!orders.length) return <CustomNoRowsOverlay />;

  return (
    <Grid item lg={6} md={6} xs={12}>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <TableContainer>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell />
                <TableCell>Order Number</TableCell>
                <TableCell>Total ($)</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>Created</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders.map((order, idx) => {
                const { total, orderNumber, status, createdAt } = order;
                const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });

                return (
                  <TableRow>
                    <TableCell>{idx + 1}</TableCell>
                    <TableCell>{orderNumber}</TableCell>
                    <TableCell>{total}</TableCell>
                    <TableCell>
                      <OrderStatusComponent status={status} />
                    </TableCell>
                    <TableCell>{formattedCreatedAt}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[]}
          component="div"
          count={count}
          rowsPerPage={rowsPerPage}
          page={currentPage}
          onPageChange={handleChangePage}
        />
      </Paper>
    </Grid>
  );
};
