// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IOrdersTableComponentProps {
  count: number;
  currentPage: number;
  orders: IOrder[];
  rowsPerPage: number;
  error: boolean;
  pending: boolean;
}
