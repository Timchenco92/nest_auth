// Modules
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Engine
import {
  getOrdersCountRequest,
  getOrdersPerMonthRequest,
  getProductsCountRequest,
  getUsersCountRequest,
  getUsersPerMonthRequest,
  dashboardOrdersCountErrorSelector,
  dashboardOrdersCountPendingSelector,
  dashboardOrdersCountSelector,
  dashboardOrdersErrorSelector,
  dashboardOrdersPendingSelector,
  dashboardOrdersSelector,
  dashboardProductsCountErrorSelector,
  dashboardProductsCountPendingSelector,
  dashboardProductsCountSelector,
  dashboardUsersCountErrorSelector,
  dashboardUsersCountPendingSelector,
  dashboardUsersCountSelector,
  dashboardUsersErrorSelector,
  dashboardUsersPendingSelector,
  dashboardUsersSelector,
  dashboardOrdersPaginationSelector,
  dashboardUsersPaginationSelector,
} from '@/store/home';

// Screens
import { HomeScreen } from '@/screens/home/home-screen';

export const HomeScreenContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();

  const ordersCount = useSelector(dashboardOrdersCountSelector);
  const orderCountError = useSelector(dashboardOrdersCountErrorSelector);
  const ordersCountPending = useSelector(dashboardOrdersCountPendingSelector);

  const orders = useSelector(dashboardOrdersSelector);
  const ordersError = useSelector(dashboardOrdersErrorSelector);
  const ordersPending = useSelector(dashboardOrdersPendingSelector);

  const productsCount = useSelector(dashboardProductsCountSelector);
  const productsCountError = useSelector(dashboardProductsCountErrorSelector);
  const productsCountPending = useSelector(dashboardProductsCountPendingSelector);

  const usersCount = useSelector(dashboardUsersCountSelector);
  const usersCountError = useSelector(dashboardUsersCountErrorSelector);
  const usersCountPending = useSelector(dashboardUsersCountPendingSelector);

  const users = useSelector(dashboardUsersSelector);
  const usersError = useSelector(dashboardUsersErrorSelector);
  const usersPending = useSelector(dashboardUsersPendingSelector);

  const { meta: ordersPaginationMeta } = useSelector(dashboardOrdersPaginationSelector);
  const { meta: usersPaginationMeta } = useSelector(dashboardUsersPaginationSelector);

  useEffect(() => {
    const action = getOrdersCountRequest();
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    const action = getOrdersPerMonthRequest('1');
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    const action = getProductsCountRequest();
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    const action = getUsersCountRequest();
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    const action = getUsersPerMonthRequest('1')
    dispatch(action);
  }, [dispatch]);

  return (
    <HomeScreen
      orderCountError={orderCountError}
      orders={orders}
      ordersCount={ordersCount}
      ordersCountPending={ordersCountPending}
      ordersError={ordersError}
      ordersPending={ordersPending}
      productsCount={productsCount}
      productsCountError={productsCountError}
      productsCountPending={productsCountPending}
      users={users}
      usersCount={usersCount}
      usersCountError={usersCountError}
      usersCountPending={usersCountPending}
      usersError={usersError}
      usersPending={usersPending}
      ordersPagination={ordersPaginationMeta}
      usersPagination={usersPaginationMeta}
    />
  );
};

export default HomeScreenContainer;
