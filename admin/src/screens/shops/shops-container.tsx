// Modules
import { FC } from 'react';
import { Route, Routes } from 'react-router-dom';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Screens
import { AddShopScreenContainer } from '@/screens/shops/screens/add-shop-screen';
import { DeleteShopScreenContainer } from '@/screens/shops/screens/delete-shop-screen';
import { ShopScreenContainer } from '@/screens/shops/screens/shop-screen';
import { ShopsScreenContainer } from '@/screens/shops/screens/shops-screen';
import { UpdateShopScreenContainer } from '@/screens/shops/screens/update-shop-screen';
import { AddShopGalleryScreenContainer } from '@/screens/shop-galleries/add-shop-gallery-screen';
import { DeleteShopGalleryScreenContainer } from '@/screens/shop-galleries/delete-shop-gallery-screen';

export const ShopsContainer: FC = (): JSX.Element => {
  return (
    <>
      <CustomSnackbarComponent />
      <Routes>
        <Route path="*" element={<ShopsScreenContainer />}>
          <Route path="add" element={<AddShopScreenContainer />} />
          <Route path=":shopId/delete" element={<DeleteShopScreenContainer />} />
          <Route path=":shopId/update" element={<UpdateShopScreenContainer />} />
        </Route>
        <Route path=":shopId/details" element={<ShopScreenContainer />}>
          <Route path="gallery/add" element={<AddShopGalleryScreenContainer />}/>
          <Route path="gallery/:shopGalleryId/delete" element={<DeleteShopGalleryScreenContainer />}/>
        </Route>
      </Routes>
    </>
  );
};

export default ShopsContainer;
