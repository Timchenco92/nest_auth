// Modules
import {
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  IconButton,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import type { BaseSyntheticEvent, FC } from 'react';

// Assets
import { CustomErrorOverlay } from '@/assets/svg'

// Components
import { LoaderComponent } from '@/components/loader-component';

// Interfaces
import type { IUpdateShopScreenProps } from '@/screens/shops/screens/update-shop-screen/interfaces';

export const UpdateShopScreen: FC<IUpdateShopScreenProps> = (props): JSX.Element => {

  const {
    errors: serverValidationErrors,
    formik,
    getShopError,
    getShopPending,
    handleClose,
    updateShopPending,
  } = props;

  const {
    dirty,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    setErrors,
    setFieldValue,
    touched,
    values,
  } = formik;

  const [imageName, setImageName] = useState<string>('');
  const imageRef = useRef<HTMLInputElement>(null);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('image', files);
    setImageName(files?.name);
  }, [setFieldValue]);

  const handleClickImage = useCallback(() => {
    imageRef?.current?.click();
  }, []);

  const handleClearImage = useCallback(async () => {
    if (!imageRef.current) return;
    setImageName('');
    imageRef.current.value = '';
    await setFieldValue('image', '');
  }, [setFieldValue]);

  const isImageFieldFullWidth: string = Boolean(imageName) ? '95%' : '100%';

  useEffect(() => {
    setImageName(values.image);
  }, [values.image]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  if (getShopError) {
    return (
      <Dialog
        fullWidth
        keepMounted
        maxWidth="md"
        onClose={handleClose}
        open={true}
        PaperProps={{ sx: { position: 'fixed', top: 100 } }}
      >
        <CustomErrorOverlay />
      </Dialog>
    );
  }

  if (getShopPending) {
    return (
      <Dialog
        fullWidth
        keepMounted
        maxWidth="md"
        onClose={handleClose}
        open={true}
        PaperProps={{ sx: { position: 'fixed', top: 100 } }}
      >
        <LoaderComponent />
      </Dialog>
    );
  }

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 100 } }}
    >
      <DialogTitle>Updating shop</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <TextField
            error={touched.title && Boolean(errors.title)}
            fullWidth
            helperText={touched.title && errors.title}
            label="Title"
            name="title"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.title}
            variant="standard"
          />
          <TextField
            error={touched.address && Boolean(errors.address)}
            fullWidth
            helperText={touched.address && errors.address}
            label="Address"
            name="address"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.address}
            variant="standard"
          />
          <TextField
            error={touched.firstPhone && Boolean(errors.firstPhone)}
            fullWidth
            helperText={touched.firstPhone && errors.firstPhone}
            inputProps={{ maxLength: 12 }}
            label="Phone"
            name="firstPhone"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.firstPhone}
            variant="standard"
          />
          <TextField
            error={touched.secondPhone && Boolean(errors.secondPhone)}
            fullWidth
            helperText={touched.secondPhone && errors.secondPhone}
            inputProps={{ maxLength: 12 }}
            label="Phone"
            name="secondPhone"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.secondPhone}
            variant="standard"
          />
          <TextField
            error={touched.thirdPhone && Boolean(errors.thirdPhone)}
            fullWidth
            helperText={touched.thirdPhone && errors.thirdPhone}
            inputProps={{ maxLength: 12 }}
            label="Phone"
            name="thirdPhone"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.thirdPhone}
            variant="standard"
          />
          <TextField
            error={touched.startTimeWork && Boolean(errors.startTimeWork)}
            fullWidth
            helperText={touched.startTimeWork && errors.startTimeWork}
            label="Start Time Work"
            name="startTimeWork"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="time"
            value={values.startTimeWork}
            variant="standard"
          />
          <TextField
            error={touched.endTimeWork && Boolean(errors.endTimeWork)}
            fullWidth
            helperText={touched.endTimeWork && errors.endTimeWork}
            label="End Time Work"
            name="endTimeWork"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="time"
            value={values.endTimeWork}
            variant="standard"
          />
          <FormGroup row>
            <FormControl sx={{ flexBasis: isImageFieldFullWidth }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={touched.image && Boolean(errors.image)}
                focused={Boolean(imageName)}
                helperText={touched.image && errors.image}
                label="Image"
                margin="dense"
                name="image"
                onClick={handleClickImage}
                required
                sx={{ marginBottom: '10px' }}
                type="text"
                value={imageName}
                variant="standard"
              />
            </FormControl>
            {Boolean(imageName) && (
              <FormControl sx={{ textAlign: 'center', justifyContent: 'flex-end', flexBasis: '5%' }}>
                <IconButton onClick={handleClearImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="image"
            onChange={handleChangeImage}
            type="file"
            ref={imageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitting || !dirty || updateShopPending}
            color="success"
            fullWidth
            type="submit"
          >
            Update
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
