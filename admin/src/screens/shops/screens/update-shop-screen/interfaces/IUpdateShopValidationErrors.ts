export interface IUpdateShopValidationErrors {
  address: string;
  endTimeWork: string;
  firstPhone: string;
  image: string;
  secondPhone: string;
  startTimeWork: string;
  thirdPhone: string;
  title: string;
}
