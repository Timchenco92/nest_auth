// Modules
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, IUpdateShopValidationErrors } from '@/screens/shops/screens/update-shop-screen/interfaces';

export interface IUpdateShopScreenProps {
  errors: IUpdateShopValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  getShopError: boolean;
  getShopPending: boolean;
  updateShopPending: boolean;
}
