// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  address: string().required(isRequired),
  endTimeWork: string().required(isRequired),
  firstPhone: string().required(isRequired),
  image: mixed().required(isRequired),
  secondPhone: string().notRequired(),
  startTimeWork: string().required(isRequired),
  thirdPhone: string().notRequired(),
  title: string().required(isRequired),
});
