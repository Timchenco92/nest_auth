// Modules
import { useCallback, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema } from '@/screens/shops/screens/update-shop-screen/form-config';

// Engine
import {
  getShopRequest,
  resetUpdateShopError,
  resetUpdateShopSuccess,
  shopErrorSelector,
  shopPendingSelector,
  shopSelector,
  updateShopErrorSelector,
  updateShopPendingSelector,
  updateShopRequest,
  updateShopSuccessSelector,
} from '@/store/shops';

// Interfaces
import type { IInitialValues } from '@/screens/shops/screens/update-shop-screen/interfaces';

// Screens
import { UpdateShopScreen } from '@/screens/shops/screens/update-shop-screen/update-shop-screen';

export const UpdateShopScreenContainer: FC = (): JSX.Element => {
  const { shopId } = useParams<'shopId'>();
  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();

  const isUpdate = useSelector(updateShopSuccessSelector);
  const error = useSelector(shopErrorSelector);
  const { errors, hasErrors } = useSelector(updateShopErrorSelector);
  const getShopPending = useSelector(shopPendingSelector);
  const shop = useSelector(shopSelector);
  const updateShopPending = useSelector(updateShopPendingSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/shops${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues: {
      address: shop.address,
      endTimeWork: shop.endTimeWork,
      firstPhone: shop.firstPhone,
      image: shop.image,
      secondPhone: shop.secondPhone || '',
      startTimeWork: shop.startTimeWork,
      thirdPhone: shop.thirdPhone || '',
      title: shop.title,
    },
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasErrors,
    validateOnBlur: !hasErrors,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    if (!shopId) return;

    const data = new FormData();
    if (values.secondPhone.length) data.append('secondPhone', values.secondPhone);
    if (values.thirdPhone.length) data.append('thirdPhone', values.thirdPhone);
    if (typeof values.image === 'object') data.append('image', values.image);
    data.append('endTimeWork', values.endTimeWork);
    data.append('startTimeWork', values.startTimeWork);
    data.append('firstPhone', values.firstPhone);
    data.append('address', values.address);
    data.append('title', values.title);
    const action = updateShopRequest(shopId, data);
    dispatch(action);
  }, [dispatch, shopId]);

  useEffect(() => {
    if (!shopId) return;

    const action = getShopRequest(shopId);
    dispatch(action);
  }, [dispatch, shopId]);

  useEffect(() => {
    if (!isUpdate) return;
    handleClose();

    return () => {
      const action = resetUpdateShopSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isUpdate]);

  useEffect(() => {
    return () => {
      const action = resetUpdateShopError();
      dispatch(action);
    };
  }, [dispatch]);

  return (
    <UpdateShopScreen
      errors={errors}
      formik={formik}
      updateShopPending={updateShopPending}
      getShopPending={getShopPending}
      handleClose={handleClose}
      getShopError={error}
    />
  );
};
