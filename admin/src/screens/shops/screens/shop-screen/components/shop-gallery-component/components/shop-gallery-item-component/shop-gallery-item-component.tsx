// Modules
import { Button, ImageListItem } from '@mui/material';
import { Link } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Interfaces
import { IShopGallery } from '@/interfaces/models';

interface IProps {
  handleOpenLightbox: (image: string) => void;
  shopGallery: IShopGallery;
}

export const ShopGalleryItemComponent: FC<IProps> = (props): JSX.Element => {
  const { handleOpenLightbox, shopGallery } = props;
  const { id, imageUrl } = shopGallery;

  return (
    <ImageListItem
      key={id}
      sx={{ cursor: 'pointer' }}
    >
      <img
        alt={id}
        loading="lazy"
        onClick={() => handleOpenLightbox(imageUrl)}
        src={imageUrl}
        srcSet={imageUrl}
      />
      <Button
        color="error"
        component={Link}
        to={`gallery/${id}/delete`}
        fullWidth
        sx={{ marginTop: '10px' }}
        type="button"
        variant="outlined"
      >
        Delete
      </Button>
    </ImageListItem>
  );
};
