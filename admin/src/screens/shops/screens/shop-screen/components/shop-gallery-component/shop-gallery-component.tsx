// Modules
import { ImageList } from '@mui/material';

// Modules Types
import { FC } from 'react';

// Assets
import { CustomErrorOverlay, CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { ShopGalleryItemComponent } from '@/screens/shops/screens/shop-screen/components/shop-gallery-component/components/shop-gallery-item-component';

// Interfaces
import type { IShopGallery } from '@/interfaces/models';

interface IProps {
  handleOpenLightbox: (image: string) => void;
  shopGalleries: IShopGallery[];
  shopGalleriesError: boolean;
}

export const ShopGalleryComponent: FC<IProps> = (props): JSX.Element => {
  const { shopGalleries, shopGalleriesError, handleOpenLightbox } = props;

  const shopGalleriesCount = shopGalleries.length;

  if (!shopGalleriesCount) return <CustomNoRowsOverlay />;

  if (shopGalleriesError) return <CustomErrorOverlay />;

  return (
    <ImageList sx={{ height: 550 }} variant="woven" cols={3} rowHeight={164}>
      {shopGalleries.map((shopGallery) => (
        <ShopGalleryItemComponent handleOpenLightbox={handleOpenLightbox} shopGallery={shopGallery} key={shopGallery.id}/>
      ))}
    </ImageList>
  );
};
