// Modules
import {
  Avatar,
  Box,
  Button,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText, Paper,
  Typography,
} from '@mui/material';
import { Link } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Components
import { ShopGalleryComponent } from '@/screens/shops/screens/shop-screen/components/shop-gallery-component';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

interface IProps {
  handleOpenLightbox: (image: string) => void;
  shop: IShop;
  shopGalleries: IShopGallery[];
  shopGalleriesError: boolean;
}

export const ShopScreen: FC<IProps> = (props): JSX.Element => {
  const { handleOpenLightbox, shop, shopGalleries, shopGalleriesError } = props;
  const {
    address,
    createdAt,
    endTimeWork,
    firstPhone,
    imageUrl,
    secondPhone,
    startTimeWork,
    thirdPhone,
    title,
    updatedAt,
  } = shop;

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  const formattedStartTimeWork = startTimeWork.substring(0, startTimeWork.lastIndexOf(':'));
  const formattedEndTimeWork = endTimeWork.substring(0, endTimeWork.lastIndexOf(':'));

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2} maxWidth="100%" margin={0}>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            Shop Details
          </Typography>
          <Paper elevation={3}>
            <Box sx={{ padding: '20px' }}>
              <Avatar src={imageUrl} sx={{ height: 130, marginX: 'auto', width: 130 }} />
            </Box>
            <Divider />
            <List>
              <ListItem>
                <ListItemText primary={`Title`} />
                <ListItemSecondaryAction>{title}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Address`} />
                <ListItemSecondaryAction>{address}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Time Of Work`} />
                <ListItemSecondaryAction>{formattedStartTimeWork} - {formattedEndTimeWork}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Phone`} />
                <ListItemSecondaryAction>{firstPhone}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Phone`} />
                <ListItemSecondaryAction>{secondPhone || 'N/A'}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Phone`} />
                <ListItemSecondaryAction>{thirdPhone || 'N/A'}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Grid container>
            <Grid item xs={6}>
              <Typography variant="h5" marginBottom="15px">
                Gallery
              </Typography>
            </Grid>
            <Grid item xs={6} textAlign="right">
              <Button
                color="success"
                component={Link}
                to="gallery/add"
                type="button"
                variant="text"
              >
                Add Image
              </Button>
            </Grid>
            <Grid item xs={12}>
              <ShopGalleryComponent
                handleOpenLightbox={handleOpenLightbox}
                shopGalleries={shopGalleries}
                shopGalleriesError={shopGalleriesError}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};
