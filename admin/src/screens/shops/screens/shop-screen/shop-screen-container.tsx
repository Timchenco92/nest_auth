// Modules
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, Outlet } from 'react-router-dom';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Components
import { ImageGalleryComponent } from '@/components/image-gallery-component';
import { LoaderComponent } from '@/components/loader-component';

// Engine
import { getShopRequest, shopSelector, shopPendingSelector, shopErrorSelector } from '@/store/shops';
import {
  shopGalleriesSelector,
  shopGalleriesErrorSelector,
  shopGalleriesPendingSelector,
} from '@/store/shop-gallery';

// Screens
import { ShopScreen } from '@/screens/shops/screens/shop-screen/shop-screen';

export const ShopScreenContainer: FC = (): JSX.Element => {
  const [open, setOpen] = useState<boolean>(false);
  const [image, setImage] = useState<string>('');

  const dispatch: Dispatch = useDispatch();
  const { shopId } = useParams<'shopId'>();

  const shop = useSelector(shopSelector);
  const shopPending = useSelector(shopPendingSelector);
  const shopError = useSelector(shopErrorSelector);

  const shopGalleries = useSelector(shopGalleriesSelector);
  const shopGalleriesPending = useSelector(shopGalleriesPendingSelector);
  const shopGalleriesError = useSelector(shopGalleriesErrorSelector);

  const images = useMemo(() => {
    const result: string[] = [];
    return shopGalleries.reduce((acc, val) => {
      const { imageUrl } = val;
      acc.push(imageUrl);
      return acc;
    }, result);
  }, [shopGalleries]);

  const handleClose = () => {
    setOpen((prevState) => !prevState);
  };

  const handleOpenLightbox = useCallback((image: string) => {
    setImage(image);
    setOpen(true);
  }, []);

  const handleNext = useCallback(() => {
    let currentIndex = images.indexOf(image);
    if (currentIndex >= images.length - 1) {
      setOpen(false);
    } else {
      let nextImage = images[currentIndex + 1];
      setImage(nextImage);
    }
  }, [image, images]);

  const handlePrevious = useCallback(() => {
    let currentIndex = images.indexOf(image);
    if (currentIndex <= 0) {
      setOpen(false);
    } else {
      let nextImage = images[currentIndex - 1];
      setImage(nextImage);
    }
  }, [image, images]);

  useEffect(() => {
    if (!shopId) return;

    const action = getShopRequest(shopId);
    dispatch(action);
  }, [dispatch, shopId]);

  if (shopPending || shopGalleriesPending) return <LoaderComponent />;

  if (shopError) return <CustomErrorOverlay />;

  return (
    <>
      <ShopScreen
        handleOpenLightbox={handleOpenLightbox}
        shop={shop}
        shopGalleries={shopGalleries}
        shopGalleriesError={shopGalleriesError}
      />
      <ImageGalleryComponent
        handleClose={handleClose}
        handleNext={handleNext}
        handlePrevious={handlePrevious}
        image={image}
        open={open}
      />
      <Outlet />
    </>
  );
};
