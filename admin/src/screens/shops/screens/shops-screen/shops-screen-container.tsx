// Modules
import { useCallback, useEffect } from 'react';
import { Link, Outlet, useLocation, useSearchParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Button, Grid } from '@mui/material';

// Modules Types
import { ChangeEvent, FC } from 'react';
import { Dispatch } from 'redux';
import { SelectChangeEvent } from '@mui/material';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Engine
import {
  changeLimit,
  changePage,
  getShopsRequest,
  paginationSelector,
  resetPaginationLimit,
  resetPaginationPage,
  shopsErrorSelector,
  shopsPendingSelector,
  shopsSelector,
} from '@/store/shops';

// Screens
import { ShopsScreen } from '@/screens/shops/screens/shops-screen/shops-screen';

const DEFAULT_PAGINATION_PAGE = '1';
const DEFAULT_LIMIT = '15';

export const ShopsScreenContainer: FC = (props): JSX.Element => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();

  const { data: shops, meta: { totalPages } } = useSelector(shopsSelector);
  const { limit, page } = useSelector(paginationSelector);
  const error = useSelector(shopsErrorSelector);
  const pending = useSelector(shopsPendingSelector);

  const addPath = search.length ? `add${search}` : 'add';

  const handleResetPage = useCallback(() => {
    const action = resetPaginationPage();
    dispatch(action);

    setSearchParams((searchParams) => {
      searchParams.delete('page');
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleChangeLimit = useCallback(async (event: SelectChangeEvent) => {
    const { target: { value } } = event;
    handleResetPage();

    const action = changeLimit(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(value) !== DEFAULT_LIMIT) {
        searchParams.set('limit', String(value));
      } else {
        searchParams.delete('limit');
      }
      return searchParams;
    });
  }, [dispatch, handleResetPage, setSearchParams]);

  const handleChangePage = useCallback((event: ChangeEvent<unknown>, page: number) => {
    const action = changePage(String(page));
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(page) !== DEFAULT_PAGINATION_PAGE) {
        searchParams.set('page', String(page));
      } else {
        searchParams.delete('page');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  useEffect(() => {
    if (!searchParams.has('page')) return;
    const qsPage = searchParams.get('page') as string;

    const action = changePage(qsPage);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('limit')) return;
    const qsLimit = searchParams.get('limit') as string;

    const action = changeLimit(qsLimit);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    const action = getShopsRequest(limit, page);
    dispatch(action);
  }, [dispatch, limit, page]);

  useEffect(() => {
    return () => {
      dispatch(resetPaginationLimit());
      dispatch(resetPaginationPage());
    };
  }, [dispatch]);

  if (error) return <CustomErrorOverlay />;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Grid
            alignItems="center"
            container
            direction="row"
            height="100%"
            justifyContent="flex-end"
          >
            <Button
              color="success"
              component={Link}
              size="large"
              to={addPath}
              type="button"
              variant="text"
            >
              Create
            </Button>
          </Grid>
        </Grid>
        <ShopsScreen
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          page={page}
          pending={pending}
          shops={shops}
          totalPages={totalPages}
        />
      </Grid>
      <Outlet />
    </Box>
  );
};
