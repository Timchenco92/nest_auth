// Modules
import { SelectChangeEvent } from '@mui/material';
import { ChangeEvent } from 'react';

// Interfaces
import { IShop } from '@/interfaces/models';

export interface IShopsScreenProps {
  shops: IShop[];
  handleChangeLimit(event: SelectChangeEvent): void;
  handleChangePage(event: ChangeEvent<unknown>, page: number): void;
  limit: string;
  page: string;
  pending: boolean;
  totalPages: number;
}
