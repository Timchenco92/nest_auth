// Modules
import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Engine
import { deleteShopRequest } from '@/store/shops';

export interface IProps {
  handleClose(): void;
}

export const DeleteShopScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;
  const { shopId } = useParams<'shopId'>();
  const dispatch: Dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!shopId) return;
    const action = deleteShopRequest(shopId);
    dispatch(action)
  }, [shopId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing a shop</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};
