// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Engine
import { deleteShopSuccessSelector, resetDeleteShopSuccess } from '@/store/shops';

// Screens
import { DeleteShopScreen } from '@/screens/shops/screens/delete-shop-screen/delete-shop-screen';

export const DeleteShopScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();
  const isDeleted = useSelector(deleteShopSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/shops${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();
    return () => {
      const action = resetDeleteShopSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteShopScreen handleClose={handleClose} />
  );
};
