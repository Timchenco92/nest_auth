// Modules
import type { FormikProps } from 'formik';

// Interfaces
import type { IAddShopValidationErrors, IInitialValues } from '@/screens/shops/screens/add-shop-screen/interfaces';

export interface IAddShopScreenProps {
  errors: IAddShopValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  pending: boolean;
}
