export interface IInitialValues {
  address: string;
  endTimeWork: Date | string;
  firstPhone: string;
  image: File | string;
  secondPhone: string;
  startTimeWork: Date | string;
  thirdPhone: string;
  title: string;
}
