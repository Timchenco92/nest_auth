// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/shops/screens/add-shop-screen/interfaces';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  address: string().required(isRequired),
  endTimeWork: string().required(isRequired),
  firstPhone: string().required(isRequired),
  image: mixed().required(isRequired),
  secondPhone: string().notRequired(),
  startTimeWork: string().required(isRequired),
  thirdPhone: string().notRequired(),
  title: string().required(isRequired),
});

export const initialValues: IInitialValues = {
  address: '',
  endTimeWork: '',
  firstPhone: '',
  image: '',
  secondPhone: '',
  startTimeWork: '',
  thirdPhone: '',
  title: '',
};
