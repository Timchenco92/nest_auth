// Modules
import {
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  IconButton,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { TimeField } from '@mui/x-date-pickers/TimeField';

// Modules Types
import { BaseSyntheticEvent, FC } from 'react';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IAddShopScreenProps } from '@/screens/shops/screens/add-shop-screen/interfaces';

export const AddShopScreen: FC<IAddShopScreenProps> = (props): JSX.Element => {
  const {
    errors: serverValidationErrors,
    formik,
    handleClose,
    pending,
  } = props;

  const {
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    setFieldError,
    setFieldTouched,
    setFieldValue,
    touched,
    values,
  } = formik;

  const [imageName, setImageName] = useState<string>('');
  const imageRef = useRef<HTMLInputElement>(null);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('image', files);
    setImageName(files?.name);
  }, [setFieldValue]);

  const handleClickImage = useCallback(async () => {
    imageRef?.current?.click();
    await setFieldTouched('image', true);
  }, [setFieldTouched]);

  const handleClearImage = useCallback(() => {
    if (!imageRef.current) return;
    setImageName('');
    imageRef.current.value = '';
    setFieldError('image', staticValidationErrors.isRequired);
  }, [setFieldError]);

  const handleChangeStartTime = useCallback(async (event: any) => {
    const { $H, $m } = event;
    const parsedTime = `${$H}:${$m}`;
    await setFieldValue('startTimeWork', parsedTime);
  }, [setFieldValue]);

  const handleBlurStartTime = useCallback(async () => {
    await setFieldTouched('startTimeWork', true);
    setFieldError('startTimeWork', staticValidationErrors.isRequired);
  }, [setFieldError, setFieldTouched]);

  const handleChangeEndTime = useCallback(async (event: any) => {
    const { $H, $m } = event;
    const parsedTime = `${$H}:${$m}`;
    await setFieldValue('endTimeWork', parsedTime);
  }, [setFieldValue]);

  const handleBlurEndTime = useCallback(async () => {
    await setFieldTouched('endTimeWork', true);
    setFieldError('endTimeWork', staticValidationErrors.isRequired);
  }, [setFieldError, setFieldTouched]);

  const isImageFieldFullWidth: string = Boolean(imageName) ? '95%' : '100%';

  const allowedDisabledConditions = [
    isValid,
    isSubmitting,
    pending,
  ];

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 100 } }}
    >
      <DialogTitle>Creating a new shop</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <TextField
            error={touched.title && Boolean(errors.title)}
            fullWidth
            helperText={touched.title && errors.title}
            label="Title"
            name="title"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.title}
            variant="standard"
          />
          <TextField
            error={touched.address && Boolean(errors.address)}
            fullWidth
            helperText={touched.address && errors.address}
            label="Address"
            name="address"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.address}
            variant="standard"
          />
          <TextField
            error={touched.firstPhone && Boolean(errors.firstPhone)}
            fullWidth
            helperText={touched.firstPhone && errors.firstPhone}
            inputProps={{ maxLength: 12 }}
            label="Phone"
            name="firstPhone"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.firstPhone}
            variant="standard"
          />
          <TextField
            error={touched.secondPhone && Boolean(errors.secondPhone)}
            fullWidth
            helperText={touched.secondPhone && errors.secondPhone}
            inputProps={{ maxLength: 12 }}
            label="Phone"
            name="secondPhone"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.secondPhone}
            variant="standard"
          />
          <TextField
            error={touched.thirdPhone && Boolean(errors.thirdPhone)}
            fullWidth
            helperText={touched.thirdPhone && errors.thirdPhone}
            inputProps={{ maxLength: 12 }}
            label="Phone"
            name="thirdPhone"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.thirdPhone}
            variant="standard"
          />
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DemoContainer components={['TimeField']}>
              <TimeField
                componentsProps={{
                  textField: {
                    error: touched.startTimeWork && Boolean(errors.startTimeWork)
                  }
                }}
                format="HH:mm"
                fullWidth
                helperText={touched.startTimeWork && errors.startTimeWork}
                label="Start Time Work"
                name="startTimeWork"
                onBlur={handleBlurStartTime}
                onChange={handleChangeStartTime}
                required
                sx={{ marginBottom: '10px' }}
                variant="standard"
              />
            </DemoContainer>
            <DemoContainer components={['TimeField']}>
              <TimeField
                componentsProps={{
                  textField: {
                    error: touched.endTimeWork && Boolean(errors.endTimeWork)
                  }
                }}
                format="HH:mm"
                fullWidth
                helperText={touched.endTimeWork && errors.endTimeWork}
                label="End Time Work"
                name="endTimeWork"
                onBlur={handleBlurEndTime}
                onChange={handleChangeEndTime}
                required
                sx={{ marginBottom: '10px' }}
                variant="standard"
              />
            </DemoContainer>
          </LocalizationProvider>
          <FormGroup row>
            <FormControl sx={{ flexBasis: isImageFieldFullWidth }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={touched.image && Boolean(errors.image)}
                focused={Boolean(imageName)}
                helperText={touched.image && errors.image}
                label="Image"
                margin="dense"
                name="image"
                onClick={handleClickImage}
                required
                sx={{ marginBottom: '10px' }}
                type="text"
                value={imageName}
                variant="standard"
              />
            </FormControl>
            {Boolean(imageName) && (
              <FormControl sx={{ textAlign: 'center', justifyContent: 'flex-end', flexBasis: '5%' }}>
                <IconButton onClick={handleClearImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="image"
            onChange={handleChangeImage}
            type="file"
            ref={imageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitDisabled}
            color="success"
            fullWidth
            type="submit"
          >
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
