// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/shops/screens/add-shop-screen/form-config';

// Engine
import {
  addShopErrorSelector,
  addShopPendingSelector,
  addShopRequest,
  addShopSuccessSelector,
  resetAddShopError,
  resetAddShopSuccess,
} from '@/store/shops';

// Interfaces
import type { IInitialValues } from '@/screens/shops/screens/add-shop-screen/interfaces';

// Screens
import { AddShopScreen } from '@/screens/shops/screens/add-shop-screen/add-shop-screen';

export const AddShopScreenContainer: FC = (): JSX.Element => {
  const { errors, hasError } = useSelector(addShopErrorSelector);
  const isAdded: boolean = useSelector(addShopSuccessSelector);
  const pending: boolean = useSelector(addShopPendingSelector);

  const dispatch: Dispatch = useDispatch();

  const navigate = useNavigate();
  const { search } = useLocation();

  const handleClose = useCallback(() => {
    navigate(`/dashboard/shops${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    const data = new FormData();
    if (typeof values.endTimeWork === 'string') data.append('endTimeWork', values.endTimeWork);
    if (typeof values.startTimeWork === 'string') data.append('startTimeWork', values.startTimeWork);
    if (values.secondPhone.length) data.append('secondPhone', values.secondPhone);
    if (values.thirdPhone.length) data.append('thirdPhone', values.thirdPhone);
    data.append('firstPhone', values.firstPhone);
    data.append('address', values.address);
    data.append('image', values.image);
    data.append('title', values.title);
    const action = addShopRequest(data);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isAdded) return;
    handleClose();

    return() => {
      const action = resetAddShopSuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isAdded]);

  useEffect(() => () => {
    const action = resetAddShopError();
    dispatch(action);
  }, [dispatch]);

  return (
    <AddShopScreen errors={errors} formik={formik} handleClose={handleClose} pending={pending} />
  );
};
