// Modules
import { ImageList } from '@mui/material';

// Modules Types
import { FC } from 'react';

// Assets
import { CustomNoRowsOverlay, CustomErrorOverlay } from '@/assets/svg';

// Components
import { ProductGalleryItemComponent } from '@/screens/products/screens/product-screen/components/product-gallery-component/components/product-gallery-item-component';

// Interfaces
import type { IProductGallery } from '@/interfaces/models';

interface IProps {
  handleOpenLightbox: (image: string) => void;
  galleries: IProductGallery[];
  productGalleriesError: boolean;
}

export const ProductGalleryComponent: FC<IProps> = (props): JSX.Element => {
  const { handleOpenLightbox, galleries, productGalleriesError } = props;

  const galleriesCount = galleries.length;

  if (!galleriesCount) return <CustomNoRowsOverlay />;

  if (productGalleriesError) return <CustomErrorOverlay />;

  return (
    <ImageList sx={{ height: 550 }} variant="woven" cols={3} rowHeight={164}>
      {galleries.map((gallery) => (
        <ProductGalleryItemComponent handleOpenLightbox={handleOpenLightbox} gallery={gallery} key={gallery.id} />
      ))}
    </ImageList>
  );
};
