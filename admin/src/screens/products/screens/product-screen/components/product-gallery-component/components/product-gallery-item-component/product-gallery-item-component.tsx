// Modules
import { Button, ImageListItem } from '@mui/material';
import { Link } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Interfaces
import type { IProductGallery } from '@/interfaces/models';

interface IProps {
  handleOpenLightbox: (image: string) => void;
  gallery: IProductGallery;
}

export const ProductGalleryItemComponent: FC<IProps> = (props): JSX.Element => {
  const { gallery, handleOpenLightbox } = props;
  const { id, imageUrl } = gallery;

  return (
    <ImageListItem
      key={id}
      sx={{ cursor: 'pointer' }}
    >
      <img
        alt={id}
        loading="lazy"
        onClick={() => handleOpenLightbox(imageUrl)}
        src={imageUrl}
        srcSet={imageUrl}
      />
      <Button
        color="error"
        component={Link}
        to={`gallery/${id}/delete`}
        fullWidth
        sx={{ marginTop: '10px' }}
        type="button"
        variant="outlined"
      >
        Delete
      </Button>
    </ImageListItem>
  );
};
