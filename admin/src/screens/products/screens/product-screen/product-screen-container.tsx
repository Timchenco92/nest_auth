// Modules
import { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, Outlet } from 'react-router-dom';

// Modules types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Components
import { ImageGalleryComponent } from '@/components/image-gallery-component';
import { LoaderComponent } from '@/components/loader-component';

// Engine
import { getProductRequest, productPendingSelector, productErrorSelector, productSelector } from '@/store/products';
import { productGalleriesSelector, productGalleriesPendingSelector, productGalleriesErrorSelector } from '@/store/product-gallery';

// Screens
import { ProductScreen } from '@/screens/products/screens/product-screen/product-screen';

export const ProductScreenContainer: FC = (): JSX.Element => {
  const [open, setOpen] = useState<boolean>(false);
  const [image, setImage] = useState<string>('');

  const dispatch: Dispatch = useDispatch();
  const { productId } = useParams<'productId'>();

  const productPending = useSelector(productPendingSelector);
  const productError = useSelector(productErrorSelector);
  const product = useSelector(productSelector);

  const productGalleries = useSelector(productGalleriesSelector);
  const productGalleriesError = useSelector(productGalleriesErrorSelector);
  const productGalleriesPending = useSelector(productGalleriesPendingSelector);

  const images = useMemo(() => {
    const result: string[] = [];
    return productGalleries.reduce((acc, val) => {
      const { imageUrl } = val;
      acc.push(imageUrl);
      return acc;
    }, result);
  }, [productGalleries]);

  const handleClose = () => {
    setOpen((prevState) => !prevState);
  };

  const handleOpenLightbox = useCallback((image: string) => {
    setImage(image);
    setOpen(true);
  }, []);

  const handleNext = useCallback(() => {
    let currentIndex = images.indexOf(image);
    if (currentIndex >= images.length - 1) {
      setOpen(false);
    } else {
      let nextImage = images[currentIndex + 1];
      setImage(nextImage);
    }
  }, [image, images]);

  const handlePrevious = useCallback(() => {
    let currentIndex = images.indexOf(image);
    if (currentIndex <= 0) {
      setOpen(false);
    } else {
      let nextImage = images[currentIndex - 1];
      setImage(nextImage);
    }
  }, [image, images]);

  useEffect(() => {
    if (!productId) return;

    const action = getProductRequest(productId);
    dispatch(action);
  }, [dispatch, productId]);

  if (productError) return <CustomErrorOverlay />;

  if (productPending || productGalleriesPending) return <LoaderComponent />;

  return (
    <>
      <ProductScreen
        handleOpenLightbox={handleOpenLightbox}
        product={product}
        productGalleries={productGalleries}
        productGalleriesError={productGalleriesError}
      />
      <ImageGalleryComponent
        handleClose={handleClose}
        handleNext={handleNext}
        handlePrevious={handlePrevious}
        image={image}
        open={open}
      />
      <Outlet />
    </>
  );
};
