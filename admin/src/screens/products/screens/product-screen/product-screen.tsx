// Modules
import { FC } from 'react';
import {
  Avatar,
  Box,
  Button,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
  Typography,
} from '@mui/material';
import { Link } from 'react-router-dom';

// Components
import { ProductGalleryComponent } from '@/screens/products/screens/product-screen/components/product-gallery-component';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import { IProduct, IProductGallery } from '@/interfaces/models';

interface IProps {
  handleOpenLightbox: (image: string) => void;
  product: IProduct;
  productGalleries: IProductGallery[];
  productGalleriesError: boolean;
}

export const ProductScreen: FC<IProps> = (props): JSX.Element => {
  const { handleOpenLightbox, product, productGalleries, productGalleriesError } = props;
  const { updatedAt, title, imageUrl, createdAt, category, price } = product;

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  const formattedCategoryCreatedAt = category?.createdAt && ConvertDatetime({ dateTime: category?.createdAt });
  const formattedCategoryUpdatedAt = category?.updatedAt && ConvertDatetime({ dateTime: category?.updatedAt });

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2} maxWidth="100%" margin={0}>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            Product Details
          </Typography>
          <Paper elevation={3}>
            <Box sx={{ padding: '20px' }}>
              <Avatar src={imageUrl} sx={{ height: 130, marginX: 'auto', width: 130 }} />
            </Box>
            <Divider />
            <List>
              <ListItem>
                <ListItemText primary={`Title`} />
                <ListItemSecondaryAction>{title}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Price`} />
                <ListItemSecondaryAction>{price} $</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            Category Details
          </Typography>
          <Paper elevation={3}>
            <Box sx={{ padding: '20px' }}>
              <Avatar src={category?.imageUrl} sx={{ height: 130, marginX: 'auto', width: 130 }} />
            </Box>
            <Divider />
            <List>
              <ListItem>
                <ListItemText primary={`Title`} />
                <ListItemSecondaryAction>{category?.title}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Count Of Products`} />
                <ListItemSecondaryAction>{category?.productsCount}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedCategoryCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedCategoryUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
      </Grid>
      <Grid container spacing={2} maxWidth="100%" m={0}>
        <Grid item xs={6}>
          <Grid container>
            <Grid item xs={6} marginBottom="15px">
              <Typography variant="h5">
                Gallery
              </Typography>
            </Grid>
            <Grid item xs={6} textAlign="right" marginBottom="15px">
              <Button
                color="success"
                component={Link}
                to="gallery/add"
                type="button"
                variant="text"
              >
                Add Image
              </Button>
            </Grid>
            <Grid item xs={12}>
              <ProductGalleryComponent
                handleOpenLightbox={handleOpenLightbox}
                galleries={productGalleries}
                productGalleriesError={productGalleriesError}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

export default ProductScreen;
