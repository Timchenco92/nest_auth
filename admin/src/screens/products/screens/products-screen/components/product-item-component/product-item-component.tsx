// Modules
import { Link, useLocation } from 'react-router-dom';
import { Delete, Edit, Info } from '@mui/icons-material';
import { Avatar, IconButton, TableCell, TableRow, Tooltip } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Constants
import { TOOLTIPS_MESSAGES } from '@/constants';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IProduct } from '@/interfaces/models';

interface IProps {
  product: IProduct;
  rowIdx: number;
}

export const ProductItemComponent: FC<IProps> = (props): JSX.Element => {
  const { product, rowIdx } = props;
  const {
    category,
    createdAt,
    id,
    imageUrl,
    price,
    title,
    updatedAt,
  } = product;

  const { search } = useLocation();

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  const deletePath = search.length ? `${id}/delete${search}` : `${id}/delete`;
  const updatePath = search.length ? `${id}/delete${search}` : `${id}/update`;

  const { isDeleted, isDetails, isUpdate } = TOOLTIPS_MESSAGES;

  return (
    <TableRow>
      <TableCell align="center">{rowIdx + 1}</TableCell>
      <TableCell align="center">
        <Avatar src={imageUrl} sx={{ marginX: 'auto' }} />
      </TableCell>
      <TableCell align="center">{title}</TableCell>
      <TableCell align="center">{price} $</TableCell>
      <TableCell align="center">{category?.title}</TableCell>
      <TableCell align="center">{formattedCreatedAt}</TableCell>
      <TableCell align="center">{formattedUpdatedAt}</TableCell>
      <TableCell align="center">
        <Tooltip placement="top" title={isDetails('Product')}>
          <IconButton component={Link} to={`${id}/details`} color="info" size="small">
            <Info />
          </IconButton>
        </Tooltip>
        <Tooltip placement="top" title={isUpdate('product')}>
          <IconButton component={Link} to={updatePath} color="warning" size="small">
            <Edit />
          </IconButton>
        </Tooltip>
        <Tooltip placement="top" title={isDeleted('product')}>
          <IconButton component={Link} to={deletePath} color="error" size="small">
            <Delete />
          </IconButton>
        </Tooltip>
      </TableCell>
    </TableRow>
  );
};
