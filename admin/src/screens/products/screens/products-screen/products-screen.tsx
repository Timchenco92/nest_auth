// Modules
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Assets
import { CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component'
import { PaginationComponent } from '@/components/pagination-component';
import { ProductItemComponent } from '@/screens/products/screens/products-screen/components/product-item-component';

// Constants
import { COLORS } from '@/constants';

// Interfaces
import { IProductsScreenProps } from '@/screens/products/screens/products-screen/interfaces';

export const ProductsScreen: FC<IProductsScreenProps> = (props): JSX.Element => {
  const { handleChangeLimit, handleChangePage, limit, page, pending, products, totalPages } = props;

  const productsCount = products.length;

  if (!productsCount) return <CustomNoRowsOverlay />

  if (pending) return <LoaderComponent />;

  return (
    <Grid item xs={12}>
      <Paper elevation={6}>
        <TableContainer>
          <Table>
            <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
              <TableRow>
                <TableCell align="center">#</TableCell>
                <TableCell />
                <TableCell align="center">Title</TableCell>
                <TableCell align="center">Price</TableCell>
                <TableCell align="center">Category</TableCell>
                <TableCell align="center">Created</TableCell>
                <TableCell align="center">Updated</TableCell>
                <TableCell align="center"/>
              </TableRow>
            </TableHead>
            <TableBody>
              {products.map((product, key) => (
                <ProductItemComponent product={product} rowIdx={key} key={product.id} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <PaginationComponent
          currentPage={page}
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          totalPages={totalPages}
        />
      </Paper>
    </Grid>
  );
};

export default ProductsScreen;
