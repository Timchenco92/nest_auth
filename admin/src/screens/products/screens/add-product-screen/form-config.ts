// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/products/screens/add-product-screen/interfaces';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  categoryId: string().required(isRequired),
  image: mixed().required(isRequired),
  price: string().required(isRequired),
  title: string().required(isRequired),
});

export const initialValues: IInitialValues = {
  categoryId: '',
  image: '',
  price: '',
  title: '',
};
