// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import { FC } from 'react';
import { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/products/screens/add-product-screen/form-config';

// Engine
import {
  addProductErrorSelector,
  addProductSuccessSelector,
  addProductRequest,
  resetAddProductError,
  resetAddProductSuccess,
} from '@/store/products';
import { getCategoriesRequest, categoriesSelector } from '@/store/categories';

// Interfaces
import type { IInitialValues } from '@/screens/products/screens/add-product-screen/interfaces';

// Screens
import { AddProductScreen } from '@/screens/products/screens/add-product-screen/add-product-screen';

export const AddProductScreenContainer: FC = (): JSX.Element => {
  const { errors, hasError } = useSelector(addProductErrorSelector);
  const isProductAdded = useSelector(addProductSuccessSelector);
  const { data: categories } = useSelector(categoriesSelector);

  const dispatch = useDispatch();

  const navigate = useNavigate();
  const { search } = useLocation();

  const handleClose = useCallback(() => {
    navigate(`/dashboard/products${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    const data = new FormData();
    data.append('categoryId', values.categoryId);
    data.append('image', values.image);
    data.append('price', values.price);
    data.append('title', values.title);
    const action = addProductRequest(data);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    const action = getCategoriesRequest('100', '1', '');
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isProductAdded) return;
    handleClose();
    return () => {
      const action = resetAddProductSuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isProductAdded]);

  useEffect(() => {
    return () => {
      const action = resetAddProductError();
      dispatch(action);
    }
  }, [dispatch]);

  return (
    <AddProductScreen
      categories={categories}
      errors={errors}
      formik={formik}
      handleClose={handleClose}
    />
  );
};

export default AddProductScreenContainer;
