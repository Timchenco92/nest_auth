// Modules
import { FormikProps } from 'formik';

// Interfaces
import type {
  IAddProductValidationErrors,
  IInitialValues,
} from '@/screens/products/screens/add-product-screen/interfaces';
import type { ICategory } from '@/interfaces/models';

export interface IAddProductScreenProps {
  categories: ICategory[];
  errors: IAddProductValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
}
