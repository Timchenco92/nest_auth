export interface IInitialValues {
  categoryId: string;
  title: string;
  image: File | string;
  price: string;
}
