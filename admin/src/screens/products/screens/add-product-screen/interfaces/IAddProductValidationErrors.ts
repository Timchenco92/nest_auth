export interface IAddProductValidationErrors {
  categoryId: string;
  image: string
  price: string;
  title: string;
}
