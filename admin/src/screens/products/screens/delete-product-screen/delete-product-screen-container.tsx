// Modules
import { FC, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { Dispatch } from 'redux';

// Engine
import { deleteProductSuccessSelector, resetDeleteProductSuccess } from '@/store/products';

// Screens
import { DeleteProductScreen } from '@/screens/products/screens/delete-product-screen/delete-product-screen';

export const DeleteProductScreenContainer: FC = (props): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();
  const isDeleted = useSelector(deleteProductSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/products${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();
    return () => {
      const action = resetDeleteProductSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteProductScreen handleClose={handleClose}/>
  );
};

export default DeleteProductScreenContainer;
