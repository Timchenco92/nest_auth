// Modules
import { FC, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import { Dispatch } from 'redux';

// Engine
import { deleteProductRequest } from '@/store/products';

export interface IProps {
  handleClose(): void;
}

export const DeleteProductScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;
  const { productId } = useParams<'productId'>();
  const dispatch: Dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!productId) return;
    const action = deleteProductRequest(productId);
    dispatch(action)
  }, [productId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing a product</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};
