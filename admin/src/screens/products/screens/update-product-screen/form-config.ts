// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  categoryId: string().required(isRequired),
  image: mixed().required(isRequired),
  price: string().required(isRequired),
  title: string().required(isRequired),
});
