// Modules
import { useCallback, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema } from '@/screens/products/screens/update-product-screen/form-config';

// Engine
import {
  getProductRequest,
  productSelector,
  resetUpdateProductError,
  resetUpdateProductSuccess,
  updateProductErrorSelector,
  updateProductRequest,
  updateProductSuccessSelector,
} from '@/store/products';

import { getCategoriesRequest, categoriesSelector } from '@/store/categories';

// Interfaces
import { IInitialValues } from '@/screens/products/screens/update-product-screen/interfaces';

// Screens
import { UpdateProductScreen } from '@/screens/products/screens/update-product-screen/update-product-screen';

export const UpdateProductScreenContainer: FC = (): JSX.Element => {
  const { productId } = useParams<'productId'>();

  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();

  const isUpdate = useSelector(updateProductSuccessSelector);
  const { errors, hasError } = useSelector(updateProductErrorSelector);
  const product = useSelector(productSelector);
  const { data: categories } = useSelector(categoriesSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/products${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues: {
      categoryId: product.categoryId,
      image: product.image,
      price: product.price,
      title: product.title,
    },
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    if (!productId) return;

    const data = new FormData();
    if (typeof values.image === 'object') data.append('image', values.image);
    data.append('title', values.title);
    data.append('price', values.price);
    data.append('categoryId', values.categoryId);

    const action = updateProductRequest({ productId, data });
    dispatch(action);
  }, [dispatch, productId]);

  useEffect(() => {
    const action = getCategoriesRequest('100', '1', '')
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!productId) return;

    const action = getProductRequest(productId);
    dispatch(action);
  }, [dispatch, productId]);

  useEffect(() => {
    if (!isUpdate) return;
    handleClose();

    return () => {
      const action = resetUpdateProductSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isUpdate]);

  useEffect(() => {
    return () => {
      const action = resetUpdateProductError();
      dispatch(action);
    };
  }, [dispatch]);

  return (
    <UpdateProductScreen
      categories={categories}
      errors={errors}
      formik={formik}
      handleClose={handleClose}
    />
  );
};
