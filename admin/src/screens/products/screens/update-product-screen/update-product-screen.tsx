// Modules
import {
  useCallback,
  useRef,
  useState,
  useEffect,
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  FormHelperText,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import { BaseSyntheticEvent, FC } from 'react';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IUpdateProductScreenProps } from '@/screens/products/screens/update-product-screen/interfaces';

export const UpdateProductScreen: FC<IUpdateProductScreenProps> = (props): JSX.Element => {
  const {
    categories,
    errors: serverValidationErrors,
    formik,
    handleClose,
  } = props;

  const {
    dirty,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    setErrors,
    setFieldError,
    setFieldTouched,
    setFieldValue,
    touched,
    values,
  } = formik;

  const [imageName, setImageName] = useState<string>('');
  const imageRef = useRef<HTMLInputElement>(null);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('image', files);
    setImageName(files?.name);
  }, [setFieldValue]);

  const handleClickImage = useCallback(() => {
    imageRef?.current?.click();
    setFieldTouched('image', true);
  }, [setFieldTouched]);

  const handleClearImage = useCallback(async () => {
    if (!imageRef.current) return;
    setImageName('');
    imageRef.current.value = '';
    await setFieldValue('image', '');
    setFieldTouched('image', true);
    setFieldError('image', staticValidationErrors.isRequired);
  }, [setFieldError, setFieldTouched, setFieldValue]);

  const isImageFieldFullWidth: string = Boolean(imageName) ? '95%' : '100%';

  useEffect(() => {
    setImageName(values.image);
  }, [values.image]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={() => ({})}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 240 } }}
    >
      <DialogTitle>Updating a product</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <FormControl fullWidth variant="standard">
            <InputLabel id="category-label">Category *</InputLabel>
            <Select
              error={touched.categoryId && Boolean(errors.categoryId)}
              labelId="category-label"
              value={values.categoryId}
              label="Category *"
              onBlur={handleBlur}
              onChange={handleChange}
              name="categoryId"
              required
            >
              {categories.map((category) => {
                if (category.id === values.categoryId) {
                  return (
                    <MenuItem value={category.id}>{category.title}</MenuItem>
                  )
                }
                return (
                  <MenuItem value={category.id}>{category.title}</MenuItem>
                )
              })}
            </Select>
            {touched.categoryId && errors.categoryId && (
              <FormHelperText error>{errors.categoryId}</FormHelperText>
            )}
          </FormControl>
          <TextField
            error={touched.title && Boolean(errors.title)}
            fullWidth
            helperText={touched.title && errors.title}
            label="Title"
            name="title"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.title}
            variant="standard"
          />
          <TextField
            error={touched.price && Boolean(errors.price)}
            fullWidth
            helperText={touched.price && errors.price}
            label="Price"
            name="price"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="number"
            value={values.price}
            variant="standard"
          />
          <FormGroup row>
            <FormControl sx={{ flexBasis: isImageFieldFullWidth }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={touched.image && Boolean(errors.image)}
                focused={Boolean(imageName)}
                helperText={touched.image && errors.image}
                label="Image"
                margin="dense"
                name="image"
                onClick={handleClickImage}
                required
                type="text"
                value={imageName}
                variant="standard"
              />
            </FormControl>
            {Boolean(imageName) && (
              <FormControl sx={{ textAlign: 'center', justifyContent: 'flex-end', flexBasis: '5%' }}>
                <IconButton onClick={handleClearImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="image"
            onChange={handleChangeImage}
            type="file"
            ref={imageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitting || !dirty}
            color="success"
            fullWidth
            type="submit"
          >
            Update
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
