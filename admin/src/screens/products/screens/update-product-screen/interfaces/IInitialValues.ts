export interface IInitialValues {
  categoryId: string
  image: string;
  price: string;
  title: string;
}
