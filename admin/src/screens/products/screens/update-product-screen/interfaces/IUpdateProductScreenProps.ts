// Modules
import { FormikProps } from 'formik';

// Interfaces
import type {
  IInitialValues,
  IUpdateProductValidationErrors,
} from '@/screens/products/screens/update-product-screen/interfaces';
import type { ICategory } from '@/interfaces/models';

export interface IUpdateProductScreenProps {
  categories: ICategory[];
  errors: IUpdateProductValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
}
