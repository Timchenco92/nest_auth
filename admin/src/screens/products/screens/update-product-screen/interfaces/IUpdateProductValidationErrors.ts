export interface IUpdateProductValidationErrors {
  categoryId: string;
  image: string
  price: string;
  title: string;
}
