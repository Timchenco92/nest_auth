// Modules
import { FC } from 'react';
import { Route, Routes } from 'react-router-dom';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Screens
import { AddProductScreenContainer } from '@/screens/products/screens/add-product-screen';
import { DeleteProductScreenContainer } from '@/screens/products/screens/delete-product-screen';
import { ProductScreenContainer } from '@/screens/products/screens/product-screen';
import { ProductsScreenContainer } from '@/screens/products/screens/products-screen';
import { UpdateProductScreenContainer } from '@/screens/products/screens/update-product-screen';
import { AddProductGalleryScreenContainer } from '@/screens/product-galleries/add-product-gallery-screen';
import { DeleteProductGalleryScreenContainer } from '@/screens/product-galleries/delete-product-gallery-screen';

export const ProductsContainer: FC = (): JSX.Element => {

  return (
    <>
      <CustomSnackbarComponent />
      <Routes>
        <Route path="*" element={<ProductsScreenContainer />}>
          <Route path="add" element={<AddProductScreenContainer />} />
          <Route path=":productId/delete" element={<DeleteProductScreenContainer />} />
          <Route path=":productId/update" element={<UpdateProductScreenContainer />} />
        </Route>
        <Route path=":productId/details" element={<ProductScreenContainer />}>
          <Route path="gallery/add" element={<AddProductGalleryScreenContainer />} />
          <Route path="gallery/:productGalleryId/delete" element={<DeleteProductGalleryScreenContainer />} />
        </Route>
      </Routes>
    </>
  );
};

export default ProductsContainer;
