// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/product-galleries/add-product-gallery-screen/form-config';

// Engine
import {
  addProductGalleryErrorSelector,
  addProductGalleryRequest,
  addProductGallerySuccessSelector,
  resetAddProductGalleryError,
  resetAddProductGallerySuccess,
} from '@/store/product-gallery';

// Interfaces
import type { IInitialValues } from '@/screens/product-galleries/add-product-gallery-screen/interfaces';

// Screens
import { AddProductGalleryScreen } from '@/screens/product-galleries/add-product-gallery-screen/add-product-gallery-screen';

export const AddProductGalleryScreenContainer: FC = (): JSX.Element => {
  const { productId } = useParams<'productId'>();
  const { errors, hasError } = useSelector(addProductGalleryErrorSelector);
  const isProductGalleryAdded = useSelector(addProductGallerySuccessSelector);

  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleClose = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    if (!productId) return;
    const data = new FormData();
    data.append('image', values.image);
    data.append('productId', productId);
    const action = addProductGalleryRequest({ productId, data });
    dispatch(action);
  }, [dispatch, productId]);

  useEffect(() => {
    if (!isProductGalleryAdded) return;
    handleClose();
    return () => {
      const action = resetAddProductGallerySuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isProductGalleryAdded]);

  useEffect(() => {
    return () => {
      const action = resetAddProductGalleryError();
      dispatch(action);
    }
  }, [dispatch]);

  return (
    <AddProductGalleryScreen errors={errors} formik={formik} handleClose={handleClose} />
  );
};
