// Modules
import { FormikProps } from 'formik';

// Interfaces
import type {
  IAddProductGalleryValidationErrors,
  IInitialValues,
} from '@/screens/product-galleries/add-product-gallery-screen/interfaces';

export interface IAddProductGalleryScreenProps {
  errors: IAddProductGalleryValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
}
