// Modules
import { object, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/product-galleries/add-product-gallery-screen/interfaces';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  image: mixed().required(isRequired),
});

export const initialValues: IInitialValues = {
  image: '',
};
