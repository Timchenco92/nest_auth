// Modules
import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Engine
import { deleteProductGalleryRequest } from '@/store/product-gallery';

interface IProps {
  handleClose(): void;
}

export const DeleteProductGalleryScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;
  const { productGalleryId, productId } = useParams<{ productGalleryId: string, productId: string }>();
  const dispatch: Dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!productGalleryId || !productId) return;

    const action = deleteProductGalleryRequest({ productGalleryId, productId });
    dispatch(action)
  }, [productGalleryId, productId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing image from gallery</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};
