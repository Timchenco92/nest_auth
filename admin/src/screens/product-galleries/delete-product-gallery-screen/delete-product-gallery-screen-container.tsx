// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Engine
import { deleteProductGallerySuccessSelector, resetDeleteProductGallerySuccess } from '@/store/product-gallery';

// Screens
import { DeleteProductGalleryScreen } from '@/screens/product-galleries/delete-product-gallery-screen/delete-product-gallery-screen';

export const DeleteProductGalleryScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const isDeleted = useSelector(deleteProductGallerySuccessSelector);

  const handleClose = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();
    return () => {
      const action = resetDeleteProductGallerySuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteProductGalleryScreen handleClose={handleClose} />
  );
};

export default DeleteProductGalleryScreenContainer;
