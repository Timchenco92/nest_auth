// Modules
import { useEffect } from 'react';
import {
  Avatar,
  Box,
  Button,
  Grid,
  TextField,
  Typography,
  colors,
} from '@mui/material';
import { LockOutlined } from '@mui/icons-material';

// Modules Types
import type { FC } from 'react';

// Interfaces
import { ILoginScreenProps } from '@/screens/auth/interfaces/ILoginScreenProps';

export const LoginScreen: FC<ILoginScreenProps> = (props): JSX.Element => {
  const { formik, errors: serverValidationErrors, pending } = props;
  const { blue } = colors;

  const {
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    touched,
    values,
  } = formik;

  const allowedDisabledConditions = [
    isValid,
    isSubmitting,
    pending,
  ];

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <form onSubmit={handleSubmit}>
      <Box
        alignItems="center"
        display="flex"
        justifyContent="center"
        minHeight="100vh"
        sx={{ flexGrow: 1 }}
      >
        <Grid
          alignItems="center"
          container
          direction="column"
          justifyContent="center"
          spacing={3}
          textAlign="center"
        >
          <Grid item display="flex" justifyContent="center">
            <Avatar sx={{
              bgcolor: blue['700'],
              height: 50,
              m: 1,
              width: 50,
            }}>
              <LockOutlined />
            </Avatar>
          </Grid>
          <Grid item>
            <Typography component="h1" variant="h5">
              Sign in
            </Typography>
          </Grid>
          <Grid item width="30%">
            <TextField
              error={touched.email && Boolean(errors.email)}
              fullWidth
              helperText={touched.email && errors.email}
              label="Email Address"
              name="email"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="email"
              value={values.email}
              variant="standard"
            />
          </Grid>
          <Grid item width="30%">
            <TextField
              error={touched.password && Boolean(errors.password)}
              fullWidth
              helperText={touched.password && errors.password}
              label="Password"
              name="password"
              onBlur={handleBlur}
              onChange={handleChange}
              required
              type="password"
              value={values.password}
              variant="standard"
            />
          </Grid>
          <Grid item width="30%">
            <Button
              disabled={isSubmitDisabled}
              fullWidth
              type="submit"
              variant="contained"
            >
              Sign In
            </Button>
          </Grid>
        </Grid>
      </Box>
    </form>
  );
};
