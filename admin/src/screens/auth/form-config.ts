// Modules
import { object, string } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/auth/interfaces/IInitialValues';

const { isEmail, isRequired, min, max } = staticValidationErrors;

export const validationSchema = object({
  email: string().email(isEmail).required(isRequired),
  password: string()
    .min(8, min('Password', 8))
    .max(16, max('Password', 16))
    .required(isRequired),
});

export const initialValues: IInitialValues = {
  email: '',
  password: '',
};
