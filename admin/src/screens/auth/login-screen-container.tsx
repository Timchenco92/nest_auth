// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { FormikProps } from 'formik';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Config
import { initialValues, validationSchema } from '@/screens/auth/form-config';

// Engine
import {
  loginAdminRequest,
  resetLoginAdminSuccess,
  loginErrorSelector,
  loginPendingSelector,
  loginSuccessSelector,
} from '@/store/auth';

// Interfaces
import type { IInitialValues } from '@/screens/auth/interfaces/IInitialValues';

// Screens
import { LoginScreen } from './login-screen';

export const LoginScreenContainer: FC = (): JSX.Element => {
  const { errors } = useSelector(loginErrorSelector);
  const pending = useSelector(loginPendingSelector);
  const success = useSelector(loginSuccessSelector);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: true,
    validateOnBlur: true,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    const { email, password } = values;
    const action = loginAdminRequest({ email, password });
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!success) return;
    navigate('', { replace: true });
    return () => {
      const action = resetLoginAdminSuccess();
      dispatch(action);
    };
  }, [dispatch, navigate, success]);

  return (
    <>
      <CustomSnackbarComponent />
      <LoginScreen errors={errors} formik={formik} pending={pending} />
    </>
  );
};
