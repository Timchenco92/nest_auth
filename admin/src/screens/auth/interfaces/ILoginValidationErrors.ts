export interface ILoginValidationErrors {
  email: string;
  password: string;
}
