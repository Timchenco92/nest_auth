// Modules Types
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, ILoginValidationErrors } from '@/screens/auth/interfaces';

export interface ILoginScreenProps {
  errors: ILoginValidationErrors;
  formik: FormikProps<IInitialValues>;
  pending: boolean;
}
