// Modules
import { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Engine
import { resetPagination } from '@/store/categories';

// Screens
import { AddCategoryScreenContainer } from '@/screens/categories/screens/add-category-screen';
import { CategoriesScreenContainer } from '@/screens/categories/screens/categories-screen';
import { CategoryScreenContainer } from '@/screens/categories/screens/category-screen';
import { DeleteCategoryScreenContainer } from '@/screens/categories/screens/delete-category-screen';
import { UpdateCategoryScreenContainer } from '@/screens/categories/screens/update-category-screen';

export const CategoriesContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();

  useEffect(() => () => {
    const action = resetPagination();
    dispatch(action);
  }, [dispatch]);

  return (
    <>
      <CustomSnackbarComponent />
      <Routes>
        <Route path="*" element={<CategoriesScreenContainer />}>
          <Route path="add" element={<AddCategoryScreenContainer />} />
          <Route path=":categoryId/delete" element={<DeleteCategoryScreenContainer />} />
          <Route path=":categoryId/update" element={<UpdateCategoryScreenContainer />} />
        </Route>
        <Route path=":categoryId/details" element={<CategoryScreenContainer />} />
      </Routes>
    </>
  );
};

export default CategoriesContainer;
