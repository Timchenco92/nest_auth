// Modules
import { FC, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { Dispatch } from 'redux';

// Engine
import { deleteCategorySuccessSelector, resetDeleteCategorySuccess } from '@/store/categories';

// Screens
import { DeleteCategoryScreen } from '@/screens/categories/screens/delete-category-screen/delete-category-screen';

export const DeleteCategoryScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();
  const isDeleted = useSelector(deleteCategorySuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/categories${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();
    return () => {
      const action = resetDeleteCategorySuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteCategoryScreen handleClose={handleClose} />
  );
};

export default DeleteCategoryScreenContainer;
