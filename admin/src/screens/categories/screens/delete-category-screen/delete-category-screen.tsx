// Modules
import { FC, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Engine
import { deleteCategoryRequest } from '@/store/categories';

export interface IProps {
  handleClose(): void;
}

export const DeleteCategoryScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;
  const { categoryId } = useParams<'categoryId'>();
  const dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!categoryId) return;
    const action = deleteCategoryRequest(categoryId);
    dispatch(action)
  }, [categoryId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing a category</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteCategoryScreen;
