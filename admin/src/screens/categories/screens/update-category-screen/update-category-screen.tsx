// Modules
import {
  useCallback,
  useRef,
  useState,
  useEffect,
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  IconButton,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import { BaseSyntheticEvent, FC } from 'react';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IUpdateCategoryScreenProps } from '@/screens/categories/screens/update-category-screen/interfaces/IUpdateCategoryScreenProps'

export const UpdateCategoryScreen: FC<IUpdateCategoryScreenProps> = (props): JSX.Element => {
  const { errors: serverValidationErrors, formik, handleClose } = props;

  const {
    dirty,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    setFieldError,
    setFieldTouched,
    setFieldValue,
    touched,
    values,
  } = formik;

  const [imageName, setImageName] = useState<string>('');
  const imageRef = useRef<HTMLInputElement>(null);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('image', files);
    setImageName(files?.name);
  }, [setFieldValue]);

  const handleClickImage = useCallback(() => {
    imageRef?.current?.click();
    setFieldTouched('image', true);
  }, [setFieldTouched]);

  const handleClearImage = useCallback(async () => {
    if (!imageRef.current) return;
    setImageName('');
    imageRef.current.value = '';
    await setFieldValue('image', '');
    setFieldTouched('image', true);
    setFieldError('image', staticValidationErrors.isRequired);
  }, [setFieldError, setFieldTouched, setFieldValue]);

  const isImageFieldFullWidth: string = Boolean(imageName) ? '95%' : '100%';

  const disabledConditions = [
    isSubmitting,
    !dirty,
    !isValid
  ];

  const isSubmitBtnDisabled = disabledConditions.some((item) => item)

  useEffect(() => {
    setImageName(values.image);
  }, [values.image]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={() => ({})}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 240 } }}
    >
      <DialogTitle>Updating a category</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <TextField
            error={touched.title && Boolean(errors.title)}
            fullWidth
            helperText={touched.title && errors.title}
            label="Title"
            name="title"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.title}
            variant="standard"
          />
          <FormGroup row>
            <FormControl sx={{ flexBasis: isImageFieldFullWidth }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={touched.image && Boolean(errors.image)}
                focused={Boolean(imageName)}
                helperText={touched.image && errors.image}
                label="Image"
                margin="dense"
                name="image"
                onClick={handleClickImage}
                required
                type="text"
                value={imageName}
                variant="standard"
              />
            </FormControl>
            {Boolean(imageName) && (
              <FormControl sx={{ textAlign: 'center', justifyContent: 'flex-end', flexBasis: '5%' }}>
                <IconButton onClick={handleClearImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="image"
            onChange={handleChangeImage}
            type="file"
            ref={imageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitBtnDisabled}
            color="success"
            fullWidth
            type="submit"
          >
            Update
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default UpdateCategoryScreen;
