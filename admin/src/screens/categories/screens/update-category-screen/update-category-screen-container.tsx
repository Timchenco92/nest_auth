// Modules
import { useCallback, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema } from '@/screens/categories/screens/update-category-screen/form-config';

// Engine
import {
  categorySelector,
  getCategoryRequest,
  paginationAndQuerySearchSelector,
  updateCategoryRequest,
  updateCategorySuccessSelector,
  updateCategoryErrorSelector,
  resetUpdateCategoryError,
  resetUpdateCategorySuccess,
} from '@/store/categories';

// Interfaces
import type { IInitialValues } from '@/screens/categories/screens/update-category-screen/interfaces/IInitialValues';

// Screens
import { UpdateCategoryScreen } from '@/screens/categories/screens/update-category-screen/update-category-screen';

export const UpdateCategoryScreenContainer: FC = (): JSX.Element => {
  const { categoryId } = useParams<'categoryId'>();

  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();

  const { page, limit } = useSelector(paginationAndQuerySearchSelector);
  const isCategoryUpdate = useSelector(updateCategorySuccessSelector);
  const { errors, hasError } = useSelector(updateCategoryErrorSelector);
  const category = useSelector(categorySelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/categories${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues: {
      title: category.title,
      image: category.image,
    },
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    if (!categoryId) return;

    const data = new FormData();
    if (typeof values.image === 'object') data.append('image', values.image);
    data.append('title', values.title);

    const action = updateCategoryRequest({ categoryId, data });
    dispatch(action);
  }, [categoryId, dispatch]);

  useEffect(() => {
    if (!categoryId) return;

    const action = getCategoryRequest(categoryId, limit, page);
    dispatch(action);
  }, [categoryId, dispatch, limit, page]);

  useEffect(() => {
    if (!isCategoryUpdate) return;
    handleClose();

    return () => {
      dispatch(resetUpdateCategorySuccess());
    };
  }, [dispatch, handleClose, isCategoryUpdate]);

  useEffect(() => {
    return () => {
      dispatch(resetUpdateCategoryError());
    };
  }, [dispatch]);

  return (
    <UpdateCategoryScreen errors={errors} formik={formik} handleClose={handleClose} />
  );
};

export default UpdateCategoryScreenContainer;
