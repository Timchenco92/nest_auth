export interface IUpdateCategoryValidationErrors {
  image: string
  title: string;
}
