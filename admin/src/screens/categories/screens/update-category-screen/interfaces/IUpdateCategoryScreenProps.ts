// Modules
import { FormikProps } from 'formik';

// Interfaces
import type { IUpdateCategoryValidationErrors } from '@/screens/categories/screens/update-category-screen/interfaces/IUpdateCategoryValidationErrors';
import type { IInitialValues } from '@/screens/categories/screens/update-category-screen/interfaces/IInitialValues';

export interface IUpdateCategoryScreenProps {
  errors: IUpdateCategoryValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
}
