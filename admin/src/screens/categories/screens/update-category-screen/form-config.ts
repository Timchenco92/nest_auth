// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  title: string().required(isRequired),
  image: mixed().required(isRequired),
});
