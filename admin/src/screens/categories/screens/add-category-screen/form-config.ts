// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/categories/screens/add-category-screen/interfaces';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  title: string().required(isRequired),
  image: mixed().required(isRequired),
});

export const initialValues: IInitialValues = {
  title: '',
  image: '',
};
