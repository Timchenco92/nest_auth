export interface IAddCategoryValidationErrors {
  image: string
  title: string;
}
