// Modules
import { FormikProps } from 'formik';

// Interfaces
import type { IAddCategoryValidationErrors } from '@/screens/categories/screens/add-category-screen/interfaces/IAddCategoryValidationErrors';
import type { IInitialValues } from '@/screens/categories/screens/add-category-screen/interfaces/IInitialValues';

export interface IAddCategoryScreenProps {
  errors: IAddCategoryValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
}
