export interface IInitialValues {
  title: string;
  image: File | string;
}
