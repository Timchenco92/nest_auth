// Modules
import { FC, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useFormik, FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/categories/screens/add-category-screen/form-config';

// Engine
import {
  addCategoryErrorSelector,
  addCategorySuccessSelector,
  addCategoryRequest,
  resetAddCategorySuccess,
  resetAddCategoryError,
} from '@/store/categories';

// Interfaces
import type { IInitialValues } from '@/screens/categories/screens/add-category-screen/interfaces/IInitialValues';

// Screens
import AddCategoryScreen from '@/screens/categories/screens/add-category-screen/add-category-screen';

export const AddCategoryScreenContainer: FC = (): JSX.Element => {
  const { errors, hasError } = useSelector(addCategoryErrorSelector);
  const isCategoryAdd = useSelector(addCategorySuccessSelector);

  const dispatch = useDispatch();

  const navigate = useNavigate();
  const { search } = useLocation();

  const handleClose = useCallback(() => {
    navigate(`/dashboard/categories${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    const data = new FormData();
    data.append('image', values.image);
    data.append('title', values.title);
    const action = addCategoryRequest(data);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isCategoryAdd) return;
    handleClose();
    return () => {
      const action = resetAddCategorySuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isCategoryAdd]);

  useEffect(() => {
    return () => {
      const action = resetAddCategoryError();
      dispatch(action);
    };
  }, [dispatch]);

  return <AddCategoryScreen
    errors={errors}
    formik={formik}
    handleClose={handleClose}
  />;
};

export default AddCategoryScreenContainer;
