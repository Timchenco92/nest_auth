// Modules Types
import type { SelectChangeEvent } from '@mui/material';
import type { ChangeEvent } from 'react';

// Interfaces
import type { ICategory } from '@/interfaces/models';

export interface ICategoriesScreenProps {
  categories: ICategory[];
  handleChangeLimit(event: SelectChangeEvent): void;
  handleChangePage(event: ChangeEvent<unknown>, page: number): void;
  limit: string;
  page: string;
  totalPages: number;
}
