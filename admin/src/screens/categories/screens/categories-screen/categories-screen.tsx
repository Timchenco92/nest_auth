// Modules
import { useSelector } from 'react-redux';
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Assets
import { CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { CategoryItemComponent } from '@/screens/categories/screens/categories-screen/components/category-item-component';
import { LoaderComponent } from '@/components/loader-component'
import { PaginationComponent } from '@/components/pagination-component';

// Constants
import { COLORS } from '@/constants';

// Engine
import { categoriesPendingSelector } from '@/store/categories';

// Interfaces
import type { ICategoriesScreenProps } from '@/screens/categories/screens/categories-screen/interfaces/ICategoriesScreenProps';

export const CategoriesScreen: FC<ICategoriesScreenProps> = (props): JSX.Element => {
  const {
    categories,
    handleChangeLimit,
    handleChangePage,
    limit,
    page,
    totalPages,
  } = props;
  const pending = useSelector(categoriesPendingSelector);

  const categoriesCount = categories.length;

  if (!categoriesCount) return <CustomNoRowsOverlay />

  if (pending) return <LoaderComponent />;

  return (
    <Grid item xs={12}>
      <Paper elevation={6}>
        <TableContainer>
          <Table>
            <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
              <TableRow>
                <TableCell align="center">#</TableCell>
                <TableCell />
                <TableCell align="center">Title</TableCell>
                <TableCell align="center">Created</TableCell>
                <TableCell align="center">Updated</TableCell>
                <TableCell align="center"/>
              </TableRow>
            </TableHead>
            <TableBody>
              {categories.map((category, key) => (
                <CategoryItemComponent category={category} rowIdx={key} key={category.id} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <PaginationComponent
          currentPage={page}
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          totalPages={totalPages}
        />
      </Paper>
    </Grid>
  );
};

export default CategoriesScreen;
