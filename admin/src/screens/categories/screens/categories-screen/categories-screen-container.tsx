// Modules
import { useCallback, useEffect } from 'react';
import { Link, Outlet, useLocation, useSearchParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Button, FormControl, Grid, IconButton, TextField } from '@mui/material';
import { Clear, Search } from '@mui/icons-material';

// Modules Types
import type { ChangeEvent, FC } from 'react';
import type { Dispatch } from 'redux';
import type { SelectChangeEvent } from '@mui/material';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Engine
import {
  categoriesErrorSelector,
  categoriesSelector,
  changeLimit,
  changePage,
  getCategoriesRequest,
  paginationAndQuerySearchSelector,
  resetPagination,
  resetQuerySearch,
  setQuerySearch,
} from '@/store/categories';

// Screens
import { CategoriesScreen } from '@/screens/categories/screens/categories-screen/categories-screen';

const DEFAULT_PAGINATION_PAGE = '1';
const DEFAULT_LIMIT = '15';
const DEFAULT_QUERY_SEARCH = '';

export const CategoriesScreenContainer: FC = (): JSX.Element => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();

  const { data: categories, meta: { totalPages } } = useSelector(categoriesSelector);
  const { limit, page, query } = useSelector(paginationAndQuerySearchSelector);
  const error = useSelector(categoriesErrorSelector);

  const categoriesCount = categories.length;

  const addPath = search.length ? `add${search}` : 'add';

  const handleChangeLimit = useCallback((event: SelectChangeEvent) => {
    const { target: { value } } = event;

    const action = changeLimit(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(value) !== DEFAULT_LIMIT) {
        searchParams.set('limit', String(value));
      } else {
        searchParams.delete('limit');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleChangePage = useCallback((event: ChangeEvent<unknown>, page: number) => {
    const action = changePage(String(page));
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(page) !== DEFAULT_PAGINATION_PAGE) {
        searchParams.set('page', String(page));
      } else {
        searchParams.delete('page');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleSearch = useCallback(({ target }: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const { value } = target;
    const action = setQuerySearch(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (value !== DEFAULT_QUERY_SEARCH) {
        searchParams.set('search', value);
      } else {
        searchParams.delete('search');
      }

      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleClearSearch = useCallback(() => {
    const action = setQuerySearch('');
    dispatch(action);

    setSearchParams((searchParams) => {
      searchParams.delete('search');
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  useEffect(() => {
    if (!searchParams.has('page')) return;
    const qsPage = searchParams.get('page') as string;

    const action = changePage(qsPage);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('limit')) return;
    const qsLimit = searchParams.get('limit') as string;

    const action = changeLimit(qsLimit);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('search')) return;
    const qsLimit = searchParams.get('search') as string;

    const action = setQuerySearch(qsLimit);
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    const action = getCategoriesRequest(limit, page, query);
    dispatch(action);
  }, [dispatch, limit, page, query]);

  useEffect(() => {
    return () => {
      dispatch(resetPagination());
      dispatch(resetQuerySearch());
    };
  }, [dispatch]);

  if (error) return <CustomErrorOverlay />;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={6} marginY="10px" paddingY="15px">
          {!!categoriesCount && (
            <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
              <FormControl fullWidth>
                <TextField
                  label="Search"
                  onChange={handleSearch}
                  placeholder="Put a title"
                  type="text"
                  value={query}
                  variant="standard"
                />
              </FormControl>
              {!query ? (
                <Search sx={{ color: 'action.active', mr: 1, my: 0.5 }} />
              ) : (
                <IconButton onClick={handleClearSearch}>
                  <Clear />
                </IconButton>
              )}
            </Box>
          )}
        </Grid>
        <Grid item xs={6}>
          <Grid
            alignItems="center"
            container
            direction="row"
            height="100%"
            justifyContent="flex-end"
          >
            <Button
              color="success"
              component={Link}
              size="large"
              to={addPath}
              type="button"
              variant="text"
            >
              Create
            </Button>
          </Grid>
        </Grid>
        <CategoriesScreen
          categories={categories}
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          page={page}
          totalPages={totalPages}
        />
      </Grid>
      <Outlet />
    </Box>
  );
};

export default CategoriesScreenContainer;
