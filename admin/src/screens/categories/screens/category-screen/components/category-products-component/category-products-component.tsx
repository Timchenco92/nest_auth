// Modules
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Assets
import { CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { CategoryProductItemComponent } from '@/screens/categories/screens/category-screen/components/category-products-component/components/category-product-item-component';
import { PaginationComponent } from '@/components/pagination-component';

// Constants
import { COLORS } from '@/constants';

// Interfaces
import type { ICategoryProductsComponentProps } from '@/screens/categories/screens/category-screen/components/category-products-component/interfaces/ICategoryProductsComponentProps';

export const CategoryProductsComponent: FC<ICategoryProductsComponentProps> = (props): JSX.Element => {
  const {
    data,
    handleChangeLimit,
    handleChangePage,
    limit,
    page,
    productsCount,
    totalPages,
  } = props;

  if (!productsCount) return <CustomNoRowsOverlay />;

  return (
    <Paper elevation={6}>
      <TableContainer>
        <Table>
          <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
            <TableRow>
              <TableCell align="center">#</TableCell>
              <TableCell />
              <TableCell align="center">Title</TableCell>
              <TableCell align="center">Created</TableCell>
              <TableCell align="center">Updated</TableCell>
              <TableCell align="center" />
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((product, key) => (
              <CategoryProductItemComponent product={product} rowIdx={key} key={product.id!} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <PaginationComponent
        currentPage={page}
        handleChangeLimit={handleChangeLimit}
        handleChangePage={handleChangePage}
        limit={limit}
        totalPages={totalPages}
      />
    </Paper>
  );
};

export default CategoryProductsComponent;
