// Modules
import { Avatar, TableCell, TableRow } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IProduct } from '@/interfaces/models';

interface IProps {
  product: IProduct;
  rowIdx: number;
}

export const CategoryProductItemComponent: FC<IProps> = (props): JSX.Element => {
  const { product, rowIdx } = props;
  const { createdAt, imageUrl, title, updatedAt } = product;

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  return (
    <TableRow>
      <TableCell align="center">{rowIdx + 1}</TableCell>
      <TableCell align="center">
        <Avatar src={imageUrl} sx={{ marginX: 'auto' }} />
      </TableCell>
      <TableCell align="center">{title}</TableCell>
      <TableCell align="center">{formattedCreatedAt}</TableCell>
      <TableCell align="center">{formattedUpdatedAt}</TableCell>
    </TableRow>
  );
};
