// Modules Types
import type { SelectChangeEvent } from '@mui/material';
import type { ChangeEvent } from 'react';

// Interfaces
import type { IProduct } from '@/interfaces/models';

export interface ICategoryProductsComponentProps {
  data: IProduct[];
  handleChangeLimit(event: SelectChangeEvent): void;
  handleChangePage(event: ChangeEvent<unknown>, page: number): void;
  limit: string;
  page: string;
  productsCount: number;
  totalPages: number;
}
