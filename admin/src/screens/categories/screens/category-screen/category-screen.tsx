// Modules
import {
  Avatar,
  Box, Divider,
  Grid,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Paper, Typography,
} from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Components
import { CategoryProductsComponent } from '@/screens/categories/screens/category-screen/components/category-products-component';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { ICategoryScreenProps } from '@/screens/categories/screens/category-screen/interfaces/ICategoryScreenProps';

export const CategoryScreen: FC<ICategoryScreenProps> = (props): JSX.Element => {
  const { category, handleChangeLimit, handleChangePage, limit, page } = props;
  const { createdAt, imageUrl, title, updatedAt, products, productsCount } = category;
  const { data, meta: { totalPages } } = products;

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={8} justifyContent="center">
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            Category Details
          </Typography>
          <Paper elevation={3}>
            <Box sx={{ padding: '20px' }}>
              <Avatar src={imageUrl} sx={{ height: 130, marginX: 'auto', width: 130 }} />
            </Box>
            <Divider />
            <List>
              <ListItem>
                <ListItemText primary={`Title`} />
                <ListItemSecondaryAction>{title}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Count of Products`} />
                <ListItemSecondaryAction>{productsCount}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            Products
          </Typography>
          <CategoryProductsComponent
            data={data}
            handleChangeLimit={handleChangeLimit}
            handleChangePage={handleChangePage}
            limit={limit}
            page={page}
            productsCount={productsCount}
            totalPages={totalPages}
          />
        </Grid>
      </Grid>
    </Box>
  );
};

export default CategoryScreen;
