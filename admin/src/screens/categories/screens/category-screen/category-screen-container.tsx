// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useSearchParams } from 'react-router-dom';

// Modules Types
import type { SelectChangeEvent } from '@mui/material';
import type { ChangeEvent, FC } from 'react';

// Modules Types
import type { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component';

// Engine
import {
  categorySelector,
  categoryErrorSelector,
  categoryPendingSelector,
  changeLimit,
  changePage,
  getCategoryRequest,
  paginationAndQuerySearchSelector,
} from '@/store/categories';

// Screens
import { CategoryScreen } from '@/screens/categories/screens/category-screen/category-screen';

const DEFAULT_PAGINATION_PAGE = '1';
const DEFAULT_LIMIT = '15';

export const CategoryScreenContainer: FC = (): JSX.Element => {
  const { categoryId } = useParams<'categoryId'>();
  const [, setSearchParams] = useSearchParams();

  const { limit, page } = useSelector(paginationAndQuerySearchSelector);
  const pending = useSelector(categoryPendingSelector);
  const error = useSelector(categoryErrorSelector);
  const category = useSelector(categorySelector);

  const dispatch: Dispatch = useDispatch();

  const handleChangeLimit = useCallback((event: SelectChangeEvent) => {
    const { target: { value } } = event;

    const action = changeLimit(value);
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(value) !== DEFAULT_LIMIT) {
        searchParams.set('limit', String(value));
      } else {
        searchParams.delete('limit');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  const handleChangePage = useCallback((event: ChangeEvent<unknown>, page: number) => {
    const action = changePage(String(page));
    dispatch(action);

    setSearchParams((searchParams) => {
      if (String(page) !== DEFAULT_PAGINATION_PAGE) {
        searchParams.set('page', String(page));
      } else {
        searchParams.delete('page');
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  useEffect(() => {
    if (!categoryId) return;

    const action = getCategoryRequest(categoryId, limit, page);
    dispatch(action);
  }, [categoryId, dispatch, limit, page]);

  if (error) return <CustomErrorOverlay />;

  if (pending) return <LoaderComponent />;

  return (
    <CategoryScreen
      category={category}
      handleChangeLimit={handleChangeLimit}
      handleChangePage={handleChangePage}
      limit={limit}
      page={page}
    />
  );
};

export default CategoryScreenContainer;
