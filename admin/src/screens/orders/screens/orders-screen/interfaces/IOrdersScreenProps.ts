// Modules
import type { SelectChangeEvent } from '@mui/material';
import type { ChangeEvent } from 'react';

// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IOrdersScreenProps {
  orders: IOrder[];
  handleChangeLimit(event: SelectChangeEvent): void;
  handleChangePage(event: ChangeEvent<unknown>, page: number): void;
  limit: string;
  page: string;
  pending: boolean;
  totalPages: number;
}
