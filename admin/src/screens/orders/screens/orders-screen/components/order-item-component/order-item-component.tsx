// Modules
import { Link, useLocation } from 'react-router-dom';
import { Delete, Edit, Info } from '@mui/icons-material';
import { IconButton, TableCell, TableRow, Tooltip } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Components
import { OrderStatusComponent } from '@/screens/orders/screens/orders-screen/components/order-status-component';

// Constants
import { TOOLTIPS_MESSAGES } from '@/constants';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IOrder } from '@/interfaces/models';

interface IProps {
  order: IOrder;
  rowIdx: number;
}

export const OrderItemComponent: FC<IProps> = (props): JSX.Element => {
  const { order, rowIdx } = props;
  const {
    createdAt,
    id,
    orderNumber,
    status,
    total,
    updatedAt,
  } = order;

  const { search } = useLocation();

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  const deletePath = search.length ? `${id}/delete${search}` : `${id}/delete`;
  const updatePath = search.length ? `${id}/delete${search}` : `${id}/update`;

  const { isDeleted, isDetails, isUpdate } = TOOLTIPS_MESSAGES;

  return (
    <TableRow>
      <TableCell align="center">{rowIdx + 1}</TableCell>
      <TableCell align="center">{orderNumber}</TableCell>
      <TableCell align="center">
        <OrderStatusComponent status={status} />
      </TableCell>
      <TableCell align="center">{total}</TableCell>
      <TableCell align="center">{formattedCreatedAt}</TableCell>
      <TableCell align="center">{formattedUpdatedAt}</TableCell>
      <TableCell align="center">
        <Tooltip placement="top" title={isDetails('Order')}>
          <IconButton component={Link} to={`${id}/details`} color="info" size="small">
            <Info />
          </IconButton>
        </Tooltip>
        <Tooltip placement="top" title={isUpdate('order')}>
          <IconButton component={Link} to={updatePath} color="warning" size="small">
            <Edit />
          </IconButton>
        </Tooltip>
        <Tooltip placement="top" title={isDeleted('order')}>
          <IconButton component={Link} to={deletePath} color="error" size="small">
            <Delete />
          </IconButton>
        </Tooltip>
      </TableCell>
    </TableRow>
  );
};
