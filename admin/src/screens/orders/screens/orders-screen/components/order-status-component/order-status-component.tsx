// Modules
import { Chip } from '@mui/material';

// Modules Types
import { FC } from 'react';

// Enums
import { EOrderStatus } from '@/interfaces/models';

// Interfaces
import { IOrder } from '@/interfaces/models';


interface IProps {
  status: IOrder['status'];
}

export const OrderStatusComponent: FC<IProps> = (props): JSX.Element => {
  const { status } = props;
  if (EOrderStatus.accepted === status) {
    return (
      <Chip color="info" label={EOrderStatus[status]} sx={{ color: 'white' }} />
    );
  } else if (EOrderStatus.in_progress === status) {
    return (
      <Chip color="warning" label={EOrderStatus[status]} sx={{ color: 'white' }} />
    );
  } else {
    return (
      <Chip color="success" label={EOrderStatus[status]} sx={{ color: 'white' }} />
    );
  }
};
