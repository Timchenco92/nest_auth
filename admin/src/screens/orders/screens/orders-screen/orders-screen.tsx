// Modules
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Assets
import { CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component'
import { OrderItemComponent } from '@/screens/orders/screens/orders-screen/components/order-item-component';
import { PaginationComponent } from '@/components/pagination-component';

// Constants
import { COLORS } from '@/constants';

// Interfaces
import type { IOrdersScreenProps } from '@/screens/orders/screens/orders-screen/interfaces';

export const OrdersScreen: FC<IOrdersScreenProps> = (props): JSX.Element => {
  const {
    handleChangeLimit,
    handleChangePage,
    limit,
    orders,
    page,
    pending,
    totalPages,
  } = props;

  const ordersCount = orders.length;

  if (!ordersCount) return <CustomNoRowsOverlay />

  if (pending) return <LoaderComponent />;

  return (
    <Grid item xs={12}>
      <Paper elevation={6}>
        <TableContainer>
          <Table>
            <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
              <TableRow>
                <TableCell align="center">#</TableCell>
                <TableCell align="center">Order Number</TableCell>
                <TableCell align="center">Status</TableCell>
                <TableCell align="center">Total ($)</TableCell>
                <TableCell align="center">Created</TableCell>
                <TableCell align="center">Updated</TableCell>
                <TableCell align="center"/>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders.map((order, key) => (
                <OrderItemComponent order={order} rowIdx={key} key={order.id} />
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <PaginationComponent
          currentPage={page}
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          totalPages={totalPages}
        />
      </Paper>
    </Grid>
  );
};
