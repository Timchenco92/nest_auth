// Modules
import { object, string } from 'yup';

export const validationSchema = object({
  delivery: string().max(255).notRequired(),
  comment: string().max(255).notRequired(),
});
