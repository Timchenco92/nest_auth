// Modules Types
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, IUpdateOrderValidationErrors } from '@/screens/orders/screens/update-order-screen/interfaces';

export interface IUpdateOrderScreenProps {
  errors: IUpdateOrderValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  updateOrderPending: boolean;
}
