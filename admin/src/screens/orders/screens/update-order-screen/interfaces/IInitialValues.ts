export interface IInitialValues {
  comment: string | null;
  delivery: string | null;
}
