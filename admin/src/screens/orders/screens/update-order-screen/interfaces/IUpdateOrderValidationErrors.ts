export interface IUpdateOrderValidationErrors {
  delivery: string;
  comment: string;
}
