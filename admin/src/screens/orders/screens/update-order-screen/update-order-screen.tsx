// Modules
import { useEffect } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';

// Modules Types
import { FC } from 'react';

// Interfaces
import type { IUpdateOrderScreenProps } from '@/screens/orders/screens/update-order-screen/interfaces';

export const UpdateOrderScreen: FC<IUpdateOrderScreenProps> = (props): JSX.Element => {
  const {
    errors: serverValidationErrors,
    formik,
    handleClose,
    updateOrderPending,
  } = props;

  const {
    dirty,
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    touched,
    values,
  } = formik;

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  const allowedDisabledConditions = [
    dirty,
    isSubmitting,
    !isValid,
    updateOrderPending,
  ];

  console.log(dirty);

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="lg"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 100 } }}
    >
      <DialogTitle>Updating order</DialogTitle>
      <DialogContent>
        <form onSubmit={handleSubmit}>
          <TextField
            error={touched.delivery && Boolean(errors.delivery)}
            fullWidth
            helperText={touched.delivery && errors.delivery}
            label="Delivery"
            name="delivery"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.delivery}
            variant="standard"
          />
          <TextField
            error={touched.comment && Boolean(errors.comment)}
            fullWidth
            helperText={touched.comment && errors.comment}
            label="Comment to order"
            name="comment"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: 7 }}
            type="text"
            value={values.comment}
            variant="standard"
          />
          <DialogActions>
            <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
            <Button
              disabled={isSubmitDisabled}
              color="success"
              fullWidth
              type="submit"
            >
              Create
            </Button>
          </DialogActions>
        </form>
      </DialogContent>
    </Dialog>
  );
};
