// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation, useParams } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema } from '@/screens/orders/screens/update-order-screen/form-config';

// Engine
import {
  orderSelector,
  resetUpdateOrderError,
  resetUpdateOrderSuccess,
  updateOrderErrorSelector,
  updateOrderPendingSelector,
  updateOrderRequest,
  updateOrderSuccessSelector,
} from '@/store/orders';

// Interfaces
import type { IInitialValues } from '@/screens/orders/screens/update-order-screen/interfaces';

// Screens
import { UpdateOrderScreen } from '@/screens/orders/screens/update-order-screen/update-order-screen';

export const UpdateOrderScreenContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();
  const { orderId } = useParams<'orderId'>();

  const updateOrderPending = useSelector(updateOrderPendingSelector);
  const isUpdated = useSelector(updateOrderSuccessSelector);
  const { errors, hasError } = useSelector(updateOrderErrorSelector);
  const { comment, delivery } = useSelector(orderSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/orders${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues: {
      comment,
      delivery,
    },
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    if (!orderId) return;

    const action = updateOrderRequest({ orderId, ...values });
    dispatch(action);
  }, [dispatch, orderId]);

  useEffect(() => {
    if (!isUpdated) return;
    handleClose();

    return () => {
      const action = resetUpdateOrderSuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isUpdated]);

  useEffect(() => () => {
    const action = resetUpdateOrderError();
    dispatch(action);
  }, [dispatch]);

  return (
    <UpdateOrderScreen
      errors={errors}
      formik={formik}
      handleClose={handleClose}
      updateOrderPending={updateOrderPending}
    />
  );
};
