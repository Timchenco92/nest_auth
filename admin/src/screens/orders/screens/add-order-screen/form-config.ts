// Modules
import { object, string, array } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/orders/screens/add-order-screen/interfaces';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  userId: string().required(isRequired),
  total: string().required(isRequired),
  delivery: string().notRequired(),
  comment: string().notRequired(),
  orderProducts: array().required(isRequired),
});

export const initialValues: IInitialValues = {
  userId: '',
  total: '',
  delivery: '',
  comment: '',
  orderProducts: [],
};
