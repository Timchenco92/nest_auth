// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/orders/screens/add-order-screen/form-config';

// Engine
import { usersSelector, usersPendingSelector } from '@/store/users';
import {
  addOrderErrorSelector,
  addOrderPendingSelector,
  addOrderRequest,
  addOrderSuccessSelector,
  resetAddOrderError,
  resetAddOrderSuccess,
} from '@/store/orders';
import { productsSelector, productsPendingSelector } from '@/store/products';

// Interfaces
import type { IInitialValues } from '@/screens/orders/screens/add-order-screen/interfaces';

// Screens
import { AddOrderScreen } from '@/screens/orders/screens/add-order-screen/add-order-screen';

export const AddOrderScreenContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();

  const { data: users } = useSelector(usersSelector);
  const usersPending = useSelector(usersPendingSelector);
  const { data: products } = useSelector(productsSelector);
  const productsPending = useSelector(productsPendingSelector);
  const { errors, hasError } = useSelector(addOrderErrorSelector);
  const orderPending = useSelector(addOrderPendingSelector);
  const isAdded = useSelector(addOrderSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/orders${search}`, {
      replace: true,
    });
  }, [navigate, search]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    let total = 0;
    values.orderProducts.forEach((product) => {
      total += Number(product.total);
    });
    values.total = String(total);

    const action = addOrderRequest(values);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isAdded) return;
    handleClose();

    return () => {
      const action = resetAddOrderSuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isAdded]);

  useEffect(() => () => {
    const action = resetAddOrderError();
    dispatch(action);
  }, [dispatch]);

  return (
    <AddOrderScreen
      errors={errors}
      formik={formik}
      handleClose={handleClose}
      orderPending={orderPending}
      products={products}
      productsPending={productsPending}
      users={users}
      usersPending={usersPending}
    />
  );
};
