// Interfaces
import type { IAddedProduct } from '@/screens/orders/screens/add-order-screen/interfaces';

export interface IInitialValues {
  comment: string;
  delivery: string;
  orderProducts: IAddedProduct[];
  total: string;
  userId: string;
}
