export interface IAddOrderValidationErrors {
  userId: string;
  total: string;
  delivery: string;
  comment: string;
  orderProducts: string;
}
