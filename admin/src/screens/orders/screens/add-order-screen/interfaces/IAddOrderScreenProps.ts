// Modules Types
import type { FormikProps } from 'formik';

// Interfaces
import type { IProduct, IUser } from '@/interfaces/models';
import type { IInitialValues, IAddOrderValidationErrors } from '@/screens/orders/screens/add-order-screen/interfaces';

export interface IAddOrderScreenProps {
  errors: IAddOrderValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  products: IProduct[];
  productsPending: boolean;
  users: IUser[];
  usersPending: boolean;
  orderPending: boolean;
}
