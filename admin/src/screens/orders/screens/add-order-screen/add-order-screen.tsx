// Modules
import {
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';

// Modules Types
import type { ChangeEvent, FC } from 'react';
import type { Dispatch } from 'redux';
import type { TextFieldProps } from '@mui/material';

// Components
import { AddedProductsComponent } from '@/screens/orders/screens/add-order-screen/components/added-products-component';
import { ProductsAutocompleteComponent } from '@/screens/orders/screens/add-order-screen/components/products-autocomplete-component';
import { UsersAutocompleteComponent } from '@/screens/orders/screens/add-order-screen/components/users-autocomplete-component';

// Engine
import {
  getUsersRequest,
  paginationAndQuerySearchSelector as usersPaginationAndQuerySearchSelector,
  resetQuerySearch as resetUsersQuerySearch,
} from '@/store/users';

import {
  getProductsRequest,
  paginationAndQuerySearchSelector as productsPaginationAndQuerySearchSelector,
  resetQuerySearch as resetProductsQuerySearch,
} from '@/store/products';

import { showToast } from '@/store/toast';

// Interfaces
import type { IAddOrderScreenProps, IAddedProduct } from '@/screens/orders/screens/add-order-screen/interfaces';
import type { IUser } from '@/interfaces/models';
import { staticValidationErrors } from '@/constants';

export const AddOrderScreen: FC<IAddOrderScreenProps> = (props): JSX.Element => {
  const {
    errors: serverValidationErrors,
    formik,
    handleClose,
    orderPending,
    products,
    productsPending,
    users,
    usersPending,
  } = props;

  const dispatch: Dispatch = useDispatch();

  const [isUserVisible, setUserIsVisible] = useState<boolean>(false);
  const [isProductVisible, setIsProductVisible] = useState<boolean>(false);
  const [user, setUser] = useState<IUser | null>(null);
  const [addedProducts, setAddedProducts] = useState<IAddedProduct[]>([]);
  const [orderTotal, setOrderTotal] = useState<number>(0);

  const userAutocompleteRef = useRef<TextFieldProps>(null);
  const productAutocompleteRef = useRef<TextFieldProps>(null);

  const { limit: usersLimit, page: usersPage } = useSelector(usersPaginationAndQuerySearchSelector);
  const { limit: productsLimit, page: productsPage } = useSelector(productsPaginationAndQuerySearchSelector);

  const {
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    setFieldError,
    setFieldTouched,
    setFieldValue,
    touched,
    values,
  } = formik;

  const handleVisibleUsersItems = () => setUserIsVisible(false);

  const handleSelectUser = useCallback((user: IUser) => {
    setFieldValue('userId', user.id)
      .then(() => {
        setUser(user);
        setUserIsVisible(false);
        dispatch(resetUsersQuerySearch());
      });
  }, [dispatch, setFieldValue]);

  const handleChangeUserAutocomplete = useCallback((event: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target;
    setFieldTouched('userId', true);
    setUserIsVisible(true);
    const action = getUsersRequest(usersLimit, usersPage, value);
    dispatch(action);
  }, [dispatch, setFieldTouched, usersLimit, usersPage]);

  const handleClearUserAutocomplete = useCallback(() => {
    setFieldValue('userId', '')
      .then(() => {
        setUser(null);
        if (userAutocompleteRef.current) userAutocompleteRef.current.value = '';
        setFieldError('userId', staticValidationErrors.isRequired);
      });
  }, [setFieldError, setFieldValue]);

  const handleVisibleProductsItems = () => setIsProductVisible(false);

  const handleSelectProduct = useCallback(async ({ price = '', productId = '', title = '' }) => {
    const isAdded = addedProducts.find((product) => product.productId === productId);
    if (isAdded) return;

    addedProducts.push({
      price,
      productId,
      quantity: 1,
      title,
      total: price,
    });

    setOrderTotal((prevState) => {
      prevState += Number(price);
      return prevState;
    });

    await setFieldValue('orderProducts', addedProducts);
    await setFieldValue('total', orderTotal);
    setIsProductVisible(false);
    if (productAutocompleteRef.current) productAutocompleteRef.current.value = '';
    dispatch(showToast({ isShowing: true, message: 'The product successfully added.', type: 'success' }));
    dispatch(resetProductsQuerySearch());
  }, [addedProducts, dispatch, orderTotal, setFieldValue]);

  const handleChangeProductAutocomplete = useCallback((event: ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target;
    setFieldTouched('products', true);
    setIsProductVisible(true);
    const action = getProductsRequest(productsLimit, productsPage, value);
    dispatch(action);
  }, [dispatch, productsLimit, productsPage, setFieldTouched]);

  const handleRemoveProductFromOrder = useCallback((product: IAddedProduct) => {
    const idx = addedProducts.indexOf(product);

    if (idx > -1) {
      addedProducts.splice(idx, 1);
      dispatch(showToast({
        isShowing: true,
        message: 'The product successfully removed from order.',
        type: 'success',
      }));
    }
  }, [addedProducts, dispatch]);

  const handleIncrementAddedProductCount = useCallback(async (productId: IAddedProduct['productId']) => {
    if (!addedProducts.length) return;

    setAddedProducts((prevState) => {
      return prevState.map((product) => {
        if (product.productId === productId) {
          product.quantity++;
          const total = product.quantity * Number(product.price);
          product.total = String(total);
          setOrderTotal((prevState) => {
            prevState += Number(product.price);
            return prevState;
          });
        }
        return product;
      });
    });

    await setFieldValue('total', orderTotal);
  }, [addedProducts.length, orderTotal, setFieldValue]);

  const handleDecrementAddedProductCount = useCallback(async (productId: IAddedProduct['productId']) => {
    if (!addedProducts.length) return;

    setAddedProducts((prevState) => {
      return prevState.map((product) => {
        if (product.productId === productId && product.quantity >= 2) {
          product.quantity--;
          const total = product.quantity * Number(product.price);
          product.total = String(total);
          setOrderTotal((prevState) => {
            prevState -= Number(product.price);
            return prevState;
          });
        }
        return product;
      });
    });

    await setFieldValue('total', orderTotal);
  }, [addedProducts.length, orderTotal, setFieldValue]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  const allowedDisabledConditions = [
    isValid,
    isSubmitting,
    orderPending,
  ];

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="lg"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 100 } }}
    >
      <DialogTitle>Creating a new order</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <UsersAutocompleteComponent
            errors={errors}
            handleBlur={handleBlur}
            handleChangeUserAutocomplete={handleChangeUserAutocomplete}
            handleClearUserAutocomplete={handleClearUserAutocomplete}
            handleSelectUser={handleSelectUser}
            handleVisibleUsersItems={handleVisibleUsersItems}
            isUserVisible={isUserVisible}
            touched={touched}
            user={user}
            userAutocompleteRef={userAutocompleteRef}
            users={users}
            usersPending={usersPending}
          />
          <ProductsAutocompleteComponent
            errors={errors}
            handleBlur={handleBlur}
            handleChangeProductAutocomplete={handleChangeProductAutocomplete}
            handleSelectProduct={handleSelectProduct}
            handleVisibleProductsItems={handleVisibleProductsItems}
            isProductVisible={isProductVisible}
            productAutocompleteRef={productAutocompleteRef}
            products={products}
            productsPending={productsPending}
            touched={touched}
          />
          <TextField
            error={touched.delivery && Boolean(errors.delivery)}
            fullWidth
            helperText={touched.delivery && errors.delivery}
            label="Delivery"
            name="delivery"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: '10px' }}
            type="text"
            value={values.delivery}
            variant="standard"
          />
          <TextField
            error={touched.comment && Boolean(errors.comment)}
            fullWidth
            helperText={touched.comment && errors.comment}
            label="Comment to order"
            name="comment"
            onBlur={handleBlur}
            onChange={handleChange}
            sx={{ marginBottom: 7 }}
            type="text"
            value={values.comment}
            variant="standard"
          />
          <AddedProductsComponent
            handleDecrementAddedProductCount={handleDecrementAddedProductCount}
            handleIncrementAddedProductCount={handleIncrementAddedProductCount}
            handleRemoveProductFromOrder={handleRemoveProductFromOrder}
            products={addedProducts}
            total={orderTotal}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={isSubmitDisabled}
            color="success"
            fullWidth
            type="submit"
          >
            Create
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
