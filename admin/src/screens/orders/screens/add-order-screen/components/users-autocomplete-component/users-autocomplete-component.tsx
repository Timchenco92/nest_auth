// Modules
import {
  CircularProgress,
  ClickAwayListener,
  FormControl,
  InputAdornment,
  MenuItem,
  Paper,
  TextField,
  IconButton,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import type { FC } from 'react';

// Interfaces
import type { IUsersAutocompleteComponentProps } from '@/screens/orders/screens/add-order-screen/components/users-autocomplete-component/interfaces';

export const UsersAutocompleteComponent: FC<IUsersAutocompleteComponentProps> = (props): JSX.Element => {
  const {
    errors,
    handleBlur,
    handleChangeUserAutocomplete,
    handleClearUserAutocomplete,
    handleSelectUser,
    handleVisibleUsersItems,
    isUserVisible,
    touched,
    user,
    userAutocompleteRef,
    users,
    usersPending,
  } = props;

  const inputValue = user && `${user.fullName} || ${ user.phone}`;

  return (
    <ClickAwayListener onClickAway={handleVisibleUsersItems}>
      <FormControl fullWidth sx={{ marginBottom: '10px' }} variant="standard">
        <TextField
          InputProps={{
            endAdornment:
              <InputAdornment position={'end'}>
                {usersPending && <CircularProgress color="primary" size={30} />}
                {user && <IconButton onClick={handleClearUserAutocomplete}><Clear /></IconButton>}
              </InputAdornment>
            }
          }
          error={touched.userId && Boolean(errors.userId)}
          helperText={touched.userId && errors.userId}
          inputRef={userAutocompleteRef}
          label="User"
          name="userId"
          onBlur={handleBlur}
          onChange={handleChangeUserAutocomplete}
          onFocus={handleVisibleUsersItems}
          placeholder="Start Enter Phone Number"
          // required
          type="text"
          value={inputValue}
          variant="standard"
        />
        {isUserVisible && (
          <Paper elevation={4}>
            {usersPending && (
              <MenuItem value="">Searching...</MenuItem>
            )}
            {users.length > 0 && !usersPending && users.map((user) => (
              <MenuItem
                onClick={() => handleSelectUser(user)}
                value={user.id}
              >
                {user.fullName} | {user.phone}
              </MenuItem>
            ))}
          </Paper>
        )}
      </FormControl>
    </ClickAwayListener>
  );
};
