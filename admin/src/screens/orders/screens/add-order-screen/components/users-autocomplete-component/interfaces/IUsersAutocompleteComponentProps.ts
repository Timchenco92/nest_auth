// Modules Types
import type { FormikErrors, FormikTouched } from 'formik';
import type { ChangeEvent, FocusEvent, Ref } from 'react';
import type { TextFieldProps } from '@mui/material';

// Interfaces
import type { IUser } from '@/interfaces/models';
import type { IInitialValues } from '@/screens/orders/screens/add-order-screen/interfaces';

export interface IUsersAutocompleteComponentProps {
  errors: FormikErrors<IInitialValues>;
  handleBlur(event: FocusEvent<any, any>): void;
  handleChangeUserAutocomplete(event: ChangeEvent<HTMLTextAreaElement>): void;
  handleClearUserAutocomplete(): void;
  handleSelectUser(user: IUser): void;
  handleVisibleUsersItems(): void;
  isUserVisible: boolean;
  touched: FormikTouched<IInitialValues>;
  user: IUser | null;
  userAutocompleteRef: Ref<TextFieldProps>;
  users: IUser[];
  usersPending: boolean;
}
