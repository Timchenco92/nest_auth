// Modules
import { useCallback } from 'react';
import {
  CircularProgress,
  ClickAwayListener,
  FormControl,
  InputAdornment,
  MenuItem,
  Paper,
  TextField,
} from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Interfaces
import type { IProductsAutocompleteComponentProps } from '@/screens/orders/screens/add-order-screen/components/products-autocomplete-component/interfaces';
import type { IProduct } from '@/interfaces/models';

export const ProductsAutocompleteComponent: FC<IProductsAutocompleteComponentProps> = (props): JSX.Element => {
  const {
    errors,
    handleBlur,
    handleChangeProductAutocomplete,
    handleSelectProduct,
    handleVisibleProductsItems,
    isProductVisible,
    productAutocompleteRef,
    products,
    productsPending,
    touched,
  } = props;

  const handleClickProductItem = useCallback((product: IProduct) => {
    const { id, price, title } = product;

    handleSelectProduct({
      price,
      productId: id,
      title,
    });
  }, [handleSelectProduct]);

  return (
    <ClickAwayListener onClickAway={handleVisibleProductsItems}>
      <FormControl fullWidth sx={{ marginBottom: '10px' }} variant="standard">
        <TextField
          InputProps={{
            endAdornment:
              <InputAdornment position={'end'}>
                {productsPending && <CircularProgress color="primary" size={30} />}
              </InputAdornment>,
          }}
          error={touched.orderProducts && Boolean(errors.orderProducts)}
          helperText={touched.orderProducts && errors.orderProducts?.toString()}
          inputRef={productAutocompleteRef}
          label="Products"
          name="products"
          onBlur={handleBlur}
          onChange={handleChangeProductAutocomplete}
          onFocus={handleVisibleProductsItems}
          placeholder="Start Enter Product Title"
          type="text"
          variant="standard"
        />
        {isProductVisible && (
          <Paper elevation={4}>
            {productsPending && (
              <MenuItem value="">Searching...</MenuItem>
            )}
            {products.length > 0 && !productsPending && products.map((product) => (
              <MenuItem
                onClick={() => handleClickProductItem(product)}
                value={product.id}
              >
                {product.title} | {product.price} $
              </MenuItem>
            ))}
          </Paper>
        )}
      </FormControl>
    </ClickAwayListener>
  );
};
