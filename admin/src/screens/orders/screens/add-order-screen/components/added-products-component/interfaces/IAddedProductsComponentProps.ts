// Interfaces
import type { IAddedProduct } from '@/screens/orders/screens/add-order-screen/interfaces';

export interface IAddedProductsComponentProps {
  handleDecrementAddedProductCount(productId: IAddedProduct['productId']): void;
  handleIncrementAddedProductCount(productId: IAddedProduct['productId']): void;
  handleRemoveProductFromOrder(product: IAddedProduct): void;
  products: IAddedProduct[];
  total: number;
}
