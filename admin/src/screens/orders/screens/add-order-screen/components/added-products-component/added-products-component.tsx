// Modules
import { Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Components
import { AddedProductsItemComponent } from '@/screens/orders/screens/add-order-screen/components/added-products-component/components/added-products-item-component';

// Interfaces
import type { IAddedProductsComponentProps } from '@/screens/orders/screens/add-order-screen/components/added-products-component/interfaces';

export const AddedProductsComponent: FC<IAddedProductsComponentProps> = (props): JSX.Element | null => {
  const {
    handleDecrementAddedProductCount,
    handleIncrementAddedProductCount,
    handleRemoveProductFromOrder,
    products,
    total,
  } = props;

  if (!products.length) return null;

  return (
    <Grid container>
      <Grid item xs={12}>
        <Paper elevation={6}>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell />
                  <TableCell align="center">Title</TableCell>
                  <TableCell align="center">Price ($)</TableCell>
                  <TableCell align="center">Quantity</TableCell>
                  <TableCell align="center">Sub Total ($)</TableCell>
                  <TableCell align="center" />
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map((product, key) => (
                  <AddedProductsItemComponent
                    handleDecrementAddedProductCount={handleDecrementAddedProductCount}
                    handleIncrementAddedProductCount={handleIncrementAddedProductCount}
                    handleRemoveProductFromOrder={handleRemoveProductFromOrder}
                    key={product.productId}
                    product={product}
                    rowIdx={key}
                  />
                ))}
                <TableRow>
                  <TableCell colSpan={6} sx={{ borderBottom: 'none' }} />
                </TableRow>
                <TableRow>
                  <TableCell colSpan={4} sx={{ borderBottom: 'none' }} />
                  <TableCell>Total ($)</TableCell>
                  <TableCell align="right">{total}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell colSpan={6} sx={{ borderBottom: 'none' }} />
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </Grid>
    </Grid>
  );
};
