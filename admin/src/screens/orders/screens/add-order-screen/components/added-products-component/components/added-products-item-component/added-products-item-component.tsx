// Modules
import { IconButton, TableCell, TableRow } from '@mui/material';
import { Add, Delete, Remove } from '@mui/icons-material';

// Modules Types
import { FC } from 'react';

// Interfaces
import type { IAddedProductsItemComponentProps } from '@/screens/orders/screens/add-order-screen/components/added-products-component/components/added-products-item-component/interfaces';

export const AddedProductsItemComponent: FC<IAddedProductsItemComponentProps> = (props): JSX.Element => {
  const {
    handleDecrementAddedProductCount,
    handleIncrementAddedProductCount,
    handleRemoveProductFromOrder,
    product,
    rowIdx,
  } = props;
  const { price, productId, quantity, title, total } = product;

  return (
    <TableRow>
      <TableCell align="center">{rowIdx + 1}</TableCell>
      <TableCell align="center">{title}</TableCell>
      <TableCell align="center">{price}</TableCell>
      <TableCell align="center">
        <IconButton disabled={quantity <= 1} onClick={() => handleDecrementAddedProductCount(productId)}>
          <Remove color={quantity === 1 ? 'disabled' : 'error'} />
        </IconButton>
        {quantity}
        <IconButton onClick={() => handleIncrementAddedProductCount(productId)}>
          <Add color="success" />
        </IconButton>
      </TableCell>
      <TableCell align="center">{total}</TableCell>
      <TableCell align="center">
        <IconButton color="error" onClick={() => handleRemoveProductFromOrder(product)} size="small">
          <Delete />
        </IconButton>
      </TableCell>
    </TableRow>
  );
};
