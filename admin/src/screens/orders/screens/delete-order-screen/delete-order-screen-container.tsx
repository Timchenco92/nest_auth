// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Engine
import { deleteOrderSuccessSelector, resetDeleteOrderSuccess } from '@/store/orders';

// Screens
import { DeleteOrderScreen } from '@/screens/orders/screens/delete-order-screen/delete-order-screen';

export const DeleteOrderScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();
  const isDeleted = useSelector(deleteOrderSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/dashboard/orders${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();

    return () => {
      const action = resetDeleteOrderSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteOrderScreen handleClose={handleClose}/>
  );
};
