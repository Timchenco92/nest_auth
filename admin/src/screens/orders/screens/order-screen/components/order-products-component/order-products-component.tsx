// Modules
import {
  Box,
  Button,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import { Link } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Components
import { CustomNoRowsOverlay } from '@/assets/svg'
import { OrderProductsItemComponent } from '@/screens/orders/screens/order-screen/components/order-products-component/components/order-products-item-component';

// Constants
import { COLORS } from '@/constants';

// Interfaces
import type { IOrderProductsComponentProps } from '@/screens/orders/screens/order-screen/components/order-products-component/interfaces';

export const OrderProductsComponent: FC<IOrderProductsComponentProps> = (props): JSX.Element => {
  const {
    handleDecrementOrderProductQuantity,
    handleIncrementOrderProductQuantity,
    orderProducts,
  } = props;

  if (!orderProducts.length) return <CustomNoRowsOverlay />;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Grid
            alignItems="center"
            container
            direction="row"
            height="100%"
            justifyContent="flex-end"
          >
            <Button
              color="success"
              component={Link}
              size="large"
              to="product/add"
              type="button"
              variant="text"
            >
              Create
            </Button>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={6}>
            <TableContainer>
              <Table>
                <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
                  <TableRow>
                    <TableCell align="center">#</TableCell>
                    <TableCell align="center">Title</TableCell>
                    <TableCell align="center">Price ($)</TableCell>
                    <TableCell align="center">Quantity</TableCell>
                    <TableCell align="center">Total ($)</TableCell>
                    <TableCell align="center"/>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {orderProducts.map((product, idx) => (
                    <OrderProductsItemComponent
                      handleDecrementOrderProductQuantity={handleDecrementOrderProductQuantity}
                      handleIncrementOrderProductQuantity={handleIncrementOrderProductQuantity}
                      orderProduct={product}
                      rowIdx={idx}
                      key={product.id}
                    />
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  );
};
