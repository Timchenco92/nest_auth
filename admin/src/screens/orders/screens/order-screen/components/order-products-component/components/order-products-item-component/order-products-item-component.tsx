// Modules
import { IconButton, TableCell, TableRow, Tooltip } from '@mui/material';
import { Link } from 'react-router-dom';
import { Add, Delete, Remove } from '@mui/icons-material';

// Modules Types
import { FC } from 'react';

// Constants
import { TOOLTIPS_MESSAGES } from '@/constants';

// Interfaces
import type { IOrderProductsItemComponentProps } from '@/screens/orders/screens/order-screen/components/order-products-component/components/order-products-item-component/interfaces';

export const OrderProductsItemComponent: FC<IOrderProductsItemComponentProps> = (props): JSX.Element => {
  const {
    handleDecrementOrderProductQuantity,
    handleIncrementOrderProductQuantity,
    rowIdx,
    orderProduct,
  } = props;
  const { quantity, total, product, id, orderId } = orderProduct;

  const { isDeleted } = TOOLTIPS_MESSAGES;

  return (
    <TableRow>
      <TableCell align="center">{rowIdx + 1}</TableCell>
      <TableCell align="center">{product?.title}</TableCell>
      <TableCell align="center">{product?.price}</TableCell>
      <TableCell align="center">
        <IconButton
          color="error"
          onClick={() => handleDecrementOrderProductQuantity(quantity, orderId, id)}
          size="small"
          sx={{ marginRight: '10px' }}
        >
          <Remove />
        </IconButton>
        {quantity}
        <IconButton
          color="success"
          onClick={() => handleIncrementOrderProductQuantity(quantity, orderId, id)}
          size="small"
          sx={{ marginLeft: '10px' }}
        >
          <Add />
        </IconButton>
      </TableCell>
      <TableCell align="center">{total}</TableCell>
      <TableCell align="center">
        <Tooltip placement="top" title={isDeleted('order')}>
          <IconButton component={Link} to={`product/${id}/delete`} color="error" size="small">
            <Delete />
          </IconButton>
        </Tooltip>
      </TableCell>
    </TableRow>
  );
};
