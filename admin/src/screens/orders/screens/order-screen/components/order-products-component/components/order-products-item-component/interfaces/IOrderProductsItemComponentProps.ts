// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

export interface IOrderProductsItemComponentProps {
  handleDecrementOrderProductQuantity: (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => void;
  handleIncrementOrderProductQuantity: (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => void;
  orderProduct: IOrderProduct;
  rowIdx: number;
}
