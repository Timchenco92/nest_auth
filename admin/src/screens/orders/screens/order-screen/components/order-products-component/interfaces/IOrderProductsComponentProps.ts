// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

export interface IOrderProductsComponentProps {
  handleDecrementOrderProductQuantity: (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => void;
  handleIncrementOrderProductQuantity: (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => void;
  orderProducts: IOrderProduct[];
}
