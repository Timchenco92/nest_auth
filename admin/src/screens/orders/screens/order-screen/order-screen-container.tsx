// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component';

// Engine
import { getOrderRequest, orderSelector, orderErrorSelector, orderPendingSelector } from '@/store/orders';
import {
  getOrderProductsRequest,
  orderProductsSelector,
  deleteOrderProductErrorSelector,
  orderProductsPendingSelector,
  incrementOrderProductQuantityRequest,
  incrementOrderProductQuantityPendingSelector,
  decrementOrderProductQuantityRequest,
  decrementOrderProductQuantityPendingSelector,
} from '@/store/order-products';

// Interfaces
import { IOrder, IOrderProduct } from '@/interfaces/models';

// Screens
import { OrderScreen } from '@/screens/orders/screens/order-screen/order-screen';

export const OrderScreenContainer: FC = (): JSX.Element | null => {
  const dispatch: Dispatch = useDispatch();
  const { orderId } = useParams<'orderId'>();

  const order = useSelector(orderSelector);
  const orderError = useSelector(orderErrorSelector);
  const orderPending = useSelector(orderPendingSelector);
  const orderProducts = useSelector(orderProductsSelector);
  const orderProductsError = useSelector(deleteOrderProductErrorSelector);
  const orderProductsPending = useSelector(orderProductsPendingSelector);
  const incrementPending = useSelector(incrementOrderProductQuantityPendingSelector);
  const decrementPending = useSelector(decrementOrderProductQuantityPendingSelector);

  const handleIncrementOrderProductQuantity = useCallback((quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => {
    console.log('yes');
    const incrementedQuantity = quantity + 1;
    const action = incrementOrderProductQuantityRequest(incrementedQuantity, orderId, orderProductId);
    dispatch(action);
  }, [dispatch]);

  const handleDecrementOrderProductQuantity = useCallback((quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => {
    console.log('yes2');
    const decrementedQuantity = quantity - 1;
    const action = decrementOrderProductQuantityRequest(decrementedQuantity, orderId, orderProductId);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!orderId) return;

    dispatch(getOrderRequest(orderId));
    dispatch(getOrderProductsRequest(orderId));
  }, [dispatch, orderId]);

  if (orderError || orderProductsError) return <CustomErrorOverlay />;

  if (orderPending || orderProductsPending) return <LoaderComponent />;

  if (incrementPending || decrementPending) return <LoaderComponent />;

  return (
    <OrderScreen
      handleIncrementOrderProductQuantity={handleIncrementOrderProductQuantity}
      handleDecrementOrderProductQuantity={handleDecrementOrderProductQuantity}
      order={order}
      orderProducts={orderProducts}
    />
  );
};
