// Modules
import {
  Avatar,
  Box,
  Divider,
  Grid, List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
  Typography,
} from '@mui/material';
import { Outlet } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Components
import { OrderProductsComponent } from '@/screens/orders/screens/order-screen/components/order-products-component';
import { OrderStatusComponent } from '@/screens/orders/screens/orders-screen/components/order-status-component';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import type { IOrderScreenProps } from '@/screens/orders/screens/order-screen/interfaces';

export const OrderScreen: FC<IOrderScreenProps> = (props): JSX.Element => {
  const {
    handleDecrementOrderProductQuantity,
    handleIncrementOrderProductQuantity,
    order,
    orderProducts,
  } = props;

  const { createdAt: orderCreatedAt, orderNumber, status, total, updatedAt: orderUpdatedAt, comment, delivery } = order;

  const formattedOrderCreatedAt = ConvertDatetime({ dateTime: orderCreatedAt });
  const formattedOrderUpdatedAt = ConvertDatetime({ dateTime: orderUpdatedAt });

  const formattedUserCreatedAt = ConvertDatetime({ dateTime: order?.user?.createdAt! });
  const formattedUserUpdatedAt = ConvertDatetime({ dateTime: order?.user?.updatedAt! });

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2} maxWidth="100%" margin={0}>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            Order Details
          </Typography>
          <Paper elevation={3}>
            <List>
              <ListItem>
                <ListItemText primary={`Number of order`} />
                <ListItemSecondaryAction>{orderNumber}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Order status`} />
                <ListItemSecondaryAction>
                  <OrderStatusComponent status={status} />
                </ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Order total ($)`} />
                <ListItemSecondaryAction>{total}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Order comment`} />
                <ListItemSecondaryAction>{comment || 'N/A'}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Order delivery`} />
                <ListItemSecondaryAction>{delivery || 'N/A'}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedOrderCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedOrderUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item md={6} xs={12} marginY="10px" paddingY="15px">
          <Typography variant="h5" marginBottom="15px">
            User Details
          </Typography>
          <Paper elevation={3}>
            <Box sx={{ padding: '20px' }}>
              <Avatar src={order?.user?.avatarUrl} sx={{ height: 130, marginX: 'auto', width: 130 }} />
            </Box>
            <Divider />
            <List>
              <ListItem>
                <ListItemText primary={`Full Name`} />
                <ListItemSecondaryAction>{order?.user?.fullName}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Phone`} />
                <ListItemSecondaryAction>{order?.user?.phone}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Email`} />
                <ListItemSecondaryAction>{order?.user?.email}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Address`} />
                <ListItemSecondaryAction>{order?.user?.address || 'N/A'}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Created`} />
                <ListItemSecondaryAction>{formattedUserCreatedAt}</ListItemSecondaryAction>
              </ListItem>
              <Divider />
              <ListItem>
                <ListItemText primary={`Updated`} />
                <ListItemSecondaryAction>{formattedUserUpdatedAt}</ListItemSecondaryAction>
              </ListItem>
            </List>
          </Paper>
        </Grid>
        <Grid item xs={12} marginY="10px" paddingY="15px">
          <OrderProductsComponent
            handleDecrementOrderProductQuantity={handleDecrementOrderProductQuantity}
            handleIncrementOrderProductQuantity={handleIncrementOrderProductQuantity}
            orderProducts={orderProducts}
          />
        </Grid>
      </Grid>
      <Outlet />
    </Box>
  );
};
