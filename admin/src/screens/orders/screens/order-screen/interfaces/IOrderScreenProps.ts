import { IOrder, IOrderProduct } from '@/interfaces/models';

export interface IOrderScreenProps {
  order: IOrder;
  orderProducts: IOrderProduct[];
  handleIncrementOrderProductQuantity: (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => void;
  handleDecrementOrderProductQuantity: (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']) => void;
}
