// Modules
import { Route, Routes } from 'react-router-dom';

// Modules Types
import { FC } from 'react';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Screens
import { AddOrderScreenContainer } from '@/screens/orders/screens/add-order-screen';
import { DeleteOrderScreenContainer } from '@/screens/orders/screens/delete-order-screen';
import { OrderScreenContainer } from '@/screens/orders/screens/order-screen';
import { OrdersScreenContainer } from '@/screens/orders/screens/orders-screen';
import { UpdateOrderScreenContainer } from '@/screens/orders/screens/update-order-screen';
import { AddProductToOrderScreenContainer } from '@/screens/order-products/add-product-to-order-screen';
import { DeleteProductFromOrderScreenContainer } from '@/screens/order-products/delete-product-from-order';

export const OrdersContainer: FC = (): JSX.Element => {
  return (
    <>
      <CustomSnackbarComponent />
      <Routes>
        <Route path="*" element={<OrdersScreenContainer />}>
          <Route path="add" element={<AddOrderScreenContainer />}/>
          <Route path=":orderId/delete" element={<DeleteOrderScreenContainer />}/>
          <Route path=":orderId/update" element={<UpdateOrderScreenContainer />}/>
        </Route>
        <Route path=":orderId/details" element={<OrderScreenContainer />}>
          <Route path="product/add" element={<AddProductToOrderScreenContainer />}/>
          <Route path="product/:orderProductId/delete" element={<DeleteProductFromOrderScreenContainer />}/>
        </Route>
      </Routes>
    </>
  );
};

export default OrdersContainer;
