// Modules
import { useEffect } from 'react';
import { Route, Routes } from 'react-router-dom';
import { useDispatch } from 'react-redux';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Components
import { CustomSnackbarComponent } from '@/components/custom-snackbar-component';

// Engine
import { resetPagination } from '@/store/banners';

// Screens
import { AddBannerScreenContainer } from '@/screens/banners/screens/add-banner-screen';
import { BannersScreenContainer } from '@/screens/banners/screens/banners-screen';
import { DeleteBannerScreenContainer } from '@/screens/banners/screens/delete-banners-screen';

export const BannersContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();

  useEffect(() => () => {
    const action = resetPagination();
    dispatch(action);
  }, [dispatch]);

  return (
    <>
      <CustomSnackbarComponent />
      <Routes>
        <Route path="*" element={<BannersScreenContainer />}>
          <Route path="add" element={<AddBannerScreenContainer />} />
          <Route path=":bannerId/delete" element={<DeleteBannerScreenContainer />} />
        </Route>
      </Routes>
    </>
  );
};

export default BannersContainer;
