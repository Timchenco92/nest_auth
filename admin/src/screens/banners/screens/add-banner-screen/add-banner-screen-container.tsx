// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { validationSchema, initialValues } from '@/screens/banners/screens/add-banner-screen/form-config';

// Engine
import {
  addBannerErrorSelector,
  addBannersRequest,
  addBannerSuccessSelector,
  resetAddBannerSuccess,
} from '@/store/banners';

// Interfaces
import type { IInitialValues } from '@/screens/banners/screens/add-banner-screen/interfaces/IInitialValues';

// Screens
import { AddBannerScreen } from '@/screens/banners/screens/add-banner-screen/add-banner-screen';

export const AddBannerScreenContainer: FC = (): JSX.Element => {
  const { errors, hasError } = useSelector(addBannerErrorSelector);
  const isBannerAdded = useSelector(addBannerSuccessSelector);
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();

  const navigate = useNavigate();

  const handleClose = useCallback(() => {
    navigate(`/dashboard/banners${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isBannerAdded) return;
    handleClose();
    return () => {
      const action = resetAddBannerSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isBannerAdded]);

  const handleSubmit = useCallback((values: IInitialValues) => {
    const data = new FormData();
    data.append('image', values.image);
    data.append('title', values.title);
    const action = addBannersRequest(data);
    dispatch(action);
  }, [dispatch]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnBlur: !hasError,
    validateOnChange: !hasError,
    validateOnMount: true,
  });

  return (
    <AddBannerScreen errors={errors} formik={formik} handleClose={handleClose} />
  );
};

export default AddBannerScreenContainer;
