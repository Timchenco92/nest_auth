// Modules
import { object, string, mixed } from 'yup';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/banners/screens/add-banner-screen/interfaces/IInitialValues';

const { isRequired } = staticValidationErrors;

export const validationSchema = object({
  image: mixed().required(isRequired),
  title: string().required(isRequired),
});

export const initialValues: IInitialValues = {
  image: '',
  title: '',
};
