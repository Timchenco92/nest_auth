// Modules Types
import type { FormikProps } from 'formik';

// Interfaces
import type { IAddBannerValidationErrors } from '@/screens/banners/screens/add-banner-screen/interfaces/IAddBannerValidationErrors';
import type { IInitialValues } from '@/screens/banners/screens/add-banner-screen/interfaces/IInitialValues';

export interface IAddBannerScreenProps {
  errors: IAddBannerValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
}
