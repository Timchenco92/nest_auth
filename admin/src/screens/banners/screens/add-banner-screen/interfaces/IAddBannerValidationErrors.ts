export interface IAddBannerValidationErrors {
  image: string;
  title: string;
}
