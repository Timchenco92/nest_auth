// Modules
import { useCallback, useEffect, useRef, useState } from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup, IconButton,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import type { BaseSyntheticEvent, FC } from 'react';

// Interfaces
import type { IAddBannerScreenProps } from '@/screens/banners/screens/add-banner-screen/interfaces/IAddBannerScreenProps';

export const AddBannerScreen: FC<IAddBannerScreenProps> = (props): JSX.Element => {
  const {
    errors: serverValidationErrors,
    formik,
    handleClose,
  } = props;

  const [bannerImage, setBannerImage] = useState<string>('');
  const bannerImageRef = useRef<HTMLInputElement>(null);

  const {
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    setFieldError,
    setFieldTouched,
    setFieldValue,
    touched,
    values,
  } = formik;

  const handleChangeBannerImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('image', files);
    setBannerImage(files?.name);
  }, [setFieldValue]);

  const handleClickBannerImage = useCallback(async () => {
    bannerImageRef?.current?.click();
    await setFieldTouched('image', true);
  }, [setFieldTouched]);

  const handleClearBannerImage = useCallback(() => {
    if (!bannerImageRef.current) return;
    setBannerImage('');
    bannerImageRef.current.value = '';
    setFieldError('image', 'This field is required');
  }, [setFieldError]);

  const allowedDisabledConditions = [
    isValid,
    isSubmitting,
  ];

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  const isBannerImageFieldFullWidth: string = Boolean(bannerImage) ? '95%' : '100%';

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 240 } }}
    >
      <DialogTitle>Creating a new banner</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <TextField
            error={touched.title && Boolean(errors.title)}
            fullWidth
            helperText={touched.title && errors.title}
            label="Title"
            name="title"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.title}
            variant="standard"
          />
          <FormGroup row>
            <FormControl sx={{ flexBasis: isBannerImageFieldFullWidth }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={formik.touched.image && Boolean(formik.errors.image)}
                focused={Boolean(bannerImage)}
                helperText={formik.touched.image && formik.errors.image}
                label="Image"
                margin="dense"
                name="image"
                onClick={handleClickBannerImage}
                required
                type="text"
                value={bannerImage}
                variant="standard"
              />
            </FormControl>
            {Boolean(bannerImage) && (
              <FormControl sx={{ textAlign: 'center', justifyContent: 'flex-end', flexBasis: '5%' }}>
                <IconButton onClick={handleClearBannerImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="image"
            onChange={handleChangeBannerImage}
            type="file"
            ref={bannerImageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button disabled={isSubmitDisabled} color="success" fullWidth type="submit">Create</Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default AddBannerScreen;
