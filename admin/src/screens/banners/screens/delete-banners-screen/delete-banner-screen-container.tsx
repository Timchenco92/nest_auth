// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';

// Engine
import { deleteBannerSuccessSelector, resetDeleteBannerSuccess } from '@/store/banners';

// Screens
import { DeleteBannerScreen } from '@/screens/banners/screens/delete-banners-screen/delete-banner-screen';

export const DeleteBannerScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const isDeleted = useSelector(deleteBannerSuccessSelector);
  const dispatch = useDispatch();
  const { search } = useLocation();

  const handleClose = useCallback(() => {
    navigate(`/dashboard/banners${search}`, { replace: true });
  }, [navigate, search]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();
    return () => {
      const action = resetDeleteBannerSuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteBannerScreen handleClose={handleClose} />
  );
};

export default DeleteBannerScreenContainer;
