// Modules
import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Modules Types
import type { FC } from 'react';

// Engine
import { deleteBannerRequest } from '@/store/banners';

export interface IProps {
  handleClose(): void;
}

export const DeleteBannerScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;

  const { bannerId } = useParams<'bannerId'>();
  const dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!bannerId) return;
    const action = deleteBannerRequest(bannerId);
    dispatch(action)
  }, [bannerId, dispatch]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing a banner</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteBannerScreen;
