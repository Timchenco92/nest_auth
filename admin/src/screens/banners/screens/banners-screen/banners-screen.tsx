// Modules
import { ChangeEvent, FC, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  Grid,
  Paper, SelectChangeEvent,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import { useSearchParams } from 'react-router-dom';

// Assets
import { CustomNoRowsOverlay } from '@/assets/svg';

// Components
import { BannerItemComponent } from '@/screens/banners/screens/banners-screen/components/banner-item-component';
import { PaginationComponent } from '@/components/pagination-component';

// Constants
import { COLORS } from '@/constants';

// Engine
import { bannersSelector, paginationSelector, changePagePagination, changeLimitPagination } from '@/store/banners';

const DEFAULT_PAGINATION_PAGE = '1';

export const BannersScreen: FC = (): JSX.Element => {
  const { data: banners, meta: { totalPages } } = useSelector(bannersSelector);
  const { limit, page } = useSelector(paginationSelector);
  const bannersCount = banners.length;
  const [, setSearchParams] = useSearchParams();

  const dispatch = useDispatch();

  const handleChangeLimit = useCallback((event: SelectChangeEvent) => {
    const { target: { value } } = event;
    const action = changeLimitPagination({ limit: value });
    dispatch(action);
  }, [dispatch]);

  const handleChangePage = useCallback((event: ChangeEvent<unknown>, page: number) => {
    const action = changePagePagination({ page: String(page) });
    dispatch(action);
    setSearchParams((searchParams) => {
      if (String(page) !== DEFAULT_PAGINATION_PAGE) {
        searchParams.set('page', String(page));
      } else {
        searchParams.delete('page')
      }
      return searchParams;
    });
  }, [dispatch, setSearchParams]);

  if (!bannersCount) return <CustomNoRowsOverlay />

  return (
    <Grid item xs={12}>
      <Paper elevation={6}>
        <TableContainer>
          <Table>
            <TableHead sx={{ borderLeft: `1px solid ${COLORS.GRAY_WITH_ALFA}` }}>
              <TableRow>
                <TableCell align="center">#</TableCell>
                <TableCell />
                <TableCell align="center">Title</TableCell>
                <TableCell align="center">Created</TableCell>
                <TableCell align="center">Updated</TableCell>
                <TableCell align="center"/>
              </TableRow>
            </TableHead>
            <TableBody>
              {banners.map((banner, key) => (
                <BannerItemComponent banner={banner} rowIdx={key} key={banner.id}/>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <PaginationComponent
          currentPage={page}
          handleChangeLimit={handleChangeLimit}
          handleChangePage={handleChangePage}
          limit={limit}
          totalPages={totalPages}
        />
      </Paper>
    </Grid>
  );
};

export default BannersScreen;
