// Modules
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Outlet, useLocation, useSearchParams } from 'react-router-dom';
import { Box, Button, Grid } from '@mui/material';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Assets
import { CustomErrorOverlay } from '@/assets/svg';

// Components
import { LoaderComponent } from '@/components/loader-component';

// Engine
import {
  getBannersRequest,
  paginationSelector,
  bannersErrorSelector,
  bannersPendingSelector,
  changePagePagination,
  changeLimitPagination,
} from '@/store/banners';

// Screens
import { BannersScreen } from '@/screens/banners/screens/banners-screen/banners-screen';

const DEFAULT_LIMIT = '15';

export const BannersScreenContainer: FC = (): JSX.Element => {
  const [searchParams, setSearchParams] = useSearchParams();
  const dispatch: Dispatch = useDispatch();
  const { search } = useLocation();

  const { limit, page } = useSelector(paginationSelector);
  const error = useSelector(bannersErrorSelector);
  const pending = useSelector(bannersPendingSelector);

  const addPath = search.length ? `add${search}` : 'add';

  useEffect(() => {
    if (!searchParams.has('page')) return;
    const qsPage = searchParams.get('page') as string;

    const action = changePagePagination({ page: qsPage });
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    if (!searchParams.has('limit')) return;
    const qsLimit = searchParams.get('limit') as string;

    const action = changeLimitPagination({ limit: qsLimit });
    dispatch(action);
  }, [dispatch, searchParams]);

  useEffect(() => {
    setSearchParams((searchParams) => {
      if (limit !== DEFAULT_LIMIT) {
        searchParams.set('limit', limit);
      } else {
        searchParams.delete('limit');
      }
      return searchParams;
    })
  }, [limit, setSearchParams]);

  useEffect(() => {
    const action = getBannersRequest({ limit, page });
    dispatch(action);
  }, [dispatch, limit, page]);

  if (error) return <CustomErrorOverlay />;

  if (pending) return <LoaderComponent />;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Grid
            alignItems="center"
            container
            direction="row"
            height="100%"
            justifyContent="flex-end"
          >
            <Button
              color="success"
              component={Link}
              size="large"
              to={addPath}
              type="button"
              variant="text"
            >
              Create
            </Button>
          </Grid>
        </Grid>
        <BannersScreen />
      </Grid>
      <Outlet />
    </Box>
  );
};

export default BannersScreenContainer;
