// Modules
import { FC } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Delete } from '@mui/icons-material';
import { Avatar, IconButton, TableCell, TableRow, Tooltip } from '@mui/material';

// Constants
import { TOOLTIPS_MESSAGES } from '@/constants';

// Helpers
import { ConvertDatetime } from '@/helpers';

// Interfaces
import { IBanner } from '@/interfaces/models/IBanner';

interface IProps {
  banner: IBanner;
  rowIdx: number;
}

export const BannerItemComponent: FC<IProps> = (props): JSX.Element => {
  const { banner, rowIdx } = props;
  const { createdAt, id, imageUrl, title, updatedAt } = banner;

  const formattedCreatedAt = ConvertDatetime({ dateTime: createdAt });
  const formattedUpdatedAt = ConvertDatetime({ dateTime: updatedAt });

  const { search } = useLocation();

  const path = search.length ? `${id}/delete${search}` : `${id}/delete`;

  const { isDeleted } = TOOLTIPS_MESSAGES;

  return (
    <TableRow>
      <TableCell align="center">{rowIdx + 1}</TableCell>
      <TableCell align="center">
        <Avatar src={imageUrl} sx={{ marginX: 'auto' }} />
      </TableCell>
      <TableCell align="center">{title}</TableCell>
      <TableCell align="center">{formattedCreatedAt}</TableCell>
      <TableCell align="center">{formattedUpdatedAt}</TableCell>
      <TableCell>
        <Tooltip placement="top" title={isDeleted('banner')}>
          <IconButton component={Link} to={path} color="error" size="small">
            <Delete />
          </IconButton>
        </Tooltip>
      </TableCell>
    </TableRow>
  );
};

export default BannerItemComponent;
