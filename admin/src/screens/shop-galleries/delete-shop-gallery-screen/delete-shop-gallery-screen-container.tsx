// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// Modules Types
import { FC } from 'react';
import { Dispatch } from 'redux';

// Engine
import { deleteShopGallerySuccessSelector, resetDeleteShopGallerySuccess } from '@/store/shop-gallery';

// Screens
import { DeleteShopGalleryScreen } from '@/screens/shop-galleries/delete-shop-gallery-screen/delete-shop-gallery-screen';

export const DeleteShopGalleryScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();
  const isDeleted = useSelector(deleteShopGallerySuccessSelector);

  const handleClose = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  useEffect(() => {
    if (!isDeleted) return;
    handleClose();
    return () => {
      const action = resetDeleteShopGallerySuccess();
      dispatch(action);
    };
  }, [dispatch, handleClose, isDeleted]);

  return (
    <DeleteShopGalleryScreen handleClose={handleClose} />
  );
};
