// Modules
import { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Engine
import { deleteShopGalleryRequest } from '@/store/shop-gallery';

interface IProps {
  handleClose(): void;
}

export const DeleteShopGalleryScreen: FC<IProps> = (props): JSX.Element => {
  const { handleClose } = props;
  const { shopGalleryId, shopId } = useParams<{ shopGalleryId: string, shopId: string }>();
  const dispatch: Dispatch = useDispatch();

  const handleDelete = useCallback(() => {
    if (!shopGalleryId || !shopId) return;

    const action = deleteShopGalleryRequest({ shopGalleryId, shopId });
    dispatch(action);
  }, [dispatch, shopGalleryId, shopId]);
  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="sm"
      open={true}
    >
      <DialogTitle>Removing image from gallery</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Deleted data cannot be recovered.<br />
          Are you sure you want to delete the data?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="error" fullWidth onClick={handleClose}>Disagree</Button>
        <Button color="success" fullWidth onClick={handleDelete}>Agree</Button>
      </DialogActions>
    </Dialog>
  );
};
