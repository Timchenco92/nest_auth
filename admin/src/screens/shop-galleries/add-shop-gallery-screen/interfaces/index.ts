export type { IAddShopGalleryScreenProps } from './IAddShopGalleryScreenProps';
export type { IAddShopGalleryValidationErrors } from './IAddShopGalleryValidationErrors';
export type { IInitialValues } from './IInitialValues';
