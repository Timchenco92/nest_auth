// Modules
import { FormikProps } from 'formik';

// Interfaces
import type {
  IAddShopGalleryValidationErrors,
  IInitialValues,
} from '@/screens/shop-galleries/add-shop-gallery-screen/interfaces';

export interface IAddShopGalleryScreenProps {
  errors: IAddShopGalleryValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose(): void;
  pending: boolean;
}
