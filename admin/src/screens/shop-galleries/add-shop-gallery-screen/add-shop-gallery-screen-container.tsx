// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from '@/screens/shop-galleries/add-shop-gallery-screen/form-config';

// Engine
import {
  addShopGalleryErrorSelector,
  addShopGalleryPendingSelector,
  addShopGalleryRequest,
  addShopGallerySuccessSelector,
  resetAddShopGalleryError,
  resetAddShopGallerySuccess,
} from '@/store/shop-gallery';

// Interfaces
import type { IInitialValues } from '@/screens/shop-galleries/add-shop-gallery-screen/interfaces';

// Screens
import { AddShopGalleryScreen } from '@/screens/shop-galleries/add-shop-gallery-screen/add-shop-gallery-screen';

export const AddShopGalleryScreenContainer: FC = (): JSX.Element => {
  const { shopId } = useParams<'shopId'>();
  const { errors, hasError } = useSelector(addShopGalleryErrorSelector);
  const isAdded = useSelector(addShopGallerySuccessSelector);
  const pending = useSelector(addShopGalleryPendingSelector);

  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleClose = useCallback(() => {
    navigate(-1);
  }, [navigate]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      handleSubmitForm(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmitForm = useCallback((values: IInitialValues) => {
    if (!shopId) return;

    const data = new FormData();
    data.append('image', values.image);
    data.append('shopId', shopId);
    const action = addShopGalleryRequest({ shopId, data });
    dispatch(action);
  }, [dispatch, shopId]);

  useEffect(() => {
    if (!isAdded) return;
    handleClose();
    return () => {
      const action = resetAddShopGallerySuccess();
      dispatch(action);
    }
  }, [dispatch, handleClose, isAdded]);

  useEffect(() => {
    return () => {
      const action = resetAddShopGalleryError();
      dispatch(action);
    }
  }, [dispatch]);

  return (
    <AddShopGalleryScreen
      errors={errors}
      formik={formik}
      handleClose={handleClose}
      pending={pending}
    />
  );
};
