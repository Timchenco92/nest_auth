// Modules
import {
  useCallback,
  useRef,
  useState,
  useEffect,
} from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  FormGroup,
  IconButton,
  TextField,
} from '@mui/material';
import { Clear } from '@mui/icons-material';

// Modules Types
import { BaseSyntheticEvent, FC } from 'react';

// Constants
import { staticValidationErrors } from '@/constants';

// Interfaces
import type { IAddShopGalleryScreenProps } from '@/screens/shop-galleries/add-shop-gallery-screen/interfaces';

export const AddShopGalleryScreen: FC<IAddShopGalleryScreenProps> = (props): JSX.Element => {
  const {
    errors: serverValidationErrors,
    formik,
    handleClose,
    pending,
  } = props;

  const {
    errors,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    setFieldError,
    setFieldTouched,
    setFieldValue,
    touched,
  } = formik;

  const [imageName, setImageName] = useState<string>('');
  const imageRef = useRef<HTMLInputElement>(null);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('image', files);
    setImageName(files?.name);
  }, [setFieldValue]);

  const handleClickImage = useCallback(() => {
    imageRef?.current?.click();
    setFieldTouched('image', true);
  }, [setFieldTouched]);

  const handleClearImage = useCallback(() => {
    if (!imageRef.current) return;
    setImageName('');
    imageRef.current.value = '';
    setFieldError('image', staticValidationErrors.isRequired);
  }, [setFieldError]);

  const isImageFieldFullWidth: string = Boolean(imageName) ? '95%' : '100%';

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <Dialog
      fullWidth
      keepMounted
      maxWidth="md"
      onClose={handleClose}
      open={true}
      PaperProps={{ sx: { position: 'fixed', top: 240 } }}
    >
      <DialogTitle>Adding Image To Shop</DialogTitle>
      <form onSubmit={handleSubmit}>
        <DialogContent>
          <FormGroup row>
            <FormControl sx={{ flexBasis: isImageFieldFullWidth, marginY: 'auto' }}>
              <TextField
                InputProps={{
                  readOnly: true,
                }}
                error={touched.image && Boolean(errors.image)}
                focused={Boolean(imageName)}
                helperText={touched.image && errors.image}
                label="Image"
                margin="dense"
                name="image"
                onClick={handleClickImage}
                // required
                type="text"
                value={imageName}
                variant="standard"
              />
            </FormControl>
            {Boolean(imageName) && (
              <FormControl sx={{
                flexBasis: '5%',
                justifyContent: 'flex-end',
                marginY: 'auto',
                textAlign: 'center',
              }}
              >
                <IconButton onClick={handleClearImage}>
                  <Clear />
                </IconButton>
              </FormControl>
            )}
          </FormGroup>
          <input
            accept="image/*"
            hidden
            name="image"
            onChange={handleChangeImage}
            type="file"
            ref={imageRef}
          />
        </DialogContent>
        <DialogActions>
          <Button color="error" fullWidth onClick={handleClose} type="button">Close</Button>
          <Button
            disabled={!isValid || isSubmitting || pending}
            color="success"
            fullWidth
            type="submit"
          >
            Add Image
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};
