// Modules
import axios, { AxiosInstance, InternalAxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';

const instance: AxiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BASE_API_URL,
  responseType: 'json',
  headers: {
    'Accept': 'application/json',
    'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    'Access-Control-Allow-Origin': '*',
    'Authorization': `Bearer ${localStorage.getItem('access_token')}`,
    'Content-Type': 'application/json',
  },
});

instance.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    const token = localStorage.getItem('access_token');
    config.headers.Authorization = `Bearer ${token}`;
    return Promise.resolve(config);
  }, (error) => {
    return Promise.reject(error);
  },
);

instance.interceptors.response.use(
  (response: AxiosResponse) => {
    return Promise.resolve(response);
  }, (error: AxiosError) => {
    return Promise.reject(error);
  },
);

export default instance;
