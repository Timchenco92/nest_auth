// Modules
import { FC, MouseEvent, useState } from 'react';
import {
  Box,
  Container,
  CssBaseline,
  Grid,
  Toolbar,
} from '@mui/material';
import { Route, Routes } from 'react-router-dom';
import { ThemeProvider, createTheme } from '@mui/material';

// Components
import { AppBarComponent } from '@/components/app-bar-component';
import { DrawerComponent } from '@/components/drawer-component';

// Constants
import { ROUTES } from '@/constants';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

export const DashboardContainer: FC = (): JSX.Element => {
  const [open, setOpen] = useState(true);
  const { screenRoutes } = ROUTES;

  const toggleDrawer = () => {
    setOpen(!open);
  };

  const handleLogout = (event: MouseEvent) => {
    event.preventDefault();
    localStorage.removeItem('access_token');
    window.location.reload();
  };

  return (
    <ThemeProvider theme={darkTheme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBarComponent handleToggleDrawer={toggleDrawer} isOpen={open} />
        <DrawerComponent handleToggleDrawer={toggleDrawer} handleLogout={handleLogout} isOpen={open} />
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <Toolbar />
          <Container maxWidth="xl" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              <Grid item xs={12} md={12} lg={12}>
                <Routes>
                  {screenRoutes.map(({ component: Component, path }) => (
                    <Route key={path} path={path} element={<Component />} />
                  ))}
                </Routes>
              </Grid>
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export default DashboardContainer;
