// Modules
import { FC, Suspense, useEffect } from 'react';
import { Routes } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

// Engine
import { adminAuthSelector, getMeRequest } from '@/store/auth';

// Routes
import { AuthRoutes, UnAuthRoutes } from '@/routes';

// Styles
import './App.css';

const App: FC = (): JSX.Element => {
  const dispatch = useDispatch();
  const isAuth = useSelector(adminAuthSelector);

  useEffect(() => {
    if (isAuth) return;

    const action = getMeRequest();
    dispatch(action);
  }, [isAuth, dispatch]);

  return (
    <div className="App">
      <Suspense fallback={'...loading'}>
        <Routes>
          {AuthRoutes}
          {UnAuthRoutes}
        </Routes>
      </Suspense>
    </div>
  );
};

export default App;
