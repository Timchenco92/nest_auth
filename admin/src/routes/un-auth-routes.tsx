// Modules
import { Route } from 'react-router-dom';

// Guards
import { UnAuthGuard } from '@/guards';

// Screens
import { LoginScreenContainer } from '@/screens/auth';

export const UnAuthRoutes = [
  <Route key="LoginScreenContainer" path="/" element={<UnAuthGuard component={<LoginScreenContainer />} />} />
]
