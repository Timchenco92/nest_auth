// Modules
import { Route } from 'react-router-dom';

// Containers
import { DashboardContainer } from '@/containers/dashboard-container';

// Guards
import { AuthGuard } from '@/guards';

// Screens
import { HomeScreenContainer } from '@/screens/home';
import { BannersContainer } from '@/screens/banners'
import { CategoriesContainer } from '@/screens/categories';
import { ProductsContainer } from '@/screens/products';
import { SettingsScreenContainer } from '@/screens/settings';
import { ShopsContainer } from '@/screens/shops';
import { UsersContainer } from '@/screens/users';

export const AuthRoutes = [
  <Route key="HomeScreenContainer" path="/dashboard/*" element={<AuthGuard component={<DashboardContainer />} />}>
    <Route key="HomeScreenContainer" index element={<AuthGuard component={<HomeScreenContainer />} />} />
    <Route key="BannersContainer" element={<AuthGuard component={<BannersContainer />} />} />
    <Route key="CategoriesContainer" element={<AuthGuard component={<CategoriesContainer />} />} />
    <Route key="ProductsContainer" element={<AuthGuard component={<ProductsContainer />} />} />
    <Route key="SettingsScreenContainer" element={<AuthGuard component={<SettingsScreenContainer />} />} />
    <Route key="ShopsContainer" element={<AuthGuard component={<ShopsContainer />} />} />
    <Route key="UsersContainer" element={<AuthGuard component={<UsersContainer />} />} />
  </Route>,
];

export default AuthRoutes;
