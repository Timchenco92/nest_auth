// Modules
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';

// Engine
import { adminAuthSelector } from '@/store/auth';

// Interfaces
import type { IProps } from '@/guards/IProps';

export const UnAuthGuard: FC<IProps> = (props) => {
  const { component } = props;
  const navigate = useNavigate();
  const isAuth = useSelector(adminAuthSelector);

  useEffect(() => {
    if (isAuth) {
      navigate('/dashboard/home', { replace: true });
    }
  }, [isAuth, navigate]);

  return (
    <>{component}</>
  );
};

export default UnAuthGuard;
