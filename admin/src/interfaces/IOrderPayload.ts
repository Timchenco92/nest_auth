// Interfaces
import { IProduct, IUser } from '@/interfaces/models';

export interface IUpdateOrderPayload {
  userId: IUser['id'];
  total: string;
  delivery: string | null;
  comment: string | null;
}

export interface IAddOrderPayload extends IUpdateOrderPayload {
  orderProducts: IAddOrderProductPayload[];
}

export interface IAddOrderProductPayload {
  productId: IProduct['id'];
  total: string;
  quantity: number;
}
