export interface IPagination {
  limit: string;
  page: string;
}
