// Modules
import { FC, LazyExoticComponent } from 'react';
import { SvgIconComponent } from '@mui/icons-material';

export interface IDrawerListRoute {
  name: string;
  path: string;
  icon: SvgIconComponent;
}

export interface IScreenRoute {
  component: LazyExoticComponent<FC>;
  path: string;
}
