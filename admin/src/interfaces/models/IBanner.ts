export interface IBanner {
  id: string | number;
  title: string;
  imageUrl: string;
  createdAt: Date;
  updatedAt: Date;
}
