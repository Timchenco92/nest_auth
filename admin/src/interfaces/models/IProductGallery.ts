export interface IProductGallery {
  id: string;
  productId: string;
  image: string;
  imageUrl: string;
  createdAt: Date;
  updatedAt: Date;
}
