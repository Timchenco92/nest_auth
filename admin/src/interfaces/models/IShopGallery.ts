export interface IShopGallery {
  id: string;
  image: string;
  imageUrl: string;
  createdAt: Date;
  updatedAt: Date;
}
