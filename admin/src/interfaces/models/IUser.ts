// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IUser {
  id: string;
  avatar: string | null;
  avatarUrl: string;
  fullName: string;
  firstName: string;
  middleName: string;
  lastName: string;
  address: string | null;
  email: string;
  phone: string;
  ordersCount?: number;
  orders?: {
    data: IOrder[];
    meta: any;
    links: any
  };
  createdAt: Date;
  updatedAt: Date;
}
