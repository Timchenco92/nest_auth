// Interfaces
import type { ICategory, IProductGallery } from '@/interfaces/models';

export interface IProduct {
  id: string;
  galleries?: IProductGallery[];
  categoryId: string;
  category?: ICategory;
  imageUrl: string;
  image: string;
  title: string;
  createdAt: Date;
  updatedAt: Date;
  price: string;
}
