// Interfaces
import type { IOrderProduct, IUser } from '@/interfaces/models';

export enum EOrderStatus {
  accepted = 'Accepted' as any,
  in_progress = 'In progress' as any,
  finished = 'Finished' as any,
}

export interface IOrder {
  comment: string | null;
  createdAt: Date;
  delivery: string | null;
  id: string;
  orderNumber: string;
  orderProducts: IOrderProduct[];
  status: EOrderStatus;
  total: string;
  updatedAt: Date;
  userId: IUser['id'];
  user?: IUser;
}
