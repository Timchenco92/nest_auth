// Interfaces
import type { IOrder, IProduct } from '@/interfaces/models';

export interface IOrderProduct {
  createdAt: Date;
  id: string;
  orderId: IOrder['id'];
  product: IProduct;
  productId: IProduct['id'];
  total: string;
  quantity: number;
  updatedAt: Date;
}
