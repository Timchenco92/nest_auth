// Interfaces
import type { IShopGallery } from '@/interfaces/models';

export interface IShop {
  id: string;
  address: string;
  title: string;
  image: string;
  imageUrl: string;
  firstPhone: string;
  secondPhone: string;
  thirdPhone: string;
  startTimeWork: string;
  endTimeWork: string;
  createdAt: Date;
  updatedAt: Date;
  galleries?: IShopGallery[];
}
