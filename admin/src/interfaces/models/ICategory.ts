// Interfaces
import type { IProduct } from '@/interfaces/models/IProduct';

export interface ICategory {
  id: string | number;
  title: string;
  image: string;
  imageUrl: string;
  products: ICategoryProducts;
  productsCount: number;
  createdAt: Date;
  updatedAt: Date;
}

interface ICategoryProducts {
  data: IProduct[];
  meta: any;
  links: any;
}
