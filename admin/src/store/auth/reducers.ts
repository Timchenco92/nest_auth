// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  GET_ME,
  GET_ME_ERROR,
  GET_ME_SUCCESS,
  LOGIN_ADMIN,
  LOGIN_ADMIN_ERROR,
  LOGIN_ADMIN_SUCCESS,
  RESET_LOGIN_ADMIN_SUCCESS,
} from './actionTypes';

// Types
import { IAuthState, TAuthTypes } from '@/store/auth';

const initialState: IAuthState = {
  admin: {
    data: {
      id: '',
      email: '',
      firstName: '',
      lastName: '',
      middleName: '',
      phone: '',
      avatarUrl: '',
      password: '',
    },
    error: false,
    isAuth: false,
    pending: false,
  },
  login: {
    error: {
      errors: {
        email: '',
        password: '',
      },
      hasError: false,
    },
    pending: false,
    success: false,
  },
};

export const authReducer: Reducer<IAuthState, TAuthTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_ME: {
      return {
        ...state,
        admin: {
          data: { ...state.admin.data },
          error: false,
          isAuth: false,
          pending: true,
        },
      };
    }
    case GET_ME_ERROR: {
      return {
        ...state,
        admin: {
          data: { ...state.admin.data },
          error: true,
          isAuth: action.isAuth,
          pending: false,
        },
      };
    }
    case GET_ME_SUCCESS: {
      return {
        ...state,
        admin: {
          data: action.payload.data,
          error: false,
          isAuth: action.payload.isAuth,
          pending: false,
        },
      };
    }
    case LOGIN_ADMIN: {
      return {
        ...state,
        login: {
          error: { ...state.login.error },
          pending: true,
          success: false,
        },
      };
    }
    case LOGIN_ADMIN_ERROR: {
      return {
        ...state,
        login: {
          error: {
            hasError: true,
            errors: action.payload.errors,
          },
          pending: false,
          success: false,
        },
      };
    }
    case LOGIN_ADMIN_SUCCESS: {
      return {
        ...state,
        login: {
          error: { ...state.login.error },
          pending: false,
          success: true,
        },
      };
    }
    case 'RESET_LOGIN_ADMIN_ERROR': {
      return {
        ...state,
        login: {
          error: {
            hasError: false,
            errors: {
              email: '',
              password: ''
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_LOGIN_ADMIN_SUCCESS: {
      return {
        ...state,
        login: {
          error: { ...state.login.error },
          pending: false,
          success: false,
        },
      };
    }
    default:
      return state;
  }
};
