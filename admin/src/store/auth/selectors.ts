// Modules
import { createSelector } from 'reselect';

// State
import type { AuthState } from '@/store/rootReducer';

const login = (state: AuthState) => ({
  error: state.auth.login.error,
  pending: state.auth.login.pending,
  success: state.auth.login.success,
});

const admin = (state: AuthState) => ({
  data: state.auth.admin.data,
  error: state.auth.admin.error,
  isAuth: state.auth.admin.isAuth,
  pending: state.auth.admin.pending,
});

export const loginErrorSelector = createSelector(login, ({ error }) => error);
export const loginPendingSelector = createSelector(login, ({ pending }) => pending);
export const loginSuccessSelector = createSelector(login, ({ success }) => success);

export const adminSelector = createSelector(admin, ({ data }) => data);
export const adminErrorSelector = createSelector(admin, ({ error }) => error);
export const adminAuthSelector = createSelector(admin, ({ isAuth }) => isAuth);
export const adminPendingSelector = createSelector(admin, ({ pending }) => pending);
