// Action Types
import { RESET_LOGIN_ADMIN_ERROR, RESET_LOGIN_ADMIN_SUCCESS } from '@/store/auth';

export interface IResetLoginAdminError {
  type: typeof RESET_LOGIN_ADMIN_ERROR;
}

export interface IResetLoginAdminSuccess {
  type: typeof RESET_LOGIN_ADMIN_SUCCESS;
}
