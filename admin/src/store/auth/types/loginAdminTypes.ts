// Action Types
import { LOGIN_ADMIN, LOGIN_ADMIN_ERROR, LOGIN_ADMIN_SUCCESS } from '@/store/auth';

export interface ILoginAdminState {
  error: ILoginAdminStateAndPayloadError;
  pending: boolean;
  success: boolean;
}

export interface ILoginAdminStateAndPayloadError {
  hasError: boolean;
  errors: ILoginValidationErrors;
}

export interface ILoginValidationErrors {
  email: string;
  password: string;
}

export interface ILoginAdmin {
  type: typeof LOGIN_ADMIN;
  payload: ILoginAdminPayload;
}

export interface ILoginAdminPayload {
  email: string;
  password: string;
}

export interface ILoginAdminError {
  type: typeof LOGIN_ADMIN_ERROR;
  payload: ILoginAdminStateAndPayloadError;
}

export interface ILoginAdminErrorPayload {
  message: string;
}

export interface ILoginAdminSuccess {
  type: typeof LOGIN_ADMIN_SUCCESS;
  payload: object;
}
