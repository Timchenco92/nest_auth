// Types
import type {
  IGetMe,
  IGetMeError,
  IGetMeState,
  IGetMeSuccess,
  ILoginAdmin,
  ILoginAdminError,
  ILoginAdminState,
  ILoginAdminSuccess,
  IResetLoginAdminError,
  IResetLoginAdminSuccess,
} from '@/store/auth';

export interface IAuthState {
  login: ILoginAdminState;
  admin: IGetMeState;
}

export type TAuthTypes =
  IGetMe
  | IGetMeError
  | IGetMeSuccess
  | ILoginAdmin
  | ILoginAdminError
  | ILoginAdminSuccess
  | IResetLoginAdminError
  | IResetLoginAdminSuccess;
