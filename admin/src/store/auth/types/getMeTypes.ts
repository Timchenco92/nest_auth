// Action Types
import { GET_ME, GET_ME_ERROR, GET_ME_SUCCESS } from '@/store/auth';

// Interfaces
import type { IAdmin } from '@/interfaces/models';

export interface IGetMeState {
  data: IAdmin;
  error: boolean;
  pending: boolean;
  isAuth: boolean;
}

export interface IGetMe {
  type: typeof GET_ME;
}

export interface IGetMeError {
  type: typeof GET_ME_ERROR;
  isAuth: boolean;
}

export interface IGetMeSuccess {
  type: typeof GET_ME_SUCCESS;
  payload: IGetMeSuccessPayload;
}

export interface IGetMeSuccessPayload {
  isAuth: boolean;
  data: IAdmin;
}
