// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect } from 'redux-saga/effects';

// Actions
import {
  getMeError,
  getMeSuccess,
  loginAdminError,
  loginAdminRequest,
  loginAdminSuccess,
} from '@/store/auth';
import { showToast } from '@/store/toast';

// Action Types
import {
  GET_ME,
  LOGIN_ADMIN,
} from '@/store/auth';

// Services
import { authService } from '@/services';

const { getMe, login } = authService;

function* loginAdminSaga({ payload }: ReturnType<typeof loginAdminRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => login(payload));
    yield put(loginAdminSuccess(data));
    yield call(() => localStorage.setItem('access_token', data?.accessToken));
    yield call(getMeSaga);
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const message = error.response?.data?.message;
      const errors = error.response?.data?.errors;
      yield put(loginAdminError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getMeSaga(): Generator<CallEffect<AxiosResponse | AxiosError>
  | PutEffect
  | ForkEffect
  | CallEffect,
  AxiosResponse | AxiosError,
  never> {
  try {
    const { data }: AxiosResponse = yield call(() => getMe());
    yield put(getMeSuccess({ isAuth: true, data }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance && error?.response?.status === 401) {
      yield call(() => localStorage.removeItem('access_token'));
      yield put(getMeError({ isAuth: false }));
    }
    return error;
  }
}

export function* authSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_ME, getMeSaga)]);
  yield all([takeLatest(LOGIN_ADMIN, loginAdminSaga)]);
}
