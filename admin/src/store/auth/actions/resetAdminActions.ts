// Action Types
import { RESET_LOGIN_ADMIN_ERROR, RESET_LOGIN_ADMIN_SUCCESS } from '@/store/auth';

// Types
import type { IResetLoginAdminError, IResetLoginAdminSuccess } from '@/store/auth';

export const resetLoginAdminError = (): IResetLoginAdminError => ({
  type: RESET_LOGIN_ADMIN_ERROR,
});

export const resetLoginAdminSuccess = (): IResetLoginAdminSuccess => ({
  type: RESET_LOGIN_ADMIN_SUCCESS,
});
