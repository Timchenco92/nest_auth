// Action Types
import { GET_ME, GET_ME_ERROR, GET_ME_SUCCESS } from '@/store/auth';

// Types
import { IGetMe, IGetMeError, IGetMeSuccess, IGetMeSuccessPayload } from '@/store/auth';

export const getMeRequest = (): IGetMe => ({
  type: GET_ME,
});

export const getMeError = ({ isAuth = false }): IGetMeError => ({
  type: GET_ME_ERROR,
  isAuth,
});

export const getMeSuccess = (payload: IGetMeSuccessPayload): IGetMeSuccess => ({
  type: GET_ME_SUCCESS,
  payload,
});
