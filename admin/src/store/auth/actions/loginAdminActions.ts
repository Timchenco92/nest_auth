// Action Types
import {
  LOGIN_ADMIN,
  LOGIN_ADMIN_ERROR,
  LOGIN_ADMIN_SUCCESS,
} from '@/store/auth';

// Types
import {
  ILoginAdmin,
  ILoginAdminError,
  ILoginAdminPayload,
  ILoginAdminSuccess,
  ILoginAdminStateAndPayloadError,
} from '@/store/auth';

export const loginAdminRequest = (payload: ILoginAdminPayload): ILoginAdmin => ({
  type: LOGIN_ADMIN,
  payload,
});

export const loginAdminError = (payload: ILoginAdminStateAndPayloadError): ILoginAdminError => ({
  type: LOGIN_ADMIN_ERROR,
  payload,
});

export const loginAdminSuccess = (payload: object): ILoginAdminSuccess => ({
  type: LOGIN_ADMIN_SUCCESS,
  payload,
});
