// Modules
import { combineReducers } from 'redux';

// Reducers
import { authReducer } from '@/store/auth';
import { bannerReducer } from '@/store/banners';
import { categoryReducer } from '@/store/categories';
import { homeReducer } from '@/store/home';
import { orderProductReducer } from '@/store/order-products';
import { orderReducer } from '@/store/orders';
import { productGalleryReducer } from '@/store/product-gallery';
import { productReducer } from '@/store/products';
import { shopReducer } from '@/store/shops';
import { shopGalleryReducer } from '@/store/shop-gallery';
import { settingsReducer } from '@/store/settings';
import { toastReducer } from '@/store/toast';
import { userReducer } from '@/store/users';

const rootReducer = combineReducers({
  auth: authReducer,
  banner: bannerReducer,
  category: categoryReducer,
  home: homeReducer,
  orderProducts: orderProductReducer,
  orders: orderReducer,
  product: productReducer,
  productGallery: productGalleryReducer,
  settings: settingsReducer,
  shops:  shopReducer,
  shopGallery:  shopGalleryReducer,
  toast:  toastReducer,
  user:  userReducer,
});

export type AuthState = ReturnType<typeof rootReducer>;
export type BannerState = ReturnType<typeof rootReducer>;
export type CategoryState = ReturnType<typeof rootReducer>;
export type HomeState = ReturnType<typeof rootReducer>;
export type OrderState = ReturnType<typeof rootReducer>;
export type OrderProductState = ReturnType<typeof rootReducer>;
export type ProductState = ReturnType<typeof rootReducer>;
export type ProductGalleryState = ReturnType<typeof rootReducer>;
export type SettingsState = ReturnType<typeof rootReducer>;
export type ShopsState = ReturnType<typeof rootReducer>;
export type ShopGalleryState = ReturnType<typeof rootReducer>;
export type ToastState = ReturnType<typeof rootReducer>;
export type UserState = ReturnType<typeof rootReducer>;


export default rootReducer;
