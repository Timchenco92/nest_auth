// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, delay, put, select, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect, SelectEffect } from 'redux-saga/effects';

// Actions
import {
  addCategoryError,
  addCategoryRequest,
  addCategorySuccess,
  deleteCategoryError,
  deleteCategoryRequest,
  deleteCategorySuccess,
  getCategoriesError,
  getCategoriesRequest,
  getCategoriesSuccess,
  getCategoryError,
  getCategoryRequest,
  getCategorySuccess,
  updateCategoryError,
  updateCategoryRequest,
  updateCategorySuccess,
} from '@/store/categories';

import { showToast } from '@/store/toast';

// Action Types
import {
  ADD_CATEGORY,
  DELETE_CATEGORY,
  GET_CATEGORIES,
  GET_CATEGORY,
  UPDATE_CATEGORY,
} from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';
import type { IGetCategoriesSuccessStateAndPayload } from '@/store/categories';

// Selectors
import { paginationAndQuerySearchSelector } from '@/store/categories';

// Services
import { categoryService } from '@/services';

const { addCategory, deleteCategory, getCategories, getCategory, updateCategory } = categoryService;

function* addCategorySaga({ payload }: ReturnType<typeof addCategoryRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => addCategory(payload));
    yield put(addCategorySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getCategoriesSaga, { limit, page, query, type: GET_CATEGORIES });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addCategoryError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteCategorySaga({ categoryId }: ReturnType<typeof deleteCategoryRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => deleteCategory(categoryId));
    yield put(deleteCategorySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getCategoriesSaga, { limit, page, query, type: GET_CATEGORIES });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteCategoryError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getCategoriesSaga({ limit, page, query }: ReturnType<typeof getCategoriesRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    if (query !== '') yield delay(1000);
    const response: AxiosResponse<IGetCategoriesSuccessStateAndPayload> = yield call(() => getCategories({
      limit,
      page,
      query,
    }));
    const { data: { data: categoriesData, meta, links } } = response;
    yield put(getCategoriesSuccess({ data: categoriesData, meta, links }));
    return response;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getCategoriesError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getCategorySaga({ categoryId, limit, page }: ReturnType<typeof getCategoryRequest>):
  Generator<CallEffect<AxiosResponse<ICategory>
      | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const response: AxiosResponse<ICategory> = yield call(() => getCategory({ categoryId, limit, page }));
    const { data } = response;
    yield put(getCategorySuccess(data));
    return response;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getCategoryError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* updateCategorySaga({ payload }: ReturnType<typeof updateCategoryRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { categoryId, data: updatedData } = payload;
    const { data }: AxiosResponse = yield call(() => updateCategory(categoryId, updatedData));
    yield put(updateCategorySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getCategoriesSaga, { limit, page, query, type: GET_CATEGORIES });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(updateCategoryError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* categorySaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_CATEGORY, addCategorySaga)]);
  yield all([takeLatest(DELETE_CATEGORY, deleteCategorySaga)]);
  yield all([takeLatest(GET_CATEGORIES, getCategoriesSaga)]);
  yield all([takeLatest(GET_CATEGORY, getCategorySaga)]);
  yield all([takeLatest(UPDATE_CATEGORY, updateCategorySaga)]);
}
