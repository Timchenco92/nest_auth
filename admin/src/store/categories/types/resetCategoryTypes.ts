// Action Types
import {
  RESET_ADD_CATEGORY_ERROR,
  RESET_ADD_CATEGORY_SUCCESS,
  RESET_DELETE_CATEGORY_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_CATEGORY_ERROR,
  RESET_UPDATE_CATEGORY_SUCCESS,
} from '@/store/categories';

export interface IResetAddCategoryError {
  type: typeof RESET_ADD_CATEGORY_ERROR;
}

export interface IResetAddCategorySuccess {
  type: typeof RESET_ADD_CATEGORY_SUCCESS;
}

export interface IResetDeleteCategorySuccess {
  type: typeof RESET_DELETE_CATEGORY_SUCCESS;
}

export interface IResetPagination {
  type: typeof RESET_PAGINATION;
}

export interface IResetQuerySearch {
  type: typeof RESET_QUERY_SEARCH;
}

export interface IResetUpdateCategoryError {
  type: typeof RESET_UPDATE_CATEGORY_ERROR;
}

export interface IResetUpdateCategorySuccess {
  type: typeof RESET_UPDATE_CATEGORY_SUCCESS;
}
