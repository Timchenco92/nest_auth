// Action Types
import { GET_CATEGORY, GET_CATEGORY_ERROR, GET_CATEGORY_SUCCESS } from '@/store/categories';

// Interfaces
import { ICategory } from '@/interfaces/models';

export interface IGetCategoryState {
  data: ICategory;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetCategory {
  type: typeof GET_CATEGORY;
  categoryId: ICategory['id'];
  limit: string;
  page: string;
}

export interface IGetCategoryError {
  type: typeof GET_CATEGORY_ERROR;
}

export interface IGetCategorySuccess {
  type: typeof GET_CATEGORY_SUCCESS;
  data: ICategory;
}
