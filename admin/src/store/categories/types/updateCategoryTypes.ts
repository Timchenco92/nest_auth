// Action Types
import { UPDATE_CATEGORY, UPDATE_CATEGORY_ERROR, UPDATE_CATEGORY_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';

export interface IUpdateCategoryState {
  error: IUpdateCategoryErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IUpdateCategory {
  type: typeof UPDATE_CATEGORY;
  payload: IUpdateCategoryPayload;
}

export interface IUpdateCategoryPayload {
  categoryId: ICategory['id'];
  data: FormData;
}

export interface IUpdateCategoryError {
  type: typeof UPDATE_CATEGORY_ERROR;
  payload: IUpdateCategoryErrorStateAndPayload;
}

export interface IUpdateCategoryErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

export interface IErrors {
  image: string;
  title: string;
}

export interface IUpdateCategorySuccess {
  type: typeof UPDATE_CATEGORY_SUCCESS;
}
