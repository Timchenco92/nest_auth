// Action Types
import { GET_CATEGORIES, GET_CATEGORIES_ERROR, GET_CATEGORIES_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';

export interface IGetCategoriesState {
  data: IGetCategoriesSuccessStateAndPayload;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetCategories {
  type: typeof GET_CATEGORIES;
  limit: string;
  page: string;
  query: string;
}

export interface IGetCategoriesError {
  type: typeof GET_CATEGORIES_ERROR;
}

export interface IGetCategoriesSuccess {
  type: typeof GET_CATEGORIES_SUCCESS;
  payload: IGetCategoriesSuccessStateAndPayload;
}

export interface IGetCategoriesSuccessStateAndPayload {
  data: ICategory[];
  meta: any;
  links: any;
}
