// Action Types
import { ADD_CATEGORY, ADD_CATEGORY_ERROR, ADD_CATEGORY_SUCCESS } from '@/store/categories';

export interface IAddCategoryState {
  error: IAddCategoryErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddCategory {
  type: typeof ADD_CATEGORY;
  payload: FormData;
}

export interface IAddCategorySuccess {
  type: typeof ADD_CATEGORY_SUCCESS;
}

export interface IAddCategoryError {
  type: typeof ADD_CATEGORY_ERROR;
  payload: IAddCategoryErrorStateAndPayload;
}

export interface IAddCategoryErrorStateAndPayload {
  errors: IErrors;
  hasError: boolean;
}

export interface IErrors {
  image: string;
  title: string;
}
