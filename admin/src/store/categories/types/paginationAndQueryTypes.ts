// Action Types
import { PAGINATION_CHANGE_LIMIT, PAGINATION_CHANGE_PAGE, SET_QUERY_SEARCH } from '@/store/categories';

export interface IPaginationAndQueryState {
  limit: string;
  page: string;
  query: string;
}

export interface IChangeLimit {
  type: typeof PAGINATION_CHANGE_LIMIT;
  limit: string;
}

export interface IChangePage {
  type: typeof PAGINATION_CHANGE_PAGE;
  page: string;
}

export interface ISetQuerySearch {
  type: typeof SET_QUERY_SEARCH;
  query: string;
}
