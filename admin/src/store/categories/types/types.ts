// Types
import type {
  IAddCategory,
  IAddCategoryError,
  IAddCategoryState,
  IAddCategorySuccess,
  IChangeLimit,
  IChangePage,
  IDeleteCategory,
  IDeleteCategoryError,
  IDeleteCategoryState,
  IDeleteCategorySuccess,
  IGetCategories,
  IGetCategoriesError,
  IGetCategoriesState,
  IGetCategoriesSuccess,
  IGetCategory,
  IGetCategoryError,
  IGetCategoryState,
  IGetCategorySuccess,
  IPaginationAndQueryState,
  IResetAddCategoryError,
  IResetAddCategorySuccess,
  IResetDeleteCategorySuccess,
  IResetPagination,
  IResetQuerySearch,
  IResetUpdateCategoryError,
  IResetUpdateCategorySuccess,
  ISetQuerySearch,
  IUpdateCategory,
  IUpdateCategoryError,
  IUpdateCategoryState,
  IUpdateCategorySuccess,
} from '@/store/categories';

export interface ICategoryState {
  addCategory: IAddCategoryState;
  deleteCategory: IDeleteCategoryState;
  categories: IGetCategoriesState;
  category: IGetCategoryState;
  updateCategory: IUpdateCategoryState;
  pagination: IPaginationAndQueryState;
}

export type TCategoryTypes = IAddCategory
  | IAddCategoryError
  | IAddCategorySuccess
  | IChangeLimit
  | IChangePage
  | IDeleteCategory
  | IDeleteCategoryError
  | IDeleteCategorySuccess
  | IGetCategory
  | IGetCategoryError
  | IGetCategorySuccess
  | IGetCategories
  | IGetCategoriesError
  | IGetCategoriesSuccess
  | IUpdateCategory
  | IUpdateCategoryError
  | IUpdateCategorySuccess
  | IResetUpdateCategorySuccess
  | ISetQuerySearch
  | IResetQuerySearch
  | IResetPagination
  | IResetDeleteCategorySuccess
  | IResetAddCategorySuccess
  | IResetAddCategoryError
  | IResetUpdateCategoryError;
