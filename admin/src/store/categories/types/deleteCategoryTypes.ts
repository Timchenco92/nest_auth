// Action Types
import { DELETE_CATEGORY, DELETE_CATEGORY_ERROR, DELETE_CATEGORY_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';

export interface IDeleteCategoryState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteCategory {
  type: typeof DELETE_CATEGORY;
  categoryId: ICategory['id'];
}

export interface IDeleteCategoryError {
  type: typeof DELETE_CATEGORY_ERROR;
}

export interface IDeleteCategorySuccess {
  type: typeof DELETE_CATEGORY_SUCCESS;
}
