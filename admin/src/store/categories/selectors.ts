// Modules
import { createSelector } from 'reselect';

// State
import type { CategoryState } from '@/store/rootReducer';

const addCategory = (state: CategoryState) => ({
  error: state.category.addCategory.error,
  pending: state.category.addCategory.pending,
  success: state.category.addCategory.success,
});

const deleteCategory = (state: CategoryState) => ({
  error: state.category.deleteCategory.error,
  pending: state.category.deleteCategory.pending,
  success: state.category.deleteCategory.success,
});

const getCategories = (state: CategoryState) => ({
  data: state.category.categories.data,
  error: state.category.categories.error,
  pending: state.category.categories.pending,
  success: state.category.categories.success,
});

const getCategory = (state: CategoryState) => ({
  data: state.category.category.data,
  error: state.category.category.error,
  pending: state.category.category.pending,
  success: state.category.category.success,
});

const paginationAndSearch = (state: CategoryState) => ({
  limit: state.category.pagination.limit,
  page: state.category.pagination.page,
  query: state.category.pagination.query,
});

const updateCategory = (state: CategoryState) => ({
  error: state.category.updateCategory.error,
  pending: state.category.updateCategory.pending,
  success: state.category.updateCategory.success,
})

export const addCategoryErrorSelector = createSelector(addCategory, ({ error }) => error);
export const addCategoryPendingSelector = createSelector(addCategory, ({ pending }) => pending);
export const addCategorySuccessSelector = createSelector(addCategory, ({ success }) => success);

export const deleteCategoryErrorSelector = createSelector(deleteCategory, ({ error }) => error);
export const deleteCategoryPendingSelector = createSelector(deleteCategory, ({ pending }) => pending);
export const deleteCategorySuccessSelector = createSelector(deleteCategory, ({ success }) => success);

export const categoriesSelector = createSelector(getCategories, ({ data }) => data);
export const categoriesErrorSelector = createSelector(getCategories, ({ error }) => error);
export const categoriesPendingSelector = createSelector(getCategories, ({ pending }) => pending);
export const categoriesSuccessSelector = createSelector(getCategories, ({ success }) => success);

export const categorySelector = createSelector(getCategory, ({ data }) => data);
export const categoryErrorSelector = createSelector(getCategory, ({ error }) => error);
export const categoryPendingSelector = createSelector(getCategory, ({ pending }) => pending);
export const categorySuccessSelector = createSelector(getCategory, ({ success }) => success);

export const paginationAndQuerySearchSelector = createSelector(
  paginationAndSearch, ({ limit, page, query }) => ({ limit, page, query }),
);

export const updateCategoryErrorSelector = createSelector(updateCategory, ({ error }) => error);
export const updateCategoryPendingSelector = createSelector(updateCategory, ({ pending }) => pending);
export const updateCategorySuccessSelector = createSelector(updateCategory, ({ success }) => success);
