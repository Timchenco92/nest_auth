// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_CATEGORY,
  ADD_CATEGORY_ERROR,
  ADD_CATEGORY_SUCCESS,
  DELETE_CATEGORY,
  DELETE_CATEGORY_ERROR,
  DELETE_CATEGORY_SUCCESS,
  GET_CATEGORIES,
  GET_CATEGORIES_ERROR,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORY,
  GET_CATEGORY_ERROR,
  GET_CATEGORY_SUCCESS,
  PAGINATION_CHANGE_LIMIT,
  PAGINATION_CHANGE_PAGE,
  RESET_ADD_CATEGORY_ERROR,
  RESET_ADD_CATEGORY_SUCCESS,
  RESET_DELETE_CATEGORY_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_CATEGORY_ERROR,
  RESET_UPDATE_CATEGORY_SUCCESS,
  SET_QUERY_SEARCH,
  UPDATE_CATEGORY,
  UPDATE_CATEGORY_ERROR,
  UPDATE_CATEGORY_SUCCESS,
} from '@/store/categories';

// Types
import type { ICategoryState, TCategoryTypes } from '@/store/categories';

const initialState: ICategoryState = {
  addCategory: {
    error: {
      hasError: false,
      errors: {
        image: '',
        title: '',
      },
    },
    pending: false,
    success: false,
  },
  category: {
    error: false,
    success: false,
    pending: false,
    data: {
      id: '',
      title: '',
      image: '',
      products: {
        data: [],
        meta: {},
        links: {},
      },
      productsCount: 0,
      createdAt: new Date(),
      imageUrl: '',
      updatedAt: new Date(),
    },
  },
  categories: {
    error: false,
    success: false,
    pending: false,
    data: {
      data: [],
      meta: {},
      links: {},
    },
  },
  deleteCategory: {
    error: false,
    success: false,
    pending: false,
  },
  pagination: {
    limit: '15',
    page: '1',
    query: '',
  },
  updateCategory: {
    error: {
      hasError: false,
      errors: {
        image: '',
        title: '',
      },
    },
    success: false,
    pending: false,
  },
};

export const categoryReducer: Reducer<ICategoryState, TCategoryTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CATEGORY: {
      return {
        ...state,
        addCategory: {
          error: { ...state.addCategory.error },
          success: false,
          pending: true,
        },
      };
    }
    case ADD_CATEGORY_ERROR: {
      return {
        ...state,
        addCategory: {
          error: {
            hasError: true,
            errors: action.payload.errors,
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_CATEGORY_SUCCESS: {
      return {
        ...state,
        addCategory: {
          error: { ...state.addCategory.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_CATEGORY: {
      return {
        ...state,
        deleteCategory: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_CATEGORY_ERROR: {
      return {
        ...state,
        deleteCategory: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_CATEGORY_SUCCESS: {
      return {
        ...state,
        deleteCategory: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_CATEGORIES: {
      return {
        ...state,
        categories: {
          data: { ...state.categories.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_CATEGORIES_ERROR: {
      return {
        ...state,
        categories: {
          data: { ...state.categories.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_CATEGORIES_SUCCESS: {
      return {
        ...state,
        categories: {
          data: {
            data: action.payload.data,
            meta: action.payload.meta,
            links: action.payload.links,
          },
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_CATEGORY: {
      return {
        ...state,
        category: {
          data: { ...state.category.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_CATEGORY_ERROR: {
      return {
        ...state,
        category: {
          data: { ...state.category.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_CATEGORY_SUCCESS: {
      return {
        ...state,
        category: {
          data: action.data,
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case PAGINATION_CHANGE_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: action.limit,
          page: state.pagination.page,
          query: state.pagination.query,
        },
      };
    }
    case PAGINATION_CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: action.page,
          query: state.pagination.query,
        },
      };
    }
    case RESET_ADD_CATEGORY_SUCCESS: {
      return {
        ...state,
        addCategory: {
          error: { ...state.addCategory.error },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_DELETE_CATEGORY_SUCCESS: {
      return {
        ...state,
        deleteCategory: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case RESET_PAGINATION: {
      return {
        ...state,
        pagination: {
          limit: '15',
          page: '1',
          query: '',
        },
      };
    }
    case RESET_QUERY_SEARCH: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: state.pagination.page,
          query: '',
        },
      };
    }
    case RESET_UPDATE_CATEGORY_SUCCESS: {
      return {
        ...state,
        updateCategory: {
          error: { ...state.updateCategory.error },
          pending: false,
          success: false,
        },
      };
    }
    case SET_QUERY_SEARCH: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: state.pagination.page,
          query: action.query,
        },
      };
    }
    case UPDATE_CATEGORY: {
      return {
        ...state,
        updateCategory: {
          error: { ...state.updateCategory.error },
          pending: true,
          success: false,
        },
      };
    }
    case UPDATE_CATEGORY_ERROR: {
      return {
        ...state,
        updateCategory: {
          error: {
            hasError: true, errors: {
              image: action.payload.errors.image,
              title: action.payload.errors.title,
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_CATEGORY_SUCCESS: {
      return {
        ...state,
        updateCategory: {
          error: { ...state.updateCategory.error },
          pending: false,
          success: true,
        },
      };
    }
    case RESET_ADD_CATEGORY_ERROR: {
      return {
        ...state,
        addCategory: {
          error: {
            hasError: false,
            errors: {
              image: '',
              title: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_UPDATE_CATEGORY_ERROR: {
      return {
        ...state,
        updateCategory: {
          error: {
            hasError: false,
            errors: {
              image: '',
              title: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    default: {
      return state;
    }
  }
};
