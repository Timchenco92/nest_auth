// Action Types
import {
  RESET_ADD_CATEGORY_ERROR,
  RESET_ADD_CATEGORY_SUCCESS,
  RESET_DELETE_CATEGORY_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_CATEGORY_ERROR,
  RESET_UPDATE_CATEGORY_SUCCESS,
} from '@/store/categories';

// Types
import type {
  IResetAddCategoryError,
  IResetAddCategorySuccess,
  IResetDeleteCategorySuccess,
  IResetPagination,
  IResetQuerySearch,
  IResetUpdateCategoryError,
  IResetUpdateCategorySuccess,
} from '@/store/categories';

export const resetAddCategorySuccess = (): IResetAddCategorySuccess => ({
  type: RESET_ADD_CATEGORY_SUCCESS,
});

export const resetDeleteCategorySuccess = (): IResetDeleteCategorySuccess => ({
  type: RESET_DELETE_CATEGORY_SUCCESS,
});

export const resetPagination = (): IResetPagination => ({
  type: RESET_PAGINATION,
});

export const resetQuerySearch = (): IResetQuerySearch => ({
  type: RESET_QUERY_SEARCH,
});

export const resetUpdateCategorySuccess = (): IResetUpdateCategorySuccess => ({
  type: RESET_UPDATE_CATEGORY_SUCCESS,
});

export const resetAddCategoryError = (): IResetAddCategoryError => ({
  type: RESET_ADD_CATEGORY_ERROR,
});

export const resetUpdateCategoryError = (): IResetUpdateCategoryError => ({
  type: RESET_UPDATE_CATEGORY_ERROR,
});
