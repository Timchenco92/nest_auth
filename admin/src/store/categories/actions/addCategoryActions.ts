// Action Types
import { ADD_CATEGORY, ADD_CATEGORY_ERROR, ADD_CATEGORY_SUCCESS } from '@/store/categories';

// Types
import type {
  IAddCategory,
  IAddCategoryError,
  IAddCategorySuccess,
  IAddCategoryErrorStateAndPayload,
} from '@/store/categories';

export const addCategoryRequest = (payload: FormData): IAddCategory => ({
  type: ADD_CATEGORY,
  payload,
});

export const addCategoryError = (payload: IAddCategoryErrorStateAndPayload): IAddCategoryError => ({
  type: ADD_CATEGORY_ERROR,
  payload,
});

export const addCategorySuccess = (): IAddCategorySuccess => ({
  type: ADD_CATEGORY_SUCCESS,
});
