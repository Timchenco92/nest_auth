// Action Types
import { GET_CATEGORY, GET_CATEGORY_ERROR, GET_CATEGORY_SUCCESS } from '@/store/categories';

// Types
import type {
  IGetCategory,
  IGetCategoryError,
  IGetCategorySuccess,
} from '@/store/categories';
import { ICategory } from '@/interfaces/models';

export const getCategoryRequest = (categoryId: ICategory['id'], limit: string, page: string): IGetCategory => ({
  type: GET_CATEGORY,
  categoryId,
  limit,
  page,
});

export const getCategoryError = (): IGetCategoryError => ({
  type: GET_CATEGORY_ERROR,
});

export const getCategorySuccess = (data: ICategory): IGetCategorySuccess => ({
  type: GET_CATEGORY_SUCCESS,
  data,
});
