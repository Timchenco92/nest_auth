// Action Types
import { UPDATE_CATEGORY, UPDATE_CATEGORY_ERROR, UPDATE_CATEGORY_SUCCESS } from '@/store/categories';

// Types
import type {
  IUpdateCategory,
  IUpdateCategoryError,
  IUpdateCategoryErrorStateAndPayload,
  IUpdateCategoryPayload,
  IUpdateCategorySuccess,
} from '@/store/categories';

export const updateCategoryRequest = (payload: IUpdateCategoryPayload): IUpdateCategory => ({
  type: UPDATE_CATEGORY,
  payload,
});

export const updateCategoryError = (payload: IUpdateCategoryErrorStateAndPayload): IUpdateCategoryError => ({
  type: UPDATE_CATEGORY_ERROR,
  payload,
});

export const updateCategorySuccess = (): IUpdateCategorySuccess => ({
  type: UPDATE_CATEGORY_SUCCESS,
});
