// Action Types
import { GET_CATEGORIES, GET_CATEGORIES_ERROR, GET_CATEGORIES_SUCCESS } from '@/store/categories';

// Types
import type {
  IGetCategories,
  IGetCategoriesError,
  IGetCategoriesSuccess,
  IGetCategoriesSuccessStateAndPayload,
} from '@/store/categories';

export const getCategoriesRequest = (limit: string, page: string, query: string): IGetCategories => ({
  type: GET_CATEGORIES,
  limit,
  page,
  query,
});

export const getCategoriesError = (): IGetCategoriesError => ({
  type: GET_CATEGORIES_ERROR,
});

export const getCategoriesSuccess = (payload: IGetCategoriesSuccessStateAndPayload): IGetCategoriesSuccess => ({
  type: GET_CATEGORIES_SUCCESS,
  payload,
});
