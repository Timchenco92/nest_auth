// Action Types
import { DELETE_CATEGORY, DELETE_CATEGORY_ERROR, DELETE_CATEGORY_SUCCESS } from '@/store/categories';

// Interfaces
import { ICategory } from '@/interfaces/models';

// Types
import type { IDeleteCategory, IDeleteCategoryError, IDeleteCategorySuccess } from '@/store/categories';

export const deleteCategoryRequest = (categoryId: ICategory['id']): IDeleteCategory => ({
  type: DELETE_CATEGORY,
  categoryId,
});

export const deleteCategoryError = (): IDeleteCategoryError => ({
  type: DELETE_CATEGORY_ERROR,
});

export const deleteCategorySuccess = (): IDeleteCategorySuccess => ({
  type: DELETE_CATEGORY_SUCCESS,
});
