// Action Types
import {
  RESET_ADD_USER_ERROR,
  RESET_ADD_USER_SUCCESS,
  RESET_DELETE_USER_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_USER_ERROR,
  RESET_UPDATE_USER_SUCCESS,
} from '@/store/users';

export interface IResetAddUserError {
  type: typeof RESET_ADD_USER_ERROR;
}

export interface IResetAddUserSuccess {
  type: typeof RESET_ADD_USER_SUCCESS;
}

export interface IResetDeleteUserSuccess {
  type: typeof RESET_DELETE_USER_SUCCESS;
}

export interface IResetPaginationLimit {
  type: typeof RESET_PAGINATION_LIMIT;
}

export interface IResetPaginationPage {
  type: typeof RESET_PAGINATION_PAGE;
}

export interface IResetQuerySearch {
  type: typeof RESET_QUERY_SEARCH;
}

export interface IResetUpdateUserError {
  type: typeof RESET_UPDATE_USER_ERROR;
}

export interface IResetUpdateUserSuccess {
  type: typeof RESET_UPDATE_USER_SUCCESS;
}
