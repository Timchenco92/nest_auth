// Types
import {
  IAddUser,
  IAddUserError,
  IAddUserState,
  IAddUserSuccess,
  IChangeLimit,
  IChangePage,
  IDeleteUser,
  IDeleteUserError,
  IDeleteUserState,
  IDeleteUserSuccess,
  IGetUser,
  IGetUserError,
  IGetUserState,
  IGetUserSuccess,
  IGetUsers,
  IGetUsersError,
  IGetUsersState,
  IGetUsersSuccess,
  IPaginationAndQueryState,
  IResetAddUserError,
  IResetAddUserSuccess,
  IResetDeleteUserSuccess,
  IResetPaginationLimit,
  IResetPaginationPage,
  IResetQuerySearch,
  IResetUpdateUserError,
  IResetUpdateUserSuccess,
  ISetQuerySearch,
  IUpdateUser,
  IUpdateUserError,
  IUpdateUserState,
  IUpdateUserSuccess,
} from '@/store/users';

export interface IUserState {
  addUser: IAddUserState;
  deleteUser: IDeleteUserState;
  user: IGetUserState;
  users: IGetUsersState;
  updateUser: IUpdateUserState;
  pagination: IPaginationAndQueryState;
}

export type TUserTypes = IAddUser
| IAddUserError
| IAddUserSuccess
| IDeleteUser
| IDeleteUserError
| IDeleteUserSuccess
| IGetUser
| IGetUserError
| IGetUserSuccess
| IGetUsers
| IGetUsersError
| IGetUsersSuccess
| IChangeLimit
| IChangePage
| ISetQuerySearch
| IUpdateUser
| IUpdateUserError
| IUpdateUserSuccess
| IResetAddUserError
| IResetAddUserSuccess
| IResetDeleteUserSuccess
| IResetPaginationLimit
| IResetPaginationPage
| IResetQuerySearch
| IResetUpdateUserError
| IResetUpdateUserSuccess;
