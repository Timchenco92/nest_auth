// Action Types
import { GET_USERS, GET_USERS_ERROR, GET_USERS_SUCCESS } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

export interface IGetUsersState {
  data: IGetUsersSuccessStateAndPayload;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetUsers {
  type: typeof GET_USERS;
  limit: string;
  page: string;
  query: string;
}

export interface IGetUsersError {
  type: typeof GET_USERS_ERROR;
}

export interface IGetUsersSuccess {
  type: typeof GET_USERS_SUCCESS;
  payload: IGetUsersSuccessStateAndPayload;
}

export interface IGetUsersSuccessStateAndPayload {
  data: IUser[];
  meta: any;
  links: any;
}
