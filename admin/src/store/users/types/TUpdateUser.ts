// Action Types
import { UPDATE_USER, UPDATE_USER_ERROR, UPDATE_USER_SUCCESS } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

export interface IUpdateUserState {
  error: IUpdateUserErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IUpdateUser {
  type: typeof UPDATE_USER;
  userId: IUser['id'];
  data: FormData;
}

export interface IUpdateUserError {
  type: typeof UPDATE_USER_ERROR;
  payload: IUpdateUserErrorStateAndPayload
}

export interface IUpdateUserErrorStateAndPayload {
  hasErrors: boolean;
  errors: IErrors
}

interface IErrors {
  avatar: string;
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  phone: string;
  address: string;
  password: string;
}

export interface IUpdateUserSuccess {
  type: typeof UPDATE_USER_SUCCESS;
}
