// Action Types
import { ADD_USER, ADD_USER_ERROR, ADD_USER_SUCCESS } from '@/store/users';

export interface IAddUserState {
  error: IAddUserStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddUser {
  type: typeof ADD_USER;
  payload: FormData;
}

export interface IAddUserError {
  type: typeof ADD_USER_ERROR;
  payload: IAddUserStateAndPayload;
}

export interface IAddUserStateAndPayload {
  errors: IErrors;
  hasError: boolean;
}

interface IErrors {
  address: string;
  avatar: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  password: string;
  phone: string;
}

export interface IAddUserSuccess {
  type: typeof ADD_USER_SUCCESS;
}
