// Action Types
import { DELETE_USER, DELETE_USER_ERROR, DELETE_USER_SUCCESS } from '@/store/users';

// interfaces
import type { IUser } from '@/interfaces/models';

export interface IDeleteUserState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteUser {
  type: typeof DELETE_USER;
  userId: IUser['id'];
}

export interface IDeleteUserError {
  type: typeof DELETE_USER_ERROR;
}

export interface IDeleteUserSuccess {
  type: typeof DELETE_USER_SUCCESS;
}
