// Action Types
import { GET_USER, GET_USER_ERROR, GET_USER_SUCCESS } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

export interface IGetUserState {
  data: IUser;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetUser {
  type: typeof GET_USER;
  userId: IUser['id'];
  limit: string;
  page: string;
}

export interface IGetUserError {
  type: typeof GET_USER_ERROR;
}

export interface IGetUserSuccess {
  type: typeof GET_USER_SUCCESS;
  payload: IUser;
}
