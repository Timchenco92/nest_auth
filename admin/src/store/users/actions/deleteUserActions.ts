// Action Types
import { DELETE_USER, DELETE_USER_ERROR, DELETE_USER_SUCCESS } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

// Types
import type { IDeleteUser, IDeleteUserError, IDeleteUserSuccess } from '@/store/users';

export const deleteUserRequest = (userId: IUser['id']): IDeleteUser => ({
  type: DELETE_USER,
  userId,
});

export const deleteUserError = (): IDeleteUserError => ({
  type: DELETE_USER_ERROR,
});

export const deleteUserSuccess = (): IDeleteUserSuccess => ({
  type: DELETE_USER_SUCCESS,
});
