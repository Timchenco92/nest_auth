// Action Types
import { UPDATE_USER, UPDATE_USER_ERROR, UPDATE_USER_SUCCESS } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

// Types
import type { IUpdateUser, IUpdateUserError, IUpdateUserSuccess, IUpdateUserErrorStateAndPayload } from '@/store/users';

export const updateUserRequest = (userId: IUser['id'], data: FormData): IUpdateUser => ({
  type: UPDATE_USER,
  userId,
  data
});

export const updateUserError = (payload: IUpdateUserErrorStateAndPayload): IUpdateUserError => ({
  type: UPDATE_USER_ERROR,
  payload,
});

export const updateUserSuccess = (): IUpdateUserSuccess => ({
  type: UPDATE_USER_SUCCESS,
});
