// Action Types
import { GET_USERS, GET_USERS_ERROR, GET_USERS_SUCCESS } from '@/store/users';

// Types
import type { IGetUsers, IGetUsersError, IGetUsersSuccess, IGetUsersSuccessStateAndPayload } from '@/store/users';

export const getUsersRequest = (limit: string, page: string, query: string): IGetUsers => ({
  type: GET_USERS,
  limit,
  page,
  query,
});

export const getUsersError = (): IGetUsersError => ({
  type: GET_USERS_ERROR,
});

export const getUsersSuccess = (payload: IGetUsersSuccessStateAndPayload): IGetUsersSuccess => ({
  type: GET_USERS_SUCCESS,
  payload,
});
