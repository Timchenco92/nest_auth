// Action Types
import { GET_USER, GET_USER_ERROR, GET_USER_SUCCESS } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

// Types
import type { IGetUser, IGetUserError, IGetUserSuccess } from '@/store/users';

export const getUserRequest = (userId: IUser['id'], limit: string, page: string): IGetUser => ({
  type: GET_USER,
  userId,
  limit,
  page,
});

export const getUserError = (): IGetUserError => ({
  type: GET_USER_ERROR,
});

export const getUserSuccess = (payload: IUser): IGetUserSuccess => ({
  type: GET_USER_SUCCESS,
  payload,
});
