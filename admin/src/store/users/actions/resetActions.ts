// Action Types
import {
  RESET_ADD_USER_ERROR,
  RESET_ADD_USER_SUCCESS,
  RESET_DELETE_USER_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_USER_ERROR,
  RESET_UPDATE_USER_SUCCESS,
} from '@/store/users';

// Types
import type {
  IResetAddUserError,
  IResetAddUserSuccess,
  IResetDeleteUserSuccess,
  IResetPaginationLimit,
  IResetPaginationPage,
  IResetQuerySearch,
  IResetUpdateUserError,
  IResetUpdateUserSuccess,
} from '@/store/users';

export const resetAddUserError = (): IResetAddUserError => ({
  type: RESET_ADD_USER_ERROR,
});

export const resetAddUserSuccess = (): IResetAddUserSuccess => ({
  type: RESET_ADD_USER_SUCCESS,
});

export const resetDeleteUserSuccess = (): IResetDeleteUserSuccess => ({
  type: RESET_DELETE_USER_SUCCESS,
});

export const resetPaginationLimit = (): IResetPaginationLimit => ({
  type: RESET_PAGINATION_LIMIT,
});

export const resetPaginationPage = (): IResetPaginationPage => ({
  type: RESET_PAGINATION_PAGE,
});

export const resetQuerySearch = (): IResetQuerySearch => ({
  type: RESET_QUERY_SEARCH,
});

export const resetUpdateUserError = (): IResetUpdateUserError => ({
  type: RESET_UPDATE_USER_ERROR,
});

export const resetUpdateUserSuccess = (): IResetUpdateUserSuccess => ({
  type: RESET_UPDATE_USER_SUCCESS,
});
