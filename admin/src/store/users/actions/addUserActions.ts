// Action Types
import { ADD_USER, ADD_USER_ERROR, ADD_USER_SUCCESS } from '@/store/users';

// Types
import type { IAddUser, IAddUserError, IAddUserSuccess, IAddUserStateAndPayload } from '@/store/users';

export const addUserRequest = (payload: FormData): IAddUser => ({
  type: ADD_USER,
  payload,
});

export const addUserError = (payload: IAddUserStateAndPayload): IAddUserError => ({
  type: ADD_USER_ERROR,
  payload,
});

export const addUserSuccess = (): IAddUserSuccess => ({
  type: ADD_USER_SUCCESS,
});
