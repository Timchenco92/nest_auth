// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, delay, put, select, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect, SelectEffect } from 'redux-saga/effects';

// Actions
import {
  addUserError,
  addUserRequest,
  addUserSuccess,
  deleteUserError,
  deleteUserRequest,
  deleteUserSuccess,
  getUserError,
  getUserRequest,
  getUserSuccess,
  getUsersError,
  getUsersRequest,
  getUsersSuccess,
  updateUserError,
  updateUserRequest,
  updateUserSuccess,
} from '@/store/users';

import { showToast } from '@/store/toast';

// Action Types
import { ADD_USER, DELETE_USER, GET_USER, GET_USERS, UPDATE_USER } from '@/store/users';

// Interfaces
import type { IUser } from '@/interfaces/models';

// Selectors
import { paginationAndQuerySearchSelector } from '@/store/users';

// Services
import { userService } from '@/services';

const { addUser, deleteUser, getUser, getUsers, updateUser } = userService;

function* addUserSaga({ payload }: ReturnType<typeof addUserRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect
    | ForkEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => addUser(payload));
    yield put(addUserSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getUsersSaga, { limit, page, query, type: GET_USERS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addUserError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteUserSaga({ userId }: ReturnType<typeof deleteUserRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect
    | ForkEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => deleteUser(userId));
    yield put(deleteUserSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getUsersSaga, { limit, page, query, type: GET_USERS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteUserError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getUserSaga({ userId, limit, page }: ReturnType<typeof getUserRequest>):
  Generator<CallEffect<AxiosResponse<IUser> | AxiosError>
    | PutEffect | ForkEffect,
    AxiosResponse | AxiosError, never> {
  try {
    const response: AxiosResponse<IUser> = yield call(() => getUser({ userId, limit, page }));
    const { data } = response;
    yield put(getUserSuccess(data));
    return response;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getUserError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getUsersSaga({ limit, page, query }: ReturnType<typeof getUsersRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect | ForkEffect,
    AxiosResponse | AxiosError, never> {
  try {
    if (query !== '') yield delay(1000);
    const { data }: AxiosResponse = yield call(() => getUsers({ limit, page, query }));
    yield put(getUsersSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getUsersError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* updateUserSaga(payload: ReturnType<typeof updateUserRequest>):
  Generator<
    SelectEffect
    | CallEffect<AxiosResponse | AxiosError>
    | PutEffect
    | ForkEffect,
    AxiosResponse
    | AxiosError,
    never
    >
{
  try {
    const { data: userData, userId } = payload;
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => updateUser(userId, userData));
    yield put(updateUserSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getUsersSaga, { limit, page, query, type: GET_USERS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(updateUserError({ hasErrors: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* userSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_USER, addUserSaga)]);
  yield all([takeLatest(DELETE_USER, deleteUserSaga)]);
  yield all([takeLatest(GET_USER, getUserSaga)]);
  yield all([takeLatest(GET_USERS, getUsersSaga)]);
  yield all([takeLatest(UPDATE_USER, updateUserSaga)]);
}
