// Modules
import { createSelector } from 'reselect';

// State
import type { UserState } from '@/store/rootReducer';

const addUser = (state: UserState) => ({
  error: state.user.addUser.error,
  pending: state.user.addUser.pending,
  success: state.user.addUser.success,
});

const deleteUser = (state: UserState) => ({
  error: state.user.deleteUser.error,
  pending: state.user.deleteUser.pending,
  success: state.user.deleteUser.success,
});

const getUser = (state: UserState) => ({
  data: state.user.user.data,
  error: state.user.user.error,
  pending: state.user.user.pending,
  success: state.user.user.success,
});

const getUsers = (state: UserState) => ({
  data: state.user.users.data,
  error: state.user.users.error,
  pending: state.user.users.pending,
  success: state.user.users.success,
});

const paginationAndSearch = (state: UserState) => ({
  page: state.user.pagination.page,
  limit: state.user.pagination.limit,
  query: state.user.pagination.query,
});

const updateUser = (state: UserState) => ({
  error: state.user.updateUser.error,
  pending: state.user.updateUser.pending,
  success: state.user.updateUser.success,
});

export const addUserErrorSelector = createSelector(addUser, ({ error }) => error);
export const addUserPendingSelector = createSelector(addUser, ({ pending }) => pending);
export const addUserSuccessSelector = createSelector(addUser, ({ success }) => success);

export const deleteUserErrorSelector = createSelector(deleteUser, ({ error }) => error);
export const deleteUserPendingSelector = createSelector(deleteUser, ({ pending }) => pending);
export const deleteUserSuccessSelector = createSelector(deleteUser, ({ success }) => success);

export const userSelector = createSelector(getUser, ({ data }) => data);
export const userErrorSelector = createSelector(getUser, ({ error }) => error);
export const userPendingSelector = createSelector(getUser, ({ pending }) => pending);
export const userSuccessSelector = createSelector(getUser, ({ success }) => success);

export const usersSelector = createSelector(getUsers, ({ data }) => data);
export const usersErrorSelector = createSelector(getUsers, ({ error }) => error);
export const usersPendingSelector = createSelector(getUsers, ({ pending }) => pending);
export const usersSuccessSelector = createSelector(getUsers, ({ success }) => success);

export const paginationAndQuerySearchSelector = createSelector(
  paginationAndSearch, ({ limit, page, query }) => ({ limit, page, query })
);

export const updateUserErrorSelector = createSelector(updateUser, ({ error }) => error);
export const updateUserPendingSelector = createSelector(updateUser, ({ pending }) => pending);
export const updateUserSuccessSelector = createSelector(updateUser, ({ success }) => success);
