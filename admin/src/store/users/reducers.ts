// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_USER,
  ADD_USER_ERROR,
  ADD_USER_SUCCESS,
  DELETE_USER,
  DELETE_USER_ERROR,
  DELETE_USER_SUCCESS,
  GET_USER,
  GET_USER_ERROR,
  GET_USER_SUCCESS,
  GET_USERS,
  GET_USERS_ERROR,
  GET_USERS_SUCCESS,
  PAGINATION_CHANGE_LIMIT,
  PAGINATION_CHANGE_PAGE,
  RESET_ADD_USER_ERROR,
  RESET_ADD_USER_SUCCESS,
  RESET_DELETE_USER_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_USER_ERROR,
  RESET_UPDATE_USER_SUCCESS,
  SET_QUERY_SEARCH,
  UPDATE_USER,
  UPDATE_USER_ERROR,
  UPDATE_USER_SUCCESS,
} from '@/store/users';

// Types
import { IUserState, TUserTypes } from '@/store/users';

const initialState: IUserState = {
  addUser: {
    error: {
      hasError: false,
      errors: {
        address: '',
        avatar: '',
        email: '',
        firstName: '',
        lastName: '',
        middleName: '',
        password: '',
        phone: '',
      },
    },
    pending: false,
    success: false,
  },
  deleteUser: {
    error: false,
    pending: false,
    success: false,
  },
  user: {
    data: {
      avatar: '',
      avatarUrl: '',
      id: '',
      email: '',
      address: '',
      updatedAt: new Date(),
      firstName: '',
      lastName: '',
      middleName: '',
      fullName: '',
      phone: '',
      createdAt: new Date(),
      orders: {
        data: [],
        meta: {},
        links: {},
      },
      ordersCount: 0,
    },
    error: false,
    pending: false,
    success: false,
  },
  users: {
    data: {
      data: [],
      meta: {},
      links: {},
    },
    error: false,
    pending: false,
    success: false,
  },
  pagination: {
    limit: '15',
    page: '1',
    query: '',
  },
  updateUser: {
    error: {
      hasErrors: false,
      errors: {
        address: '',
        phone: '',
        middleName: '',
        lastName: '',
        firstName: '',
        email: '',
        password: '',
        avatar: '',
      },
    },
    pending: false,
    success: false,
  },
};

export const userReducer: Reducer<IUserState, TUserTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER: {
      return {
        ...state,
        addUser: {
          error: { ...state.addUser.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_USER_ERROR: {
      return {
        ...state,
        addUser: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_USER_SUCCESS: {
      return {
        ...state,
        addUser: {
          error: { ...state.addUser.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_USER: {
      return {
        ...state,
        deleteUser: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_USER_ERROR: {
      return {
        ...state,
        deleteUser: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_USER_SUCCESS: {
      return {
        ...state,
        deleteUser: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_USER: {
      return {
        ...state,
        user: {
          data: { ...state.user.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_USER_ERROR: {
      return {
        ...state,
        user: {
          data: { ...state.user.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_USER_SUCCESS: {
      return {
        ...state,
        user: {
          data: action.payload,
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_USERS: {
      return {
        ...state,
        users: {
          data: { ...state.users.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_USERS_ERROR: {
      return {
        ...state,
        users: {
          data: { ...state.users.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_USERS_SUCCESS: {
      return {
        ...state,
        users: {
          data: {
            data: action.payload.data,
            meta: action.payload.meta,
            links: action.payload.links,
          },
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case PAGINATION_CHANGE_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: action.limit,
          page: state.pagination.page,
          query: state.pagination.query,
        },
      };
    }
    case PAGINATION_CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: action.page,
          query: state.pagination.query,
        },
      };
    }
    case RESET_ADD_USER_ERROR: {
      return {
        ...state,
        addUser: {
          error: {
            hasError: false,
            errors: {
              address: '',
              avatar: '',
              password: '',
              email: '',
              firstName: '',
              lastName: '',
              middleName: '',
              phone: '',
            },
          },
          pending: false,
          success: state.addUser.success,
        },
      };
    }
    case RESET_ADD_USER_SUCCESS: {
      return {
        ...state,
        addUser: {
          error: {
            hasError: false,
            errors: {
              address: '',
              avatar: '',
              password: '',
              email: '',
              firstName: '',
              lastName: '',
              middleName: '',
              phone: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_DELETE_USER_SUCCESS: {
      return {
        ...state,
        deleteUser: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case RESET_PAGINATION_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: '15',
          page: state.pagination.page,
          query: state.pagination.query,
        },
      };
    }
    case RESET_PAGINATION_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: '1',
          query: state.pagination.query,
        },
      };
    }
    case RESET_QUERY_SEARCH: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: state.pagination.page,
          query: '',
        },
      };
    }
    case RESET_UPDATE_USER_ERROR: {
      return {
        ...state,
        updateUser: {
          error: {
            hasErrors: false,
            errors: {
              address: '',
              phone: '',
              middleName: '',
              lastName: '',
              firstName: '',
              email: '',
              password: '',
              avatar: '',
            },
          },
          pending: state.updateUser.pending,
          success: state.updateUser.success,
        },
      };
    }
    case RESET_UPDATE_USER_SUCCESS: {
      return {
        ...state,
        updateUser: {
          error: {
            hasErrors: false,
            errors: {
              address: '',
              phone: '',
              middleName: '',
              lastName: '',
              firstName: '',
              email: '',
              password: '',
              avatar: '',
            },
          },
          pending: state.updateUser.pending,
          success: false,
        },
      };
    }
    case SET_QUERY_SEARCH: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: state.pagination.page,
          query: action.query,
        },
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        updateUser: {
          error: { ...state.updateUser.error },
          pending: true,
          success: false,
        },
      };
    }
    case UPDATE_USER_ERROR: {
      return {
        ...state,
        updateUser: {
          error: {
            hasErrors: action.payload.hasErrors,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_USER_SUCCESS: {
      return {
        ...state,
        updateUser: {
          error: { ...state.updateUser.error },
          pending: false,
          success: true,
        },
      };
    }
    default: {
      return state;
    }
  }
};
