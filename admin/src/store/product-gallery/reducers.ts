// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_PRODUCT_GALLERY,
  ADD_PRODUCT_GALLERY_ERROR,
  ADD_PRODUCT_GALLERY_SUCCESS,
  DELETE_PRODUCT_GALLERY,
  DELETE_PRODUCT_GALLERY_ERROR,
  DELETE_PRODUCT_GALLERY_SUCCESS,
  GET_PRODUCT_GALLERIES,
  GET_PRODUCT_GALLERIES_ERROR,
  GET_PRODUCT_GALLERIES_SUCCESS,
  RESET_ADD_PRODUCT_GALLERY_ERROR,
  RESET_ADD_PRODUCT_GALLERY_SUCCESS,
  RESET_DELETE_PRODUCT_GALLERY_SUCCESS,
} from '@/store/product-gallery';

// Types
import type { IProductGalleryState, TProductGalleryType } from '@/store/product-gallery';

const initialState: IProductGalleryState = {
  addProductGallery: {
    error: {
      hasError: false,
      errors: {
        productId: '',
        image: '',
      },
    },
    pending: false,
    success: false,
  },
  deleteProductGallery: {
    error: false,
    pending: false,
    success: false,
  },
  productGalleries: {
    data: [],
    error: false,
    pending: false,
  },
};

export const productGalleryReducer: Reducer<IProductGalleryState, TProductGalleryType> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT_GALLERY: {
      return {
        ...state,
        addProductGallery: {
          error: { ...state.addProductGallery.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_PRODUCT_GALLERY_ERROR: {
      return {
        ...state,
        addProductGallery: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_PRODUCT_GALLERY_SUCCESS: {
      return {
        ...state,
        addProductGallery: {
          error: { ...state.addProductGallery.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_PRODUCT_GALLERY: {
      return {
        ...state,
        deleteProductGallery: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_PRODUCT_GALLERY_ERROR: {
      return {
        ...state,
        deleteProductGallery: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_PRODUCT_GALLERY_SUCCESS: {
      return {
        ...state,
        deleteProductGallery: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_PRODUCT_GALLERIES: {
      return {
        ...state,
        productGalleries: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_PRODUCT_GALLERIES_ERROR: {
      return {
        ...state,
        productGalleries: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_PRODUCT_GALLERIES_SUCCESS: {
      return {
        ...state,
        productGalleries: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case RESET_ADD_PRODUCT_GALLERY_ERROR: {
      return {
        ...state,
        addProductGallery: {
          error: {
            hasError: false,
            errors: {
              image: '',
              productId: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_ADD_PRODUCT_GALLERY_SUCCESS: {
      return {
        ...state,
        addProductGallery: {
          error: { ...state.addProductGallery.error },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_DELETE_PRODUCT_GALLERY_SUCCESS: {
      return {
        ...state,
        deleteProductGallery: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    default: {
      return state;
    }
  }
};
