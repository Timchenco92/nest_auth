// Action Types
import {
  GET_PRODUCT_GALLERIES,
  GET_PRODUCT_GALLERIES_ERROR,
  GET_PRODUCT_GALLERIES_SUCCESS,
} from '@/store/product-gallery';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';

// Types
import type {
  IGetProductGalleries,
  IGetProductGalleriesError,
  IGetProductGalleriesSuccess,
} from '@/store/product-gallery';

export const getProductGalleriesRequest = (productId: IProduct['id']): IGetProductGalleries => ({
  type: GET_PRODUCT_GALLERIES,
  productId,
});

export const getProductGalleriesError = (): IGetProductGalleriesError => ({
  type: GET_PRODUCT_GALLERIES_ERROR,
});

export const getProductGalleriesSuccess = (payload: IProductGallery[]): IGetProductGalleriesSuccess => ({
  type: GET_PRODUCT_GALLERIES_SUCCESS,
  payload,
});
