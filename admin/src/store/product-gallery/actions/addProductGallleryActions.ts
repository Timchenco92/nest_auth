// Action Types
import {
  ADD_PRODUCT_GALLERY,
  ADD_PRODUCT_GALLERY_ERROR,
  ADD_PRODUCT_GALLERY_SUCCESS,
} from '@/store/product-gallery';

// Types
import type {
  IAddProductGallery,
  IAddProductGalleryError,
  IAddProductGalleryErrorStateAndPayload,
  IAddProductGalleryPayload,
  IAddProductGallerySuccess,
} from '@/store/product-gallery';

export const addProductGalleryRequest = (payload: IAddProductGalleryPayload): IAddProductGallery => ({
  type: ADD_PRODUCT_GALLERY,
  payload,
});

export const addProductGalleryError = (payload: IAddProductGalleryErrorStateAndPayload): IAddProductGalleryError => ({
  type: ADD_PRODUCT_GALLERY_ERROR,
  payload,
});

export const addProductGallerySuccess = (): IAddProductGallerySuccess => ({
  type: ADD_PRODUCT_GALLERY_SUCCESS,
});
