// Action Types
import {
  DELETE_PRODUCT_GALLERY,
  DELETE_PRODUCT_GALLERY_ERROR,
  DELETE_PRODUCT_GALLERY_SUCCESS,
} from '@/store/product-gallery';

// Types
import type {
  IDeleteProductGallery,
  IDeleteProductGalleryError,
  IDeleteProductGalleryPayload,
  IDeleteProductGallerySuccess,
} from '@/store/product-gallery';

export const deleteProductGalleryRequest = (payload: IDeleteProductGalleryPayload): IDeleteProductGallery => ({
  type: DELETE_PRODUCT_GALLERY,
  payload,
});

export const deleteProductGalleryError = (): IDeleteProductGalleryError => ({
  type: DELETE_PRODUCT_GALLERY_ERROR,
});

export const deleteProductGallerySuccess = (): IDeleteProductGallerySuccess => ({
  type: DELETE_PRODUCT_GALLERY_SUCCESS,
});
