// Action Types
import {
  RESET_ADD_PRODUCT_GALLERY_ERROR,
  RESET_ADD_PRODUCT_GALLERY_SUCCESS,
  RESET_DELETE_PRODUCT_GALLERY_SUCCESS,
} from '@/store/product-gallery';

// Types
import type {
  IResetAddProductGalleryError,
  IResetAddProductGallerySuccess,
  IResetDeleteProductGallerySuccess,
} from '@/store/product-gallery';

export const resetAddProductGalleryError = (): IResetAddProductGalleryError => ({
  type: RESET_ADD_PRODUCT_GALLERY_ERROR,
});

export const resetAddProductGallerySuccess = (): IResetAddProductGallerySuccess => ({
  type: RESET_ADD_PRODUCT_GALLERY_SUCCESS,
});

export const resetDeleteProductGallerySuccess = (): IResetDeleteProductGallerySuccess => ({
  type: RESET_DELETE_PRODUCT_GALLERY_SUCCESS,
});
