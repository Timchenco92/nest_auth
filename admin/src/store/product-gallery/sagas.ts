// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect } from 'redux-saga/effects';

// Actions
import {
  addProductGalleryRequest,
  addProductGalleryError,
  addProductGallerySuccess,
  deleteProductGalleryError,
  deleteProductGalleryRequest,
  deleteProductGallerySuccess,
  getProductGalleriesRequest,
  getProductGalleriesError,
  getProductGalleriesSuccess,
} from '@/store/product-gallery';

import { showToast } from '@/store/toast';

// Action Types
import { ADD_PRODUCT_GALLERY, DELETE_PRODUCT_GALLERY, GET_PRODUCT_GALLERIES } from '@/store/product-gallery';
import { GET_PRODUCT } from '@/store/products';

// Saga
import { getProductSaga } from '@/store/products/sagas';

// Services
import { productGalleryService } from '@/services';
import { IProduct, IProductGallery } from '@/interfaces/models';

const { addProductGallery, deleteProductGallery, getProductGalleries } = productGalleryService;

function* addProductGallerySaga({ payload }: ReturnType<typeof addProductGalleryRequest>):
  Generator<
    CallEffect<AxiosResponse | AxiosError> | CallEffect<IProduct> | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data: formData, productId } = payload;
    const { data }: AxiosResponse = yield call(() => addProductGallery(formData));
    yield call(getProductSaga, { productId, type: GET_PRODUCT });
    yield put(addProductGallerySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addProductGalleryError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteProductGallerySaga({ payload }: ReturnType<typeof deleteProductGalleryRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError> | CallEffect<IProduct> | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { productGalleryId, productId } = payload;
    const { data }: AxiosResponse = yield call(() => deleteProductGallery(productGalleryId));
    yield call(getProductSaga, { productId, type: GET_PRODUCT });
    yield put(deleteProductGallerySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteProductGalleryError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getProductGalleriesSaga({ productId }: ReturnType<typeof getProductGalleriesRequest>):
  Generator<CallEffect<AxiosResponse<IProductGallery[]> | AxiosError> | PutEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getProductGalleries(productId));
    yield put(getProductGalleriesSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getProductGalleriesError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* productGallerySaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_PRODUCT_GALLERY, addProductGallerySaga)]);
  yield all([takeLatest(DELETE_PRODUCT_GALLERY, deleteProductGallerySaga)]);
  yield all([takeLatest(GET_PRODUCT_GALLERIES, getProductGalleriesSaga)]);
}
