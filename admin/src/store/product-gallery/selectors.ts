// Modules
import { createSelector } from 'reselect';

// State
import type { ProductGalleryState } from '@/store/rootReducer';

const addProductGallery = (state: ProductGalleryState) => ({
  error: state.productGallery.addProductGallery.error,
  pending: state.productGallery.addProductGallery.pending,
  success: state.productGallery.addProductGallery.success,
});
const deleteProductGallery = (state: ProductGalleryState) => ({
  error: state.productGallery.deleteProductGallery.error,
  pending: state.productGallery.deleteProductGallery.pending,
  success: state.productGallery.deleteProductGallery.success,
});

const productGalleries = (state: ProductGalleryState) => ({
  data: state.productGallery.productGalleries.data,
  error: state.productGallery.productGalleries.error,
  pending: state.productGallery.productGalleries.pending,
});

export const addProductGalleryErrorSelector = createSelector(addProductGallery, ({ error }) => error);
export const addProductGalleryPendingSelector = createSelector(addProductGallery, ({ pending }) => pending);
export const addProductGallerySuccessSelector = createSelector(addProductGallery, ({ success }) => success);

export const deleteProductGalleryErrorSelector = createSelector(deleteProductGallery, ({ error }) => error);
export const deleteProductGalleryPendingSelector = createSelector(deleteProductGallery, ({ pending }) => pending);
export const deleteProductGallerySuccessSelector = createSelector(deleteProductGallery, ({ success }) => success);

export const productGalleriesSelector = createSelector(productGalleries, ({ data }) => data);
export const productGalleriesErrorSelector = createSelector(productGalleries, ({ error }) => error);
export const productGalleriesPendingSelector = createSelector(productGalleries, ({ pending }) => pending);
