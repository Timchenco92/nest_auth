// Action Types
import {
  RESET_ADD_PRODUCT_GALLERY_ERROR,
  RESET_ADD_PRODUCT_GALLERY_SUCCESS,
  RESET_DELETE_PRODUCT_GALLERY_SUCCESS,
} from '@/store/product-gallery';

export interface IResetAddProductGalleryError {
  type: typeof RESET_ADD_PRODUCT_GALLERY_ERROR;
}

export interface IResetAddProductGallerySuccess {
  type: typeof RESET_ADD_PRODUCT_GALLERY_SUCCESS;
}

export interface IResetDeleteProductGallerySuccess {
  type: typeof RESET_DELETE_PRODUCT_GALLERY_SUCCESS;
}
