// Action Types
import { ADD_PRODUCT_GALLERY, ADD_PRODUCT_GALLERY_ERROR, ADD_PRODUCT_GALLERY_SUCCESS } from '@/store/product-gallery';

// Interfaces
import { IProduct } from '@/interfaces/models';

export interface IAddProductGalleryState {
  error: IAddProductGalleryErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddProductGallery {
  type: typeof ADD_PRODUCT_GALLERY;
  payload: IAddProductGalleryPayload;
}

export interface IAddProductGalleryPayload {
  data: FormData;
  productId: IProduct['id'];
}

export interface IAddProductGalleryError {
  type: typeof ADD_PRODUCT_GALLERY_ERROR;
  payload: IAddProductGalleryErrorStateAndPayload;
}

export interface IAddProductGalleryErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

export interface IErrors {
  productId: string;
  image: string;
}

export interface IAddProductGallerySuccess {
  type: typeof ADD_PRODUCT_GALLERY_SUCCESS;
}
