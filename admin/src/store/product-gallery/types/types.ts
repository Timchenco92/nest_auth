// Types
import type {
  IAddProductGallery,
  IAddProductGalleryError,
  IAddProductGalleryState,
  IAddProductGallerySuccess,
  IDeleteProductGallery,
  IDeleteProductGalleryError,
  IDeleteProductGalleryState,
  IDeleteProductGallerySuccess,
  IGetProductGalleries,
  IGetProductGalleriesError,
  IGetProductGalleriesSuccess,
  IGetProductGalleriesState,
  IResetAddProductGalleryError,
  IResetAddProductGallerySuccess,
  IResetDeleteProductGallerySuccess,
} from '@/store/product-gallery';

export interface IProductGalleryState {
  addProductGallery: IAddProductGalleryState;
  deleteProductGallery: IDeleteProductGalleryState;
  productGalleries: IGetProductGalleriesState;
}

export type TProductGalleryType = IAddProductGallery
  | IAddProductGalleryError
  | IAddProductGallerySuccess
  | IDeleteProductGallery
  | IDeleteProductGalleryError
  | IDeleteProductGallerySuccess
  | IResetAddProductGalleryError
  | IResetAddProductGallerySuccess
  | IResetDeleteProductGallerySuccess
  | IGetProductGalleries
  | IGetProductGalleriesError
  | IGetProductGalleriesSuccess;
