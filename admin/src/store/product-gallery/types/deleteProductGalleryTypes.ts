// Action Types
import {
  DELETE_PRODUCT_GALLERY,
  DELETE_PRODUCT_GALLERY_ERROR,
  DELETE_PRODUCT_GALLERY_SUCCESS,
} from '@/store/product-gallery';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';

export interface IDeleteProductGalleryState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteProductGallery {
  type: typeof DELETE_PRODUCT_GALLERY;
  payload: IDeleteProductGalleryPayload;
}

export interface IDeleteProductGalleryPayload {
  productGalleryId: IProductGallery['id'];
  productId: IProduct['id'];
}

export interface IDeleteProductGalleryError {
  type: typeof DELETE_PRODUCT_GALLERY_ERROR;
}

export interface IDeleteProductGallerySuccess {
  type: typeof DELETE_PRODUCT_GALLERY_SUCCESS;
}
