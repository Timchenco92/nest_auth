// Action Types
import { GET_PRODUCT_GALLERIES, GET_PRODUCT_GALLERIES_ERROR, GET_PRODUCT_GALLERIES_SUCCESS } from '@/store/product-gallery';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';

export interface IGetProductGalleriesState {
  data: IProductGallery[];
  error: boolean;
  pending: boolean;
}

export interface IGetProductGalleries {
  type: typeof GET_PRODUCT_GALLERIES;
  productId: IProduct['id'];
}

export interface IGetProductGalleriesError {
  type: typeof GET_PRODUCT_GALLERIES_ERROR;
}

export interface IGetProductGalleriesSuccess {
  type: typeof GET_PRODUCT_GALLERIES_SUCCESS;
  payload: IProductGallery[];
}
