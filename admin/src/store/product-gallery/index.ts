// Actions
export {
  addProductGalleryError,
  addProductGalleryRequest,
  addProductGallerySuccess,
} from './actions/addProductGallleryActions';

export {
  deleteProductGalleryError,
  deleteProductGalleryRequest,
  deleteProductGallerySuccess,
} from './actions/deleteProductGallleryActions';

export {
  getProductGalleriesError,
  getProductGalleriesRequest,
  getProductGalleriesSuccess,
} from './actions/getProductGalleriesActions';

export {
  resetAddProductGalleryError,
  resetAddProductGallerySuccess,
  resetDeleteProductGallerySuccess,
} from './actions/resetProductGallleryActions';

// Action Types
export {
  ADD_PRODUCT_GALLERY,
  ADD_PRODUCT_GALLERY_ERROR,
  ADD_PRODUCT_GALLERY_SUCCESS,
  DELETE_PRODUCT_GALLERY,
  DELETE_PRODUCT_GALLERY_ERROR,
  DELETE_PRODUCT_GALLERY_SUCCESS,
  GET_PRODUCT_GALLERIES,
  GET_PRODUCT_GALLERIES_ERROR,
  GET_PRODUCT_GALLERIES_SUCCESS,
  RESET_ADD_PRODUCT_GALLERY_ERROR,
  RESET_ADD_PRODUCT_GALLERY_SUCCESS,
  RESET_DELETE_PRODUCT_GALLERY_SUCCESS,
} from './actionTypes';

// Reducers
export { productGalleryReducer } from './reducers';

// Sagas
export { productGallerySaga, getProductGalleriesSaga } from './sagas';

// Selectors
export {
  addProductGalleryErrorSelector,
  addProductGalleryPendingSelector,
  addProductGallerySuccessSelector,
  deleteProductGalleryErrorSelector,
  deleteProductGalleryPendingSelector,
  deleteProductGallerySuccessSelector,
  productGalleriesSelector,
  productGalleriesErrorSelector,
  productGalleriesPendingSelector,
} from './selectors';

// Types
export type {
  IAddProductGallery,
  IAddProductGalleryError,
  IAddProductGalleryErrorStateAndPayload,
  IAddProductGalleryPayload,
  IAddProductGalleryState,
  IAddProductGallerySuccess,
} from './types/addProductGalleryTypes';

export type {
  IDeleteProductGallery,
  IDeleteProductGalleryError,
  IDeleteProductGalleryPayload,
  IDeleteProductGalleryState,
  IDeleteProductGallerySuccess,
} from './types/deleteProductGalleryTypes';

export type {
  IGetProductGalleries,
  IGetProductGalleriesError,
  IGetProductGalleriesState,
  IGetProductGalleriesSuccess,
} from './types/getProductGalleriesTypes';

export type {
  IResetAddProductGalleryError,
  IResetAddProductGallerySuccess,
  IResetDeleteProductGallerySuccess,
} from './types/resetProductGalleryTypes';

export type { IProductGalleryState, TProductGalleryType } from './types/types';
