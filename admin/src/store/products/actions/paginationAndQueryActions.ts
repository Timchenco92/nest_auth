// Action Types
import { PAGINATION_CHANGE_LIMIT, PAGINATION_CHANGE_PAGE, SET_QUERY_SEARCH } from '@/store/products';

// Types
import type { IChangeLimit, IChangePage, ISetQuerySearch } from '@/store/products';

export const changeLimit = (limit: string): IChangeLimit => ({
  type: PAGINATION_CHANGE_LIMIT,
  limit,
});

export const changePage = (page: string): IChangePage => ({
  type: PAGINATION_CHANGE_PAGE,
  page,
});

export const setQuerySearch = (query: string): ISetQuerySearch => ({
  type: SET_QUERY_SEARCH,
  query,
});
