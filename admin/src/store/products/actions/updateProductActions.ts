// Action Types
import { UPDATE_PRODUCT, UPDATE_PRODUCT_ERROR, UPDATE_PRODUCT_SUCCESS } from '@/store/products';

// Types
import type {
  IUpdateProduct,
  IUpdateProductError,
  IUpdateProductSuccess,
  IUpdateProductPayload,
  IUpdateProductErrorStateAndPayload,
} from '@/store/products';

export const updateProductRequest = (payload: IUpdateProductPayload): IUpdateProduct => ({
  type: UPDATE_PRODUCT,
  payload,
});

export const updateProductError = (payload: IUpdateProductErrorStateAndPayload): IUpdateProductError => ({
  type: UPDATE_PRODUCT_ERROR,
  payload,
});

export const updateProductSuccess = (): IUpdateProductSuccess => ({
  type: UPDATE_PRODUCT_SUCCESS,
});
