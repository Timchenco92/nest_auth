// Action Types
import { ADD_PRODUCT, ADD_PRODUCT_ERROR, ADD_PRODUCT_SUCCESS } from '@/store/products';

// Types
import type {
  IAddProduct,
  IAddProductError,
  IAddProductErrorStateAndPayload,
  IAddProductSuccess,
} from '@/store/products';

export const addProductRequest = (payload: FormData): IAddProduct => ({
  type: ADD_PRODUCT,
  payload,
});

export const addProductError = (payload: IAddProductErrorStateAndPayload): IAddProductError => ({
  type: ADD_PRODUCT_ERROR,
  payload,
});

export const addProductsSuccess = (): IAddProductSuccess => ({
  type: ADD_PRODUCT_SUCCESS,
});
