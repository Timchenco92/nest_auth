// Action Types
import { GET_PRODUCT, GET_PRODUCT_ERROR, GET_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

// Types
import type { IGetProduct, IGetProductError, IGetProductSuccess } from '@/store/products';

export const getProductRequest = (productId: IProduct['id']): IGetProduct => ({
  type: GET_PRODUCT,
  productId,
});

export const getProductError = (): IGetProductError => ({
  type: GET_PRODUCT_ERROR,
});

export const getProductSuccess = (data: IProduct): IGetProductSuccess => ({
  type: GET_PRODUCT_SUCCESS,
  data,
});
