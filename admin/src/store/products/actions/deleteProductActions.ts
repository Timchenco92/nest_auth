// Action Types
import { DELETE_PRODUCT, DELETE_PRODUCT_ERROR, DELETE_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

// Types
import type {
  IDeleteProduct,
  IDeleteProductError,
  IDeleteProductSuccess,
} from '@/store/products';

export const deleteProductRequest = (productId: IProduct['id']): IDeleteProduct => ({
  type: DELETE_PRODUCT,
  productId,
});

export const deleteProductError = (): IDeleteProductError => ({
  type: DELETE_PRODUCT_ERROR,
});

export const deleteProductSuccess = (): IDeleteProductSuccess => ({
  type: DELETE_PRODUCT_SUCCESS,
});
