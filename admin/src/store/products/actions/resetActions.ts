// Action Types
import {
  RESET_ADD_PRODUCT_ERROR,
  RESET_ADD_PRODUCT_SUCCESS,
  RESET_DELETE_PRODUCT_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_PRODUCT_ERROR,
  RESET_UPDATE_PRODUCT_SUCCESS,
} from '@/store/products';

// Types
import type {
  IResetAddProductError,
  IResetAddProductSuccess,
  IResetDeleteProductSuccess,
  IResetPagination,
  IResetQuerySearch,
  IResetUpdateProductError,
  IResetUpdateProductSuccess,
} from '@/store/products';

export const resetAddProductError = (): IResetAddProductError => ({
  type: RESET_ADD_PRODUCT_ERROR,
});

export const resetAddProductSuccess = (): IResetAddProductSuccess => ({
  type: RESET_ADD_PRODUCT_SUCCESS,
});

export const resetDeleteProductSuccess = (): IResetDeleteProductSuccess => ({
  type: RESET_DELETE_PRODUCT_SUCCESS,
});

export const resetPagination = (): IResetPagination => ({
  type: RESET_PAGINATION,
});

export const resetQuerySearch = (): IResetQuerySearch => ({
  type: RESET_QUERY_SEARCH,
});

export const resetUpdateProductError = (): IResetUpdateProductError => ({
  type: RESET_UPDATE_PRODUCT_ERROR,
});

export const resetUpdateProductSuccess = (): IResetUpdateProductSuccess => ({
  type: RESET_UPDATE_PRODUCT_SUCCESS,
});
