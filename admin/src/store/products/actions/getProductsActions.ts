// Action Types
import { GET_PRODUCTS, GET_PRODUCTS_ERROR, GET_PRODUCTS_SUCCESS } from '@/store/products';

// Types
import type {
  IGetProducts,
  IGetProductsError,
  IGetProductsSuccess,
  IGetProductsSuccessStateAndPayload,
} from '@/store/products';

export const getProductsRequest = (limit: string, page: string, query: string): IGetProducts => ({
  type: GET_PRODUCTS,
  limit,
  page,
  query,
});

export const getProductsError = (): IGetProductsError => ({
  type: GET_PRODUCTS_ERROR,
});

export const getProductsSuccess = (payload: IGetProductsSuccessStateAndPayload): IGetProductsSuccess => ({
  type: GET_PRODUCTS_SUCCESS,
  payload,
});
