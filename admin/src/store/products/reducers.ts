// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_PRODUCT,
  ADD_PRODUCT_ERROR,
  ADD_PRODUCT_SUCCESS,
  DELETE_PRODUCT,
  DELETE_PRODUCT_ERROR,
  DELETE_PRODUCT_SUCCESS,
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_ERROR,
  GET_PRODUCT_SUCCESS,
  PAGINATION_CHANGE_LIMIT,
  PAGINATION_CHANGE_PAGE,
  RESET_ADD_PRODUCT_ERROR,
  RESET_ADD_PRODUCT_SUCCESS,
  RESET_DELETE_PRODUCT_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_PRODUCT_ERROR,
  RESET_UPDATE_PRODUCT_SUCCESS,
  SET_QUERY_SEARCH,
  UPDATE_PRODUCT,
  UPDATE_PRODUCT_ERROR,
  UPDATE_PRODUCT_SUCCESS,
} from '@/store/products';

// Types
import type { IProductState, TProductTypes } from '@/store/products';

const initialState: IProductState = {
  addProduct: {
    error: {
      hasError: false,
      errors: {
        image: '',
        title: '',
        price: '',
        categoryId: '',
      },
    },
    pending: false,
    success: false,
  },
  deleteProduct: {
    error: false,
    pending: false,
    success: false,
  },
  product: {
    data: {
      price: '',
      id: '',
      title: '',
      updatedAt: new Date(),
      categoryId: '',
      image: '',
      imageUrl: '',
      createdAt: new Date(),
    },
    error: false,
    pending: false,
    success: false,
  },
  products: {
    data: {
      data: [],
      meta: {},
      links: {},
    },
    error: false,
    pending: false,
    success: false,
  },
  updateProduct: {
    error: {
      hasError: false,
      errors: {
        categoryId: '',
        title: '',
        image: '',
        price: '',
      },
    },
    pending: false,
    success: false,
  },
  paginationAndSearch: {
    limit: '15',
    page: '1',
    query: '',
  },
};

export const productReducer: Reducer<IProductState, TProductTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRODUCT: {
      return {
        ...state,
        addProduct: {
          error: { ...state.addProduct.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_PRODUCT_ERROR: {
      return {
        ...state,
        addProduct: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_PRODUCT_SUCCESS: {
      return {
        ...state,
        addProduct: {
          error: { ...state.addProduct.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_PRODUCT: {
      return {
        ...state,
        deleteProduct: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_PRODUCT_ERROR: {
      return {
        ...state,
        deleteProduct: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_PRODUCT_SUCCESS: {
      return {
        ...state,
        deleteProduct: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_PRODUCT: {
      return {
        ...state,
        product: {
          data: { ...state.product.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_PRODUCT_ERROR: {
      return {
        ...state,
        product: {
          data: { ...state.product.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_PRODUCT_SUCCESS: {
      return {
        ...state,
        product: {
          data: { ...action.data },
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_PRODUCTS: {
      return {
        ...state,
        products: {
          data: { ...state.products.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_PRODUCTS_ERROR: {
      return {
        ...state,
        products: {
          data: { ...state.products.data },
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case GET_PRODUCTS_SUCCESS: {
      return {
        ...state,
        products: {
          data: {
            data: action.payload.data,
            meta: action.payload.meta,
            links: action.payload.links,
          },
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case PAGINATION_CHANGE_LIMIT: {
      return {
        ...state,
        paginationAndSearch: {
          limit: action.limit,
          page: state.paginationAndSearch.page,
          query: state.paginationAndSearch.query,
        },
      };
    }
    case PAGINATION_CHANGE_PAGE: {
      return {
        ...state,
        paginationAndSearch: {
          limit: state.paginationAndSearch.limit,
          page: action.page,
          query: state.paginationAndSearch.query,
        },
      };
    }
    case RESET_ADD_PRODUCT_ERROR: {
      return {
        ...state,
        addProduct: {
          error: {
            hasError: false,
            errors: {
              categoryId: '',
              title: '',
              image: '',
              price: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_ADD_PRODUCT_SUCCESS: {
      return {
        ...state,
        addProduct: {
          error: {
            hasError: false,
            errors: {
              categoryId: '',
              title: '',
              image: '',
              price: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_DELETE_PRODUCT_SUCCESS: {
      return {
        ...state,
        deleteProduct: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case RESET_PAGINATION: {
      return {
        ...state,
        paginationAndSearch: {
          limit: '15',
          page: '1',
          query: state.paginationAndSearch.query,
        },
      };
    }
    case RESET_QUERY_SEARCH: {
      return {
        ...state,
        paginationAndSearch: {
          limit: state.paginationAndSearch.limit,
          page: state.paginationAndSearch.page,
          query: '',
        },
      };
    }
    case RESET_UPDATE_PRODUCT_ERROR: {
      return {
        ...state,
        updateProduct: {
          error: {
            hasError: false,
            errors: {
              categoryId: '',
              title: '',
              image: '',
              price: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_UPDATE_PRODUCT_SUCCESS: {
      return {
        ...state,
        updateProduct: {
          error: { ...state.updateProduct.error },
          pending: false,
          success: false,
        },
      };
    }
    case SET_QUERY_SEARCH: {
      return {
        ...state,
        paginationAndSearch: {
          limit: state.paginationAndSearch.limit,
          page: state.paginationAndSearch.page,
          query: action.query,
        },
      };
    }
    case UPDATE_PRODUCT: {
      return {
        ...state,
        updateProduct: {
          error: { ...state.updateProduct.error },
          pending: true,
          success: false,
        },
      };
    }
    case UPDATE_PRODUCT_ERROR: {
      return {
        ...state,
        updateProduct: {
          error: {
            hasError: true,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_PRODUCT_SUCCESS: {
      return {
        ...state,
        updateProduct: {
          error: { ...state.updateProduct.error },
          pending: false,
          success: true,
        },
      };
    }
    default: {
      return state;
    }
  }
};
