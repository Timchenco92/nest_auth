// Actions
export { addProductError, addProductRequest, addProductsSuccess } from './actions/addProductActions';
export { deleteProductError, deleteProductRequest, deleteProductSuccess } from './actions/deleteProductActions';
export { getProductError, getProductRequest, getProductSuccess } from './actions/getProductActions';
export { getProductsError, getProductsRequest, getProductsSuccess } from './actions/getProductsActions';
export { changeLimit, changePage, setQuerySearch } from './actions/paginationAndQueryActions';
export {
  resetAddProductError,
  resetAddProductSuccess,
  resetDeleteProductSuccess,
  resetPagination,
  resetQuerySearch,
  resetUpdateProductError,
  resetUpdateProductSuccess,
} from './actions/resetActions';

export { updateProductError, updateProductRequest, updateProductSuccess } from './actions/updateProductActions';

// Action Types
export {
  ADD_PRODUCT,
  ADD_PRODUCT_ERROR,
  ADD_PRODUCT_SUCCESS,
  DELETE_PRODUCT,
  DELETE_PRODUCT_ERROR,
  DELETE_PRODUCT_SUCCESS,
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_ERROR,
  GET_PRODUCT_SUCCESS,
  PAGINATION_CHANGE_LIMIT,
  PAGINATION_CHANGE_PAGE,
  RESET_ADD_PRODUCT_ERROR,
  RESET_ADD_PRODUCT_SUCCESS,
  RESET_DELETE_PRODUCT_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_PRODUCT_ERROR,
  RESET_UPDATE_PRODUCT_SUCCESS,
  SET_QUERY_SEARCH,
  UPDATE_PRODUCT,
  UPDATE_PRODUCT_ERROR,
  UPDATE_PRODUCT_SUCCESS,
} from './actionTypes';

// Reducer
export { productReducer } from './reducers';

// Saga
export { getProductSaga, productSaga } from './sagas';

// Selectors
export {
  addProductErrorSelector,
  addProductPendingSelector,
  addProductSuccessSelector,
  deleteProductErrorSelector,
  deleteProductPendingSelector,
  deleteProductSuccessSelector,
  paginationAndQuerySearchSelector,
  productErrorSelector,
  productPendingSelector,
  productSelector,
  productSuccessSelector,
  productsErrorSelector,
  productsPendingSelector,
  productsSelector,
  productsSuccessSelector,
  updateProductErrorSelector,
  updateProductPendingSelector,
  updateProductSuccessSelector,
} from './selectors';

// Types
export type {
  IAddProduct,
  IAddProductError,
  IAddProductErrorStateAndPayload,
  IAddProductState,
  IAddProductSuccess,
} from './types/addProductTypes';

export type {
  IDeleteProduct,
  IDeleteProductError,
  IDeleteProductState,
  IDeleteProductSuccess,
} from './types/deleteProductTypes';

export type {
  IGetProducts,
  IGetProductsError,
  IGetProductsState,
  IGetProductsSuccess,
  IGetProductsSuccessStateAndPayload,
} from './types/getProductsTypes';

export type {
  IGetProduct,
  IGetProductError,
  IGetProductState,
  IGetProductSuccess,
} from './types/getProductTypes';

export type {
  IChangeLimit,
  IChangePage,
  IPaginationAndQueryState,
  ISetQuerySearch,
} from './types/paginationAndQueryTypes';

export type {
  IResetAddProductError,
  IResetAddProductSuccess,
  IResetDeleteProductSuccess,
  IResetPagination,
  IResetQuerySearch,
  IResetUpdateProductError,
  IResetUpdateProductSuccess,
} from './types/resetProductTypes';

export type {
  IUpdateProductErrorStateAndPayload,
  IUpdateProduct,
  IUpdateProductError,
  IUpdateProductPayload,
  IUpdateProductState,
  IUpdateProductSuccess,
} from './types/updateProductTypes';

export type { IProductState, TProductTypes } from './types/types';
