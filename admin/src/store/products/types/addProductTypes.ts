// Action Types
import { ADD_PRODUCT, ADD_PRODUCT_ERROR, ADD_PRODUCT_SUCCESS } from '@/store/products';

export interface IAddProductState {
  error: IAddProductErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddProduct {
  type: typeof ADD_PRODUCT;
  payload: FormData;
}

export interface IAddProductError {
  type: typeof ADD_PRODUCT_ERROR;
  payload: IAddProductErrorStateAndPayload;
}

export interface IAddProductSuccess {
  type: typeof ADD_PRODUCT_SUCCESS;
}

export interface IAddProductErrorStateAndPayload {
  errors: IErrors;
  hasError: boolean;
}

export interface IErrors {
  categoryId: string;
  image: string;
  title: string;
  price: string;
}
