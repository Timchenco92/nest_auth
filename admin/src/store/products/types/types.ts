// Types
import type {
  IAddProduct,
  IAddProductError,
  IAddProductState,
  IAddProductSuccess,
  IChangeLimit,
  IChangePage,
  IDeleteProduct,
  IDeleteProductError,
  IDeleteProductState,
  IDeleteProductSuccess,
  IGetProduct,
  IGetProductError,
  IGetProductState,
  IGetProductSuccess,
  IGetProducts,
  IGetProductsError,
  IGetProductsState,
  IGetProductsSuccess,
  IPaginationAndQueryState,
  IResetAddProductError,
  IResetAddProductSuccess,
  IResetDeleteProductSuccess,
  IResetPagination,
  IResetQuerySearch,
  IResetUpdateProductError,
  IResetUpdateProductSuccess,
  ISetQuerySearch,
  IUpdateProduct,
  IUpdateProductError,
  IUpdateProductState,
  IUpdateProductSuccess,
} from '@/store/products';

export interface IProductState {
  addProduct: IAddProductState;
  deleteProduct: IDeleteProductState;
  products: IGetProductsState;
  product: IGetProductState;
  updateProduct: IUpdateProductState;
  paginationAndSearch: IPaginationAndQueryState;
}

export type TProductTypes = IAddProduct
  | IAddProductError
  | IAddProductSuccess
  | IChangeLimit
  | IChangePage
  | IDeleteProduct
  | IDeleteProductError
  | IDeleteProductSuccess
  | IGetProduct
  | IGetProductError
  | IGetProductSuccess
  | IGetProducts
  | IGetProductsError
  | IGetProductsSuccess
  | IResetAddProductError
  | IResetAddProductSuccess
  | IResetDeleteProductSuccess
  | IResetPagination
  | IResetQuerySearch
  | IResetUpdateProductError
  | IResetUpdateProductSuccess
  | ISetQuerySearch
  | IUpdateProduct
  | IUpdateProductError
  | IUpdateProductSuccess;
