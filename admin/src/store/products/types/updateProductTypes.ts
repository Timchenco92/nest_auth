// Action Types
import { UPDATE_PRODUCT, UPDATE_PRODUCT_ERROR, UPDATE_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

export interface IUpdateProductState {
  error: IUpdateProductErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IUpdateProduct {
  type: typeof UPDATE_PRODUCT;
  payload: IUpdateProductPayload;
}

export interface IUpdateProductPayload {
  data: FormData;
  productId: IProduct['id'];
}

export interface IUpdateProductError {
  type: typeof UPDATE_PRODUCT_ERROR;
  payload: IUpdateProductErrorStateAndPayload;
}

export interface IUpdateProductErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

export interface IErrors {
  categoryId: string;
  image: string;
  title: string;
  price: string;
}

export interface IUpdateProductSuccess {
  type: typeof UPDATE_PRODUCT_SUCCESS;
}
