// Action Types
import { GET_PRODUCT, GET_PRODUCT_ERROR, GET_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

export interface IGetProductState {
  data: IProduct;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetProduct {
  type: typeof GET_PRODUCT;
  productId: IProduct['id'];
}

export interface IGetProductError {
  type: typeof GET_PRODUCT_ERROR;
}

export interface IGetProductSuccess {
  type: typeof GET_PRODUCT_SUCCESS;
  data: IProduct;
}
