// Action Types
import { DELETE_PRODUCT, DELETE_PRODUCT_ERROR, DELETE_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

export interface IDeleteProductState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteProduct {
  type: typeof DELETE_PRODUCT;
  productId: IProduct['id'];
}

export interface IDeleteProductError {
  type: typeof DELETE_PRODUCT_ERROR;
}

export interface IDeleteProductSuccess {
  type: typeof DELETE_PRODUCT_SUCCESS;
}
