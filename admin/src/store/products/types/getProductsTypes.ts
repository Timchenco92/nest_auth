// Action Types
import { GET_PRODUCTS, GET_PRODUCTS_ERROR, GET_PRODUCTS_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

export interface IGetProductsState {
  data: IGetProductsSuccessStateAndPayload;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetProducts {
  type: typeof GET_PRODUCTS;
  limit: string;
  page: string;
  query: string;
}

export interface IGetProductsError {
  type: typeof GET_PRODUCTS_ERROR;
}

export interface IGetProductsSuccess {
  type: typeof GET_PRODUCTS_SUCCESS;
  payload: IGetProductsSuccessStateAndPayload;
}

export interface IGetProductsSuccessStateAndPayload {
  data: IProduct[];
  meta: any;
  links: any;
}
