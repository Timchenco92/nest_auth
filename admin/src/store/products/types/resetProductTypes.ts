// Action Types
import {
  RESET_ADD_PRODUCT_ERROR,
  RESET_ADD_PRODUCT_SUCCESS,
  RESET_DELETE_PRODUCT_SUCCESS,
  RESET_PAGINATION,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_PRODUCT_ERROR,
  RESET_UPDATE_PRODUCT_SUCCESS,
} from '@/store/products';

export interface IResetAddProductError {
  type: typeof RESET_ADD_PRODUCT_ERROR;
}

export interface IResetAddProductSuccess {
  type: typeof RESET_ADD_PRODUCT_SUCCESS;
}

export interface IResetDeleteProductSuccess {
  type: typeof RESET_DELETE_PRODUCT_SUCCESS;
}

export interface IResetPagination {
  type: typeof RESET_PAGINATION;
}

export interface IResetQuerySearch {
  type: typeof RESET_QUERY_SEARCH;
}

export interface IResetUpdateProductError {
  type: typeof RESET_UPDATE_PRODUCT_ERROR;
}

export interface IResetUpdateProductSuccess {
  type: typeof RESET_UPDATE_PRODUCT_SUCCESS;
}
