// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, delay, put, select, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect, SelectEffect } from 'redux-saga/effects';

// Actions
import {
  addProductError,
  addProductRequest,
  addProductsSuccess,
  deleteProductError,
  deleteProductRequest,
  deleteProductSuccess,
  getProductError,
  getProductRequest,
  getProductSuccess,
  getProductsError,
  getProductsRequest,
  getProductsSuccess,
  updateProductError,
  updateProductRequest,
  updateProductSuccess,
} from '@/store/products';

import { showToast } from '@/store/toast';

// Action Types
import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCT,
  UPDATE_PRODUCT,
} from '@/store/products';

import { GET_PRODUCT_GALLERIES } from '@/store/product-gallery';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';
import type { IGetProductsSuccessStateAndPayload } from '@/store/products';

// Sagas
import { getProductGalleriesSaga } from '@/store/product-gallery';

// Selectors
import { paginationAndQuerySearchSelector } from '@/store/products';

// Services
import { productService } from '@/services';

const { addProduct, deleteProduct, getProduct, getProducts, updateProduct } = productService;

function* addProductSaga({ payload }: ReturnType<typeof addProductRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => addProduct(payload));
    yield put(addProductsSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getProductsSaga, { limit, page, query, type: GET_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addProductError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteProductSaga({ productId }: ReturnType<typeof deleteProductRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => deleteProduct(productId));
    yield put(deleteProductSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getProductsSaga, { limit, page, query, type: GET_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteProductError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getProductsSaga({ limit, page, query }: ReturnType<typeof getProductsRequest>):
  Generator<CallEffect<AxiosResponse<IProduct[]>
      | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    if (query !== '') yield delay(1000);
    const response: AxiosResponse<IGetProductsSuccessStateAndPayload> = yield call(() => getProducts({
      limit,
      page,
      query,
    }));
    const { data: { data: categoriesData, meta, links } } = response;
    yield put(getProductsSuccess({ data: categoriesData, meta, links }));
    return response;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getProductsError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getProductSaga({ productId }: ReturnType<typeof getProductRequest>):
  Generator<CallEffect<AxiosResponse<IProduct> | AxiosError>
    | CallEffect<AxiosResponse<IProductGallery[]> | AxiosError>
    | PutEffect,
    IProduct,
    never> {
  try {
    const { data }: AxiosResponse<IProduct> = yield call(() => getProduct(productId));
    yield call(getProductGalleriesSaga, { productId, type: GET_PRODUCT_GALLERIES });
    yield put(getProductSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getProductError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* updateProductSaga({ payload }: ReturnType<typeof updateProductRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data: updatedData, productId } = payload;
    const { data }: AxiosResponse = yield call(() => updateProduct(productId, updatedData));
    yield put(updateProductSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getProductsSaga, { limit, page, query, type: GET_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(updateProductError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* productSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_PRODUCT, addProductSaga)]);
  yield all([takeLatest(DELETE_PRODUCT, deleteProductSaga)]);
  yield all([takeLatest(GET_PRODUCTS, getProductsSaga)]);
  yield all([takeLatest(GET_PRODUCT, getProductSaga)]);
  yield all([takeLatest(UPDATE_PRODUCT, updateProductSaga)]);
}
