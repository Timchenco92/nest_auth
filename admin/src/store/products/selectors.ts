// Modules
import { createSelector } from 'reselect';

// State
import type { ProductState } from '@/store/rootReducer';

const addProduct = (state: ProductState) => ({
  error: state.product.addProduct.error,
  pending: state.product.addProduct.pending,
  success: state.product.addProduct.success,
});

const deleteProduct = (state: ProductState) => ({
  error: state.product.deleteProduct.error,
  pending: state.product.deleteProduct.pending,
  success: state.product.deleteProduct.success,
});

const getProducts = (state: ProductState) => ({
  data: state.product.products.data,
  error: state.product.products.error,
  pending: state.product.products.pending,
  success: state.product.products.success,
});

const getProduct = (state: ProductState) => ({
  data: state.product.product.data,
  error: state.product.product.error,
  pending: state.product.product.pending,
  success: state.product.product.success,
});

const paginationAndSearch = (state: ProductState) => ({
  limit: state.product.paginationAndSearch.limit,
  page: state.product.paginationAndSearch.page,
  query: state.product.paginationAndSearch.query,
});

const updateProduct = (state: ProductState) => ({
  error: state.product.updateProduct.error,
  pending: state.product.updateProduct.pending,
  success: state.product.updateProduct.success,
});

export const addProductErrorSelector = createSelector(addProduct, ({ error }) => error);
export const addProductPendingSelector = createSelector(addProduct, ({ pending }) => pending);
export const addProductSuccessSelector = createSelector(addProduct, ({ success }) => success);

export const deleteProductErrorSelector = createSelector(deleteProduct, ({ error }) => error);
export const deleteProductPendingSelector = createSelector(deleteProduct, ({ pending }) => pending);
export const deleteProductSuccessSelector = createSelector(deleteProduct, ({ success }) => success);

export const productsSelector = createSelector(getProducts, ({ data }) => data);
export const productsErrorSelector = createSelector(getProducts, ({ error }) => error);
export const productsPendingSelector = createSelector(getProducts, ({ pending }) => pending);
export const productsSuccessSelector = createSelector(getProducts, ({ success }) => success);

export const productSelector = createSelector(getProduct, ({ data }) => data);
export const productErrorSelector = createSelector(getProduct, ({ error }) => error);
export const productPendingSelector = createSelector(getProduct, ({ pending }) => pending);
export const productSuccessSelector = createSelector(getProduct, ({ success }) => success);

export const paginationAndQuerySearchSelector = createSelector(
  paginationAndSearch, ({ limit, page, query }) => ({ limit, page, query }),
);

export const updateProductErrorSelector = createSelector(updateProduct, ({ error }) => error);
export const updateProductPendingSelector = createSelector(updateProduct, ({ pending }) => pending);
export const updateProductSuccessSelector = createSelector(updateProduct, ({ success }) => success);
