// Modules
import { legacy_createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
// import logger from 'redux-logger';

// Reducers
import rootReducer from '@/store/rootReducer';

// Sagas
import rootSaga from '@/store/rootSaga';

const sagaMiddleware = createSagaMiddleware();

// const store = legacy_createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware, logger)));
const store = legacy_createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(rootSaga);

export default store;
