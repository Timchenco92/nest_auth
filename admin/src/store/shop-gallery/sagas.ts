// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect } from 'redux-saga/effects';

// Actions
import {
  addShopGalleryError,
  addShopGalleryRequest,
  addShopGallerySuccess,
  deleteShopGalleryError,
  deleteShopGalleryRequest,
  deleteShopGallerySuccess,
  getShopGalleriesError,
  getShopGalleriesRequest,
  getShopGalleriesSuccess,
} from '@/store/shop-gallery';

import { showToast } from '@/store/toast';

// Action Types
import { ADD_SHOP_GALLERY, DELETE_SHOP_GALLERY, GET_SHOP_GALLERIES } from '@/store/shop-gallery';
import { GET_SHOP } from '@/store/shops';

// Saga
import { getShopSaga } from '@/store/shops';

// Services
import { shopGalleryService } from '@/services';

const { addShopGallery, deleteShopGallery, getShopGalleries } = shopGalleryService;

function* addShopGallerySaga({ payload }: ReturnType<typeof addShopGalleryRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { shopId, data: formData } = payload;
    const { data }: AxiosResponse = yield call(() => addShopGallery(formData));
    yield put(addShopGallerySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getShopSaga, { shopId, type: GET_SHOP });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addShopGalleryError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteShopGallerySaga({ payload }: ReturnType<typeof deleteShopGalleryRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { shopId, shopGalleryId } = payload;
    const { data }: AxiosResponse = yield call(() => deleteShopGallery(shopGalleryId));
    yield put(deleteShopGallerySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getShopSaga, { shopId, type: GET_SHOP });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteShopGalleryError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getShopGalleriesSaga({ shopId }: ReturnType<typeof getShopGalleriesRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getShopGalleries(shopId));
    yield put(getShopGalleriesSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getShopGalleriesError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* shopGallerySaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_SHOP_GALLERY, addShopGallerySaga)]);
  yield all([takeLatest(DELETE_SHOP_GALLERY, deleteShopGallerySaga)]);
  yield all([takeLatest(GET_SHOP_GALLERIES, getShopGalleriesSaga)]);
}
