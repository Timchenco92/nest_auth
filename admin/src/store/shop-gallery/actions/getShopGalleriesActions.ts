// Action Types
import { GET_SHOP_GALLERIES, GET_SHOP_GALLERIES_ERROR, GET_SHOP_GALLERIES_SUCCESS } from '@/store/shop-gallery';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

// Types
import type { IGetShopGalleries, IGetShopGalleriesError, IGetShopGalleriesSuccess } from '@/store/shop-gallery';

export const getShopGalleriesRequest = (shopId: IShop['id']): IGetShopGalleries => ({
  type: GET_SHOP_GALLERIES,
  shopId,
});

export const getShopGalleriesError = (): IGetShopGalleriesError => ({
  type: GET_SHOP_GALLERIES_ERROR,
});

export const getShopGalleriesSuccess = (payload: IShopGallery[]): IGetShopGalleriesSuccess => ({
  type: GET_SHOP_GALLERIES_SUCCESS,
  payload,
});
