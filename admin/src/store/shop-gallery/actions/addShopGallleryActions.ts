// Action Types
import {
  ADD_SHOP_GALLERY,
  ADD_SHOP_GALLERY_ERROR,
  ADD_SHOP_GALLERY_SUCCESS,
} from '@/store/shop-gallery';

// Types
import type {
  IAddShopGallery,
  IAddShopGalleryError,
  IAddShopGalleryErrorStateAndPayload,
  IAddShopGallerySuccess,
  IAddShopGalleryPayload,
} from '@/store/shop-gallery';

export const addShopGalleryRequest = (payload: IAddShopGalleryPayload): IAddShopGallery => ({
  type: ADD_SHOP_GALLERY,
  payload,
});

export const addShopGalleryError = (payload: IAddShopGalleryErrorStateAndPayload): IAddShopGalleryError => ({
  type: ADD_SHOP_GALLERY_ERROR,
  payload,
});

export const addShopGallerySuccess = (): IAddShopGallerySuccess => ({
  type: ADD_SHOP_GALLERY_SUCCESS,
});
