// Action Types
import {
  DELETE_SHOP_GALLERY,
  DELETE_SHOP_GALLERY_ERROR,
  DELETE_SHOP_GALLERY_SUCCESS,
} from '@/store/shop-gallery';

// Types
import type {
  IDeleteShopGallery,
  IDeleteShopGalleryError,
  IDeleteShopGalleryPayload,
  IDeleteShopGallerySuccess,
} from '@/store/shop-gallery';

export const deleteShopGalleryRequest = (payload: IDeleteShopGalleryPayload): IDeleteShopGallery => ({
  type: DELETE_SHOP_GALLERY,
  payload,
});

export const deleteShopGalleryError = (): IDeleteShopGalleryError => ({
  type: DELETE_SHOP_GALLERY_ERROR,
});

export const deleteShopGallerySuccess = (): IDeleteShopGallerySuccess => ({
  type: DELETE_SHOP_GALLERY_SUCCESS,
});
