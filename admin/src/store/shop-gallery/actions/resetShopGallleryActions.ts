// Action Types
import {
  RESET_ADD_SHOP_GALLERY_ERROR,
  RESET_ADD_SHOP_GALLERY_SUCCESS,
  RESET_DELETE_SHOP_GALLERY_SUCCESS,
} from '@/store/shop-gallery';

// Types
import type {
  IResetAddShopGalleryError,
  IResetAddShopGallerySuccess,
  IResetDeleteShopGallerySuccess,
} from '@/store/shop-gallery';

export const resetAddShopGalleryError = (): IResetAddShopGalleryError => ({
  type: RESET_ADD_SHOP_GALLERY_ERROR,
});

export const resetAddShopGallerySuccess = (): IResetAddShopGallerySuccess => ({
  type: RESET_ADD_SHOP_GALLERY_SUCCESS,
});

export const resetDeleteShopGallerySuccess = (): IResetDeleteShopGallerySuccess => ({
  type: RESET_DELETE_SHOP_GALLERY_SUCCESS,
});
