// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_SHOP_GALLERY,
  ADD_SHOP_GALLERY_ERROR,
  ADD_SHOP_GALLERY_SUCCESS,
  DELETE_SHOP_GALLERY,
  DELETE_SHOP_GALLERY_ERROR,
  DELETE_SHOP_GALLERY_SUCCESS,
  GET_SHOP_GALLERIES,
  GET_SHOP_GALLERIES_ERROR,
  GET_SHOP_GALLERIES_SUCCESS,
  RESET_ADD_SHOP_GALLERY_ERROR,
  RESET_ADD_SHOP_GALLERY_SUCCESS,
  RESET_DELETE_SHOP_GALLERY_SUCCESS,
} from '@/store/shop-gallery';

// Types
import type { IShopGalleryState, TShopGalleryTypes } from '@/store/shop-gallery';

const initialState: IShopGalleryState = {
  addShopGallery: {
    error: {
      hasError: false,
      errors: {
        image: '',
        shopId: '',
      },
    },
    pending: false,
    success: false,
  },
  deleteShopGallery: {
    error: false,
    pending: false,
    success: false,
  },
  shopGalleries: {
    data: [],
    error: false,
    pending: false,
  },
};

export const shopGalleryReducer: Reducer<IShopGalleryState, TShopGalleryTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SHOP_GALLERY: {
      return {
        ...state,
        addShopGallery: {
          error: { ...state.addShopGallery.error },
          pending: true,
          success: false,
        }
      }
    }
    case ADD_SHOP_GALLERY_ERROR: {
      return {
        ...state,
        addShopGallery: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        }
      }
    }
    case ADD_SHOP_GALLERY_SUCCESS: {
      return {
        ...state,
        addShopGallery: {
          error: { ...state.addShopGallery.error },
          pending: false,
          success: true,
        }
      }
    }
    case DELETE_SHOP_GALLERY: {
      return {
        ...state,
        deleteShopGallery: {
          error: false,
          pending: true,
          success: false,
        }
      }
    }
    case DELETE_SHOP_GALLERY_ERROR: {
      return {
        ...state,
        deleteShopGallery: {
          error: true,
          pending: false,
          success: false,
        }
      }
    }
    case DELETE_SHOP_GALLERY_SUCCESS: {
      return {
        ...state,
        deleteShopGallery: {
          error: false,
          pending: false,
          success: true,
        }
      }
    }
    case GET_SHOP_GALLERIES: {
      return {
        ...state,
        shopGalleries: {
          data: [],
          error: false,
          pending: true,
        },
      }
    }
    case GET_SHOP_GALLERIES_ERROR: {
      return {
        ...state,
        shopGalleries: {
          data: [],
          error: true,
          pending: false,
        },
      }
    }
    case GET_SHOP_GALLERIES_SUCCESS: {
      return {
        ...state,
        shopGalleries: {
          data: action.payload,
          error: false,
          pending: false,
        },
      }
    }
    case RESET_ADD_SHOP_GALLERY_ERROR: {
      return {
        ...state,
        addShopGallery: {
          error: {
            hasError: false,
            errors: { shopId: '', image: '' },
          },
          pending: false,
          success: false,
        }
      }
    }
    case RESET_ADD_SHOP_GALLERY_SUCCESS: {
      return {
        ...state,
        addShopGallery: {
          error: {
            hasError: false,
            errors: { shopId: '', image: '' },
          },
          pending: false,
          success: false,
        }
      }
    }
    case RESET_DELETE_SHOP_GALLERY_SUCCESS: {
      return {
        ...state,
        deleteShopGallery: {
          error: false,
          pending: false,
          success: false,
        },
      }
    }
    default: {
      return state;
    }
  }
}
