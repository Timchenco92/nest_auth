// Action Types
import { ADD_SHOP_GALLERY, ADD_SHOP_GALLERY_ERROR, ADD_SHOP_GALLERY_SUCCESS } from '@/store/shop-gallery';

// Interfaces
import { IShop } from '@/interfaces/models';

export interface IAddShopGalleryState {
  error: IAddShopGalleryErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddShopGallery {
  type: typeof ADD_SHOP_GALLERY;
  payload: IAddShopGalleryPayload;
}

export interface IAddShopGalleryPayload {
  data: FormData;
  shopId: IShop['id'];
}

export interface IAddShopGalleryError {
  type: typeof ADD_SHOP_GALLERY_ERROR;
  payload: IAddShopGalleryErrorStateAndPayload;
}

export interface IAddShopGalleryErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

interface IErrors {
  shopId: string;
  image: string;
}

export interface IAddShopGallerySuccess {
  type: typeof ADD_SHOP_GALLERY_SUCCESS;
}
