// Action Types
import {
  RESET_ADD_SHOP_GALLERY_ERROR,
  RESET_ADD_SHOP_GALLERY_SUCCESS,
  RESET_DELETE_SHOP_GALLERY_SUCCESS,
} from '@/store/shop-gallery';

export interface IResetAddShopGalleryError {
  type: typeof RESET_ADD_SHOP_GALLERY_ERROR;
}

export interface IResetAddShopGallerySuccess {
  type: typeof RESET_ADD_SHOP_GALLERY_SUCCESS;
}

export interface IResetDeleteShopGallerySuccess {
  type: typeof RESET_DELETE_SHOP_GALLERY_SUCCESS;
}
