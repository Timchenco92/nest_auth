// Types
import type {
  IAddShopGallery,
  IAddShopGalleryError,
  IAddShopGalleryState,
  IAddShopGallerySuccess,
  IDeleteShopGallery,
  IDeleteShopGalleryError,
  IDeleteShopGalleryState,
  IDeleteShopGallerySuccess,
  IGetShopGalleries,
  IGetShopGalleriesError,
  IGetShopGalleriesState,
  IGetShopGalleriesSuccess,
  IResetAddShopGalleryError,
  IResetAddShopGallerySuccess,
  IResetDeleteShopGallerySuccess,
} from '@/store/shop-gallery';

export interface IShopGalleryState {
  addShopGallery: IAddShopGalleryState;
  deleteShopGallery: IDeleteShopGalleryState;
  shopGalleries: IGetShopGalleriesState;
}

export type TShopGalleryTypes = IAddShopGallery
| IAddShopGalleryError
| IAddShopGallerySuccess
| IDeleteShopGallery
| IDeleteShopGalleryError
| IDeleteShopGallerySuccess
| IResetAddShopGalleryError
| IResetAddShopGallerySuccess
| IResetDeleteShopGallerySuccess
| IGetShopGalleries
| IGetShopGalleriesError
| IGetShopGalleriesSuccess;
