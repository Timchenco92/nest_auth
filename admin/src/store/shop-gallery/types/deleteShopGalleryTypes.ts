// Action Types
import {
  DELETE_SHOP_GALLERY,
  DELETE_SHOP_GALLERY_ERROR,
  DELETE_SHOP_GALLERY_SUCCESS,
} from '@/store/shop-gallery';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

export interface IDeleteShopGalleryState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteShopGallery {
  type: typeof DELETE_SHOP_GALLERY;
  payload: IDeleteShopGalleryPayload;
}

export interface IDeleteShopGalleryPayload {
  shopGalleryId: IShopGallery['id'];
  shopId: IShop['id'];
}

export interface IDeleteShopGalleryError {
  type: typeof DELETE_SHOP_GALLERY_ERROR;
}

export interface IDeleteShopGallerySuccess {
  type: typeof DELETE_SHOP_GALLERY_SUCCESS;
}
