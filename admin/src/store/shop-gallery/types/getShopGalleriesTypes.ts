// Action Types
import { GET_SHOP_GALLERIES, GET_SHOP_GALLERIES_ERROR, GET_SHOP_GALLERIES_SUCCESS } from '@/store/shop-gallery';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

export interface IGetShopGalleriesState {
  data: IShopGallery[];
  error: boolean;
  pending: boolean;
}

export interface IGetShopGalleries {
  type: typeof GET_SHOP_GALLERIES;
  shopId: IShop['id'];
}

export interface IGetShopGalleriesError {
  type: typeof GET_SHOP_GALLERIES_ERROR;
}

export interface IGetShopGalleriesSuccess {
  type: typeof GET_SHOP_GALLERIES_SUCCESS;
  payload: IShopGallery[];
}
