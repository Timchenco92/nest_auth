export const ADD_SHOP_GALLERY = 'ADD_SHOP_GALLERY';
export const ADD_SHOP_GALLERY_ERROR = 'ADD_SHOP_GALLERY_ERROR';
export const ADD_SHOP_GALLERY_SUCCESS = 'ADD_SHOP_GALLERY_SUCCESS';

export const DELETE_SHOP_GALLERY = 'DELETE_SHOP_GALLERY';
export const DELETE_SHOP_GALLERY_ERROR = 'DELETE_SHOP_GALLERY_ERROR';
export const DELETE_SHOP_GALLERY_SUCCESS = 'DELETE_SHOP_GALLERY_SUCCESS';

export const GET_SHOP_GALLERIES = 'GET_SHOP_GALLERIES';
export const GET_SHOP_GALLERIES_ERROR = 'GET_SHOP_GALLERIES_ERROR';
export const GET_SHOP_GALLERIES_SUCCESS = 'GET_SHOP_GALLERIES_SUCCESS';

export const RESET_ADD_SHOP_GALLERY_ERROR = 'RESET_ADD_SHOP_GALLERY_ERROR';
export const RESET_ADD_SHOP_GALLERY_SUCCESS = 'RESET_ADD_SHOP_GALLERY_SUCCESS';
export const RESET_DELETE_SHOP_GALLERY_SUCCESS = 'RESET_DELETE_SHOP_GALLERY_SUCCESS';
