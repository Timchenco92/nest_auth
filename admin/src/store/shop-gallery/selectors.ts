// Modules
import { createSelector } from 'reselect';

// State
import type { ShopGalleryState } from '@/store/rootReducer';

const addShopGallery = (state: ShopGalleryState) => ({
  error: state.shopGallery.addShopGallery.error,
  pending: state.shopGallery.addShopGallery.pending,
  success: state.shopGallery.addShopGallery.success,
});

const deleteShopGallery = (state: ShopGalleryState) => ({
  error: state.shopGallery.deleteShopGallery.error,
  pending: state.shopGallery.deleteShopGallery.pending,
  success: state.shopGallery.deleteShopGallery.success,
});

const getShopGalleries = (state: ShopGalleryState) => ({
  data: state.shopGallery.shopGalleries.data,
  error: state.shopGallery.shopGalleries.error,
  pending: state.shopGallery.shopGalleries.pending,
});

export const addShopGalleryErrorSelector = createSelector(addShopGallery, ({ error }) => error);
export const addShopGalleryPendingSelector = createSelector(addShopGallery, ({ pending }) => pending);
export const addShopGallerySuccessSelector = createSelector(addShopGallery, ({ success }) => success);

export const deleteShopGalleryErrorSelector = createSelector(deleteShopGallery, ({ error }) => error);
export const deleteShopGalleryPendingSelector = createSelector(deleteShopGallery, ({ pending }) => pending);
export const deleteShopGallerySuccessSelector = createSelector(deleteShopGallery, ({ success }) => success);

export const shopGalleriesSelector = createSelector(getShopGalleries, ({ data }) => data);
export const shopGalleriesErrorSelector = createSelector(getShopGalleries, ({ error }) => error);
export const shopGalleriesPendingSelector = createSelector(getShopGalleries, ({ pending }) => pending);
