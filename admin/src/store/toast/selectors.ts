// Modules
import { createSelector } from 'reselect';

// State
import type { ToastState } from '@/store/rootReducer';

const toasts = (state: ToastState) => ({
  isShowing: state.toast.isShowing,
  message: state.toast.message,
  type: state.toast.type,
});

export const isShowingSelector = createSelector(toasts, ({ isShowing }) => isShowing);
export const messageSelector = createSelector(toasts, ({ message }) => message);
export const typeSelector = createSelector(toasts, ({ type }) => type);
