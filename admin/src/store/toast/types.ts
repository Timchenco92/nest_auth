// Modules
import { AlertColor } from '@mui/material';

// Action Types
import { HIDE_TOAST, SHOW_TOAST } from '@/store/toast';

export interface IToastState {
  isShowing: boolean;
  message: string;
  type: AlertColor;
}

export interface IToastPayload {
  isShowing: boolean;
  type: AlertColor;
  message: string;
}

export interface IToastHideAction {
  type: typeof HIDE_TOAST;
  payload: IToastPayload;
}

export interface IToastShowAction {
  type: typeof SHOW_TOAST;
  payload: IToastPayload;
}

export type TToastActions = IToastHideAction | IToastShowAction;
