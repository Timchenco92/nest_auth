// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  RESET_UPDATE_ADMIN_ERROR,
  RESET_UPDATE_ADMIN_SUCCESS,
  UPDATE_ADMIN,
  UPDATE_ADMIN_ERROR,
  UPDATE_ADMIN_SUCCESS,
} from '@/store/settings';

// Types
import type { ISettingsState, TSettingsType } from '@/store/settings';

const initialState: ISettingsState = {
  updateSettings: {
    error: {
      hasError: false,
      errors: {
        password: '',
        email: '',
        firstName: '',
        lastName: '',
        middleName: '',
        phone: '',
      },
    },
    pending: false,
    success: false,
  },
};

export const settingsReducer: Reducer<ISettingsState, TSettingsType> = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_ADMIN: {
      return {
        ...state,
        updateSettings: {
          error: state.updateSettings.error,
          pending: true,
          success: false,
        },
      };
    }
    case UPDATE_ADMIN_ERROR: {
      return {
        ...state,
        updateSettings: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_ADMIN_SUCCESS: {
      return {
        ...state,
        updateSettings: {
          error: state.updateSettings.error,
          pending: false,
          success: true,
        },
      };
    }
    case RESET_UPDATE_ADMIN_ERROR: {
      return {
        ...state,
        updateSettings: {
          error: {
            hasError: false,
            errors: {
              firstName: '',
              middleName: '',
              lastName: '',
              email: '',
              phone: '',
              password: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_UPDATE_ADMIN_SUCCESS: {
      return {
        ...state,
        updateSettings: {
          error: {
            hasError: false,
            errors: {
              firstName: '',
              middleName: '',
              lastName: '',
              email: '',
              phone: '',
              password: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    default: {
      return state;
    }
  }
};
