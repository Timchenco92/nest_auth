// Action Types
import { UPDATE_ADMIN, UPDATE_ADMIN_ERROR, UPDATE_ADMIN_SUCCESS } from '@/store/settings';

// Types
import type {
  IUpdateAdmin,
  IUpdateAdminError,
  IUpdateAdminErrorStateAndPayload,
  IUpdateAdminPayload,
  IUpdateAdminSuccess,
} from '@/store/settings';

export const updateAdminSettingsRequest = (payload: IUpdateAdminPayload): IUpdateAdmin => ({
  type: UPDATE_ADMIN,
  payload,
});

export const updateAdminSettingsError = (payload: IUpdateAdminErrorStateAndPayload): IUpdateAdminError => ({
  type: UPDATE_ADMIN_ERROR,
  payload,
});

export const updateAdminSettingsSuccess = (): IUpdateAdminSuccess => ({
  type: UPDATE_ADMIN_SUCCESS,
});
