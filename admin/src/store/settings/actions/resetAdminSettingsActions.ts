// Action Types
import { RESET_UPDATE_ADMIN_ERROR, RESET_UPDATE_ADMIN_SUCCESS } from '@/store/settings';

// Types
import type { IResetUpdateAdminError, IResetUpdateAdminSuccess } from '@/store/settings';

export const resetUpdateAdminError = (): IResetUpdateAdminError => ({
  type: RESET_UPDATE_ADMIN_ERROR,
});

export const resetUpdateAdminSuccess = (): IResetUpdateAdminSuccess => ({
  type: RESET_UPDATE_ADMIN_SUCCESS,
});
