// Action Types
import { RESET_UPDATE_ADMIN_ERROR, RESET_UPDATE_ADMIN_SUCCESS } from '@/store/settings';

export interface IResetUpdateAdminError {
  type: typeof RESET_UPDATE_ADMIN_ERROR;
}

export interface IResetUpdateAdminSuccess {
  type: typeof RESET_UPDATE_ADMIN_SUCCESS;
}
