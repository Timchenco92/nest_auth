// Types
import type {
  IResetUpdateAdminError,
  IResetUpdateAdminSuccess,
  IUpdateAdmin,
  IUpdateAdminError,
  IUpdateAdminSettingsState,
  IUpdateAdminSuccess,
} from '@/store/settings';

export interface ISettingsState {
  updateSettings: IUpdateAdminSettingsState;
}

export type TSettingsType = IUpdateAdmin |
  IUpdateAdminError |
  IUpdateAdminSuccess |
  IResetUpdateAdminError |
  IResetUpdateAdminSuccess;
