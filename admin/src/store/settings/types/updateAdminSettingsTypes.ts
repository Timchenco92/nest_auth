// Action Types
import { UPDATE_ADMIN, UPDATE_ADMIN_ERROR, UPDATE_ADMIN_SUCCESS } from '@/store/settings';

// Interfaces
import type { IAdmin } from '@/interfaces/models';

export interface IUpdateAdminSettingsState {
  error: IUpdateAdminErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IUpdateAdmin {
  type: typeof UPDATE_ADMIN;
  payload: IUpdateAdminPayload;
}

export interface IUpdateAdminPayload {
  data: IUpdateAdminPayloadData;
  id: IAdmin['id'];
}

export interface IUpdateAdminPayloadData {
  firstName?: string;
  middleName?: string;
  lastName?: string;
  phone?: string;
  email?: string;
  password?: string;
}

export interface IUpdateAdminError {
  type: typeof UPDATE_ADMIN_ERROR;
  payload: IUpdateAdminErrorStateAndPayload;
}

export interface IUpdateAdminErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

interface IErrors {
  firstName: string;
  middleName: string;
  lastName: string;
  phone: string;
  email: string;
  password: string;
}

export interface IUpdateAdminSuccess {
  type: typeof UPDATE_ADMIN_SUCCESS;
}
