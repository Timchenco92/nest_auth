// Modules
import { createSelector } from 'reselect';

// State
import type { SettingsState } from '@/store/rootReducer';

const updateAdminSettings = (state: SettingsState) => ({
  error: state.settings.updateSettings.error,
  pending: state.settings.updateSettings.pending,
  success: state.settings.updateSettings.success,
});

export const updateAdminSettingsErrorSelector = createSelector(updateAdminSettings, ({ error }) => error);
export const updateAdminSettingsPendingSelector = createSelector(updateAdminSettings, ({ pending }) => pending);
export const updateAdminSettingsSuccessSelector = createSelector(updateAdminSettings, ({ success }) => success);
