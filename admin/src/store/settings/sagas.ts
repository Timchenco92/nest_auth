// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect } from 'redux-saga/effects';

// Actions
import { updateAdminSettingsRequest, updateAdminSettingsError, updateAdminSettingsSuccess } from '@/store/settings';
import { showToast } from '@/store/toast';

// Action Types
import { UPDATE_ADMIN } from '@/store/settings';

// Sagas
import { getMeSaga } from '@/store/auth';

// Services
import { settingsServices } from '@/services';

const { updateAdminSettings } = settingsServices;

function* updateAdminSettingsSaga({ payload }: ReturnType<typeof updateAdminSettingsRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | CallEffect
    | PutEffect,
    CallEffect |
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { id, data: updatedData } = payload;
    const { data }: AxiosResponse = yield call(() => updateAdminSettings(id, updatedData));
    yield put(updateAdminSettingsSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getMeSaga);
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(updateAdminSettingsError(errors));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* settingsSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(UPDATE_ADMIN, updateAdminSettingsSaga)]);
}
