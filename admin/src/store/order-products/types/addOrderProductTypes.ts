// Action Types
import { ADD_ORDER_PRODUCT, ADD_ORDER_PRODUCT_ERROR, ADD_ORDER_PRODUCT_SUCCESS } from '@/store/order-products';

// Interfaces
import type { IOrder, IProduct } from '@/interfaces/models';

export interface IAddOrderProductState {
  error: IAddOrderProductErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddOrderProduct {
  type: typeof ADD_ORDER_PRODUCT;
  payload: IAddOrderProductPayload;
}

export interface IAddOrderProductPayload {
  orderId: IOrder['id'];
  orderProducts: IAddOrderProductsPayload[];
  orderTotal: number;
}

export interface IAddOrderProductsPayload {
  productId: IProduct['id'];
  total: string;
  quantity: number;
}

export interface IAddOrderProductError {
  type: typeof ADD_ORDER_PRODUCT_ERROR;
  payload: IAddOrderProductErrorStateAndPayload;
}

export interface IAddOrderProductErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

interface IErrors {
  orderId: string;
  productId: string;
  total: string;
  quantity: string;
  price: string;
}

export interface IAddOrderProductSuccess {
  type: typeof ADD_ORDER_PRODUCT_SUCCESS;
}
