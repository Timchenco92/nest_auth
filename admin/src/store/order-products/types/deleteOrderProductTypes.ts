// Action Types
import { DELETE_ORDER_PRODUCT, DELETE_ORDER_PRODUCT_ERROR, DELETE_ORDER_PRODUCT_SUCCESS } from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

export interface IDeleteOrderProductState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteOrderProduct {
  type: typeof DELETE_ORDER_PRODUCT;
  orderProductId: IOrderProduct['id'];
  orderId: IOrder['id'];
}

export interface IDeleteOrderProductError {
  type: typeof DELETE_ORDER_PRODUCT_ERROR;
}

export interface IDeleteOrderProductSuccess {
  type: typeof DELETE_ORDER_PRODUCT_SUCCESS;
}
