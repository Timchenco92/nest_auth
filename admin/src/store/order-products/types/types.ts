// Types
import type {
  IAddOrderProduct,
  IAddOrderProductError,
  IAddOrderProductState,
  IAddOrderProductSuccess,
  IDecrementOrderProductQuantity,
  IDecrementOrderProductQuantityError,
  IDecrementOrderProductQuantityState,
  IDecrementOrderProductQuantitySuccess,
  IDeleteOrderProduct,
  IDeleteOrderProductError,
  IDeleteOrderProductState,
  IDeleteOrderProductSuccess,
  IGetOrderProducts,
  IGetOrderProductsError,
  IGetOrderProductsState,
  IGetOrderProductsSuccess,
  IIncrementOrderProductQuantity,
  IIncrementOrderProductQuantityError,
  IIncrementOrderProductQuantityState,
  IIncrementOrderProductQuantitySuccess,
  IResetDeleteOrderProductSuccess,
} from '@/store/order-products';

export interface IOrderProductState {
  addOrderProduct: IAddOrderProductState;
  deleteOrderProduct: IDeleteOrderProductState;
  orderProducts: IGetOrderProductsState;
  incrementProductQuantity: IIncrementOrderProductQuantityState;
  decrementProductQuantity: IDecrementOrderProductQuantityState;
}

export type TOrderProductsTypes = IAddOrderProduct
| IAddOrderProductError
| IAddOrderProductSuccess
| IDecrementOrderProductQuantity
| IDecrementOrderProductQuantityError
| IDecrementOrderProductQuantitySuccess
| IDeleteOrderProduct
| IDeleteOrderProductError
| IDeleteOrderProductSuccess
| IGetOrderProducts
| IGetOrderProductsError
| IGetOrderProductsSuccess
| IIncrementOrderProductQuantity
| IIncrementOrderProductQuantityError
| IResetDeleteOrderProductSuccess
| IIncrementOrderProductQuantitySuccess;
