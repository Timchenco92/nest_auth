// Action Types
import { GET_ORDER_PRODUCTS, GET_ORDER_PRODUCTS_ERROR, GET_ORDER_PRODUCTS_SUCCESS } from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

export interface IGetOrderProductsState {
  data: IOrderProduct[];
  error: boolean;
  pending: boolean;
}

export interface IGetOrderProducts {
  type: typeof GET_ORDER_PRODUCTS;
  orderId: IOrder['id'];
}

export interface IGetOrderProductsError {
  type: typeof GET_ORDER_PRODUCTS_ERROR;
}

export interface IGetOrderProductsSuccess {
  type: typeof GET_ORDER_PRODUCTS_SUCCESS;
  payload: IOrderProduct[];
}
