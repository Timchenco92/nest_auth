// Action Types
import {
  INCREMENT_ORDER_PRODUCT_QUANTITY,
  INCREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
  INCREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
} from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

export interface IIncrementOrderProductQuantityState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IIncrementOrderProductQuantity {
  type: typeof INCREMENT_ORDER_PRODUCT_QUANTITY;
  quantity: number;
  orderId: IOrder['id'];
  orderProductId: IOrderProduct['id'];
}

export interface IIncrementOrderProductQuantityError {
  type: typeof INCREMENT_ORDER_PRODUCT_QUANTITY_ERROR;
}

export interface IIncrementOrderProductQuantitySuccess {
  type: typeof INCREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS;
}
