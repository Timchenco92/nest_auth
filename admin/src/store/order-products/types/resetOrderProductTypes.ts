// Action Types
import { RESET_ADD_ORDER_PRODUCT_SUCCESS, RESET_DELETE_ORDER_PRODUCT_SUCCESS } from '@/store/order-products';

export interface IResetAddOrderProductSuccess {
  type: typeof RESET_ADD_ORDER_PRODUCT_SUCCESS;
}

export interface IResetDeleteOrderProductSuccess {
  type: typeof RESET_DELETE_ORDER_PRODUCT_SUCCESS;
}
