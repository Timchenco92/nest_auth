// Action Types
import {
  DECREMENT_ORDER_PRODUCT_QUANTITY,
  DECREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
  DECREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
} from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

export interface IDecrementOrderProductQuantityState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDecrementOrderProductQuantity {
  type: typeof DECREMENT_ORDER_PRODUCT_QUANTITY;
  quantity: number;
  orderId: IOrder['id'];
  orderProductId: IOrderProduct['id'];
}

export interface IDecrementOrderProductQuantityError {
  type: typeof DECREMENT_ORDER_PRODUCT_QUANTITY_ERROR;
}

export interface IDecrementOrderProductQuantitySuccess  {
  type: typeof DECREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS;
}
