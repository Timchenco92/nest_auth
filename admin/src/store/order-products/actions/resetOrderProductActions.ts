// Action Types
import { RESET_ADD_ORDER_PRODUCT_SUCCESS, RESET_DELETE_ORDER_PRODUCT_SUCCESS } from '@/store/order-products';

// Types
import type { IResetAddOrderProductSuccess, IResetDeleteOrderProductSuccess } from '@/store/order-products';

export const resetAddOrderProductSuccess = (): IResetAddOrderProductSuccess => ({
  type: RESET_ADD_ORDER_PRODUCT_SUCCESS,
});

export const resetDeleteOrderProductSuccess = (): IResetDeleteOrderProductSuccess => ({
  type: RESET_DELETE_ORDER_PRODUCT_SUCCESS,
});
