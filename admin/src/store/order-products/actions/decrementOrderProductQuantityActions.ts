// Action Types
import {
  DECREMENT_ORDER_PRODUCT_QUANTITY,
  DECREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
  DECREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
} from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

// Types
import type {
  IDecrementOrderProductQuantity,
  IDecrementOrderProductQuantityError,
  IDecrementOrderProductQuantitySuccess,
} from '@/store/order-products';

export const decrementOrderProductQuantityRequest = (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']): IDecrementOrderProductQuantity => ({
  type: DECREMENT_ORDER_PRODUCT_QUANTITY,
  quantity,
  orderId,
  orderProductId
});

export const decrementOrderProductQuantityError = (): IDecrementOrderProductQuantityError => ({
  type: DECREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
});

export const decrementOrderProductQuantitySuccess = (): IDecrementOrderProductQuantitySuccess => ({
  type: DECREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
});
