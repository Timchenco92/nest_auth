// Action Types
import { ADD_ORDER_PRODUCT, ADD_ORDER_PRODUCT_ERROR, ADD_ORDER_PRODUCT_SUCCESS } from '@/store/order-products';

// Types
import type {
  IAddOrderProduct,
  IAddOrderProductError,
  IAddOrderProductErrorStateAndPayload,
  IAddOrderProductPayload,
  IAddOrderProductSuccess,
} from '@/store/order-products';

export const addOrderProductRequest = (payload: IAddOrderProductPayload): IAddOrderProduct => ({
  type: ADD_ORDER_PRODUCT,
  payload,
});

export const addOrderProductError = (payload: IAddOrderProductErrorStateAndPayload): IAddOrderProductError => ({
  type: ADD_ORDER_PRODUCT_ERROR,
  payload,
});

export const addOrderProductSuccess = (): IAddOrderProductSuccess => ({
  type: ADD_ORDER_PRODUCT_SUCCESS,
});
