// Action Types
import { GET_ORDER_PRODUCTS, GET_ORDER_PRODUCTS_ERROR, GET_ORDER_PRODUCTS_SUCCESS } from '@/store/order-products';

// Interfaces
import type { IOrder, IOrderProduct } from '@/interfaces/models';

// Types
import type { IGetOrderProducts, IGetOrderProductsError, IGetOrderProductsSuccess } from '@/store/order-products';

export const getOrderProductsRequest = (orderId: IOrder['id']): IGetOrderProducts => ({
  type: GET_ORDER_PRODUCTS,
  orderId,
});

export const getOrderProductsError = (): IGetOrderProductsError => ({
  type: GET_ORDER_PRODUCTS_ERROR,
});

export const getOrderProductsSuccess = (payload: IOrderProduct[]): IGetOrderProductsSuccess => ({
  type: GET_ORDER_PRODUCTS_SUCCESS,
  payload
});
