// Action Types
import { DELETE_ORDER_PRODUCT, DELETE_ORDER_PRODUCT_ERROR, DELETE_ORDER_PRODUCT_SUCCESS } from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

// Types
import type { IDeleteOrderProduct, IDeleteOrderProductError, IDeleteOrderProductSuccess } from '@/store/order-products';

export const deleteOrderProductRequest = (orderProductId: IOrderProduct['id'], orderId: IOrder['id']): IDeleteOrderProduct => ({
  type: DELETE_ORDER_PRODUCT,
  orderProductId,
  orderId,
});

export const deleteOrderProductError = (): IDeleteOrderProductError => ({
  type: DELETE_ORDER_PRODUCT_ERROR,
});

export const deleteOrderProductSuccess = (): IDeleteOrderProductSuccess => ({
  type: DELETE_ORDER_PRODUCT_SUCCESS,
});
