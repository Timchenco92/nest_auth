// Action Types
import {
  INCREMENT_ORDER_PRODUCT_QUANTITY,
  INCREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
  INCREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
} from '@/store/order-products';

// Interfaces
import type { IOrderProduct, IOrder } from '@/interfaces/models';

// Types
import type {
  IIncrementOrderProductQuantity,
  IIncrementOrderProductQuantityError,
  IIncrementOrderProductQuantitySuccess,
} from '@/store/order-products';

export const incrementOrderProductQuantityRequest = (quantity: number, orderId: IOrder['id'], orderProductId: IOrderProduct['id']): IIncrementOrderProductQuantity => ({
  type: INCREMENT_ORDER_PRODUCT_QUANTITY,
  quantity,
  orderId,
  orderProductId,
});

export const incrementOrderProductQuantityError = (): IIncrementOrderProductQuantityError => ({
  type: INCREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
});

export const incrementOrderProductQuantitySuccess = (): IIncrementOrderProductQuantitySuccess => ({
  type: INCREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
});
