// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_ORDER_PRODUCT,
  ADD_ORDER_PRODUCT_ERROR,
  ADD_ORDER_PRODUCT_SUCCESS,
  DECREMENT_ORDER_PRODUCT_QUANTITY,
  DECREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
  DECREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
  DELETE_ORDER_PRODUCT,
  DELETE_ORDER_PRODUCT_ERROR,
  DELETE_ORDER_PRODUCT_SUCCESS,
  GET_ORDER_PRODUCTS,
  GET_ORDER_PRODUCTS_ERROR,
  GET_ORDER_PRODUCTS_SUCCESS,
  INCREMENT_ORDER_PRODUCT_QUANTITY,
  INCREMENT_ORDER_PRODUCT_QUANTITY_ERROR,
  INCREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS,
  RESET_DELETE_ORDER_PRODUCT_SUCCESS,
} from '@/store/order-products';

// Types
import type { IOrderProductState, TOrderProductsTypes } from '@/store/order-products';

const initialState: IOrderProductState = {
  addOrderProduct: {
    error: {
      hasError: false,
      errors: {
        productId: '',
        orderId: '',
        total: '',
        price: '',
        quantity: '',
      },
    },
    pending: false,
    success: false,
  },
  deleteOrderProduct: {
    error: false,
    pending: false,
    success: false,
  },
  orderProducts: {
    data: [],
    error: false,
    pending: false,
  },
  incrementProductQuantity: {
    error: false,
    pending: false,
    success: false,
  },
  decrementProductQuantity: {
    error: false,
    pending: false,
    success: false,
  },
};

export const orderProductReducer: Reducer<IOrderProductState, TOrderProductsTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ORDER_PRODUCT: {
      return {
        ...state,
        addOrderProduct: {
          error: { ...state.addOrderProduct.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_ORDER_PRODUCT_ERROR: {
      return {
        ...state,
        addOrderProduct: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_ORDER_PRODUCT_SUCCESS: {
      return {
        ...state,
        addOrderProduct: {
          error: { ...state.addOrderProduct.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_ORDER_PRODUCT: {
      return {
        ...state,
        deleteOrderProduct: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_ORDER_PRODUCT_ERROR: {
      return {
        ...state,
        deleteOrderProduct: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_ORDER_PRODUCT_SUCCESS: {
      return {
        ...state,
        deleteOrderProduct: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_ORDER_PRODUCTS: {
      return {
        ...state,
        orderProducts: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_ORDER_PRODUCTS_ERROR: {
      return {
        ...state,
        orderProducts: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_ORDER_PRODUCTS_SUCCESS: {
      return {
        ...state,
        orderProducts: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case INCREMENT_ORDER_PRODUCT_QUANTITY: {
      return {
        ...state,
        incrementProductQuantity: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case INCREMENT_ORDER_PRODUCT_QUANTITY_ERROR: {
      return {
        ...state,
        incrementProductQuantity: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case INCREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS: {
      return {
        ...state,
        incrementProductQuantity: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case DECREMENT_ORDER_PRODUCT_QUANTITY: {
      return {
        ...state,
        decrementProductQuantity: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DECREMENT_ORDER_PRODUCT_QUANTITY_ERROR: {
      return {
        ...state,
        decrementProductQuantity: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DECREMENT_ORDER_PRODUCT_QUANTITY_SUCCESS: {
      return {
        ...state,
        decrementProductQuantity: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case RESET_DELETE_ORDER_PRODUCT_SUCCESS: {
      return {
        ...state,
        deleteOrderProduct: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    default : {
      return state;
    }
  }
};
