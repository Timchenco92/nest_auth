// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, delay, put, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect } from 'redux-saga/effects';

// Actions
import {
  addOrderProductError,
  addOrderProductRequest,
  addOrderProductSuccess,
  decrementOrderProductQuantityError,
  decrementOrderProductQuantityRequest,
  decrementOrderProductQuantitySuccess,
  deleteOrderProductError,
  deleteOrderProductRequest,
  deleteOrderProductSuccess,
  getOrderProductsError,
  getOrderProductsRequest,
  getOrderProductsSuccess,
  incrementOrderProductQuantityError,
  incrementOrderProductQuantityRequest,
  incrementOrderProductQuantitySuccess,
} from '@/store/order-products';

import { showToast } from '@/store/toast';

// Action Types
import {
  ADD_ORDER_PRODUCT,
  DECREMENT_ORDER_PRODUCT_QUANTITY,
  DELETE_ORDER_PRODUCT,
  GET_ORDER_PRODUCTS,
  INCREMENT_ORDER_PRODUCT_QUANTITY,
} from '@/store/order-products';

// Services
import { orderProductService } from '@/services';

const { addProductToOrder, deleteProductFromOrder, getOrderProducts, decrementOrderProductQuantity, incrementOrderProductQuantity } = orderProductService;

function* addOrderProductSaga({ payload }: ReturnType<typeof addOrderProductRequest>):
  Generator<CallEffect<AxiosResponse> | CallEffect | PutEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { orderId } = payload;
    const { data }: AxiosResponse = yield call(() => addProductToOrder(payload));
    yield put(addOrderProductSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrderProductsSaga, { orderId, type: GET_ORDER_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addOrderProductError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteOrderProductSaga({ orderProductId, orderId }: ReturnType<typeof deleteOrderProductRequest>):
  Generator<CallEffect<AxiosResponse>
    | PutEffect
    | CallEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => deleteProductFromOrder(orderProductId));
    yield put(deleteOrderProductSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrderProductsSaga, { orderId, type: GET_ORDER_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteOrderProductError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getOrderProductsSaga({ orderId }: ReturnType<typeof getOrderProductsRequest>):
  Generator<CallEffect<AxiosResponse>
    | PutEffect
    | CallEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getOrderProducts(orderId));
    yield put(getOrderProductsSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getOrderProductsError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* incrementOrderProductQuantitySaga({ quantity, orderId, orderProductId }: ReturnType<typeof incrementOrderProductQuantityRequest>):
  Generator<CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    yield delay(1000);
    const { data }: AxiosResponse = yield call(() => incrementOrderProductQuantity(orderProductId, quantity));
    yield put(incrementOrderProductQuantitySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrderProductsSaga, { orderId, type: GET_ORDER_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(incrementOrderProductQuantityError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* decrementOrderProductQuantitySaga({ orderProductId, quantity, orderId }: ReturnType<typeof decrementOrderProductQuantityRequest>):
  Generator<CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    yield delay(1000);
    const { data }: AxiosResponse = yield call(() => decrementOrderProductQuantity(orderProductId, quantity));
    yield put(decrementOrderProductQuantitySuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrderProductsSaga, { orderId, type: GET_ORDER_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(decrementOrderProductQuantityError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* orderProductSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_ORDER_PRODUCT, addOrderProductSaga)]);
  yield all([takeLatest(DELETE_ORDER_PRODUCT, deleteOrderProductSaga)]);
  yield all([takeLatest(GET_ORDER_PRODUCTS, getOrderProductsSaga)]);
  yield all([takeLatest(DECREMENT_ORDER_PRODUCT_QUANTITY, decrementOrderProductQuantitySaga)]);
  yield all([takeLatest(INCREMENT_ORDER_PRODUCT_QUANTITY, incrementOrderProductQuantitySaga)]);
}
