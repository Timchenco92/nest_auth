// Modules
import { createSelector } from 'reselect';

// State
import type { OrderProductState } from '@/store/rootReducer';

const addOrderProduct = (state: OrderProductState) => ({
  error: state.orderProducts.addOrderProduct.error,
  pending: state.orderProducts.addOrderProduct.pending,
  success: state.orderProducts.addOrderProduct.success,
});

const deleteOrderProduct = (state: OrderProductState) => ({
  error: state.orderProducts.deleteOrderProduct.error,
  pending: state.orderProducts.deleteOrderProduct.pending,
  success: state.orderProducts.deleteOrderProduct.success,
});

const getOrderProducts = (state: OrderProductState) => ({
  data: state.orderProducts.orderProducts.data,
  error: state.orderProducts.orderProducts.error,
  pending: state.orderProducts.orderProducts.pending,
});

const incrementOrderProductQuantity = (state: OrderProductState) => ({
  error: state.orderProducts.incrementProductQuantity.error,
  pending: state.orderProducts.incrementProductQuantity.pending,
  success: state.orderProducts.incrementProductQuantity.success,
});

const decrementOrderProductQuantity = (state: OrderProductState) => ({
  error: state.orderProducts.decrementProductQuantity.error,
  pending: state.orderProducts.decrementProductQuantity.pending,
  success: state.orderProducts.decrementProductQuantity.success,
});


export const addOrderProductErrorSelector = createSelector(addOrderProduct, ({ error }) => error);
export const addOrderProductPendingSelector = createSelector(addOrderProduct, ({ pending }) => pending);
export const addOrderProductSuccessSelector = createSelector(addOrderProduct, ({ success }) => success);

export const deleteOrderProductErrorSelector = createSelector(deleteOrderProduct, ({ error }) => error);
export const deleteOrderProductPendingSelector = createSelector(deleteOrderProduct, ({ pending }) => pending);
export const deleteOrderProductSuccessSelector = createSelector(deleteOrderProduct, ({ success }) => success);

export const orderProductsSelector = createSelector(getOrderProducts, ({ data }) => data);
export const orderProductsPendingSelector = createSelector(getOrderProducts, ({ error }) => error);
export const orderProductsSuccessSelector = createSelector(getOrderProducts, ({ pending }) => pending);

export const incrementOrderProductQuantityErrorSelector = createSelector(incrementOrderProductQuantity, ({ error }) => error);
export const incrementOrderProductQuantityPendingSelector = createSelector(incrementOrderProductQuantity, ({ pending }) => pending);
export const incrementOrderProductQuantitySuccessSelector = createSelector(incrementOrderProductQuantity, ({ success }) => success);

export const decrementOrderProductQuantityErrorSelector = createSelector(decrementOrderProductQuantity, ({ error }) => error);
export const decrementOrderProductQuantityPendingSelector = createSelector(decrementOrderProductQuantity, ({ pending }) => pending);
export const decrementOrderProductQuantitySuccessSelector = createSelector(decrementOrderProductQuantity, ({ success }) => success);
