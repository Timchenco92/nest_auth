// Modules
import { all, fork } from 'redux-saga/effects';

// Sagas
import { authSaga } from '@/store/auth';
import { bannerSaga } from '@/store/banners';
import { categorySaga } from '@/store/categories';
import { homeSaga } from '@/store/home';
import { orderProductSaga } from '@/store/order-products';
import { orderSaga } from '@/store/orders';
import { productGallerySaga } from '@/store/product-gallery';
import { productSaga } from '@/store/products';
import { settingsSaga } from '@/store/settings';
import { shopSaga } from '@/store/shops';
import { shopGallerySaga } from '@/store/shop-gallery';
import { toastSaga } from '@/store/toast';
import { userSaga } from '@/store/users';

function* rootSaga() {
  yield all([fork(authSaga)]);
  yield all([fork(bannerSaga)]);
  yield all([fork(categorySaga)]);
  yield all([fork(homeSaga)]);
  yield all([fork(orderProductSaga)]);
  yield all([fork(orderSaga)]);
  yield all([fork(productGallerySaga)]);
  yield all([fork(productSaga)]);
  yield all([fork(settingsSaga)]);
  yield all([fork(shopSaga)]);
  yield all([fork(shopGallerySaga)]);
  yield all([fork(toastSaga)]);
  yield all([fork(userSaga)]);
}

export default rootSaga;
