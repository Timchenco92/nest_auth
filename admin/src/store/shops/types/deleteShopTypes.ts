// Action Types
import { DELETE_SHOP, DELETE_SHOP_ERROR, DELETE_SHOP_SUCCESS } from '@/store/shops';

// interfaces
import type { IShop } from '@/interfaces/models';

export interface IDeleteShopState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteShop {
  type: typeof DELETE_SHOP;
  shopId: IShop['id'];
}

export interface IDeleteShopError {
  type: typeof DELETE_SHOP_ERROR;
}

export interface IDeleteShopSuccess {
  type: typeof DELETE_SHOP_SUCCESS;
}
