// Action Types
import { GET_SHOPS, GET_SHOPS_ERROR, GET_SHOPS_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

export interface IGetShopsState {
  data: IGetShopsSuccessStateAndPayload;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetShops {
  type: typeof GET_SHOPS;
  limit: string;
  page: string;
}

export interface IGetShopsError {
  type: typeof GET_SHOPS_ERROR;
}

export interface IGetShopsSuccess {
  type: typeof GET_SHOPS_SUCCESS;
  payload: IGetShopsSuccessStateAndPayload;
}

export interface IGetShopsSuccessStateAndPayload {
  data: IShop[];
  meta: any;
  links: any;
}
