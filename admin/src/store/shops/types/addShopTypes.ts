// Action Types
import { ADD_SHOP, ADD_SHOP_ERROR, ADD_SHOP_SUCCESS } from '@/store/shops';

export interface IAddShopState {
  error: IAddShopErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddShop {
  type: typeof ADD_SHOP;
  payload: FormData;
}

export interface IAddShopError {
  type: typeof ADD_SHOP_ERROR;
  payload: IAddShopErrorStateAndPayload;
}

export interface IAddShopErrorStateAndPayload {
  errors: IErrors;
  hasError: boolean;
}

interface IErrors {
  address: string;
  title: string;
  image: string;
  firstPhone: string;
  secondPhone: string;
  thirdPhone: string;
  startTimeWork: string;
  endTimeWork: string;
}

export interface IAddShopSuccess {
  type: typeof ADD_SHOP_SUCCESS;
}
