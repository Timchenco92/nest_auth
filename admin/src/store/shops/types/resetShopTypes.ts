// Action Types
import {
  RESET_ADD_SHOP_ERROR,
  RESET_ADD_SHOP_SUCCESS,
  RESET_DELETE_SHOP_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_UPDATE_SHOP_ERROR,
  RESET_UPDATE_SHOP_SUCCESS,
} from '@/store/shops';


export interface IResetAddShopError {
  type: typeof RESET_ADD_SHOP_ERROR;
}

export interface IResetAddShopSuccess {
  type: typeof RESET_ADD_SHOP_SUCCESS;
}

export interface IResetDeleteShopSuccess {
  type: typeof RESET_DELETE_SHOP_SUCCESS;
}

export interface IResetPaginationLimit {
  type: typeof RESET_PAGINATION_LIMIT;
}

export interface IResetPaginationPage {
  type: typeof RESET_PAGINATION_PAGE;
}

export interface IResetUpdateShopError {
  type: typeof RESET_UPDATE_SHOP_ERROR;
}

export interface IResetUpdateShopSuccess {
  type: typeof RESET_UPDATE_SHOP_SUCCESS;
}
