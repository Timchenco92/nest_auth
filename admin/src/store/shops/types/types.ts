// Types
import type {
  IAddShopState,
  IDeleteShopState,
  IGetShopState,
  IGetShopsState,
  IUpdateShopState,
  IPaginationState,
  IAddShop,
  IAddShopError,
  IAddShopSuccess,
  IChangeLimit,
  IChangePage,
  IDeleteShop,
  IDeleteShopError,
  IDeleteShopSuccess,
  IGetShop,
  IGetShopError,
  IGetShopSuccess,
  IGetShops,
  IGetShopsError,
  IGetShopsSuccess,
  IResetAddShopError,
  IResetAddShopSuccess,
  IResetDeleteShopSuccess,
  IResetPaginationLimit,
  IResetPaginationPage,
  IResetUpdateShopError,
  IResetUpdateShopSuccess,
  IUpdateShop,
  IUpdateShopError,
  IUpdateShopSuccess,
} from '@/store/shops';


export interface IShopState {
  addShop: IAddShopState;
  deleteShop: IDeleteShopState;
  shop: IGetShopState;
  shops: IGetShopsState;
  updateShop: IUpdateShopState;
  pagination: IPaginationState;
}

export type TShopTypes = IAddShop
| IAddShopError
| IAddShopSuccess
| IChangeLimit
| IChangePage
| IDeleteShop
| IDeleteShopError
| IDeleteShopSuccess
| IGetShop
| IGetShopError
| IGetShopSuccess
| IGetShops
| IGetShopsError
| IGetShopsSuccess
| IResetAddShopError
| IResetAddShopSuccess
| IResetDeleteShopSuccess
| IResetPaginationLimit
| IResetPaginationPage
| IResetUpdateShopError
| IResetUpdateShopSuccess
| IUpdateShop
| IUpdateShopError
| IUpdateShopSuccess;
