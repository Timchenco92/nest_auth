// Action Types
import { GET_SHOP, GET_SHOP_ERROR, GET_SHOP_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

export interface IGetShopState {
  data: IShop;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetShop {
  type: typeof GET_SHOP;
  shopId: IShop['id'];
}

export interface IGetShopError {
  type: typeof GET_SHOP_ERROR;
}

export interface IGetShopSuccess {
  type: typeof GET_SHOP_SUCCESS;
  payload: IShop;
}
