// Action Types
import { UPDATE_SHOP, UPDATE_SHOP_ERROR, UPDATE_SHOP_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

export interface IUpdateShopState {
  error: IUpdateShopErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IUpdateShop {
  type: typeof UPDATE_SHOP;
  shopId: IShop['id'];
  data: FormData;
}

export interface IUpdateShopError {
  type: typeof UPDATE_SHOP_ERROR;
  payload: IUpdateShopErrorStateAndPayload
}

export interface IUpdateShopErrorStateAndPayload {
  hasErrors: boolean;
  errors: IErrors
}

interface IErrors {
  address: string;
  title: string;
  image: string;
  firstPhone: string;
  secondPhone: string;
  thirdPhone: string;
  startTimeWork: string;
  endTimeWork: string;
}

export interface IUpdateShopSuccess {
  type: typeof UPDATE_SHOP_SUCCESS;
}
