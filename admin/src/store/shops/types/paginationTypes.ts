// Action Types
import { PAGINATION_CHANGE_LIMIT, PAGINATION_CHANGE_PAGE } from '@/store/shops';

export interface IPaginationState {
  limit: string;
  page: string;
}

export interface IChangeLimit {
  type: typeof PAGINATION_CHANGE_LIMIT;
  limit: string;
}

export interface IChangePage {
  type: typeof PAGINATION_CHANGE_PAGE;
  page: string
}
