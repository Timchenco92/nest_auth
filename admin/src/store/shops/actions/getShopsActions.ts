// Action Types
import { GET_SHOPS, GET_SHOPS_ERROR, GET_SHOPS_SUCCESS } from '@/store/shops';

// Types
import type { IGetShops, IGetShopsError, IGetShopsSuccess, IGetShopsSuccessStateAndPayload } from '@/store/shops';

export const getShopsRequest = (limit: string, page: string): IGetShops => ({
  type: GET_SHOPS,
  limit,
  page,
});

export const getShopsError = (): IGetShopsError => ({
  type: GET_SHOPS_ERROR,
});

export const getShopsSuccess = (payload: IGetShopsSuccessStateAndPayload): IGetShopsSuccess => ({
  type: GET_SHOPS_SUCCESS,
  payload,
});
