// Action Types
import { GET_SHOP, GET_SHOP_ERROR, GET_SHOP_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

// Types
import type { IGetShop, IGetShopError, IGetShopSuccess } from '@/store/shops';

export const getShopRequest = (shopId: IShop['id']): IGetShop => ({
  type: GET_SHOP,
  shopId,
});

export const getShopError = (): IGetShopError => ({
  type: GET_SHOP_ERROR,
});

export const getShopSuccess = (payload: IShop): IGetShopSuccess => ({
  type: GET_SHOP_SUCCESS,
  payload,
});
