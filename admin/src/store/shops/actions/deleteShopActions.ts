// Action Types
import { DELETE_SHOP, DELETE_SHOP_ERROR, DELETE_SHOP_SUCCESS } from '@/store/shops';

// Types
import type { IDeleteShop, IDeleteShopError, IDeleteShopSuccess } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

export const deleteShopRequest = (shopId: IShop['id']): IDeleteShop => ({
  type: DELETE_SHOP,
  shopId,
});

export const deleteShopError = (): IDeleteShopError => ({
  type: DELETE_SHOP_ERROR,
});

export const deleteShopSuccess = (): IDeleteShopSuccess => ({
  type: DELETE_SHOP_SUCCESS,
});
