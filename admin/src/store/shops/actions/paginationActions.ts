// Action Types
import { PAGINATION_CHANGE_LIMIT, PAGINATION_CHANGE_PAGE } from '@/store/shops';

// Types
import type { IChangeLimit, IChangePage } from '@/store/shops';

export const changeLimit = (limit: string): IChangeLimit => ({
  type: PAGINATION_CHANGE_LIMIT,
  limit,
});

export const changePage = (page: string): IChangePage => ({
  type: PAGINATION_CHANGE_PAGE,
  page,
});
