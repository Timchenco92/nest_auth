// Action Types
import {
  RESET_ADD_SHOP_ERROR,
  RESET_ADD_SHOP_SUCCESS,
  RESET_DELETE_SHOP_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_UPDATE_SHOP_ERROR,
  RESET_UPDATE_SHOP_SUCCESS,
} from '@/store/shops';

// Types
import type {
  IResetAddShopError,
  IResetAddShopSuccess,
  IResetDeleteShopSuccess,
  IResetPaginationLimit,
  IResetPaginationPage,
  IResetUpdateShopError,
  IResetUpdateShopSuccess,
} from '@/store/shops';

export const resetAddShopError = (): IResetAddShopError => ({
  type: RESET_ADD_SHOP_ERROR,
});

export const resetAddShopSuccess = (): IResetAddShopSuccess => ({
  type: RESET_ADD_SHOP_SUCCESS,
});

export const resetDeleteShopSuccess = (): IResetDeleteShopSuccess => ({
  type: RESET_DELETE_SHOP_SUCCESS,
});

export const resetPaginationLimit = (): IResetPaginationLimit => ({
  type: RESET_PAGINATION_LIMIT,
});

export const resetPaginationPage = (): IResetPaginationPage => ({
  type: RESET_PAGINATION_PAGE,
});

export const resetUpdateShopError = (): IResetUpdateShopError => ({
  type: RESET_UPDATE_SHOP_ERROR,
});

export const resetUpdateShopSuccess = (): IResetUpdateShopSuccess => ({
  type: RESET_UPDATE_SHOP_SUCCESS,
});
