// Action Types
import { UPDATE_SHOP, UPDATE_SHOP_ERROR, UPDATE_SHOP_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

// Types
import type { IUpdateShop, IUpdateShopError, IUpdateShopSuccess, IUpdateShopErrorStateAndPayload } from '@/store/shops';

export const updateShopRequest = (shopId: IShop['id'], data: FormData): IUpdateShop => ({
  type: UPDATE_SHOP,
  data,
  shopId,
});

export const updateShopError = (payload: IUpdateShopErrorStateAndPayload): IUpdateShopError => ({
  type: UPDATE_SHOP_ERROR,
  payload,
});

export const updateShopSuccess = (): IUpdateShopSuccess => ({
  type: UPDATE_SHOP_SUCCESS,
});
