// Action Types
import { ADD_SHOP, ADD_SHOP_ERROR, ADD_SHOP_SUCCESS } from '@/store/shops';

// Types
import type { IAddShop, IAddShopError, IAddShopSuccess, IAddShopErrorStateAndPayload } from '@/store/shops';

export const addShopRequest = (payload: FormData): IAddShop => ({
  type: ADD_SHOP,
  payload
});

export const addShopError = (payload: IAddShopErrorStateAndPayload): IAddShopError => ({
  type: ADD_SHOP_ERROR,
  payload,
});

export const addShopSuccess = (): IAddShopSuccess => ({
  type: ADD_SHOP_SUCCESS,
});
