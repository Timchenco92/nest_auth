// Modules
import { createSelector } from 'reselect';

// State
import type { ShopsState } from '@/store/rootReducer';

const addShop = (state: ShopsState) => ({
  error: state.shops.addShop.error,
  pending: state.shops.addShop.pending,
  success: state.shops.addShop.success,
});

const deleteShop = (state: ShopsState) => ({
  error: state.shops.deleteShop.error,
  pending: state.shops.deleteShop.pending,
  success: state.shops.deleteShop.success,
});

const getShop = (state: ShopsState) => ({
  data: state.shops.shop.data,
  error: state.shops.shop.error,
  pending: state.shops.shop.pending,
  success: state.shops.shop.success,
});

const getShops = (state: ShopsState) => ({
  data: state.shops.shops.data,
  error: state.shops.shops.error,
  pending: state.shops.shops.pending,
  success: state.shops.shops.success,
});

const pagination = (state: ShopsState) => ({
  page: state.shops.pagination.page,
  limit: state.shops.pagination.limit,
});

const updateShop = (state: ShopsState) => ({
  error: state.shops.updateShop.error,
  pending: state.shops.updateShop.pending,
  success: state.shops.updateShop.success,
});

export const addShopErrorSelector = createSelector(addShop, ({ error }) => error);
export const addShopPendingSelector = createSelector(addShop, ({ pending }) => pending);
export const addShopSuccessSelector = createSelector(addShop, ({ success }) => success);

export const deleteShopErrorSelector = createSelector(deleteShop, ({ error }) => error);
export const deleteShopPendingSelector = createSelector(deleteShop, ({ pending }) => pending);
export const deleteShopSuccessSelector = createSelector(deleteShop, ({ success }) => success);

export const shopSelector = createSelector(getShop, ({ data }) => data);
export const shopErrorSelector = createSelector(getShop, ({ error }) => error);
export const shopPendingSelector = createSelector(getShop, ({ pending }) => pending);
export const shopSuccessSelector = createSelector(getShop, ({ success }) => success);

export const shopsSelector = createSelector(getShops, ({ data }) => data);
export const shopsErrorSelector = createSelector(getShops, ({ error }) => error);
export const shopsPendingSelector = createSelector(getShops, ({ pending }) => pending);
export const shopsSuccessSelector = createSelector(getShops, ({ success }) => success);

export const paginationSelector = createSelector(pagination, ({ limit, page }) => ({ limit, page }));

export const updateShopErrorSelector = createSelector(updateShop, ({ error }) => error);
export const updateShopPendingSelector = createSelector(updateShop, ({ pending }) => pending);
export const updateShopSuccessSelector = createSelector(updateShop, ({ success }) => success);
