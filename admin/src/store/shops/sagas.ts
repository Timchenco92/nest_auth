// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect, SelectEffect } from 'redux-saga/effects';

// Actions
import {
  addShopError,
  addShopRequest,
  addShopSuccess,
  deleteShopError,
  deleteShopRequest,
  deleteShopSuccess,
  getShopError,
  getShopRequest,
  getShopSuccess,
  getShopsError,
  getShopsRequest,
  getShopsSuccess,
  updateShopError,
  updateShopRequest,
  updateShopSuccess,
} from '@/store/shops';

import { showToast } from '@/store/toast';

// Action Types
import { ADD_SHOP, DELETE_SHOP, GET_SHOP, GET_SHOPS, UPDATE_SHOP } from '@/store/shops';
import { GET_SHOP_GALLERIES } from '@/store/shop-gallery';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

// Sagas
import { getShopGalleriesSaga } from '@/store/shop-gallery';

// Selectors
import { paginationSelector } from '@/store/shops';

// Services
import { shopService } from '@/services';

const { addShop, deleteShop, getShop, getShops, updateShop } = shopService;

function* addShopSaga({ payload }: ReturnType<typeof addShopRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page } = yield select(paginationSelector);
    const { data }: AxiosResponse = yield call(() => addShop(payload));
    yield put(addShopSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getShopsSaga, { limit, page, type: GET_SHOPS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addShopError({ hasError: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteShopSaga({ shopId }: ReturnType<typeof deleteShopRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never>
{
  try {
    const { limit, page } = yield select(paginationSelector);
    const { data }: AxiosResponse = yield call(() => deleteShop(shopId));
    yield put(deleteShopSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getShopsSaga, { limit, page, type: GET_SHOPS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteShopError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getShopSaga({ shopId }: ReturnType<typeof getShopRequest>):
  Generator<CallEffect<AxiosResponse<IShop> | AxiosError>
    | CallEffect<AxiosResponse<IShopGallery[]> | AxiosError>
    | PutEffect,
    | AxiosError
    | unknown,
    never> {
  try {
    const { data }: AxiosResponse<IShop> = yield call(() => getShop(shopId));
    yield call(getShopGalleriesSaga, { shopId, type: GET_SHOP_GALLERIES })
    yield put(getShopSuccess(data));
    return data;
  } catch (error) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getShopError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getShopsSaga({ limit, page }: ReturnType<typeof getShopsRequest>):
Generator<
  CallEffect<AxiosResponse | AxiosError>
  | PutEffect,
  AxiosResponse
  | AxiosError,
  never
  >
{
  try {
    const { data }: AxiosResponse = yield call(() => getShops({ limit, page }));
    yield put(getShopsSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getShopsError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* updateShopSaga({ shopId, data: shopData }: ReturnType<typeof updateShopRequest>):
  Generator<
    SelectEffect
    | CallEffect<AxiosResponse | AxiosError>
    | PutEffect,
    AxiosResponse | AxiosError,
    never
    >
{
  try {
    const { limit, page } = yield select(paginationSelector);
    const { data }: AxiosResponse = yield call(() => updateShop(shopId, shopData));
    yield put(updateShopSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getShopsSaga, { limit, page, type: GET_SHOPS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(updateShopError({ hasErrors: true, errors }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* shopSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_SHOP, addShopSaga)]);
  yield all([takeLatest(DELETE_SHOP, deleteShopSaga)]);
  yield all([takeLatest(GET_SHOP, getShopSaga)]);
  yield all([takeLatest(GET_SHOPS, getShopsSaga)]);
  yield all([takeLatest(UPDATE_SHOP, updateShopSaga)]);
}
