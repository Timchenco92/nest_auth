// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  UPDATE_SHOP_SUCCESS,
  UPDATE_SHOP_ERROR,
  ADD_SHOP_ERROR,
  ADD_SHOP_SUCCESS,
  DELETE_SHOP,
  DELETE_SHOP_ERROR,
  DELETE_SHOP_SUCCESS,
  GET_SHOP,
  GET_SHOPS,
  GET_SHOPS_ERROR,
  GET_SHOPS_SUCCESS,
  GET_SHOP_ERROR,
  GET_SHOP_SUCCESS,
  RESET_ADD_SHOP_ERROR,
  RESET_ADD_SHOP_SUCCESS,
  RESET_DELETE_SHOP_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_UPDATE_SHOP_ERROR,
  RESET_UPDATE_SHOP_SUCCESS,
  UPDATE_SHOP,
  PAGINATION_CHANGE_PAGE,
  PAGINATION_CHANGE_LIMIT,
  ADD_SHOP,
} from '@/store/shops';

// Types
import type { IShopState, TShopTypes } from '@/store/shops';

const initialState: IShopState = {
  addShop: {
    error: {
      hasError: false,
      errors: {
        address: '',
        image: '',
        title: '',
        endTimeWork: '',
        startTimeWork: '',
        firstPhone: '',
        secondPhone: '',
        thirdPhone: '',
      },
    },
    pending: false,
    success: false,
  },
  deleteShop: {
    error: false,
    pending: false,
    success: false,
  },
  shop: {
    data: {
      id: '',
      address: '',
      title: '',
      image: '',
      imageUrl: '',
      createdAt: new Date(),
      endTimeWork: '',
      startTimeWork: '',
      firstPhone: '',
      secondPhone: '',
      thirdPhone: '',
      updatedAt: new Date(),
      galleries: [],
    },
    error: false,
    pending: false,
    success: false,
  },
  pagination: {
    limit: '15',
    page: '1',
  },
  shops: {
    data: {
      data: [],
      meta: {},
      links: {},
    },
    error: false,
    pending: false,
    success: false,
  },
  updateShop: {
    error: {
      hasErrors: false,
      errors: {
        address: '',
        image: '',
        title: '',
        thirdPhone: '',
        secondPhone: '',
        firstPhone: '',
        startTimeWork: '',
        endTimeWork: '',
      },
    },
    pending: false,
    success: false,
  },
};

export const shopReducer: Reducer<IShopState, TShopTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_SHOP: {
      return {
        ...state,
        addShop: {
          error: { ...state.addShop.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_SHOP_ERROR: {
      return {
        ...state,
        addShop: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_SHOP_SUCCESS: {
      return {
        ...state,
        addShop: {
          error: { ...state.addShop.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_SHOP: {
      return {
        ...state,
        deleteShop: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_SHOP_ERROR: {
      return {
        ...state,
        deleteShop: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_SHOP_SUCCESS: {
      return {
        ...state,
        deleteShop: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_SHOP: {
      return {
        ...state,
        shop: {
          data: { ...state.shop.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_SHOP_ERROR: {
      return {
        ...state,
        shop: {
          data: { ...state.shop.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_SHOP_SUCCESS: {
      return {
        ...state,
        shop: {
          data: action.payload,
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_SHOPS: {
      return {
        ...state,
        shops: {
          data: { ...state.shops.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_SHOPS_ERROR: {
      return {
        ...state,
        data: { ...state.shops.data },
        error: true,
        pending: false,
        success: false,
      };
    }
    case GET_SHOPS_SUCCESS: {
      return {
        ...state,
        shops: {
          data: {
            data: action.payload.data,
            meta: action.payload.meta,
            links: action.payload.links,
          },
          error: false,
          pending: false,
          success: true,
        }
      };
    }
    case PAGINATION_CHANGE_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: action.limit,
          page: state.pagination.page,
        },
      };
    }
    case PAGINATION_CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: action.page,
        },
      };
    }
    case RESET_ADD_SHOP_ERROR: {
      return {
        ...state,
        addShop: {
          error: {
            hasError: false,
            errors: {
              address: '',
              image: '',
              endTimeWork: '',
              startTimeWork: '',
              firstPhone: '',
              secondPhone: '',
              thirdPhone: '',
              title: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_ADD_SHOP_SUCCESS: {
      return {
        ...state,
        addShop: {
          error: {
            hasError: false,
            errors: {
              address: '',
              image: '',
              endTimeWork: '',
              startTimeWork: '',
              firstPhone: '',
              secondPhone: '',
              thirdPhone: '',
              title: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_DELETE_SHOP_SUCCESS: {
      return {
        ...state,
        deleteShop: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case RESET_PAGINATION_LIMIT: {
      return {
        ...state,
        pagination: {
          page: state.pagination.page,
          limit: '15',
        },
      };
    }
    case RESET_PAGINATION_PAGE: {
      return {
        ...state,
        pagination: {
          page: '1',
          limit: state.pagination.limit,
        },
      };
    }
    case RESET_UPDATE_SHOP_ERROR: {
      return {
        ...state,
        updateShop: {
          error: {
            hasErrors: false,
            errors: {
              address: '',
              image: '',
              title: '',
              thirdPhone: '',
              secondPhone: '',
              firstPhone: '',
              startTimeWork: '',
              endTimeWork: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_UPDATE_SHOP_SUCCESS: {
      return {
        ...state,
        updateShop: {
          error: {
            hasErrors: false,
            errors: {
              address: '',
              image: '',
              title: '',
              thirdPhone: '',
              secondPhone: '',
              firstPhone: '',
              startTimeWork: '',
              endTimeWork: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_SHOP: {
      return {
        ...state,
        updateShop: {
          error: { ...state.updateShop.error },
          pending: true,
          success: false,
        },
      };
    }
    case UPDATE_SHOP_ERROR: {
      return {
        ...state,
        updateShop: {
          error: {
            hasErrors: action.payload.hasErrors,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_SHOP_SUCCESS: {
      return {
        ...state,
        updateShop: {
          error: { ...state.updateShop.error },
          pending: false,
          success: true,
        },
      };
    }
    default: {
      return state;
    }
  }
};
