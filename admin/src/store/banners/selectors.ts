// Modules
import { createSelector } from 'reselect';

// State
import type { BannerState } from '@/store/rootReducer';

const addBanner = (state: BannerState) => ({
  error: state.banner.addBanner.error,
  pending: state.banner.addBanner.pending,
  success: state.banner.addBanner.success,
});

const deleteBanner = (state: BannerState) => ({
  error: state.banner.deleteBanner.error,
  pending: state.banner.deleteBanner.pending,
  success: state.banner.deleteBanner.success,
});

const getBanners = (state: BannerState) => ({
  data: state.banner.getBanners.data,
  error: state.banner.getBanners.error,
  pending: state.banner.getBanners.pending,
  success: state.banner.getBanners.success,
});

const pagination = (state: BannerState) => ({
  limit: state.banner.pagination.limit,
  page: state.banner.pagination.page,
});

export const addBannerErrorSelector = createSelector(addBanner, ({ error }) => error);
export const addBannerPendingSelector = createSelector(addBanner, ({ pending }) => pending);
export const addBannerSuccessSelector = createSelector(addBanner, ({ success }) => success);

export const deleteBannerErrorSelector = createSelector(deleteBanner, ({ error }) => error);
export const deleteBannerPendingSelector = createSelector(deleteBanner, ({ pending }) => pending);
export const deleteBannerSuccessSelector = createSelector(deleteBanner, ({ success }) => success);

export const bannersSelector = createSelector(getBanners, ({ data }) => data);
export const bannersErrorSelector = createSelector(getBanners, ({ error }) => error);
export const bannersPendingSelector = createSelector(getBanners, ({ pending }) => pending);
export const bannersSuccessSelector = createSelector(getBanners, ({ success }) => success);
export const paginationSelector = createSelector(pagination, ({ limit, page }) => ({ limit, page }))
