// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_BANNER,
  ADD_BANNER_ERROR,
  ADD_BANNER_SUCCESS,
  DELETE_BANNER,
  DELETE_BANNER_ERROR,
  DELETE_BANNER_SUCCESS,
  GET_BANNERS,
  GET_BANNERS_ERROR,
  GET_BANNERS_SUCCESS,
  PAGINATION_CHANGE_LIMIT,
  PAGINATION_CHANGE_PAGE,
  RESET_ADD_BANNER_SUCCESS,
  RESET_DELETE_BANNER_SUCCESS,
  RESET_PAGINATION,
} from '@/store/banners';

// Types
import type { IBannerState, TBannerTypes } from '@/store/banners';

const initialState: IBannerState = {
  addBanner: {
    error: {
      errors: {
        image: '',
        title: '',
      },
      hasError: false,
      message: '',
    },
    pending: false,
    success: false,
  },
  deleteBanner: {
    error: false,
    pending: false,
    success: false,
  },
  getBanners: {
    data: {
      data: [],
      meta: {},
      links: {},
    },
    error: false,
    pending: false,
    success: false,
  },
  pagination: {
    limit: '15',
    page: '1',
  },
};

export const bannerReducer: Reducer<IBannerState, TBannerTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_BANNER:
      return {
        ...state,
        addBanner: {
          pending: true,
          error: { ...state.addBanner.error },
          success: false,
        },
      };
    case ADD_BANNER_ERROR:
      return {
        ...state,
        addBanner: {
          error: {
            errors: { ...action.payload.errors },
            hasError: action.payload.hasError,
            message: action.payload.message,
          },
          pending: false,
          success: false,
        },
      };
    case ADD_BANNER_SUCCESS:
      return {
        ...state,
        addBanner: {
          error: { ...state.addBanner.error },
          pending: false,
          success: true,
        },
      };
    case DELETE_BANNER:
      return {
        ...state,
        deleteBanner: {
          error: false,
          pending: true,
          success: false,
        },
      };
    case DELETE_BANNER_ERROR:
      return {
        ...state,
        deleteBanner: {
          error: true,
          pending: false,
          success: false,
        },
      };
    case DELETE_BANNER_SUCCESS:
      return {
        ...state,
        deleteBanner: {
          error: false,
          pending: false,
          success: true,
        },
      };
    case GET_BANNERS:
      return {
        ...state,
        getBanners: {
          data: { ...state.getBanners.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    case GET_BANNERS_ERROR:
      return {
        ...state,
        getBanners: {
          data: { ...state.getBanners.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    case GET_BANNERS_SUCCESS:
      return {
        ...state,
        getBanners: {
          data: {
            data: action.payload.data,
            meta: action.payload.meta,
            links: action.payload.links,
          },
          error: false,
          pending: false,
          success: true,
        },
      };
    case RESET_ADD_BANNER_SUCCESS:
      return {
        ...state,
        addBanner: {
          error: {
            message: '',
            errors: {
              image: '',
              title: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    case RESET_DELETE_BANNER_SUCCESS:
      return {
        ...state,
        deleteBanner: {
          error: false,
          pending: false,
          success: false,
        },
      };
    case PAGINATION_CHANGE_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: action.payload?.limit,
          page: state.pagination.page,
        },
      };
    }
    case PAGINATION_CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: action.payload?.page,
        },
      };
    }
    case RESET_PAGINATION: {
      return {
        ...state,
        pagination: {
          limit: '15',
          page: '1',
        },
      };
    }
    default:
      return state;
  }
};
