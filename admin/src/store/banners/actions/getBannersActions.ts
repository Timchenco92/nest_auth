// Action Types
import { GET_BANNERS, GET_BANNERS_ERROR, GET_BANNERS_SUCCESS } from '@/store/banners';

// Types
import {
  IGetBanners,
  IGetBannersError,
  IGetBannersPayload,
  IGetBannersSuccess,
  IGetBannersSuccessPayload,
} from '@/store/banners';

export const getBannersRequest = ({ limit = '15', page = '1' }: IGetBannersPayload): IGetBanners => ({
  type: GET_BANNERS,
  limit,
  page,
});

export const getBannersError = (): IGetBannersError => ({
  type: GET_BANNERS_ERROR,
});

export const getBannersSuccess = (payload: IGetBannersSuccessPayload): IGetBannersSuccess => ({
  type: GET_BANNERS_SUCCESS,
  payload,
});
