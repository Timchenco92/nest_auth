// Action Types
import { ADD_BANNER, ADD_BANNER_ERROR, ADD_BANNER_SUCCESS } from '@/store/banners';

// Types
import {
  IAddBanner,
  IAddBannerError,
  IAddBannerErrorStateAndPayload,
  IAddBannerSuccess,
} from '@/store/banners';

export const addBannersRequest = (payload: FormData): IAddBanner => ({
  type: ADD_BANNER,
  payload,
});

export const addBannerError = (payload: IAddBannerErrorStateAndPayload): IAddBannerError => ({
  type: ADD_BANNER_ERROR,
  payload,
});

export const addBannerSuccess = (): IAddBannerSuccess => ({
  type: ADD_BANNER_SUCCESS,
});
