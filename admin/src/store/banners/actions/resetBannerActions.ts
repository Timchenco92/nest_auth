// Action Types
import { RESET_ADD_BANNER_SUCCESS, RESET_DELETE_BANNER_SUCCESS, RESET_PAGINATION } from '@/store/banners';

// Types
import { IResetAddBannerSuccess, IResetDeleteBannerSuccess } from '@/store/banners';

export const resetAddBannerSuccess = (): IResetAddBannerSuccess => ({
  type: RESET_ADD_BANNER_SUCCESS,
});

export const resetDeleteBannerSuccess = (): IResetDeleteBannerSuccess => ({
  type: RESET_DELETE_BANNER_SUCCESS,
});

export const resetPagination = () => ({
  type: RESET_PAGINATION,
});
