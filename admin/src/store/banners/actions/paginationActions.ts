// Action Types
import { PAGINATION_CHANGE_LIMIT, PAGINATION_CHANGE_PAGE } from '@/store/banners';

// Types
import type { IChangePage, IChangeLimit, IChangePagePayload, IChangeLimitPayload } from '@/store/banners';

export const changePagePagination = (payload: IChangePagePayload): IChangePage => ({
  type: PAGINATION_CHANGE_PAGE,
  payload,
});

export const changeLimitPagination = (payload: IChangeLimitPayload): IChangeLimit => ({
  type: PAGINATION_CHANGE_LIMIT,
  payload,
});
