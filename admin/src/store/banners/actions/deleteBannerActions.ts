// Action Types
import { DELETE_BANNER, DELETE_BANNER_ERROR, DELETE_BANNER_SUCCESS } from '@/store/banners';

// Types
import {
  IDeleteBanner,
  IDeleteBannerError,
  IDeleteBannerSuccess,
} from '@/store/banners';

export const deleteBannerRequest = (bannerId: string | number): IDeleteBanner => ({
  type: DELETE_BANNER,
  bannerId,
});

export const deleteBannerError = (): IDeleteBannerError => ({
  type: DELETE_BANNER_ERROR,
});

export const deleteBannerSuccess = (): IDeleteBannerSuccess => ({
  type: DELETE_BANNER_SUCCESS,
});
