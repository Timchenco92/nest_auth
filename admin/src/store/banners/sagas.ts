// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, select, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect, SelectEffect } from 'redux-saga/effects';

// Actions
import {
  addBannerError,
  addBannerSuccess,
  addBannersRequest,
  deleteBannerError,
  deleteBannerRequest,
  deleteBannerSuccess,
  getBannersError,
  getBannersRequest,
  getBannersSuccess,
} from '@/store/banners';
import { showToast } from '@/store/toast';

// Action Types
import {
  ADD_BANNER,
  DELETE_BANNER,
  GET_BANNERS,
} from '@/store/banners';

// Interfaces
import type { IBanner } from '@/interfaces/models/IBanner';
import type { IGetBannersSuccessPayload } from '@/store/banners';

// Selectors
import { paginationSelector } from '@/store/banners';

// Services
import { bannerService } from '@/services';

const { addBanner, deleteBanner, getBanners } = bannerService;

function* addBannerSaga({ payload }: ReturnType<typeof addBannersRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page } = yield select(paginationSelector);
    const { data }: AxiosResponse = yield call(() => addBanner(payload));
    yield put(addBannerSuccess());
    yield call(getAllBannersSaga, { limit, page, type: GET_BANNERS });
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addBannerError({ errors, hasError: true, message }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteBannerSaga({ bannerId }: ReturnType<typeof deleteBannerRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page } = yield select(paginationSelector);
    const { data }: AxiosResponse = yield call(() => deleteBanner(bannerId));
    const { message } = data;
    yield put(deleteBannerSuccess());
    yield call(getAllBannersSaga, { limit, page, type: GET_BANNERS });
    yield put(showToast({ isShowing: true, message, type: 'success' }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteBannerError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getAllBannersSaga({ limit, page }: ReturnType<typeof getBannersRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse<IBanner[]>
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const response: AxiosResponse<IGetBannersSuccessPayload> = yield call(() => getBanners({ limit, page }));
    const { data: { data: bannerData, meta, links } } = response;
    yield put(getBannersSuccess({ data: bannerData, meta, links }));
    return response;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getBannersError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* bannerSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_BANNER, addBannerSaga)]);
  yield all([takeLatest(DELETE_BANNER, deleteBannerSaga)]);
  yield all([takeLatest(GET_BANNERS, getAllBannersSaga)]);
}
