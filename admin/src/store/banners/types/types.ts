// Types
import type {
  IAddBannerState,
  IAddBannerError,
  IAddBannerSuccess,
  IAddBanner,
} from '@/store/banners';

import type {
  IDeleteBannerState,
  IDeleteBannerSuccess,
  IDeleteBannerError,
  IDeleteBanner,
} from '@/store/banners';

import type { IGetBannersState, IGetBannersError, IGetBannersSuccess, IGetBanners } from '@/store/banners';
import type { IResetAddBannerSuccess, IResetDeleteBannerSuccess, IResetBannerPagination } from '@/store/banners';
import type { IPaginationState, IChangePage, IChangeLimit } from '@/store/banners';

export interface IBannerState {
  addBanner: IAddBannerState;
  deleteBanner: IDeleteBannerState;
  getBanners: IGetBannersState;
  pagination: IPaginationState;
}

export type TBannerTypes =
  IAddBanner
  | IAddBannerError
  | IAddBannerSuccess
  | IChangeLimit
  | IChangePage
  | IDeleteBanner
  | IDeleteBannerError
  | IDeleteBannerSuccess
  | IGetBanners
  | IGetBannersError
  | IGetBannersSuccess
  | IResetAddBannerSuccess
  | IResetBannerPagination
  | IResetDeleteBannerSuccess;
