// Action Types
import { RESET_ADD_BANNER_SUCCESS, RESET_DELETE_BANNER_SUCCESS, RESET_PAGINATION } from '@/store/banners';

export interface IResetAddBannerSuccess {
  type: typeof RESET_ADD_BANNER_SUCCESS;
}

export interface IResetDeleteBannerSuccess {
  type: typeof RESET_DELETE_BANNER_SUCCESS;
}

export interface IResetBannerPagination {
  type: typeof RESET_PAGINATION;
}
