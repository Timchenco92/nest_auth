// Action Types
import { DELETE_BANNER, DELETE_BANNER_ERROR, DELETE_BANNER_SUCCESS } from '@/store/banners';

// Interfaces
import { IBanner } from '@/interfaces/models/IBanner';

export interface IDeleteBannerState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteBanner {
  type: typeof DELETE_BANNER;
  bannerId: IBanner['id'];
}

export interface IDeleteBannerError {
  type: typeof DELETE_BANNER_ERROR;
}

export interface IDeleteBannerSuccess {
  type: typeof DELETE_BANNER_SUCCESS;
}
