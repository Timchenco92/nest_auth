// Action Types
import { GET_BANNERS, GET_BANNERS_ERROR, GET_BANNERS_SUCCESS } from '@/store/banners';

// Interfaces
import { IBanner } from '@/interfaces/models';

export interface IGetBannersState {
  data: IGetBannersSuccessPayload;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetBanners {
  type: typeof GET_BANNERS;
  limit: string;
  page: string;
}

export interface IGetBannersError {
  type: typeof GET_BANNERS_ERROR;
}

export interface IGetBannersSuccess {
  type: typeof GET_BANNERS_SUCCESS;
  payload: IGetBannersSuccessPayload;
}

export interface IGetBannersPayload {
  limit: string;
  page: string;
}

export interface IGetBannersSuccessPayload {
  data: IBanner[];
  meta: any;
  links: any;
}
