// Action Types
import { PAGINATION_CHANGE_LIMIT, PAGINATION_CHANGE_PAGE } from '@/store/banners';

export interface IPaginationState {
  limit: string;
  page: string;
}

export interface IChangeLimit {
  type: typeof PAGINATION_CHANGE_LIMIT;
  payload: IChangeLimitPayload;
}

export interface IChangeLimitPayload {
  limit: string;
}

export interface IChangePage {
  type: typeof PAGINATION_CHANGE_PAGE;
  payload: IChangePagePayload;
}

export interface IChangePagePayload {
  page: string;
}
