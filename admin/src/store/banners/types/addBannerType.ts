// Action Types
import { ADD_BANNER, ADD_BANNER_ERROR, ADD_BANNER_SUCCESS } from '@/store/banners';

export interface IAddBannerState {
  error: IAddBannerErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddBanner {
  type: typeof ADD_BANNER;
  payload: FormData;
}

export interface IAddBannerSuccess {
  type: typeof ADD_BANNER_SUCCESS;
}

export interface IAddBannerError {
  type: typeof ADD_BANNER_ERROR;
  payload: IAddBannerErrorStateAndPayload;
}

export interface IAddBannerPayload {
  title: string;
  image: File | Blob;
}

export interface IAddBannerErrorStateAndPayload {
  errors: IErrors;
  hasError: boolean;
  message: string;
}

export interface IErrors {
  image: string;
  title: string;
}
