export const ADD_ORDER = 'ADD_ORDER';
export const ADD_ORDER_ERROR = 'ADD_ORDER_ERROR';
export const ADD_ORDER_SUCCESS = 'ADD_ORDER_SUCCESS';

export const DELETE_ORDER = 'DELETE_ORDER';
export const DELETE_ORDER_ERROR = 'DELETE_ORDER_ERROR';
export const DELETE_ORDER_SUCCESS = 'DELETE_ORDER_SUCCESS';

export const GET_ORDER = 'GET_ORDER';
export const GET_ORDER_ERROR = 'GET_ORDER_ERROR';
export const GET_ORDER_SUCCESS = 'GET_ORDER_SUCCESS';

export const GET_ORDERS = 'GET_ORDERS';
export const GET_ORDERS_ERROR = 'GET_ORDERS_ERROR';
export const GET_ORDERS_SUCCESS = 'GET_ORDERS_SUCCESS';

export const PAGINATION_CHANGE_LIMIT = 'PAGINATION_CHANGE_LIMIT';
export const PAGINATION_CHANGE_PAGE = 'PAGINATION_CHANGE_PAGE';

export const RESET_ADD_ORDER_SUCCESS = 'RESET_ADD_ORDER_SUCCESS';
export const RESET_ADD_ORDER_ERROR = 'RESET_ADD_ORDER_ERROR';
export const RESET_DELETE_ORDER_SUCCESS = 'RESET_DELETE_ORDER_SUCCESS';
export const RESET_PAGINATION_PAGE = 'RESET_PAGINATION_PAGE';
export const RESET_PAGINATION_LIMIT = 'RESET_PAGINATION_LIMIT';
export const RESET_QUERY_SEARCH = 'RESET_QUERY_SEARCH';
export const RESET_UPDATE_ORDER_SUCCESS = 'RESET_UPDATE_ORDER_SUCCESS';
export const RESET_UPDATE_ORDER_ERROR = 'RESET_UPDATE_ORDER_ERROR';

export const SET_QUERY_SEARCH = 'SET_QUERY_SEARCH';

export const UPDATE_ORDER = 'UPDATE_ORDER';
export const UPDATE_ORDER_ERROR = 'UPDATE_ORDER_ERROR';
export const UPDATE_ORDER_SUCCESS = 'UPDATE_ORDER_SUCCESS';
