// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, delay, put, select, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect, SelectEffect } from 'redux-saga/effects';

// Actions
import {
  addOrderError,
  addOrderRequest,
  addOrderSuccess,
  deleteOrderError,
  deleteOrderRequest,
  deleteOrderSuccess,
  getOrderError,
  getOrderRequest,
  getOrderSuccess,
  getOrdersError,
  getOrdersRequest,
  getOrdersSuccess,
  updateOrderError,
  updateOrderRequest,
  updateOrderSuccess,
} from '@/store/orders';
import { showToast } from '@/store/toast';

// Action Types
import { ADD_ORDER, DELETE_ORDER, GET_ORDER, GET_ORDERS, UPDATE_ORDER } from '@/store/orders';
import { GET_ORDER_PRODUCTS } from '@/store/order-products';

// Sagas
import { getOrderProductsSaga } from '@/store/order-products';

// Selectors
import { paginationAndQuerySearchSelector } from '@/store/orders';

// Services
import { orderService } from '@/services';

const { addOrder, deleteOrder, getOrder, getOrders, updateOrder } = orderService;

function* addOrderSaga({ payload }: ReturnType<typeof addOrderRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse>
    | CallEffect
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => addOrder(payload));
    yield put(addOrderSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrdersSaga, { limit, page, query, type: GET_ORDERS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addOrderError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* deleteOrderSaga({ orderId }: ReturnType<typeof deleteOrderRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => deleteOrder(orderId));
    yield put(deleteOrderSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrdersSaga, { limit, page, query, type: GET_ORDERS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(deleteOrderError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getOrderSaga({ orderId }: ReturnType<typeof getOrderRequest>):
  Generator<CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getOrder(orderId));
    yield put(getOrderSuccess(data));
    yield call(getOrderProductsSaga, { orderId, type: GET_ORDER_PRODUCTS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getOrderError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getOrdersSaga({ limit, page, query }: ReturnType<typeof getOrdersRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect,
    AxiosResponse | AxiosError, never> {
  try {
    if (query !== '') yield delay(1000);
    const { data }: AxiosResponse = yield call(() => getOrders({ limit, page, query }));
    yield put(getOrdersSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getOrdersError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* updateOrderSaga({ payload }: ReturnType<typeof updateOrderRequest>):
  Generator<SelectEffect
    | CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { orderId, delivery, comment } = payload;
    const { limit, page, query } = yield select(paginationAndQuerySearchSelector);
    const { data }: AxiosResponse = yield call(() => updateOrder(orderId, comment, delivery));
    yield put(updateOrderSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: 'success' }));
    yield call(getOrdersSaga, { limit, page, query, type: GET_ORDERS });
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(updateOrderError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export interface IAddProductToOrderStateAndPayload {
  price: string;
  productId: string;
  quantity: number;
  title: string;
  total: string;
}

export function* orderSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_ORDER, addOrderSaga)]);
  yield all([takeLatest(DELETE_ORDER, deleteOrderSaga)]);
  yield all([takeLatest(GET_ORDER, getOrderSaga)]);
  yield all([takeLatest(GET_ORDERS, getOrdersSaga)]);
  yield all([takeLatest(UPDATE_ORDER, updateOrderSaga)]);
}
