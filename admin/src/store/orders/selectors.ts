// Modules
import { createSelector } from 'reselect';

// State
import type { OrderState } from '@/store/rootReducer';

const addOrder = (state: OrderState) => ({
  error: state.orders.addOrder.error,
  pending: state.orders.addOrder.pending,
  success: state.orders.addOrder.success,
});

const deleteOrder = (state: OrderState) => ({
  error: state.orders.deleteOrder.error,
  pending: state.orders.deleteOrder.pending,
  success: state.orders.deleteOrder.success,
});

const getOrders = (state: OrderState) => ({
  data: state.orders.orders.data,
  error: state.orders.orders.error,
  pending: state.orders.orders.pending,
  success: state.orders.orders.success,
});

const getOrder = (state: OrderState) => ({
  data: state.orders.order.data,
  error: state.orders.order.error,
  pending: state.orders.order.pending,
  success: state.orders.order.success,
});

const paginationAndSearch = (state: OrderState) => ({
  page: state.orders.pagination.page,
  limit: state.orders.pagination.limit,
  query: state.orders.pagination.query,
});

const updateOrder = (state: OrderState) => ({
  error: state.orders.updateOrder.error,
  pending: state.orders.updateOrder.pending,
  success: state.orders.updateOrder.success,
});

export const addOrderErrorSelector = createSelector(addOrder, ({ error }) => error);
export const addOrderPendingSelector = createSelector(addOrder, ({ pending }) => pending);
export const addOrderSuccessSelector = createSelector(addOrder, ({ success }) => success);

export const deleteOrderErrorSelector = createSelector(deleteOrder, ({ error }) => error);
export const deleteOrderPendingSelector = createSelector(deleteOrder, ({ pending }) => pending);
export const deleteOrderSuccessSelector = createSelector(deleteOrder, ({ success }) => success);

export const ordersSelector = createSelector(getOrders, ({ data }) => data);
export const ordersErrorSelector = createSelector(getOrders, ({ error }) => error);
export const ordersPendingSelector = createSelector(getOrders, ({ pending }) => pending);
export const ordersSuccessSelector = createSelector(getOrders, ({ success }) => success);

export const orderSelector = createSelector(getOrder, ({ data }) => data);
export const orderErrorSelector = createSelector(getOrder, ({ error }) => error);
export const orderPendingSelector = createSelector(getOrder, ({ pending }) => pending);
export const orderSuccessSelector = createSelector(getOrder, ({ success }) => success);

export const paginationAndQuerySearchSelector = createSelector(
  paginationAndSearch, ({ limit, page, query }) => ({ limit, page, query })
);

export const updateOrderErrorSelector = createSelector(updateOrder, ({ error }) => error);
export const updateOrderPendingSelector = createSelector(updateOrder, ({ pending }) => pending);
export const updateOrderSuccessSelector = createSelector(updateOrder, ({ success }) => success);
