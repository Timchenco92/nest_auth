// Action Types
import { GET_ORDERS, GET_ORDERS_ERROR, GET_ORDERS_SUCCESS } from '@/store/orders';

// Types
import type {
  IGetOrders,
  IGetOrdersError,
  IGetOrdersSuccess,
  IGetOrdersSuccessStateAndPayload,
} from '@/store/orders';

export const getOrdersRequest = (limit: string, page: string, query: string): IGetOrders => ({
  type: GET_ORDERS,
  limit,
  page,
  query,
});
export const getOrdersError = (): IGetOrdersError => ({
  type: GET_ORDERS_ERROR
});
export const getOrdersSuccess = (payload: IGetOrdersSuccessStateAndPayload): IGetOrdersSuccess => ({
  type: GET_ORDERS_SUCCESS,
  payload,
});
