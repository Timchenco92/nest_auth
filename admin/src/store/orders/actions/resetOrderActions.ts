// Action Types
import {
  RESET_ADD_ORDER_ERROR,
  RESET_ADD_ORDER_SUCCESS,
  RESET_DELETE_ORDER_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_ORDER_ERROR,
  RESET_UPDATE_ORDER_SUCCESS,
} from '@/store/orders';

// Types
import type {
  IResetAddOrderError,
  IResetAddOrderSuccess,
  IResetDeleteOrderSuccess,
  IResetPaginationLimit,
  IResetPaginationPage,
  IResetQuerySearch,
  IResetUpdateOrderError,
  IResetUpdateOrderSuccess,
} from '@/store/orders';

export const resetAddOrderError = (): IResetAddOrderError => ({
  type: RESET_ADD_ORDER_ERROR,
});

export const resetAddOrderSuccess = (): IResetAddOrderSuccess => ({
  type: RESET_ADD_ORDER_SUCCESS,
});

export const resetDeleteOrderSuccess = (): IResetDeleteOrderSuccess => ({
  type: RESET_DELETE_ORDER_SUCCESS,
});

export const resetPaginationLimit = (): IResetPaginationLimit => ({
  type: RESET_PAGINATION_LIMIT,
});

export const resetPaginationPage = (): IResetPaginationPage => ({
  type: RESET_PAGINATION_PAGE,
});

export const resetQuerySearch = (): IResetQuerySearch => ({
  type: RESET_QUERY_SEARCH,
});

export const resetUpdateOrderError = (): IResetUpdateOrderError => ({
  type: RESET_UPDATE_ORDER_ERROR,
});

export const resetUpdateOrderSuccess = (): IResetUpdateOrderSuccess => ({
  type: RESET_UPDATE_ORDER_SUCCESS,
});
