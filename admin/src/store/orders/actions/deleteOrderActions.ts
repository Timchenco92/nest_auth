// Action Types
import { DELETE_ORDER, DELETE_ORDER_ERROR, DELETE_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

// Types
import type { IDeleteOrder, IDeleteOrderError, IDeleteOrderSuccess } from '@/store/orders';

export const deleteOrderRequest = (orderId: IOrder['id']): IDeleteOrder => ({
  type: DELETE_ORDER,
  orderId,
});

export const deleteOrderError = (): IDeleteOrderError => ({
  type: DELETE_ORDER_ERROR,
});

export const deleteOrderSuccess = (): IDeleteOrderSuccess => ({
  type: DELETE_ORDER_SUCCESS,
});
