// Action Types
import { UPDATE_ORDER, UPDATE_ORDER_ERROR, UPDATE_ORDER_SUCCESS } from '@/store/orders';

// Types
import type {
  IUpdateOrder,
  IUpdateOrderError,
  IUpdateOrderErrorStateAndPayload,
  IUpdateOrderPayload,
  IUpdateOrderSuccess,
} from '@/store/orders';

export const updateOrderRequest = (payload: IUpdateOrderPayload): IUpdateOrder => ({
  type: UPDATE_ORDER,
  payload,
});

export const updateOrderError = (payload: IUpdateOrderErrorStateAndPayload): IUpdateOrderError => ({
  type: UPDATE_ORDER_ERROR,
  payload,
});

export const updateOrderSuccess = (): IUpdateOrderSuccess => ({
  type: UPDATE_ORDER_SUCCESS,
});
