// Action Types
import { ADD_ORDER, ADD_ORDER_ERROR, ADD_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IAddOrderPayload } from '@/interfaces'

// Types
import type { IAddOrder, IAddOrderError, IAddOrderErrorStateAndPayload, IAddOrderSuccess } from '@/store/orders';

export const addOrderRequest = (payload: IAddOrderPayload): IAddOrder => ({
  type: ADD_ORDER,
  payload,
});

export const addOrderError = (payload: IAddOrderErrorStateAndPayload): IAddOrderError => ({
  type: ADD_ORDER_ERROR,
  payload,
});

export const addOrderSuccess = (): IAddOrderSuccess => ({
  type: ADD_ORDER_SUCCESS,
});
