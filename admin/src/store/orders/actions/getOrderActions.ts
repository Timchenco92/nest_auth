// Action Types
import { GET_ORDER, GET_ORDER_ERROR, GET_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

// Types
import type { IGetOrder, IGetOrderError, IGetOrderSuccess } from '@/store/orders';

export const getOrderRequest = (orderId: IOrder['id']): IGetOrder => ({
  type: GET_ORDER,
  orderId,
});

export const getOrderError = (): IGetOrderError => ({
  type: GET_ORDER_ERROR
});

export const getOrderSuccess = (payload: IOrder): IGetOrderSuccess => ({
  type: GET_ORDER_SUCCESS,
  payload,
});
