// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_ORDER,
  ADD_ORDER_ERROR,
  ADD_ORDER_SUCCESS,
  DELETE_ORDER,
  DELETE_ORDER_ERROR,
  DELETE_ORDER_SUCCESS,
  GET_ORDER,
  GET_ORDERS,
  GET_ORDERS_ERROR,
  GET_ORDERS_SUCCESS,
  GET_ORDER_ERROR,
  GET_ORDER_SUCCESS,
  PAGINATION_CHANGE_LIMIT,
  PAGINATION_CHANGE_PAGE,
  RESET_ADD_ORDER_ERROR,
  RESET_ADD_ORDER_SUCCESS,
  RESET_DELETE_ORDER_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_ORDER_ERROR,
  RESET_UPDATE_ORDER_SUCCESS,
  SET_QUERY_SEARCH,
  UPDATE_ORDER,
  UPDATE_ORDER_ERROR,
  UPDATE_ORDER_SUCCESS,
} from '@/store/orders';

// Enums
import { EOrderStatus } from '@/interfaces/models';

// Types
import type { IOrdersState, TOrdersTypes } from '@/store/orders';

const initialState: IOrdersState = {
  addOrder: {
    error: {
      errors: {
        orderProducts: '',
        total: '',
        delivery: '',
        comment: '',
        userId: '',
      },
      hasError: false,
    },
    pending: false,
    success: false,
  },
  deleteOrder: {
    error: false,
    pending: false,
    success: false,
  },
  order: {
    data: {
      id: '',
      total: '',
      userId: '',
      comment: null,
      status: EOrderStatus.accepted,
      orderNumber: '',
      orderProducts: [],
      delivery: null,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    error: false,
    pending: false,
    success: false,
  },
  orders: {
    data: {
      data: [],
      meta: {},
      links: {},
    },
    error: false,
    pending: false,
    success: false,
  },
  pagination: {
    limit: '15',
    page: '1',
    query: '',
  },
  updateOrder: {
    error: {
      hasError: false,
      errors: {
        comment: '', delivery: '',
      },
    },
    pending: false,
    success: false,
  },
};

export const orderReducer: Reducer<IOrdersState, TOrdersTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ORDER: {
      return {
        ...state,
        addOrder: {
          error: { ...state.addOrder.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_ORDER_ERROR: {
      return {
        ...state,
        addOrder: {
          error: {
            errors: { ...action.payload.errors },
            hasError: action.payload.hasError,
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_ORDER_SUCCESS: {
      return {
        ...state,
        addOrder: {
          error: { ...state.addOrder.error },
          pending: false,
          success: true,
        },
      };
    }
    case DELETE_ORDER: {
      return {
        ...state,
        deleteOrder: {
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case DELETE_ORDER_ERROR: {
      return {
        ...state,
        deleteOrder: {
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case DELETE_ORDER_SUCCESS: {
      return {
        ...state,
        deleteOrder: {
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_ORDER: {
      return {
        ...state,
        order: {
          data: { ...state.order.data },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_ORDER_ERROR: {
      return {
        ...state,
        order: {
          data: { ...state.order.data },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_ORDER_SUCCESS: {
      return {
        ...state,
        order: {
          data: { ...action.payload },
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case GET_ORDERS: {
      return {
        ...state,
        orders: {
          data: {
            data: [],
            meta: {},
            links: {},
          },
          error: false,
          pending: true,
          success: false,
        },
      };
    }
    case GET_ORDERS_ERROR: {
      return {
        ...state,
        orders: {
          data: {
            data: [],
            meta: {},
            links: {},
          },
          error: true,
          pending: false,
          success: false,
        },
      };
    }
    case GET_ORDERS_SUCCESS: {
      return {
        ...state,
        orders: {
          data: {
            data: action.payload.data,
            meta: action.payload.meta,
            links: action.payload.links,
          },
          error: false,
          pending: false,
          success: true,
        },
      };
    }
    case PAGINATION_CHANGE_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: action.limit,
          page: state.pagination.page,
          query: state.pagination.query,
        },
      };
    }
    case PAGINATION_CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: action.page,
          query: state.pagination.query,
        },
      };
    }
    case RESET_ADD_ORDER_ERROR: {
      return {
        ...state,
        addOrder: {
          error: {
            hasError: false,
            errors: {
              orderProducts: '',
              comment: '',
              total: '',
              userId: '',
              delivery: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_ADD_ORDER_SUCCESS: {
      return {
        ...state,
        addOrder: {
          error: {
            hasError: false,
            errors: {
              orderProducts: '',
              comment: '',
              total: '',
              userId: '',
              delivery: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_DELETE_ORDER_SUCCESS: {
      return {
        ...state,
        deleteOrder: {
          error: false,
          pending: false,
          success: false,
        },
      };
    }
    case RESET_PAGINATION_LIMIT: {
      return {
        ...state,
        pagination: {
          limit: '15',
          page: state.pagination.page,
          query: state.pagination.query,
        },
      };
    }
    case RESET_PAGINATION_PAGE: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: '1',
          query: state.pagination.query,
        },
      };
    }
    case RESET_QUERY_SEARCH: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: state.pagination.page,
          query: '',
        },
      };
    }
    case SET_QUERY_SEARCH: {
      return {
        ...state,
        pagination: {
          limit: state.pagination.limit,
          page: state.pagination.page,
          query: action.query,
        },
      };
    }
    case UPDATE_ORDER: {
      return {
        ...state,
        updateOrder: {
          error: { ...state.updateOrder.error },
          pending: true,
          success: false,
        },
      };
    }
    case UPDATE_ORDER_ERROR: {
      return {
        ...state,
        updateOrder: {
          error: {
            hasError: action.payload.hasError,
            errors: { ...action.payload.errors },
          },
          pending: false,
          success: false,
        },
      };
    }
    case UPDATE_ORDER_SUCCESS: {
      return {
        ...state,
        updateOrder: {
          error: { ...state.updateOrder.error },
          pending: false,
          success: true,
        },
      };
    }
    case RESET_UPDATE_ORDER_ERROR: {
      return {
        ...state,
        updateOrder: {
          error: {
            hasError: false,
            errors: {
              comment: '', delivery: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_UPDATE_ORDER_SUCCESS: {
      return {
        ...state,
        updateOrder: {
          error: {
            hasError: false,
            errors: {
              comment: '', delivery: '',
            },
          },
          pending: false,
          success: false,
        },
      };
    }
    default: {
      return state;
    }
  }
};
