// Types
import type {
  IAddOrder,
  IAddOrderError,
  IAddOrderState,
  IAddOrderSuccess,
  IChangeLimit,
  IChangePage,
  IDeleteOrder,
  IDeleteOrderError,
  IDeleteOrderState,
  IDeleteOrderSuccess,
  IGetOrder,
  IGetOrderError,
  IGetOrderState,
  IGetOrderSuccess,
  IGetOrders,
  IGetOrdersError,
  IGetOrdersState,
  IGetOrdersSuccess,
  IPaginationAndQueryState,
  IResetAddOrderError,
  IResetAddOrderSuccess,
  IResetDeleteOrderSuccess,
  IResetPaginationLimit,
  IResetPaginationPage,
  IResetQuerySearch,
  IResetUpdateOrderError,
  IResetUpdateOrderSuccess,
  ISetQuerySearch,
  IUpdateOrder,
  IUpdateOrderError,
  IUpdateOrderState,
  IUpdateOrderSuccess,
} from '@/store/orders';

export interface IOrdersState {
  addOrder: IAddOrderState;
  deleteOrder: IDeleteOrderState;
  orders: IGetOrdersState;
  order: IGetOrderState;
  pagination: IPaginationAndQueryState;
  updateOrder: IUpdateOrderState;
}

export type TOrdersTypes = IAddOrder
  | IAddOrderError
  | IAddOrderSuccess
  | IChangeLimit
  | IChangePage
  | IDeleteOrder
  | IDeleteOrderError
  | IDeleteOrderSuccess
  | IGetOrder
  | IGetOrderError
  | IGetOrderSuccess
  | IGetOrders
  | IGetOrdersError
  | IGetOrdersSuccess
  | IResetAddOrderError
  | IResetAddOrderSuccess
  | IResetDeleteOrderSuccess
  | IResetPaginationLimit
  | IResetPaginationPage
  | IResetQuerySearch
  | IResetUpdateOrderError
  | IResetUpdateOrderSuccess
  | ISetQuerySearch
  | IUpdateOrder
  | IUpdateOrderError
  | IUpdateOrderSuccess;
