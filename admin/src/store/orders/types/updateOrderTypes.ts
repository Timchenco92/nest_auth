// Action Types
import { UPDATE_ORDER, UPDATE_ORDER_ERROR, UPDATE_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IUpdateOrderState {
  error: IUpdateOrderErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IUpdateOrder {
  type: typeof UPDATE_ORDER;
  payload: IUpdateOrderPayload;
}

export interface IUpdateOrderPayload {
  comment: string | null;
  delivery: string | null;
  orderId: IOrder['id'];
}

export interface IUpdateOrderError {
  type: typeof UPDATE_ORDER_ERROR;
  payload: IUpdateOrderErrorStateAndPayload;
}

export interface IUpdateOrderErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

interface IErrors {
  comment: string;
  delivery: string;
}

export interface IUpdateOrderSuccess {
  type: typeof UPDATE_ORDER_SUCCESS;
}
