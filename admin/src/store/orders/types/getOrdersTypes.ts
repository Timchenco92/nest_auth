// Action Types
import { GET_ORDERS, GET_ORDERS_ERROR, GET_ORDERS_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IGetOrdersState {
  data: IGetOrdersSuccessStateAndPayload;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetOrders {
  type: typeof GET_ORDERS;
  limit: string;
  page: string;
  query: string;
}

export interface IGetOrdersError {
  type: typeof GET_ORDERS_ERROR;
}

export interface IGetOrdersSuccess {
  type: typeof GET_ORDERS_SUCCESS;
  payload: IGetOrdersSuccessStateAndPayload;
}

export interface IGetOrdersSuccessStateAndPayload {
  data: IOrder[];
  meta: any;
  links: any;
}
