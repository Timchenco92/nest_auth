// Action Types
import {
  RESET_ADD_ORDER_ERROR,
  RESET_ADD_ORDER_SUCCESS,
  RESET_DELETE_ORDER_SUCCESS,
  RESET_PAGINATION_LIMIT,
  RESET_PAGINATION_PAGE,
  RESET_QUERY_SEARCH,
  RESET_UPDATE_ORDER_ERROR,
  RESET_UPDATE_ORDER_SUCCESS,
} from '@/store/orders';

export interface IResetAddOrderError {
  type: typeof RESET_ADD_ORDER_ERROR;
}
export interface IResetAddOrderSuccess {
  type: typeof RESET_ADD_ORDER_SUCCESS;
}
export interface IResetDeleteOrderSuccess {
  type: typeof RESET_DELETE_ORDER_SUCCESS;
}
export interface IResetPaginationLimit {
  type: typeof RESET_PAGINATION_LIMIT;
}
export interface IResetPaginationPage {
  type: typeof RESET_PAGINATION_PAGE;
}
export interface IResetQuerySearch {
  type: typeof RESET_QUERY_SEARCH;
}
export interface IResetUpdateOrderError {
  type: typeof RESET_UPDATE_ORDER_ERROR;
}
export interface IResetUpdateOrderSuccess {
  type: typeof RESET_UPDATE_ORDER_SUCCESS;
}
