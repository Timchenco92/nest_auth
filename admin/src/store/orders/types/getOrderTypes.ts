// Action Types
import { GET_ORDER, GET_ORDER_ERROR, GET_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IGetOrderState {
  data: IOrder;
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IGetOrder {
  type: typeof GET_ORDER;
  orderId: IOrder['id'];
}

export interface IGetOrderError {
  type: typeof GET_ORDER_ERROR;
}

export interface IGetOrderSuccess {
  type: typeof GET_ORDER_SUCCESS;
  payload: IOrder;
}
