// Action Types
import { DELETE_ORDER, DELETE_ORDER_ERROR, DELETE_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IDeleteOrderState {
  error: boolean;
  pending: boolean;
  success: boolean;
}

export interface IDeleteOrder {
  type: typeof DELETE_ORDER;
  orderId: IOrder['id'];
}

export interface IDeleteOrderError {
  type: typeof DELETE_ORDER_ERROR;
}

export interface IDeleteOrderSuccess {
  type: typeof DELETE_ORDER_SUCCESS;
}
