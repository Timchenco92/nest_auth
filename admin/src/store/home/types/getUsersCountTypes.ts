// Action Types
import { GET_USERS_COUNT, GET_USERS_COUNT_ERROR, GET_USERS_COUNT_SUCCESS } from '@/store/home';

export interface IGetUsersCountState {
  count: number;
  error: boolean;
  pending: boolean;
}

export interface IGetUsersCount {
  type: typeof GET_USERS_COUNT;
}

export interface IGetUsersCountError {
  type: typeof GET_USERS_COUNT_ERROR;
}

export interface IGetUsersCountSuccess {
  type: typeof GET_USERS_COUNT_SUCCESS;
  payload: number;
}
