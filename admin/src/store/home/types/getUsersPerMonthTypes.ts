// Action Types
import { GET_USERS_PER_MONTH, GET_USERS_PER_MONTH_ERROR, GET_USERS_PER_MONTH_SUCCESS } from '@/store/home';

// Interfaces
import type { IUser } from '@/interfaces/models';

export interface IGetUsersPerMonthState {
  data: IGetUsersPerMonthSuccessStateAndPayload['data'];
  meta: IGetUsersPerMonthSuccessStateAndPayload['meta'];
  links: IGetUsersPerMonthSuccessStateAndPayload['links'];
  error: boolean;
  pending: boolean;
}

export interface IGetUsersPerMonth {
  type: typeof GET_USERS_PER_MONTH;
  page: string;
}

export interface IGetUsersPerMonthError {
  type: typeof GET_USERS_PER_MONTH_ERROR;
}

export interface IGetUsersPerMonthSuccess {
  type: typeof GET_USERS_PER_MONTH_SUCCESS;
  payload: IGetUsersPerMonthSuccessStateAndPayload;
}

export interface IGetUsersPerMonthSuccessStateAndPayload {
  data: IUser[];
  meta: any;
  links: any;
}
