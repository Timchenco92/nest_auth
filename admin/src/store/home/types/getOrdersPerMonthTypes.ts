// Action Types
import { GET_ORDERS_PER_MONTH, GET_ORDERS_PER_MONTH_ERROR, GET_ORDERS_PER_MONTH_SUCCESS } from '@/store/home';

// Interfaces
import type { IOrder } from '@/interfaces/models';

export interface IGetOrdersPerMonthState {
  data: IGetOrdersSuccessStateAndPayload['data'];
  meta: IGetOrdersSuccessStateAndPayload['meta'];
  links: IGetOrdersSuccessStateAndPayload['links'];
  error: boolean;
  pending: boolean;
}

export interface IGetOrdersPerMonth {
  type: typeof GET_ORDERS_PER_MONTH;
  page: string;
}

export interface IGetOrdersPerMonthError {
  type: typeof GET_ORDERS_PER_MONTH_ERROR;
}

export interface IGetOrdersPerMonthSuccess {
  type: typeof GET_ORDERS_PER_MONTH_SUCCESS;
  payload: IGetOrdersSuccessStateAndPayload;
}

export interface IGetOrdersSuccessStateAndPayload {
  data: IOrder[];
  meta: any;
  links: any;
}
