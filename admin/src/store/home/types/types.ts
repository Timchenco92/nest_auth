// Types
import type {
  IGetOrdersCount,
  IGetOrdersCountError,
  IGetOrdersCountState,
  IGetOrdersCountSuccess,
  IGetOrdersPerMonth,
  IGetOrdersPerMonthError,
  IGetOrdersPerMonthState,
  IGetOrdersPerMonthSuccess,
  IGetProductsCount,
  IGetProductsCountError,
  IGetProductsCountState,
  IGetProductsCountSuccess,
  IGetUsersCount,
  IGetUsersCountError,
  IGetUsersCountState,
  IGetUsersCountSuccess,
  IGetUsersPerMonth,
  IGetUsersPerMonthError,
  IGetUsersPerMonthState,
  IGetUsersPerMonthSuccess,
} from '@/store/home';

export interface IHomeState {
  ordersCount: IGetOrdersCountState;
  orders: IGetOrdersPerMonthState;
  productsCount: IGetProductsCountState;
  usersCount: IGetUsersCountState;
  users: IGetUsersPerMonthState;
}

export type THomeTypes = IGetOrdersCount |
  IGetOrdersCountError |
  IGetOrdersCountSuccess |
  IGetOrdersPerMonth |
  IGetOrdersPerMonthError |
  IGetOrdersPerMonthSuccess |
  IGetProductsCount |
  IGetProductsCountError |
  IGetProductsCountSuccess |
  IGetUsersCount |
  IGetUsersCountError |
  IGetUsersCountSuccess |
  IGetUsersPerMonth |
  IGetUsersPerMonthError |
  IGetUsersPerMonthSuccess;
