// Action Types
import { GET_ORDERS_COUNT, GET_ORDERS_COUNT_ERROR, GET_ORDERS_COUNT_SUCCESS } from '@/store/home';

export interface IGetOrdersCountState {
  count: number;
  error: boolean;
  pending: boolean;
}

export interface IGetOrdersCount {
  type: typeof GET_ORDERS_COUNT;
}

export interface IGetOrdersCountError {
  type: typeof GET_ORDERS_COUNT_ERROR;
}

export interface IGetOrdersCountSuccess {
  type: typeof GET_ORDERS_COUNT_SUCCESS;
  payload: number;
}
