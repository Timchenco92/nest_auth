// Action Types
import { GET_PRODUCTS_COUNT, GET_PRODUCTS_COUNT_ERROR, GET_PRODUCTS_COUNT_SUCCESS } from '@/store/home';

export interface IGetProductsCountState {
  count: number;
  error: boolean;
  pending: boolean;
}

export interface IGetProductsCount {
  type: typeof GET_PRODUCTS_COUNT;
}

export interface IGetProductsCountError {
  type: typeof GET_PRODUCTS_COUNT_ERROR;
}

export interface IGetProductsCountSuccess {
  type: typeof GET_PRODUCTS_COUNT_SUCCESS;
  payload: number;
}
