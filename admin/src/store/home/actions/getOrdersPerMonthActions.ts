// Action Types
import { GET_ORDERS_PER_MONTH, GET_ORDERS_PER_MONTH_ERROR, GET_ORDERS_PER_MONTH_SUCCESS } from '@/store/home';

// Types
import type {
  IGetOrdersPerMonth,
  IGetOrdersPerMonthError,
  IGetOrdersPerMonthSuccess,
  IGetOrdersSuccessStateAndPayload,
} from '@/store/home';

export const getOrdersPerMonthRequest = (page: string): IGetOrdersPerMonth => {
  console.log(page);
  return {
    type: GET_ORDERS_PER_MONTH,
    page,
  }
};

export const getOrdersPerMonthError = (): IGetOrdersPerMonthError => ({
  type: GET_ORDERS_PER_MONTH_ERROR,
});

export const getOrdersPerMonthSuccess = (payload: IGetOrdersSuccessStateAndPayload): IGetOrdersPerMonthSuccess => ({
  type: GET_ORDERS_PER_MONTH_SUCCESS,
  payload
});
