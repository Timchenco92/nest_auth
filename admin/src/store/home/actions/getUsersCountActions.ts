// Action Types
import { GET_USERS_COUNT, GET_USERS_COUNT_ERROR, GET_USERS_COUNT_SUCCESS } from '@/store/home';

// Types
import type { IGetUsersCount, IGetUsersCountError, IGetUsersCountSuccess } from '@/store/home';

export const getUsersCountRequest = (): IGetUsersCount => ({
  type: GET_USERS_COUNT,
});

export const getUsersCountError = (): IGetUsersCountError => ({
  type: GET_USERS_COUNT_ERROR,
});

export const getUsersCountSuccess = (payload: number): IGetUsersCountSuccess => ({
  type: GET_USERS_COUNT_SUCCESS,
  payload,
});
