// Action Types
import { GET_USERS_PER_MONTH, GET_USERS_PER_MONTH_ERROR, GET_USERS_PER_MONTH_SUCCESS } from '@/store/home';

// Types
import type {
  IGetUsersPerMonth,
  IGetUsersPerMonthError,
  IGetUsersPerMonthSuccess,
  IGetUsersPerMonthSuccessStateAndPayload,
} from '@/store/home';

export const getUsersPerMonthRequest = (page: string): IGetUsersPerMonth => ({
  type: GET_USERS_PER_MONTH,
  page,
});

export const getUsersPerMonthError = (): IGetUsersPerMonthError => ({
  type: GET_USERS_PER_MONTH_ERROR,
});

export const getUsersPerMonthSuccess = (payload: IGetUsersPerMonthSuccessStateAndPayload): IGetUsersPerMonthSuccess => ({
  type: GET_USERS_PER_MONTH_SUCCESS,
  payload,
});
