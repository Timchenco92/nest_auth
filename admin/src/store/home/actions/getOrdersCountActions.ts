// Action Types
import { GET_ORDERS_COUNT, GET_ORDERS_COUNT_ERROR, GET_ORDERS_COUNT_SUCCESS } from '@/store/home';

// Types
import type { IGetOrdersCount, IGetOrdersCountError, IGetOrdersCountSuccess } from '@/store/home';

export const getOrdersCountRequest = (): IGetOrdersCount => ({
  type: GET_ORDERS_COUNT,
});

export const getOrdersCountError = (): IGetOrdersCountError => ({
  type: GET_ORDERS_COUNT_ERROR,
});

export const getOrdersCountSuccess = (payload: number): IGetOrdersCountSuccess => ({
  type: GET_ORDERS_COUNT_SUCCESS,
  payload,
});
