// Action Types
import { GET_PRODUCTS_COUNT, GET_PRODUCTS_COUNT_ERROR, GET_PRODUCTS_COUNT_SUCCESS } from '@/store/home';

// Types
import type { IGetProductsCount, IGetProductsCountError, IGetProductsCountSuccess } from '@/store/home';

export const getProductsCountRequest = (): IGetProductsCount => ({
  type: GET_PRODUCTS_COUNT,
});

export const getProductsCountError = (): IGetProductsCountError => ({
  type: GET_PRODUCTS_COUNT_ERROR,
});

export const getProductsCountSuccess = (payload: number): IGetProductsCountSuccess => ({
  type: GET_PRODUCTS_COUNT_SUCCESS,
  payload,
});
