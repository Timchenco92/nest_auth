// Modules
import { createSelector } from 'reselect';

// State
import type { HomeState } from '@/store/rootReducer';

const orders = (state: HomeState) => ({
  data: state.home.orders.data,
  error: state.home.orders.error,
  links: state.home.orders.links,
  meta: state.home.orders.meta,
  pending: state.home.orders.pending,
});

const ordersCount = (state: HomeState) => ({
  count: state.home.ordersCount.count,
  error: state.home.ordersCount.error,
  pending: state.home.ordersCount.pending,
});

const productsCount = (state: HomeState) => ({
  count: state.home.productsCount.count,
  error: state.home.productsCount.error,
  pending: state.home.productsCount.pending,
});

const users = (state: HomeState) => ({
  data: state.home.users.data,
  error: state.home.users.error,
  links: state.home.users.links,
  meta: state.home.users.meta,
  pending: state.home.users.pending,
});

const usersCount = (state: HomeState) => ({
  count: state.home.usersCount.count,
  error: state.home.usersCount.error,
  pending: state.home.usersCount.pending,
});

export const dashboardOrdersSelector = createSelector(orders, ({ data }) => data);
export const dashboardOrdersErrorSelector = createSelector(orders, ({ error }) => error);
export const dashboardOrdersPendingSelector = createSelector(orders, ({ pending }) => pending);

export const dashboardOrdersCountSelector = createSelector(ordersCount, ({ count }) => count);
export const dashboardOrdersCountErrorSelector = createSelector(ordersCount, ({ error }) => error);
export const dashboardOrdersCountPendingSelector = createSelector(ordersCount, ({ pending }) => pending);

export const dashboardProductsCountSelector = createSelector(productsCount, ({ count }) => count);
export const dashboardProductsCountErrorSelector = createSelector(productsCount, ({ error }) => error);
export const dashboardProductsCountPendingSelector = createSelector(productsCount, ({ pending }) => pending);

export const dashboardUsersSelector = createSelector(users, ({ data }) => data);
export const dashboardUsersErrorSelector = createSelector(users, ({ error }) => error);
export const dashboardUsersPendingSelector = createSelector(users, ({ pending }) => pending);

export const dashboardUsersCountSelector = createSelector(usersCount, ({ count }) => count);
export const dashboardUsersCountErrorSelector = createSelector(usersCount, ({ error }) => error);
export const dashboardUsersCountPendingSelector = createSelector(usersCount, ({ pending }) => pending);

export const dashboardUsersPaginationSelector = createSelector(users, ({ meta, links }) => ({ meta, links }));
export const dashboardOrdersPaginationSelector = createSelector(orders, ({ meta, links }) => ({ meta, links }));
