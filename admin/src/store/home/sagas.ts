// Modules
import { AxiosError, AxiosResponse } from 'axios';
import { all, call, put, takeLatest } from 'redux-saga/effects';
import type { AllEffect, CallEffect, ForkEffect, PutEffect } from 'redux-saga/effects';

// Actions
import {
  getOrdersCountError,
  getOrdersCountSuccess,
  getOrdersPerMonthError,
  getOrdersPerMonthRequest,
  getOrdersPerMonthSuccess,
  getProductsCountError,
  getProductsCountSuccess,
  getUsersCountError,
  getUsersCountSuccess,
  getUsersPerMonthError,
  getUsersPerMonthRequest,
  getUsersPerMonthSuccess,
} from '@/store/home';

import { showToast } from '@/store/toast';

// Action Types
import {
  GET_ORDERS_COUNT,
  GET_ORDERS_PER_MONTH,
  GET_PRODUCTS_COUNT,
  GET_USERS_COUNT,
  GET_USERS_PER_MONTH,
} from '@/store/home';

// Services
import { homeService } from '@/services';

const { getOrdersCount, getOrdersPerMonth, getUsersPerMonth, getProductsCount, getUsersCount } = homeService;

function* getOrdersCountSaga():
  Generator<CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getOrdersCount());
    yield put(getOrdersCountSuccess(data?.ordersCount));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getOrdersCountError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getOrdersPerMonthSaga({ page }: ReturnType<typeof getOrdersPerMonthRequest>):
  Generator<CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getOrdersPerMonth(page));
    yield put(getOrdersPerMonthSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getOrdersPerMonthError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getUsersPerMonthSaga({ page }: ReturnType<typeof getUsersPerMonthRequest>):
  Generator<CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getUsersPerMonth(page));
    yield put(getUsersPerMonthSuccess(data));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getUsersPerMonthError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getProductsCountSaga():
  Generator<CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getProductsCount());
    yield put(getProductsCountSuccess(data?.productsCount));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getProductsCountError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getUsersCountSaga():
  Generator<CallEffect<AxiosResponse
    | AxiosError>
    | PutEffect,
    AxiosResponse
    | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getUsersCount());
    yield put(getUsersCountSuccess(data?.usersCount));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { message } = error.response?.data!;
      yield put(getUsersCountError());
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* homeSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_ORDERS_COUNT, getOrdersCountSaga)]);
  yield all([takeLatest(GET_ORDERS_PER_MONTH, getOrdersPerMonthSaga)]);
  yield all([takeLatest(GET_USERS_PER_MONTH, getUsersPerMonthSaga)]);
  yield all([takeLatest(GET_PRODUCTS_COUNT, getProductsCountSaga)]);
  yield all([takeLatest(GET_USERS_COUNT, getUsersCountSaga)]);
}
