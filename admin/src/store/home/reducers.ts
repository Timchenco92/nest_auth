// Modules
import type { Reducer } from 'redux';

// Action Types
import {
  GET_ORDERS_COUNT,
  GET_ORDERS_COUNT_ERROR,
  GET_ORDERS_COUNT_SUCCESS,
  GET_ORDERS_PER_MONTH,
  GET_ORDERS_PER_MONTH_ERROR,
  GET_ORDERS_PER_MONTH_SUCCESS,
  GET_PRODUCTS_COUNT,
  GET_PRODUCTS_COUNT_ERROR,
  GET_PRODUCTS_COUNT_SUCCESS,
  GET_USERS_COUNT,
  GET_USERS_COUNT_ERROR,
  GET_USERS_COUNT_SUCCESS,
  GET_USERS_PER_MONTH,
  GET_USERS_PER_MONTH_ERROR,
  GET_USERS_PER_MONTH_SUCCESS,
} from '@/store/home';

// Types
import type { IHomeState, THomeTypes } from '@/store/home';

const initialState: IHomeState = {
  orders: {
    data: [],
    links: {},
    meta: {},
    error: false,
    pending: false,
  },
  ordersCount: {
    count: 0,
    error: false,
    pending: false,
  },
  productsCount: {
    count: 0,
    error: false,
    pending: false,
  },
  users: {
    data: [],
    links: {},
    meta: {},
    error: false,
    pending: false,
  },
  usersCount: {
    count: 0,
    error: false,
    pending: false,
  },
};

export const homeReducer: Reducer<IHomeState, THomeTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORDERS_COUNT: {
      return {
        ...state,
        ordersCount: {
          count: state.ordersCount.count,
          error: false,
          pending: true,
        },
      };
    }
    case GET_ORDERS_COUNT_ERROR: {
      return {
        ...state,
        ordersCount: {
          count: state.ordersCount.count,
          error: true,
          pending: false,
        },
      };
    }
    case GET_ORDERS_COUNT_SUCCESS: {
      return {
        ...state,
        ordersCount: {
          count: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_ORDERS_PER_MONTH: {
      return {
        ...state,
        orders: {
          data: state.orders.data,
          error: false,
          links: state.orders.links,
          meta: state.orders.meta,
          pending: true,
        },
      };
    }
    case GET_ORDERS_PER_MONTH_ERROR: {
      return {
        ...state,
        orders: {
          data: state.orders.data,
          error: true,
          links: state.orders.links,
          meta: state.orders.meta,
          pending: false,
        },
      };
    }
    case GET_ORDERS_PER_MONTH_SUCCESS: {
      return {
        ...state,
        orders: {
          data: action.payload.data,
          error: false,
          pending: false,
          meta: action.payload.meta,
          links: action.payload.links,
        },
      };
    }
    case GET_PRODUCTS_COUNT: {
      return {
        ...state,
        productsCount: {
          count: state.productsCount.count,
          error: false,
          pending: true,
        },
      };
    }
    case GET_PRODUCTS_COUNT_ERROR: {
      return {
        ...state,
        productsCount: {
          count: state.productsCount.count,
          error: true,
          pending: false,
        },
      };
    }
    case GET_PRODUCTS_COUNT_SUCCESS: {
      return {
        ...state,
        productsCount: {
          count: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_USERS_COUNT: {
      return {
        ...state,
        usersCount: {
          count: state.usersCount.count,
          error: false,
          pending: true,
        },
      };
    }
    case GET_USERS_COUNT_ERROR: {
      return {
        ...state,
        usersCount: {
          count: state.usersCount.count,
          error: true,
          pending: false,
        },
      };
    }
    case GET_USERS_COUNT_SUCCESS: {
      return {
        ...state,
        usersCount: {
          count: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_USERS_PER_MONTH: {
      return {
        ...state,
        users: {
          data: state.users.data,
          error: false,
          pending: true,
          meta: state.users.meta,
          links: state.users.links,
        },
      };
    }
    case GET_USERS_PER_MONTH_ERROR: {
      return {
        ...state,
        users: {
          data: state.users.data,
          error: true,
          pending: false,
          meta: state.users.meta,
          links: state.users.links,
        },
      };
    }
    case GET_USERS_PER_MONTH_SUCCESS: {
      return {
        ...state,
        users: {
          data: action.payload.data,
          error: false,
          pending: false,
          meta: action.payload.meta,
          links: action.payload.links,
        },
      };
    }
    default: {
      return state;
    }
  }
};
