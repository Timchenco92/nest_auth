interface IProps {
  dateTime: Date;
}

export const ConvertDatetime = ({ dateTime }: IProps) => {
  const date = new Date(dateTime);
  const formatDate = date.toLocaleDateString('en-US', {
    year: 'numeric',
    month: 'long',
    day: "numeric"
  });

  return formatDate;
}
