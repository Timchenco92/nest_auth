interface IColors {
  [key: string]: string
}

export const COLORS: IColors = {
  GRAY_WITH_ALFA: 'rgba(224, 224, 224, 1)',
  GREEN_GRADIENT_500: 'linear-gradient(60deg, rgba(245,0,87,1) 0%, rgba(255,138,128,1) 100%)',
  ORANGE_GRADIENT_500: 'linear-gradient(60deg, rgba(251,140,0,1) 0%, rgba(255,202,41,1) 100%)',
  PINK_GRADIENT_500: 'linear-gradient(60deg, rgba(67,160,71,1) 0%, rgba(255,235,59,1) 100%)',
}
