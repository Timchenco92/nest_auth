export const TOOLTIPS_MESSAGES = {
  isDetails: (key: string) => `${key} details`,
  isUpdate: (key: string) => `Update current ${key}`,
  isDeleted: (key: string) => `Delete current ${key}`,
};
