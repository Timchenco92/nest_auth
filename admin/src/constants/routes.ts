// Modules
import { lazy } from 'react';
import {
  Dashboard,
  FormatAlignJustify,
  Image,
  MiscellaneousServices,
  Person,
  ShoppingCart,
  Smartphone,
  Store,
} from '@mui/icons-material';

// Interfaces
import type { IScreenRoute, IDrawerListRoute } from '@/interfaces';

// Screens
const bannersScreen = lazy(async () => await import('@/screens/banners/banners-container'));
const categoriesScreen = lazy(async () => await import('@/screens/categories/categories-container'));
const homeScreen = lazy(async () => await import('@/screens/home/home-screen-container'));
const ordersScreen = lazy(async () => await import('@/screens/orders/orders-container'));
const productsScreen = lazy(async () => await import('@/screens/products/products-container'));
const settingsScreen = lazy(async () => await import('@/screens/settings/settings-screen-container'));
const shopsScreen = lazy(async () => await import('@/screens/shops/shops-container'));
const usersScreen = lazy(async () => await import('@/screens/users/users-container'));

const listDrawerRoutes: IDrawerListRoute[] = [
  {
    icon: Dashboard,
    name: 'Home',
    path: '/dashboard/home',
  },
  {
    icon: Image,
    name: 'Banners',
    path: '/dashboard/banners',
  },
  {
    icon: FormatAlignJustify,
    name: 'Categories',
    path: '/dashboard/categories',
  },
  {
    icon: Smartphone,
    name: 'Products',
    path: '/dashboard/products',
  },
  {
    icon: ShoppingCart,
    name: 'Orders',
    path: '/dashboard/orders',
  },
  {
    icon: Store,
    name: 'Shops',
    path: '/dashboard/shops',
  },
  {
    icon: Person,
    name: 'Users',
    path: '/dashboard/users',
  },
  {
    icon: MiscellaneousServices,
    name: 'Settings',
    path: '/dashboard/settings',
  },
];

const screenRoutes: IScreenRoute[] = [
  {
    component: bannersScreen,
    path: 'banners/*',
  },
  {
    component: categoriesScreen,
    path: 'categories/*',
  },
  {
    component: homeScreen,
    path: 'home',
  },
  {
    component: ordersScreen,
    path: 'orders/*',
  },
  {
    component: productsScreen,
    path: 'products/*',
  },
  {
    component: shopsScreen,
    path: 'shops/*',
  },
  {
    component: usersScreen,
    path: 'users/*',
  },
  {
    component: settingsScreen,
    path: 'settings',
  },
];

const routes = Object.freeze({
  listDrawerRoutes: listDrawerRoutes,
  screenRoutes: screenRoutes,
});

export default routes;
