// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

const getOrdersCount = async (): Promise<AxiosResponse | AxiosError> => {
  return API.get('/home/orders-count');
}

const getOrdersPerMonth = async (page: string): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/home/orders-per-month?page=${page}`);
}

const getProductsCount = async (): Promise<AxiosResponse | AxiosError> => {
  return API.get('/home/products-count');
}

const getUsersCount = async (): Promise<AxiosResponse | AxiosError> => {
  return API.get('/home/users-count');
}

const getUsersPerMonth = async (page: string): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/home/users-per-month?page=${page}`);
}

export const homeService = {
  getOrdersCount,
  getOrdersPerMonth,
  getProductsCount,
  getUsersCount,
  getUsersPerMonth,
};
