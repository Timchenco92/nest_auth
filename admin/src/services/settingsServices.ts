// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IAdmin } from '@/interfaces/models';

interface IAdminUpdatedData {
  firstName?: string;
  middleName?: string;
  lastName?: string;
  phone?: string;
  email?: string;
  password?: string;
}

const updateAdminSettings = async (id: IAdmin['id'], data: IAdminUpdatedData): Promise<AxiosResponse | AxiosError> => {
  return API.put(`/settings/${id}`, data);
};

export const settingsServices = {
  updateAdminSettings,
};
