// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IProduct } from '@/interfaces/models';
import type { IPagination } from '@/interfaces';

interface IGetProducts extends IPagination {
  query: string;
}

const addProduct = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/products', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};
const deleteProduct = async (productId: IProduct['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/products/${productId}`);
};
const getProduct = async (productId: IProduct['id']): Promise<AxiosResponse<IProduct> | AxiosError> => {
  return API.get(`/products/${productId}`);
};
const getProducts = async ({ limit, page, query }: IGetProducts): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/products?limit=${limit}&page=${page}&search=${query}`);
};
const updateProduct = async (productId: IProduct['id'], data: FormData) => {
  return API.patch(`/products/${productId}`, data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

export const productService = {
  addProduct,
  deleteProduct,
  getProduct,
  getProducts,
  updateProduct,
};
