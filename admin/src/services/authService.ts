// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

export interface ILoginData {
  email: string;
  password: string;
}

const getMe = async (): Promise<AxiosResponse | AxiosError> => {
  return API.get('/auth/me');
}

const login = async (data: ILoginData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/auth/login', data);
}

export const authService = {
  getMe,
  login,
};
