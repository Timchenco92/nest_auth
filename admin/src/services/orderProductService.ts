// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IOrder, IOrderProduct } from '@/interfaces/models';

const addProductToOrder = async (data: any): Promise<AxiosResponse | AxiosError> => {
  return API.post('/order-products', data);
};

const incrementOrderProductQuantity = async (id: IOrderProduct['id'], quantity: number): Promise<AxiosResponse | AxiosError> => {
  return API.patch(`/order-products/${id}/increment`, { quantity });
};

const decrementOrderProductQuantity = async (id: IOrderProduct['id'], quantity: number): Promise<AxiosResponse | AxiosError> => {
  return API.patch(`/order-products/${id}/decrement`, { quantity });
};

const deleteProductFromOrder = async (id: IOrderProduct['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/order-products/${id}`);
};

const getOrderProducts = async (orderId: IOrder['id']): Promise<AxiosResponse<IOrderProduct[]> | AxiosError> => {
  return API.get(`/order-products/${orderId}`);
};


export const orderProductService = {
  addProductToOrder,
  deleteProductFromOrder,
  getOrderProducts,
  incrementOrderProductQuantity,
  decrementOrderProductQuantity,
};
