// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IShop } from '@/interfaces/models';
import type { IPagination } from '@/interfaces';

const addShop = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/shops', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const deleteShop = async (id: IShop['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/shops/${id}`);
};

const getShop = async (id: IShop['id']): Promise<AxiosResponse<IShop> | AxiosError> => {
  return API.get(`/shops/${id}`)
};

const getShops = async ({ limit, page }: IPagination): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/shops?limit=${limit}&page=${page}`)
};

const updateShop = async (id: IShop['id'], data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.patch(`/shops/${id}`, data, { headers: { 'Content-Type': 'multipart/form-data' } })
};

export const shopService = {
  addShop,
  deleteShop,
  getShop,
  getShops,
  updateShop,
};
