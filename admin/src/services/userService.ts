// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IUser } from '@/interfaces/models';
import type { IPagination } from '@/interfaces';

interface IGetUsers extends IPagination {
  query: string;
}

interface IGetUser extends IPagination {
  userId: IUser['id'];
}

const addUser = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/users', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const deleteUser = async (userId: IUser['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/users/${userId}`);
};

const getUser = async ({ userId, limit, page }: IGetUser): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/users/${userId}?limit=${limit}&page=${page}`);
};

const getUsers = async ({ limit, page, query }: IGetUsers): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/users?limit=${limit}&page=${page}&search=${query}`);
};

const updateUser = async (userId: IUser['id'], data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.patch(`/users/${userId}`, data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

export const userService = {
  addUser,
  deleteUser,
  getUser,
  getUsers,
  updateUser,
};
