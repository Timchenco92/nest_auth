// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import { IShop, IShopGallery } from '@/interfaces/models';

const addShopGallery = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/shop-galleries', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const deleteShopGallery = async (shopGalleryId: IShopGallery['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/shop-galleries/${shopGalleryId}`);
};

const getShopGalleries = async (shopId: IShop['id']): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/shop-galleries/${shopId}`);
};

export const shopGalleryService = {
  addShopGallery,
  deleteShopGallery,
  getShopGalleries,
};
