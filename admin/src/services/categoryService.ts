// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { ICategory } from '@/interfaces/models';
import type { IPagination } from '@/interfaces';

interface IGetCategories extends IPagination {
  query: string;
}

interface IGetCategory extends IPagination {
  categoryId: ICategory['id'];
}

const addCategory = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/categories', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const deleteCategory = async (categoryId: ICategory['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/categories/${categoryId}`);
};

const getCategories = async ({ limit, page, query }: IGetCategories): Promise<AxiosResponse<ICategory[]> | AxiosError> => {
  return API.get(`/categories?limit=${limit}&page=${page}&search=${query}`);
};

const getCategory = async ({ categoryId, limit, page }: IGetCategory): Promise<AxiosResponse<ICategory> | AxiosError> => {
  return API.get(`/categories/${categoryId}?limit=${limit}&page=${page}`);
};

const updateCategory = async (
  categoryId: ICategory['id'],
  data: FormData,
): Promise<AxiosResponse | AxiosError> => {
  return API.patch(`/categories/${categoryId}`, data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

export const categoryService = {
  addCategory,
  deleteCategory,
  getCategories,
  getCategory,
  updateCategory,
};
