// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IBanner } from '@/interfaces/models';

const addBanner = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/banners', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const deleteBanner = async (bannerId: IBanner['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/banners/${bannerId}`);
};

const getBanners = async ({ limit = '15', page = '1' }): Promise<AxiosResponse<IBanner[]> | AxiosError> => {
  return API.get(`/banners?limit=${limit}&page=${page}`);
};

export const bannerService = {
  addBanner,
  deleteBanner,
  getBanners,
};
