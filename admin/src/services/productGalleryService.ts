// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';

const addProductGallery = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/product-galleries', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const deleteProductGallery = async (galleryProductId: IProductGallery['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/product-galleries/${galleryProductId}`);
};

const getProductGalleries = async (productId: IProduct['id']): Promise<AxiosResponse<IProductGallery[]> | AxiosError> => {
  return API.get(`/product-galleries/${productId}`);
};

export const productGalleryService = {
  addProductGallery,
  deleteProductGallery,
  getProductGalleries,
};
