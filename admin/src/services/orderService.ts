// Modules
import { AxiosError, AxiosResponse } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IOrder } from '@/interfaces/models';
import type { IPagination, IAddOrderPayload } from '@/interfaces';

interface IGetOrders extends IPagination {
  query: string;
}

const addOrder = async (data: IAddOrderPayload): Promise<AxiosResponse | AxiosError> => {
  return API.post('/orders', data);
};

const deleteOrder = async (id: IOrder['id']): Promise<AxiosResponse | AxiosError> => {
  return API.delete(`/orders/${id}`);
};

const getOrder = async (id: IOrder['id']): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/orders/${id}`);
};

const getOrders = async ({ limit, page, query }: IGetOrders): Promise<AxiosResponse | AxiosError> => {
  return API.get(`/orders?limit=${limit}&page=${page}&search=${query}`);
};

const updateOrder = async (orderId: IOrder['id'], comment: string | null, delivery: string | null): Promise<AxiosResponse | AxiosError> => {
  return API.patch(`/orders/${orderId}`, { comment, delivery });
};

export const orderService = {
  addOrder,
  deleteOrder,
  getOrder,
  getOrders,
  updateOrder,
};
