// Core
import { Module, Global } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';

// Config
import { MailConfig } from '@/config';

// Services
import { MailService } from './mail.service';

@Global()
@Module({
  imports: [MailerModule.forRootAsync({ useClass: MailConfig })],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
