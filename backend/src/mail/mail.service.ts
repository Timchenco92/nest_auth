import { Inject, Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { UserEntity } from '@/database/entities';

@Injectable()
export class MailService {
  @Inject(MailerService)
  private mailerService: MailerService;

  async adminUserCreateSuccessfully(
    user: UserEntity,
    subject: string,
    template: string,
  ) {
    await this.mailerService.sendMail({
      to: user.email,
      // subject: 'Welcome to Nice App! Confirm your Email',
      subject: subject,
      // template: './confirmation',
      template: template,
      context: {
        name: user.fullName,
        url: user.password,
      },
    });
  }
}
