// Entities
import { UserEntity } from '@/database/entities';

export class AdminCreatedUserEvent {
  user: UserEntity;
}
