// Entities
import { OrderEntity, UserEntity } from '@/database/entities';

interface IAdminOrderCreateEvent {
  address: UserEntity['address'];
  comment: OrderEntity['comment'];
  createdAt: OrderEntity['createdAt'];
  delivery: OrderEntity['delivery'];
  email: UserEntity['email'];
  fullName: UserEntity['fullName'];
  orderNumber: OrderEntity['orderNumber'];
  phone: UserEntity['phone'];
  total: OrderEntity['total'];
}

export class AdminOrderCreateEvent {
  public payload: IAdminOrderCreateEvent;
}
