// Entities
import { UserEntity } from '@/database/entities';

export class ClientRegisterUserEvent {
  user: UserEntity;
}
