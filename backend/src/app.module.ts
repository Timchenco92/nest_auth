// Core
import { Module, OnModuleInit } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CacheModule } from '@nestjs/cache-manager';
import { EventEmitterModule } from '@nestjs/event-emitter';

// Controllers
import { AppController } from './app.controller';

// Helpers
import { getEnvPath } from '@/helpers';

// Modules
import { ApiModule } from '@/api';
import { MailModule } from '@/mail';

// Services
import { AppService } from './app.service';
import { TypeOrmConfigService } from '@/typeorm';

const envFilePath: string = getEnvPath(`../${__dirname}`);

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath, isGlobal: true }),
    TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
    }),
    CacheModule.register({ isGlobal: true }),
    ApiModule,
    MailModule,
    EventEmitterModule.forRoot(),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements OnModuleInit {
  onModuleInit(): any {
    console.log('init');
  }
}
