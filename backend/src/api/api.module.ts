// Core
import { Module } from '@nestjs/common';

// Modules
import { AdminModule } from '@/api/admin';

// Providers
import { FormatErrorsProvider } from '@/api/providers';
import { NoImageExceptionProvider } from '@/api/providers';
import { SubscriberProvider } from '@/api/providers';
import { SlugifyProvider } from '@/api/providers';
import { ClientModule } from '@/api/client';

@Module({
  imports: [AdminModule, ClientModule],
  providers: [
    FormatErrorsProvider,
    NoImageExceptionProvider,
    SubscriberProvider,
    SlugifyProvider,
  ],
  controllers: [],
  exports: [SubscriberProvider],
})
export class ApiModule {}
