// Core
import { Module } from '@nestjs/common';

// Modules
import { AuthModule } from '@/api/client/auth';
import { BannersModule } from '@/api/client/banners';
import { RoutesModule } from '@/api/client/routes';
import { UsersModule } from '@/api/client/users';

@Module({
  imports: [AuthModule, UsersModule, RoutesModule, BannersModule],
})
export class ClientModule {}
