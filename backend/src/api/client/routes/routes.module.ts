// Core
import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';

// Modules
import { AuthModule } from '@/api/client/auth';
import { BannersModule } from '@/api/client/banners';
import { ClientModule } from '@/api/client';
import { UsersModule } from '@/api/client/users';

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'client',
        module: ClientModule,
        children: [
          {
            path: 'auth',
            module: AuthModule,
          },
          {
            path: 'banners',
            module: BannersModule,
          },
          {
            path: 'users',
            module: UsersModule,
          },
        ],
      },
    ]),
  ],
})
export class RoutesModule {}
