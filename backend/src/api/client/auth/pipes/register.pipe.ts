// Core
import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

let imageObject;

@Injectable()
export class RegisterPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype, type } = metadata;
    const errorsProvider = new FormatErrorsProvider();

    if (type === 'custom') {
      return (imageObject = plainToInstance(metatype, value));
    }

    const object = plainToInstance(metatype, {
      image: { ...imageObject },
      ...value,
    });

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: 'The given data was invalid',
        });
      }
    }
    return value;
  }
}
