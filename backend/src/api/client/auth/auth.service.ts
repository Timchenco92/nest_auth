// Core
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import type { Cache } from 'cache-manager';
import { CACHE_MANAGER } from '@nestjs/cache-manager';
import { compare } from 'bcrypt';
import { EventEmitter2 } from '@nestjs/event-emitter';

// Constants
import { CLIENT_EVENT_TYPES } from '@/constants';

// DTO
import { LoginDto, RegisterDto } from '@/api/client/auth/dto';

// Events
import { ClientRegisterUserEvent } from '@/events';

// Services
import { UsersService } from '@/api/client/users';

const { clientRegisterUser } = CLIENT_EVENT_TYPES;

@Injectable()
export class AuthService {
  @Inject(UsersService) private readonly usersService: UsersService;
  @Inject(JwtService) private jwtService: JwtService;
  @Inject(ConfigService) private config: ConfigService;
  @Inject(CACHE_MANAGER) private cacheManager: Cache;
  @Inject(EventEmitter2)
  private readonly eventEmitter: EventEmitter2;

  async validateUser(email: string, password: string) {
    const user = await this.usersService.findByEmail(email);

    if (!user) {
      throw new UnauthorizedException(`The user with ${email} is not exist`);
    }

    const isPasswordValid = await compare(password, user.password);

    if (!isPasswordValid) {
      throw new UnauthorizedException('Invalid password');
    }

    return user;
  }

  async login(loginDto: LoginDto) {
    const { email, password } = loginDto;

    const user = await this.validateUser(email, password);

    const tokenExpirationTime = this.config.get<number>('JWT_EXP_H');

    const payload = { sub: user.id, email };
    const accessToken = this.jwtService.sign(payload);
    await this.cacheManager.set(
      'access_token',
      accessToken,
      tokenExpirationTime,
    );
    return {
      accessToken,
    };
  }

  async register(registerDto: RegisterDto) {
    return this.usersService
      .create(registerDto)
      .then(async (response) => {
        const { message, user } = response;
        const { id, email } = user;
        const payload = { sub: id, email };
        const tokenExpirationTime = this.config.get<number>('JWT_EXP_H');
        const clientRegisterUserEvent = new ClientRegisterUserEvent();

        const accessToken = this.jwtService.sign(payload);

        await this.cacheManager.set(
          'access_token',
          accessToken,
          tokenExpirationTime,
        );

        await this.eventEmitter.emitAsync(
          clientRegisterUser,
          clientRegisterUserEvent,
        );

        return {
          accessToken,
          message,
        };
      })
      .catch((error) => error);
  }
}
