// Core
import {
  Body,
  Controller,
  Get,
  Inject,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';

// Decorators
import { LoginUserDecorator } from '@/api/decorators';

// DTO
import { LoginDto, RegisterDto } from '@/api/client/auth/dto';

// Entities
import { UserEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Guards
import { JwtAuthGuard } from '@/api/client/auth/guards';

// Pipes
import { LoginPipe, RegisterPipe } from '@/api/client/auth/pipes';

// Services
import { AuthService } from './auth.service';

@Controller()
export class AuthController {
  @Inject(AuthService) private readonly authService: AuthService;

  @UsePipes(new LoginPipe())
  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }

  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'avatar', tableName: 'users' }),
  )
  @UsePipes(new RegisterPipe())
  @Post('register')
  async register(
    @Body() registerDto: RegisterDto,
    @UploadedFile()
    file: Express.Multer.File,
  ) {
    if (file) registerDto.avatar = file.filename;
    return this.authService.register(registerDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get('me')
  async getMe(@LoginUserDecorator() user: UserEntity) {
    return user;
  }
}
