// Core
import {
  IsNotEmpty,
  IsEmail,
  IsPhoneNumber,
  ValidateIf,
} from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Decorators
import { IsUnique } from '@/api/decorators';

const { isNotEmpty, isEmail, isPhoneNumber } = ERROR_MESSAGES;

export class RegisterDto {
  @IsNotEmpty({ message: () => isNotEmpty('First Name') })
  firstName: string;

  @IsNotEmpty({ message: () => isNotEmpty('Middle Name') })
  middleName: string;

  @IsNotEmpty({ message: () => isNotEmpty('Last Name') })
  lastName: string;

  @IsEmail({}, { message: isEmail })
  @IsUnique('users', 'email', 'Email')
  email: string;

  @IsPhoneNumber('UA', { message: isPhoneNumber })
  @IsUnique('users', 'phone', 'Phone')
  phone: string;

  @IsNotEmpty({ message: () => isNotEmpty('Password') })
  password: string;

  @ValidateIf(
    (object, value) => object.hasOwnProperty('address') && value !== '',
  )
  address: string;

  avatar: string;
}
