// Core
import { IsNotEmpty, IsEmail } from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

const { isEmail, isNotEmpty } = ERROR_MESSAGES;

export class LoginDto {
  @IsEmail({}, { message: isEmail })
  email: string;

  @IsNotEmpty({ message: () => isNotEmpty('Password') })
  password: string;
}
