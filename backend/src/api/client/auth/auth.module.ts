// Core
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

// Config
import { JwtConfig } from '@/config';

// Controller
import { AuthController } from './auth.controller';

// Modules
import { UsersModule } from '@/api/client/users';

// Services
import { AuthService } from './auth.service';

// Strategies
import { JwtStrategy, LocalStrategy } from './strategies';
import { AuthSubscriber } from '@/database/subscribers/client/auth.subscriber';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.registerAsync({ useClass: JwtConfig }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy, AuthSubscriber],
})
export class AuthModule {}
