// Core
import { Controller, Get, Inject } from '@nestjs/common';

// Services
import { BannersService } from './banners.service';

@Controller()
export class BannersController {
  @Inject()
  private readonly bannersService: BannersService;

  @Get()
  async findAll() {
    return this.bannersService.findAll();
  }
}
