// Core
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

// Entities
import { BannerEntity } from '@/database/entities';

@Injectable()
export class BannersService {
  @InjectRepository(BannerEntity)
  private bannerRepository: Repository<BannerEntity>;

  async findAll(): Promise<BannerEntity[] | null> {
    return this.bannerRepository
      .find()
      .then((response) => response)
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }
}
