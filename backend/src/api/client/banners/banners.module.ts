// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { BannersController } from './banners.controller';

// Entities
import { BannerEntity } from '@/database/entities';

// Services
import { BannersService } from './banners.service';

@Module({
  imports: [TypeOrmModule.forFeature([BannerEntity])],
  controllers: [BannersController],
  providers: [BannersService],
})
export class BannersModule {}
