// Core
import { Controller, Body, Patch, Param, Inject } from '@nestjs/common';

// DTO
import { UpdateUserDto } from './dto/update-user.dto';

// Entities
import { UserEntity } from '@/database/entities';

// Services
import { UsersService } from './users.service';

@Controller()
export class UsersController {
  @Inject()
  private readonly usersService: UsersService;

  @Patch(':id')
  update(
    @Param('id') id: UserEntity['id'],
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.update(+id, updateUserDto);
  }
}
