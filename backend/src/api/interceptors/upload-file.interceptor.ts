// Modules
import { NestInterceptor, Type } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';

interface IUploadFileInterceptorArguments {
  fieldName: string;
  tableName: string | undefined;
}

export function UploadFileInterceptor({
  fieldName,
  tableName,
}: IUploadFileInterceptorArguments): Type<NestInterceptor> {
  const path = join(__dirname, '../../../', 'public', 'uploads');

  if (!existsSync(path)) {
    mkdirSync(path);
  }

  const destination = tableName ? `${path}/${tableName}` : path;

  const storage = diskStorage({
    destination,
    filename: (req, file, callback) => {
      const uniquePrefix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
      const fileName = `${uniquePrefix}-${file.originalname}`;
      callback(null, fileName);
    },
  });

  return FileInterceptor(fieldName, { storage });
}
