// Core
import { Controller, Get, UseGuards } from '@nestjs/common';

// Entities
import { AdminEntity } from '@/database/entities';

// Decorators
import { LoginAdminDecorator } from '@/api/decorators';

// Guards
import { JwtAuthGuard } from '@/api/admin/auth/guards';

@Controller()
export class AdminController {
  @UseGuards(JwtAuthGuard)
  @Get()
  getProfile(@LoginAdminDecorator() user: AdminEntity) {
    return user;
  }
}
