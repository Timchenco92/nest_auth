// Core
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Entities
import { AdminEntity } from '@/database/entities';

@Injectable()
export class AdminService {
  @InjectRepository(AdminEntity)
  private readonly adminRepository: Repository<AdminEntity>;

  async findByEmail(email: string): Promise<AdminEntity | undefined> {
    return this.adminRepository.findOne({ where: { email } });
  }

  async findById(id: number): Promise<AdminEntity | undefined> {
    return this.adminRepository.findOne({ where: { id } });
  }

  async create(email: string, password: string): Promise<AdminEntity> {
    const user = this.adminRepository.create({ email, password });
    return this.adminRepository.save(user);
  }
}
