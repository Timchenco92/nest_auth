// Core
import { ArrayMinSize, IsArray, IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// DTO
import { OrderProductDto } from '@/api/admin/order-products/dto';

const { isNotEmpty, arrayMinSize } = ERROR_MESSAGES;

export class CreateOrderProductDto {
  @IsNotEmpty({ message: () => isNotEmpty('Order ID') })
  orderId: number;

  @IsNotEmpty({ message: () => isNotEmpty('Total') })
  orderTotal: number;

  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1, { message: () => arrayMinSize('Products', 1) })
  @Type(() => OrderProductDto)
  orderProducts: OrderProductDto[];
}
