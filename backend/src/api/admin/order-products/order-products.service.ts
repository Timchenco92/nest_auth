// Core
import {
  Injectable,
  UnprocessableEntityException,
  NotFoundException,
} from '@nestjs/common';
import { InsertResult, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import {
  CreateOrderProductDto,
  UpdateOrderProductDto,
} from '@/api/admin/order-products/dto';

// Entities
import {
  OrderProductEntity,
  OrderEntity,
  ProductEntity,
} from '@/database/entities';

const { isCreated, isDeleted, isUpdated } = RESPONSE_MESSAGES;

@Injectable()
export class OrderProductsService {
  @InjectRepository(OrderProductEntity)
  private readonly orderProductRepository: Repository<OrderProductEntity>;
  @InjectRepository(ProductEntity)
  private readonly productRepository: Repository<ProductEntity>;
  @InjectRepository(OrderEntity)
  private readonly orderRepository: Repository<OrderEntity>;

  async findAll(id: OrderEntity['id']) {
    return this.orderProductRepository
      .find({
        where: { orderId: id },
        relations: ['product'],
        order: { id: 'ASC' },
      })
      .then((response) => response)
      .catch(() => {
        throw new NotFoundException({
          message: 'Can not find products in order.',
        });
      });
  }

  async create(
    createOrderProductDto: CreateOrderProductDto,
  ): Promise<InsertResult | any> {
    const { orderId, orderProducts, orderTotal } = createOrderProductDto;
    const dataToSave = this.orderProductRepository.create(orderProducts);

    return this.orderRepository
      .findOneByOrFail({ id: orderId })
      .then(({ total }) => {
        const newTotal = orderTotal + Number(total);

        return this.orderProductRepository
          .save(dataToSave)
          .then(() => {
            return this.orderRepository
              .update({ id: orderId }, { total: String(newTotal) })
              .then(() => ({ message: isCreated('order product') }))
              .catch(() => {
                throw new UnprocessableEntityException({
                  message: 'An error occurred when updating an order.',
                });
              });
          })
          .catch(() => {
            throw new UnprocessableEntityException({
              message: 'An error occurred when adding a product to the order.',
            });
          });
      })
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }

  async incrementOrderProductQuantity(
    id: OrderProductEntity['id'],
    updateOrderProductDto: UpdateOrderProductDto,
  ) {
    const { quantity } = updateOrderProductDto;

    return this.orderProductRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        const { price, total } = response;
        const newTotal = Number(total) + Number(price);

        return this.orderProductRepository
          .update(
            { id },
            {
              quantity,
              total: String(newTotal),
            },
          )
          .then(() => ({ message: isUpdated('product in order') }))
          .catch(() => {
            throw new UnprocessableEntityException({
              message: 'Can not update product in order.',
            });
          });
      })
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }

  async decrementOrderProductQuantity(
    id: OrderProductEntity['id'],
    updateOrderProductDto: UpdateOrderProductDto,
  ) {
    const { quantity } = updateOrderProductDto;

    return this.orderProductRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        const { price, total } = response;
        const newTotal = Number(total) - Number(price);

        return this.orderProductRepository
          .update(
            { id },
            {
              quantity,
              total: String(newTotal),
            },
          )
          .then(() => ({ message: isUpdated('product in order') }))
          .catch(() => {
            throw new UnprocessableEntityException({
              message: 'Can not update product in order.',
            });
          });
      })
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }

  async remove(id: OrderProductEntity['id']) {
    return this.orderProductRepository
      .findOneByOrFail({ id })
      .then((response) => {
        const { orderId, total } = response;

        return this.orderRepository
          .findOneByOrFail({ id: orderId })
          .then(({ total: orderTotal }) => {
            const newTotal = Number(orderTotal) - Number(total);

            return this.orderRepository
              .update({ id: orderId }, { total: newTotal.toString() })
              .then(() => {
                return this.orderProductRepository
                  .remove(response)
                  .then(() => ({ message: isDeleted('product from order') }))
                  .catch(() => {
                    throw new UnprocessableEntityException({
                      message: 'Can not remove product from order.',
                    });
                  });
              })
              .catch(() => {
                throw new UnprocessableEntityException({
                  message: 'Can not update order.',
                });
              });
          })
          .catch(() => {
            throw new NotFoundException({
              message: 'Can not find the order.',
            });
          });
      })
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }
}
