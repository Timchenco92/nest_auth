// Core
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Inject,
} from '@nestjs/common';

// DTO
import {
  CreateOrderProductDto,
  UpdateOrderProductDto,
} from '@/api/admin/order-products/dto';
import { OrderProductsService } from './order-products.service';
import { OrderProductEntity } from '@/database/entities';

@Controller()
export class OrderProductsController {
  @Inject(OrderProductsService)
  private readonly orderProductsService: OrderProductsService;

  @Get(':id')
  async findAll(@Param('id') id: string) {
    return this.orderProductsService.findAll(+id);
  }

  @Post()
  create(@Body() createOrderProductDto: CreateOrderProductDto) {
    return this.orderProductsService.create(createOrderProductDto);
  }

  @Patch(':id/increment')
  async incrementOrderProductQuantity(
    @Param('id') id: OrderProductEntity['id'],
    @Body() updateOrderProductDto: UpdateOrderProductDto,
  ) {
    return this.orderProductsService.incrementOrderProductQuantity(
      id,
      updateOrderProductDto,
    );
  }

  @Patch(':id/decrement')
  async decrementOrderProductQuantity(
    @Param('id') id: OrderProductEntity['id'],
    @Body() updateOrderProductDto: UpdateOrderProductDto,
  ) {
    return this.orderProductsService.decrementOrderProductQuantity(
      id,
      updateOrderProductDto,
    );
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderProductsService.remove(+id);
  }
}
