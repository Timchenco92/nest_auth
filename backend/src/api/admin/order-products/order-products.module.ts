// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { OrderProductsController } from './order-products.controller';

// Entities
import {
  OrderEntity,
  OrderProductEntity,
  ProductEntity,
} from '@/database/entities';

// Services
import { OrderProductsService } from './order-products.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([OrderProductEntity, ProductEntity, OrderEntity]),
  ],
  controllers: [OrderProductsController],
  providers: [OrderProductsService],
})
export class OrderProductsModule {}
