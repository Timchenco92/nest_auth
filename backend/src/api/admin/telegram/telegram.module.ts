import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Telegraf } from 'telegraf';

@Module({
  providers: [
    {
      provide: 'TELEGRAM_API_CONNECTION',
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const telegramApiToken = config.get<string>('TELEGRAM_BOT_API_KEY');
        const bot = new Telegraf(telegramApiToken);
        return bot;
      },
    },
  ],
  exports: ['TELEGRAM_API_CONNECTION'],
})
export class TelegramModule {}
