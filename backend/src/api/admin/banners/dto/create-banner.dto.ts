// Decorators
import { IsCustomFile, IsUnique } from '@/api/decorators';

export class CreateBannerDto {
  @IsUnique('banners', 'title', 'Title')
  title: string;

  @IsCustomFile()
  image: string;
}
