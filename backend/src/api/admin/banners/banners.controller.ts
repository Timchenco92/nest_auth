// Core
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Inject,
  UsePipes,
  UseInterceptors,
  UploadedFile,
  ParseFilePipe,
} from '@nestjs/common';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { DeleteResult, InsertResult } from 'typeorm';

// DTO
import { CreateBannerDto } from '@/api/admin/banners/dto';

// Entities
import { BannerEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import { AddBannerPipe } from '@/api/admin/banners/pipes';

// Services
import { BannersService } from '@/api/admin/banners/banners.service';

@Controller()
export class BannersController {
  @Inject(BannersService)
  private readonly bannersService: BannersService;

  @UsePipes(new AddBannerPipe())
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'banners' }),
  )
  @Post()
  create(
    @Body() createBannerDto: CreateBannerDto,
    @UploadedFile(new ParseFilePipe({ fileIsRequired: true }))
    file: Express.Multer.File,
  ): Promise<Promise<InsertResult> | Promise<any>> {
    createBannerDto.image = file.filename;
    return this.bannersService.create(createBannerDto);
  }

  @Get()
  findAll(@Paginate() query: PaginateQuery): Promise<Paginated<BannerEntity>> {
    return this.bannersService.findAll(query);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DeleteResult> {
    return this.bannersService.remove(+id);
  }
}
