// Core
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository } from 'typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import { CreateBannerDto } from '@/api/admin/banners/dto';

// Entities
import { BannerEntity } from '@/database/entities';

const { isCreated, isDeleted } = RESPONSE_MESSAGES;

@Injectable()
export class BannersService {
  @InjectRepository(BannerEntity)
  private readonly bannerRepository: Repository<BannerEntity>;

  async create(
    createBannerDto: CreateBannerDto,
  ): Promise<Promise<InsertResult> | Promise<any>> {
    const banner = await this.bannerRepository.create({ ...createBannerDto });
    return this.bannerRepository
      .save(banner)
      .then(() => ({ message: isCreated('banner') }))
      .catch((error) => error);
  }

  async findAll(query: PaginateQuery): Promise<Paginated<BannerEntity>> {
    const { limit: maxLimit } = query;

    return paginate(query, this.bannerRepository, {
      maxLimit,
      sortableColumns: ['id'],
    })
      .then((response) => response)
      .catch((error) => error);
  }

  async remove(id: BannerEntity['id']): Promise<DeleteResult> {
    const banner = await this.bannerRepository.findOneBy({ id });
    return this.bannerRepository
      .remove(banner)
      .then(() => ({ message: isDeleted('banner') }))
      .catch((error) => error);
  }
}
