// Core
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import { UpdateSettingDto } from '@/api/admin/settings/dto';

// Entities
import { AdminEntity } from '@/database/entities';

const { isUpdated } = RESPONSE_MESSAGES;

@Injectable()
export class SettingsService {
  @InjectRepository(AdminEntity)
  private readonly adminRepository: Repository<AdminEntity>;

  async update(id: AdminEntity['id'], updateSettingDto: UpdateSettingDto) {
    return this.adminRepository
      .findOneByOrFail({ id })
      .then(() => {
        return this.adminRepository
          .update({ id }, { ...updateSettingDto })
          .then(() => ({ message: isUpdated('admin') }))
          .catch(() => {
            throw new UnprocessableEntityException({
              message: 'An error occurred updated an admin',
            });
          });
      })
      .catch((error) => error);
  }
}
