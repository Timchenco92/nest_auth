// Core
import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToInstance } from 'class-transformer';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

const { errorValidation } = RESPONSE_MESSAGES;

@Injectable()
export class UpdateSettingPipe implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype } = metadata;

    const errorsProvider = new FormatErrorsProvider();

    const object = plainToInstance(metatype, value);

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: errorValidation,
        });
      }
    }

    return value;
  }
}
