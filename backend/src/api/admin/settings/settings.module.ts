// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { SettingsController } from '@/api/admin/settings/settings.controller';

// Entities
import { AdminEntity } from '@/database/entities';

// Services
import { SettingsService } from '@/api/admin/settings/settings.service';

@Module({
  imports: [TypeOrmModule.forFeature([AdminEntity])],
  controllers: [SettingsController],
  providers: [SettingsService],
})
export class SettingsModule {}
