// Core
import {
  Controller,
  Body,
  Patch,
  Param,
  Inject,
  UsePipes,
} from '@nestjs/common';

// DTO
import { UpdateSettingDto } from '@/api/admin/settings/dto';

// Entities
import { AdminEntity } from '@/database/entities';

// Pipes
import { UpdateSettingPipe } from '@/api/admin/settings/pipes';

// Services
import { SettingsService } from '@/api/admin/settings/settings.service';

@Controller()
export class SettingsController {
  @Inject()
  private readonly settingsService: SettingsService;

  @UsePipes(UpdateSettingPipe)
  @Patch(':id')
  async update(
    @Param('id') id: AdminEntity['id'],
    @Body() updateSettingDto: UpdateSettingDto,
  ) {
    return this.settingsService.update(+id, updateSettingDto);
  }
}
