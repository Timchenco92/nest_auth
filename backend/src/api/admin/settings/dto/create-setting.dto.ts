// Core
import { IsNotEmpty, MaxLength, IsEmail } from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Decorators
import { IsUnique } from '@/api/decorators';

const { isNotEmpty, maxLength, isEmail } = ERROR_MESSAGES;

export class CreateSettingDto {
  @IsNotEmpty({ message: () => isNotEmpty('First Name') })
  @MaxLength(50, { message: () => maxLength('First Name', 50) })
  firstName: string;

  @IsNotEmpty({ message: () => isNotEmpty('Middle Name') })
  @MaxLength(50, { message: () => maxLength('Middle Name', 50) })
  middleName: string;

  @IsNotEmpty({ message: () => isNotEmpty('Last Name') })
  @MaxLength(50, { message: () => maxLength('Last Name', 50) })
  lastName: string;

  @IsEmail({}, { message: () => isEmail })
  email: string;

  @IsUnique('admins', 'phone', 'Phone')
  phone: string;

  @IsNotEmpty({ message: () => isNotEmpty('Password') })
  @MaxLength(50, { message: () => maxLength('Password', 50) })
  password: string;
}
