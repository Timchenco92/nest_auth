// Core
import {
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
  ValidateIf,
  IsEmail,
  IsPhoneNumber,
} from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Decorators
import { IsUnique } from '@/api/decorators';

const { isNotEmpty, maxLength, minLength, isString, isPhoneNumber, isEmail } =
  ERROR_MESSAGES;

export class CreateUserDto {
  id: number | undefined;

  @IsNotEmpty({ message: () => isNotEmpty('First Name') })
  firstName: string;

  @IsNotEmpty({ message: () => isNotEmpty('First Name') })
  middleName: string;

  @IsNotEmpty({ message: () => isNotEmpty('First Name') })
  lastName: string;

  avatar: string;

  @IsPhoneNumber('UA', { message: () => isPhoneNumber })
  @IsUnique('users', 'phone', 'Phone')
  phone: string;

  @IsEmail({}, { message: () => isEmail })
  @IsUnique('users', 'email', 'Email')
  email: string;

  @ValidateIf((event) => event.address !== undefined || null)
  @MaxLength(100, { message: () => maxLength('Address', 100) })
  @IsString({ message: () => isString('Address') })
  address: string;

  @IsNotEmpty({ message: () => isNotEmpty('Password') })
  @MinLength(6, { message: () => minLength('Password', 6) })
  @MaxLength(12, { message: () => maxLength('Password', 12) })
  password: string;
}
