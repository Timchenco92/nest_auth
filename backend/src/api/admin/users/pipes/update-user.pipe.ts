// Core
import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { EntityNotFoundError, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// Entities
import { UserEntity } from '@/database/entities';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

let imageObject;

const { errorValidation } = RESPONSE_MESSAGES;

@Injectable()
export class UpdateUserPipe implements PipeTransform {
  @Inject(REQUEST)
  private request: Request;
  @InjectRepository(UserEntity)
  private readonly userRepository: Repository<UserEntity>;

  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype, type, data } = metadata;
    const { id: userId } = this.request.params || undefined;

    const isExist = await this.userRepository.exist({ where: { id: +userId } });
    if (!isExist) {
      throw new HttpException(
        new EntityNotFoundError(UserEntity, userId),
        HttpStatus.NOT_FOUND,
      );
    }

    const errorsProvider = new FormatErrorsProvider();

    if (type === 'custom') {
      if (data) {
        return (imageObject = plainToInstance(metatype, value));
      }
    }

    const object = plainToInstance(metatype, {
      image: imageObject && { ...imageObject },
      id: userId,
      ...value,
    });

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: errorValidation,
        });
      }
    }

    return value;
  }
}
