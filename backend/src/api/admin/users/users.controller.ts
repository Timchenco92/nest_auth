import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

// DTO
import { CreateUserDto, UpdateUserDto } from '@/api/admin/users/dto';

// Entities
import { UserEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import { CreateUserPipe, UpdateUserPipe } from '@/api/admin/users/pipes';

// Services
import { UsersService } from '@/api/admin/users/users.service';

@Controller()
export class UsersController {
  @Inject(UsersService)
  private readonly usersService: UsersService;

  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'avatar', tableName: 'users' }),
  )
  @UsePipes(new CreateUserPipe())
  @Post()
  create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile()
    file: Express.Multer.File,
  ) {
    if (file) createUserDto.avatar = file.filename;
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll(@Paginate() query: PaginateQuery) {
    return this.usersService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: UserEntity['id'], @Paginate() query: PaginateQuery) {
    return this.usersService.findOne(+id, query);
  }

  @UsePipes(UpdateUserPipe)
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'avatar', tableName: 'users' }),
  )
  @Patch(':id')
  update(
    @Param('id') id: UserEntity['id'],
    @Body() updateUserDto: UpdateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) updateUserDto.avatar = file.filename;
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: UserEntity['id']) {
    return this.usersService.remove(+id);
  }
}
