// Core
import {
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, InsertResult, DeleteResult } from 'typeorm';
import { EventEmitter2 } from '@nestjs/event-emitter';

// Constants
import { RESPONSE_MESSAGES, ADMIN_EVENT_TYPES } from '@/constants';

// DTO
import { CreateUserDto, UpdateUserDto } from '@/api/admin/users/dto';

// Entities
import { OrderEntity, UserEntity } from '@/database/entities';

// Events
import { AdminCreatedUserEvent } from '@/events';

const { isCreated, isDeleted, isUpdated, notFount } = RESPONSE_MESSAGES;
const { adminUserCreated } = ADMIN_EVENT_TYPES;

@Injectable()
export class UsersService {
  @InjectRepository(UserEntity)
  private readonly userRepository: Repository<UserEntity>;
  @InjectRepository(OrderEntity)
  private readonly orderRepository: Repository<OrderEntity>;
  @Inject(EventEmitter2)
  private readonly eventEmitter: EventEmitter2;

  async create(
    createUserDto: CreateUserDto,
  ): Promise<Promise<InsertResult> | Promise<any>> {
    const user = this.userRepository.create({ ...createUserDto });
    const adminCreatedUserEvent = new AdminCreatedUserEvent();
    return this.userRepository
      .save(user)
      .then((response) => {
        adminCreatedUserEvent.user = response;
        adminCreatedUserEvent.user.password = createUserDto.password;
        this.eventEmitter.emit(adminUserCreated, adminCreatedUserEvent);
        return {
          message: isCreated('user'),
        };
      })
      .catch((error) => error);
  }

  async findAll(query: PaginateQuery): Promise<Paginated<UserEntity>> {
    const { limit: maxLimit } = query;

    const queryBuilder = await this.userRepository
      .createQueryBuilder('user')
      .loadRelationCountAndMap(
        'user.ordersCount',
        'user.orders',
        'ordersCount',
      );

    return paginate(query, queryBuilder, {
      maxLimit,
      searchableColumns: ['phone'],
      sortableColumns: ['id'],
    })
      .then((response) => response)
      .catch((error) => error);
  }

  async findOne(
    id: UserEntity['id'],
    query: PaginateQuery,
  ): Promise<UserEntity | NotFoundException> {
    const { limit: maxLimit } = query;

    return this.userRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        response.orders = await paginate(query, this.orderRepository, {
          maxLimit,
          relations: {
            orderProducts: {
              product: true,
            },
          },
          sortableColumns: ['id'],
          where: { userId: id },
        });
        return response;
      })
      .catch(() => {
        throw new NotFoundException({ message: notFount('User') });
      });
  }

  async update(
    id: UserEntity['id'],
    updateUserDto: UpdateUserDto,
  ): Promise<{ message: string } | void> {
    return this.userRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        const toSaveUser = await this.userRepository.create({
          ...response,
          ...updateUserDto,
        });

        return this.userRepository
          .save(toSaveUser)
          .then(() => ({ message: isUpdated('user') }))
          .catch((err) => {
            throw new HttpException(err, HttpStatus.UNPROCESSABLE_ENTITY);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  async remove(id: UserEntity['id']): Promise<DeleteResult> {
    return this.userRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        return this.userRepository
          .remove(response)
          .then(() => ({ message: isDeleted('user') }))
          .catch((error) => error);
      })
      .catch((err) => err);
  }
}
