// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { UsersController } from './users.controller';

// Entities
import { OrderEntity, UserEntity } from '@/database/entities';

// Listeners
import { AdminCreatedUserListener } from '@/listeners';

// Notifications
import { AdminCreatedUserNotification } from '@/notifications';

// Services
import { UsersService } from './users.service';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity, OrderEntity])],
  controllers: [UsersController],
  providers: [
    UsersService,
    AdminCreatedUserListener,
    AdminCreatedUserNotification,
  ],
  exports: [AdminCreatedUserListener, AdminCreatedUserNotification],
})
export class UsersModule {}
