// Core
import { Body, Controller, Inject, Post, UsePipes } from '@nestjs/common';

// DTO
import { LoginDto } from '@/api/admin/auth/dto';

// Pipes
import { LoginPipe } from '@/api/admin/auth/pipes';

// Services
import { AuthService } from './auth.service';

@Controller()
export class AuthController {
  @Inject(AuthService)
  private readonly authService: AuthService;

  @UsePipes(new LoginPipe())
  @Post()
  async login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }
}
