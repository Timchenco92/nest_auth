// Core
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

// Config
import { JwtConfig } from '@/config';

// Controllers
import { AuthController } from './auth.controller';

// Modules
import { AdminModule } from '@/api/admin/admin';

// Services
import { AuthService } from './auth.service';

// Strategies
import { JwtStrategy, LocalStrategy } from './strategies';

@Module({
  imports: [
    AdminModule,
    PassportModule,
    JwtModule.registerAsync({
      useClass: JwtConfig,
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
