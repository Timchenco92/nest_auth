// Core
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { compare } from 'bcrypt';

// Entities
import { AdminEntity } from '@/database/entities';

// Services
import { AdminService } from '@/api/admin/admin/admin.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  @Inject(AdminService)
  private readonly adminService: AdminService;

  constructor() {
    super({
      usernameField: 'email',
    });
  }

  async validate(
    email: string,
    password: string,
  ): Promise<AdminEntity | UnauthorizedException> {
    const user = await this.adminService.findByEmail(email);
    if (!user) {
      throw new UnauthorizedException({
        message: `The admin with ${email} is not exist`,
      });
    }

    const isPasswordValid = await compare(password, user.password);

    if (!isPasswordValid) {
      throw new UnauthorizedException({ message: 'Invalid password' });
    }

    return user;
  }
}
