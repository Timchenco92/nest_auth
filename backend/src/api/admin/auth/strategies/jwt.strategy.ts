// Core
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { ConfigService } from '@nestjs/config';

// Entities
import { AdminEntity } from '@/database/entities';

// Services
import { AdminService } from '@/api/admin/admin/admin.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  @Inject(AdminService)
  private readonly adminService: AdminService;

  constructor(private readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET'),
    });
  }

  async validate(
    payload: any,
  ): Promise<Partial<AdminEntity> | UnauthorizedException> {
    const user = await this.adminService.findById(payload.sub);
    if (!user) {
      throw new UnauthorizedException('Invalid token');
    }
    const { password, ...rest } = user;
    return rest;
  }
}
