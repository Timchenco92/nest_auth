// Core
import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

@Injectable()
export class LoginPipe implements PipeTransform {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    const object = plainToInstance(metatype, value);
    const errorsProvider = new FormatErrorsProvider();

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: 'The given data was invalid',
        });
      }
    }
    return value;
  }
}
