// Core
import { Inject, Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcrypt';
import { Cache } from 'cache-manager';
import { CACHE_MANAGER } from '@nestjs/cache-manager';

// DTO
import { LoginDto } from '@/api/admin/auth/dto';

// Entities
import { AdminEntity } from '@/database/entities';

// Services
import { AdminService } from '@/api/admin/admin';

@Injectable()
export class AuthService {
  @Inject(AdminService) private adminService: AdminService;
  @Inject(JwtService) private jwtService: JwtService;
  @Inject(ConfigService) private config: ConfigService;
  @Inject(CACHE_MANAGER) private cacheManager: Cache;

  async validateUser(email: string, password: string): Promise<AdminEntity> {
    const user = await this.adminService.findByEmail(email);

    if (!user) {
      throw new UnauthorizedException(`The record with ${email} is not exist`);
    }

    const isPasswordValid = await compare(password, user.password);

    if (!isPasswordValid) {
      throw new UnauthorizedException('Invalid password');
    }

    return user;
  }

  async login(loginDto: LoginDto): Promise<{ accessToken: string }> {
    const { email, password } = loginDto;

    const user = await this.validateUser(email, password);

    const tokenExpirationTime = this.config.get<number>('JWT_EXP_H');

    const payload = { sub: user.id };
    const accessToken = this.jwtService.sign(payload);
    await this.cacheManager.set(
      'access_token',
      accessToken,
      tokenExpirationTime,
    );
    return {
      accessToken,
    };
  }
}
