// Core
import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';

// Modules
import { AdminModule as AdminAuthModule } from '@/api/admin/admin';
import { AdminModule } from '@/api/admin';
import { AuthModule } from '@/api/admin/auth/auth.module';
import { BannersModule } from '@/api/admin/banners';
import { CategoriesModule } from '@/api/admin/categories';
import { HomeModule } from '@/api/admin/home';
import { OrderProductsModule } from '@/api/admin/order-products';
import { OrdersModule } from '@/api/admin/orders';
import { ProductGalleryModule } from '@/api/admin/product-gallery';
import { ProductsModule } from '@/api/admin/products';
import { ShopsModule } from '@/api/admin/shops';
import { ShopGalleryModule } from '@/api/admin/shop-gallery';
import { UsersModule } from '@/api/admin/users/users.module';
import { SettingsModule } from '@/api/admin/settings';

@Module({
  imports: [
    RouterModule.register([
      {
        path: 'admin',
        module: AdminModule,
        children: [
          {
            module: AuthModule,
            path: 'auth/login',
          },
          {
            module: AdminAuthModule,
            path: 'auth/me',
          },
          {
            module: UsersModule,
            path: 'users',
          },
          {
            module: BannersModule,
            path: 'banners',
          },
          {
            module: CategoriesModule,
            path: 'categories',
          },
          {
            module: ProductsModule,
            path: 'products',
          },
          {
            module: ProductGalleryModule,
            path: 'product-galleries',
          },
          {
            module: OrdersModule,
            path: 'orders',
          },
          {
            module: OrderProductsModule,
            path: 'order-products',
          },
          {
            module: ShopsModule,
            path: 'shops',
          },
          {
            module: ShopGalleryModule,
            path: 'shop-galleries',
          },
          {
            module: SettingsModule,
            path: 'settings',
          },
          {
            module: HomeModule,
            path: 'home',
          },
        ],
      },
    ]),
  ],
})
export class AdminRouterModule {}
