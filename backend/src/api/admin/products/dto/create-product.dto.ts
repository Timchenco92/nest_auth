// Core
import { IsNotEmpty } from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Decorators
import { IsCustomFile, IsParentExist, IsUnique } from '@/api/decorators';

const { isNotEmpty } = ERROR_MESSAGES;

export class CreateProductDto {
  @IsParentExist('categories', 'id')
  categoryId: number;

  @IsCustomFile()
  image: any;

  @IsUnique('products', 'title', 'Title')
  title: string;

  @IsNotEmpty({ message: () => isNotEmpty('Price') })
  price: string;
}
