// Core
import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

// DTO
import { CreateProductDto, UpdateProductDto } from '@/api/admin/products/dto';

// Entities
import { ProductEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import { AddProductPipe, UpdateProductPipe } from '@/api/admin/products/pipes';

// Services
import { ProductsService } from './products.service';

@Controller()
export class ProductsController {
  @Inject(ProductsService)
  private readonly productsService: ProductsService;

  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'products' }),
  )
  @UsePipes(new AddProductPipe())
  @Post()
  async create(
    @Body() createProductDto: CreateProductDto,
    @UploadedFile(
      new ParseFilePipe({
        fileIsRequired: true,
      }),
    )
    file: Express.Multer.File,
  ) {
    createProductDto.image = file?.filename;
    return this.productsService.create(createProductDto);
  }

  @Get()
  async findAll(@Paginate() query: PaginateQuery) {
    return this.productsService.findAll(query);
  }

  @Get(':id')
  async findOne(@Param('id') id: ProductEntity['id']) {
    return this.productsService.findOne(+id);
  }

  @UsePipes(new UpdateProductPipe())
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'products' }),
  )
  @Patch(':id')
  async update(
    @Param('id') id: ProductEntity['id'],
    @Body() updateProductDto: UpdateProductDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) updateProductDto.image = file.filename;
    return this.productsService.update(+id, updateProductDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: ProductEntity['id']) {
    return this.productsService.remove(+id);
  }
}
