// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { ProductsController } from './products.controller';

// Entities
import { CategoryEntity, ProductEntity } from '@/database/entities';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

// Services
import { ProductsService } from './products.service';

@Module({
  imports: [TypeOrmModule.forFeature([CategoryEntity, ProductEntity])],
  controllers: [ProductsController],
  providers: [ProductsService, FormatErrorsProvider],
})
export class ProductsModule {}
