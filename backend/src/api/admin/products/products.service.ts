// Core
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import { CreateProductDto, UpdateProductDto } from '@/api/admin/products/dto';

// Entities
import { CategoryEntity, ProductEntity } from '@/database/entities';

const { isCreated, isDeleted, isUpdated } = RESPONSE_MESSAGES;

@Injectable()
export class ProductsService {
  @InjectRepository(ProductEntity)
  private readonly productRepository: Repository<ProductEntity>;
  @InjectRepository(CategoryEntity)
  private readonly categoryRepository: Repository<CategoryEntity>;

  async create(
    createProductDto: CreateProductDto,
  ): Promise<Promise<InsertResult> | Promise<any>> {
    const { title, image, price, categoryId } = createProductDto;
    const product = await this.productRepository.create({
      categoryId,
      image,
      price,
      title,
    });

    return this.productRepository
      .save(product)
      .then((response) => {
        return {
          message: isCreated('product'),
          ...response,
        };
      })
      .catch((error) => {
        return {
          error,
        };
      });
  }

  async findAll(query: PaginateQuery): Promise<Paginated<ProductEntity>> {
    const { limit: maxLimit } = query;

    const queryBuilder = await this.productRepository
      .createQueryBuilder('products')
      .loadRelationCountAndMap(
        'products.galleriesCount',
        'products.galleries',
        'galleriesCount',
      );

    return paginate(query, queryBuilder, {
      maxLimit,
      sortableColumns: ['id'],
      searchableColumns: ['title'],
      relations: ['category'],
    })
      .then((response) => response)
      .catch((error) => error);
  }

  async findOne(id: ProductEntity['id']): Promise<ProductEntity> {
    return this.productRepository
      .findOne({ where: { id } })
      .then(async (response) => {
        response.category = await this.categoryRepository
          .createQueryBuilder('category')
          .loadRelationCountAndMap(
            'category.productsCount',
            'category.products',
            'productsCount',
          )
          .where('category.id = :categoryId', {
            categoryId: response.categoryId,
          })
          .getOne();
        return response;
      })
      .catch((error) => error);
  }

  async update(
    id: ProductEntity['id'],
    updateProductDto: UpdateProductDto,
  ): Promise<UpdateResult> {
    const product = await this.productRepository.findOneBy({ id });

    const toSaveProduct = await this.productRepository.create({
      ...product,
      ...updateProductDto,
    });

    return this.productRepository
      .save(toSaveProduct)
      .then(() => ({ message: isUpdated('product') }))
      .catch((error) => error);
  }

  async remove(id: ProductEntity['id']): Promise<DeleteResult> {
    const product = await this.productRepository.findOneBy({ id });

    return this.productRepository
      .remove(product)
      .then(() => ({
        message: isDeleted('product'),
      }))
      .catch((error) => error);
  }
}
