// Core
import {
  IsNotEmpty,
  ValidateIf,
  IsArray,
  ValidateNested,
  ArrayMinSize,
  MaxLength,
} from 'class-validator';
import { Type } from 'class-transformer';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// DTO
import { OrderProductDto } from './order-product.dto';

const { isNotEmpty, arrayMinSize } = ERROR_MESSAGES;

export class CreateOrderDto {
  @IsNotEmpty({ message: () => isNotEmpty('User') })
  userId: number;

  @IsNotEmpty({ message: () => isNotEmpty('Total') })
  total: string;

  @ValidateIf(
    (object, value) => object.hasOwnProperty('delivery') && value !== '',
  )
  @MaxLength(255)
  delivery: string;

  @ValidateIf(
    (object, value) => object.hasOwnProperty('comment') && value !== '',
  )
  @MaxLength(255)
  comment: string;

  @IsArray()
  @ValidateNested({ each: true })
  @ArrayMinSize(1, { message: () => arrayMinSize('Products', 1) })
  @Type(() => OrderProductDto)
  orderProducts: OrderProductDto[];
}
