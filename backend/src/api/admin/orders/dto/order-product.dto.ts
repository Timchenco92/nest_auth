// Core
import { IsNotEmpty } from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

const { isNotEmpty } = ERROR_MESSAGES;

export class OrderProductDto {
  @IsNotEmpty({ message: () => isNotEmpty('Total') })
  total: string;

  @IsNotEmpty({ message: () => isNotEmpty('Quantity') })
  quantity: number;

  @IsNotEmpty({ message: () => isNotEmpty('Price') })
  price: string;
}
