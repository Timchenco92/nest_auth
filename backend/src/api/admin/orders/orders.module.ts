// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { OrdersController } from './orders.controller';

// Entities
import {
  OrderEntity,
  OrderProductEntity,
  UserEntity,
} from '@/database/entities';

// Listeners
import { AdminOrderCreateListener } from '@/listeners';

// Modules
import { TelegramModule } from '@/api/admin/telegram';

// Notifications
import { AdminOrderCreateNotification } from '@/notifications';

// Services
import { OrdersService } from './orders.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([OrderEntity, OrderProductEntity, UserEntity]),
    TelegramModule,
  ],
  controllers: [OrdersController],
  providers: [
    OrdersService,
    AdminOrderCreateListener,
    AdminOrderCreateNotification,
  ],
  exports: [AdminOrderCreateListener, AdminOrderCreateNotification],
})
export class OrdersModule {}
