// Core
import {
  Inject,
  Injectable,
  NotFoundException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { EventEmitter2 } from '@nestjs/event-emitter';

// Constants
import { ADMIN_EVENT_TYPES, RESPONSE_MESSAGES } from '@/constants';

// DTO
import { CreateOrderDto, UpdateOrderDto } from '@/api/admin/orders/dto';

// Entities
import { OrderEntity, UserEntity } from '@/database/entities';

// Events
import { AdminOrderCreateEvent } from '@/events';

const { isCreated, isDeleted, isUpdated } = RESPONSE_MESSAGES;
const { adminOrderCreated } = ADMIN_EVENT_TYPES;

@Injectable()
export class OrdersService {
  @InjectRepository(OrderEntity)
  private readonly orderRepository: Repository<OrderEntity>;
  @InjectRepository(UserEntity)
  private readonly userRepository: Repository<UserEntity>;
  @Inject(EventEmitter2)
  private readonly eventEmitter: EventEmitter2;

  async create(
    createOrderDto: CreateOrderDto,
  ): Promise<{ message: string } | void> {
    return this.orderRepository.manager
      .transaction(async (entityManager) => {
        const order = entityManager.create(OrderEntity, { ...createOrderDto });
        return entityManager.save(OrderEntity, order, {
          transaction: true,
          listeners: true,
          reload: true,
        });
      })
      .then(async (resp) => {
        const { total, delivery, orderNumber, createdAt, userId, comment } =
          resp;
        const { address, email, phone, fullName } =
          await this.userRepository.findOneBy({
            id: userId,
          });
        const adminOrderCreateEvent = new AdminOrderCreateEvent();
        adminOrderCreateEvent.payload = {
          total,
          createdAt,
          comment,
          delivery,
          address,
          phone,
          email,
          fullName,
          orderNumber,
        };

        await this.eventEmitter.emitAsync(
          adminOrderCreated,
          adminOrderCreateEvent,
        );

        return {
          message: isCreated('order'),
        };
      })
      .catch(() => {
        throw new UnprocessableEntityException({
          message: 'An error occurred when updating an order.',
        });
      });
  }

  async findAll(query: PaginateQuery): Promise<Paginated<OrderEntity>> {
    const { limit: maxLimit } = query;

    const queryBuilder = await this.orderRepository
      .createQueryBuilder('order')
      .loadRelationCountAndMap(
        'order.orderProductsCount',
        'order.orderProducts',
        'orderProductsCount',
      );

    return paginate(query, queryBuilder, {
      maxLimit,
      searchableColumns: ['orderNumber'],
      sortableColumns: ['id'],
    });
  }

  async findOne(
    id: OrderEntity['id'],
  ): Promise<OrderEntity | { message: string }> {
    return this.orderRepository
      .findOne({ where: { id }, relations: ['user'] })
      .then((response) => response)
      .catch(() => {
        throw new NotFoundException({
          message: 'The order is not exist',
        });
      });
  }

  async update(
    id: number,
    updateOrderDto: UpdateOrderDto,
  ): Promise<UpdateResult | string> {
    return this.orderRepository
      .findOneByOrFail({ id })
      .then(() => {
        return this.orderRepository
          .update({ id }, { ...updateOrderDto })
          .then(() => ({ message: isUpdated('order') }))
          .catch(() => {
            throw new UnprocessableEntityException({
              message: 'An error occurred when updating an order.',
            });
          });
      })
      .catch((error) => error);
  }

  async remove(id: number): Promise<DeleteResult | string> {
    return this.orderRepository
      .findOneByOrFail({ id })
      .then((response) => {
        return this.orderRepository
          .remove(response)
          .then(() => ({ message: isDeleted('order') }))
          .catch((error) => error);
      })
      .catch((err) => err);
  }
}
