// Core
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Inject,
  UsePipes,
} from '@nestjs/common';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

// DTO
import { CreateOrderDto, UpdateOrderDto } from '@/api/admin/orders/dto';

// Entities
import { OrderEntity } from '@/database/entities';

// Pipes
import { AddOrderPipe } from '@/api/admin/orders/pipes';

// Services
import { OrdersService } from './orders.service';

@Controller()
export class OrdersController {
  @Inject(OrdersService)
  private readonly ordersService: OrdersService;

  @UsePipes(AddOrderPipe)
  @Post()
  async create(@Body() createOrderDto: CreateOrderDto) {
    return this.ordersService.create(createOrderDto);
  }

  @Get()
  async findAll(@Paginate() query: PaginateQuery) {
    return this.ordersService.findAll(query);
  }

  @Get(':id')
  async findOne(@Param('id') id: OrderEntity['id']) {
    return this.ordersService.findOne(+id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: OrderEntity['id'],
    @Body() updateOrderDto: UpdateOrderDto,
  ) {
    return this.ordersService.update(+id, updateOrderDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: OrderEntity['id']) {
    return this.ordersService.remove(+id);
  }
}
