// Core
import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository } from 'typeorm';

// Entities
import { UserEntity } from '@/database/entities';

// Providers
import { FormatErrorsProvider } from '@/api/providers';
import { CreateOrderDto } from '@/api/admin/orders/dto';

@Injectable()
export class AddOrderPipe implements PipeTransform {
  @InjectRepository(UserEntity)
  private readonly userRepository: Repository<UserEntity>;

  async transform(value: CreateOrderDto, metadata: ArgumentMetadata) {
    const { metatype } = metadata;
    const errorsProvider = new FormatErrorsProvider();
    const { userId } = value;

    const object = plainToInstance(metatype, value);

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: 'The given data was invalid',
        });
      }

      const isExist = await this.userRepository.exist({
        where: { id: +userId },
      });
      if (!isExist) {
        throw new HttpException(
          new EntityNotFoundError(UserEntity, userId),
          HttpStatus.NOT_FOUND,
        );
      }
    }

    return value;
  }
}
