// Decorators
import { IsCustomFile, IsUnique } from '@/api/decorators';

export class CreateCategoryDto {
  @IsUnique('categories', 'title', 'Title')
  title: string;

  @IsCustomFile()
  image: any;
}
