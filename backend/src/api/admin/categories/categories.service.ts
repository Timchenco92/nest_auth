// Core
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import {
  CreateCategoryDto,
  UpdateCategoryDto,
} from '@/api/admin/categories/dto';

// Entities
import { CategoryEntity, ProductEntity } from '@/database/entities';

const { isCreated, isDeleted, isUpdated, notFount } = RESPONSE_MESSAGES;

@Injectable()
export class CategoriesService {
  @InjectRepository(CategoryEntity)
  private readonly categoryRepository: Repository<CategoryEntity>;
  @InjectRepository(ProductEntity)
  private readonly productRepository: Repository<ProductEntity>;

  async create(
    createCategoryDto: CreateCategoryDto,
  ): Promise<Promise<InsertResult> | Promise<any>> {
    const category = await this.categoryRepository.create({
      ...createCategoryDto,
    });

    return this.categoryRepository
      .save(category)
      .then(() => ({ message: isCreated('category') }))
      .catch((error) => error);
  }

  async findAll(query: PaginateQuery): Promise<Paginated<CategoryEntity>> {
    const { limit: maxLimit } = query;
    const queryBuilder = await this.categoryRepository
      .createQueryBuilder('category')
      .loadRelationCountAndMap(
        'category.productsCount',
        'category.products',
        'productsCount',
      );

    return paginate(query, queryBuilder, {
      maxLimit,
      sortableColumns: ['id'],
      searchableColumns: ['title'],
    })
      .then((response) => response)
      .catch((error) => error);
  }

  async findOne(id: CategoryEntity['id'], query: PaginateQuery): Promise<any> {
    const { limit: maxLimit } = query;

    try {
      const category = await this.categoryRepository
        .createQueryBuilder('category')
        .loadRelationCountAndMap(
          'category.productsCount',
          'category.products',
          'productsCount',
        )
        .where({ id })
        .getOne();

      category.products = await paginate(query, this.productRepository, {
        maxLimit,
        sortableColumns: ['id'],
        where: { categoryId: category.id },
      });

      return category;
    } catch (error) {
      return error;
    }
  }

  async update(
    id: CategoryEntity['id'],
    updateCategoryDto: UpdateCategoryDto,
  ): Promise<UpdateResult> {
    const category = await this.categoryRepository.findOneBy({ id });

    const toSaveCategory = await this.categoryRepository.create({
      ...category,
      ...updateCategoryDto,
    });

    return this.categoryRepository
      .save(toSaveCategory)
      .then(() => ({ message: isUpdated('category') }))
      .catch((error) => error);
  }

  async remove(id: CategoryEntity['id']): Promise<DeleteResult> {
    const category = await this.categoryRepository.findOne({ where: { id } });

    return this.categoryRepository
      .remove(category)
      .then(() => ({ message: isDeleted('category') }))
      .catch((error) => error);
  }
}
