// Core
import { ArgumentMetadata, Inject } from '@nestjs/common';
import { Injectable, PipeTransform } from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

let imageObject;

@Injectable()
export class UpdateCategoryPipe implements PipeTransform {
  @Inject(REQUEST)
  private request: Request;

  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype, type, data } = metadata;
    const { id: categoryId } = this.request.params || undefined;
    const errorsProvider = new FormatErrorsProvider();

    if (type === 'custom') {
      if (data) {
        return (imageObject = plainToInstance(metatype, value));
      }
    }

    const object = plainToInstance(metatype, {
      image: imageObject && { ...imageObject },
      id: categoryId,
      ...value,
    });

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: 'The given data was invalid',
        });
      }
    }
    return value;
  }
}
