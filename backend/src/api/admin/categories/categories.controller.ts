// Core
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Inject,
  Param,
  ParseFilePipe,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { Paginate, PaginateQuery } from 'nestjs-paginate';
import { InsertResult } from 'typeorm';

// DTO
import {
  CreateCategoryDto,
  UpdateCategoryDto,
} from '@/api/admin/categories/dto';

// Entities
import { CategoryEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import {
  AddCategoryPipe,
  UpdateCategoryPipe,
} from '@/api/admin/categories/pipes';

// Providers
import { NoImageExceptionProvider } from '@/api/providers';

// Services
import { CategoriesService } from '@/api/admin/categories/categories.service';

@Controller()
export class CategoriesController {
  @Inject(CategoriesService)
  private readonly categoriesService: CategoriesService;

  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'categories' }),
  )
  @UsePipes(new AddCategoryPipe())
  @Post()
  async create(
    @Body() createCategoryDto: CreateCategoryDto,
    @UploadedFile(
      new ParseFilePipe({
        fileIsRequired: true,
        errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
        exceptionFactory: () => {
          const noImageProvider = new NoImageExceptionProvider();
          noImageProvider.fileExceptionFactory();
        },
      }),
    )
    file: Express.Multer.File,
  ): Promise<Promise<InsertResult> | Promise<any>> {
    createCategoryDto.image = file.filename;
    return this.categoriesService.create(createCategoryDto);
  }

  @Get()
  async findAll(@Paginate() query: PaginateQuery) {
    return this.categoriesService.findAll(query);
  }

  @Get(':id')
  async findOne(
    @Param('id') id: CategoryEntity['id'],
    @Paginate() query: PaginateQuery,
  ) {
    return this.categoriesService.findOne(+id, query);
  }

  @UsePipes(UpdateCategoryPipe)
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'categories' }),
  )
  @Patch(':id')
  async update(
    @Param('id') id: CategoryEntity['id'],
    @Body() updateCategoryDto: UpdateCategoryDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) updateCategoryDto.image = file.filename;
    return this.categoriesService.update(+id, updateCategoryDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: CategoryEntity['id']) {
    return this.categoriesService.remove(+id);
  }
}
