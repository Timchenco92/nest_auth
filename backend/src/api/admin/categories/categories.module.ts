// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { CategoriesController } from '@/api/admin/categories/categories.controller';

// Entities
import { CategoryEntity, ProductEntity } from '@/database/entities';

// Services
import { CategoriesService } from '@/api/admin/categories/categories.service';

@Module({
  imports: [TypeOrmModule.forFeature([CategoryEntity, ProductEntity])],
  controllers: [CategoriesController],
  providers: [CategoriesService],
})
export class CategoriesModule {}
