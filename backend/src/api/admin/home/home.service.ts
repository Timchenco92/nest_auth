// Core
import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, Repository } from 'typeorm';

// Entities
import { OrderEntity, ProductEntity, UserEntity } from '@/database/entities';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

@Injectable()
export class HomeService {
  @InjectRepository(OrderEntity)
  private readonly orderRepository: Repository<OrderEntity>;
  @InjectRepository(ProductEntity)
  private readonly productRepository: Repository<ProductEntity>;
  @InjectRepository(UserEntity)
  private readonly userRepository: Repository<UserEntity>;

  async getOrdersCount(): Promise<{ ordersCount: number } | void> {
    return this.orderRepository
      .count()
      .then((response) => ({ ordersCount: response }))
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }

  async getOrdersPerMonth(
    query: PaginateQuery,
  ): Promise<Paginated<OrderEntity>> {
    const { limit: maxLimit } = query;

    const currentDate = new Date();
    const startOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      1,
    );
    const endOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 1,
      0,
    );

    return paginate(query, this.orderRepository, {
      maxLimit,
      sortableColumns: ['id'],
      where: { createdAt: Between(startOfMonth, endOfMonth) },
      defaultSortBy: [['createdAt', 'ASC']],
    })
      .then((response) => response)
      .catch((error) => error);
  }

  async getProductsCount(): Promise<{ productsCount: number } | void> {
    return this.productRepository
      .count()
      .then((response) => ({ productsCount: response }))
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }

  async getUsersCount(): Promise<{ usersCount: number } | void> {
    return this.userRepository
      .count()
      .then((response) => ({ usersCount: response }))
      .catch((error) => {
        throw new UnprocessableEntityException(error);
      });
  }

  async getUsersPerMonth(query: PaginateQuery): Promise<Paginated<UserEntity>> {
    const { limit: maxLimit } = query;

    const currentDate = new Date();
    const startOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      1,
    );
    const endOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 1,
      0,
    );

    return paginate(query, this.userRepository, {
      maxLimit,
      sortableColumns: ['id'],
      where: { createdAt: Between(startOfMonth, endOfMonth) },
      defaultSortBy: [['createdAt', 'ASC']],
    })
      .then((response) => response)
      .catch((error) => error);
  }
}
