// Core
import { Controller, Get, Inject } from '@nestjs/common';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

// Entities
import { OrderEntity, UserEntity } from '@/database/entities';

// Services
import { HomeService } from './home.service';

@Controller()
export class HomeController {
  @Inject() private readonly homeService: HomeService;

  @Get('orders-count')
  async getOrdersCount(): Promise<{ ordersCount: number } | void> {
    return this.homeService.getOrdersCount();
  }

  @Get('orders-per-month')
  async getOrdersPerMonth(
    @Paginate() query: PaginateQuery,
  ): Promise<Paginated<OrderEntity>> {
    return this.homeService.getOrdersPerMonth(query);
  }

  @Get('products-count')
  async getProductsCount(): Promise<{ productsCount: number } | void> {
    return this.homeService.getProductsCount();
  }

  @Get('users-count')
  async getUsersCount(): Promise<{ usersCount: number } | void> {
    return this.homeService.getUsersCount();
  }

  @Get('users-per-month')
  async getUsersPerMonth(
    @Paginate() query: PaginateQuery,
  ): Promise<Paginated<UserEntity>> {
    return this.homeService.getUsersPerMonth(query);
  }
}
