// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { HomeController } from './home.controller';

// Entities
import { OrderEntity, ProductEntity, UserEntity } from '@/database/entities';

// Services
import { HomeService } from './home.service';

@Module({
  imports: [TypeOrmModule.forFeature([OrderEntity, UserEntity, ProductEntity])],
  controllers: [HomeController],
  providers: [HomeService],
})
export class HomeModule {}
