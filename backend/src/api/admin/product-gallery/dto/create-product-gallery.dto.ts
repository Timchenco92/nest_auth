// Core
import { IsNotEmpty } from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Decorators
import { IsCustomFile } from '@/api/decorators';

const { isNotEmpty } = ERROR_MESSAGES;

export class CreateProductGalleryDto {
  @IsNotEmpty({ message: () => isNotEmpty('Product') })
  productId: number;

  @IsCustomFile()
  image: string;
}
