export { ProductGalleryController } from './product-gallery.controller';
export { ProductGalleryModule } from './product-gallery.module';
export { ProductGalleryService } from './product-gallery.service';
