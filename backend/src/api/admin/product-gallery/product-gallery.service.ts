// Core
import { Injectable } from '@nestjs/common';
import { DeleteResult, InsertResult, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import { CreateProductGalleryDto } from '@/api/admin/product-gallery/dto';

// Entities
import { ProductEntity, ProductGalleryEntity } from '@/database/entities';

const { isCreated, isDeleted } = RESPONSE_MESSAGES;

@Injectable()
export class ProductGalleryService {
  @InjectRepository(ProductGalleryEntity)
  private readonly productGalleryRepository: Repository<ProductGalleryEntity>;

  async create(
    createProductGalleryDto: CreateProductGalleryDto,
  ): Promise<InsertResult> {
    const productGallery = await this.productGalleryRepository.create({
      ...createProductGalleryDto,
    });

    return this.productGalleryRepository
      .save(productGallery)
      .then(() => ({ message: isCreated('Product Image') }))
      .catch((error) => error);
  }

  async findAll(productId: ProductEntity['id']): Promise<ProductGalleryEntity> {
    return this.productGalleryRepository
      .find({ where: { productId }})
      .then((response) => response)
      .catch((error) => error);
  }

  async remove(id: ProductGalleryEntity['id']): Promise<DeleteResult> {
    return this.productGalleryRepository
      .findOneBy({ id })
      .then((response) => {
        return this.productGalleryRepository
          .remove(response)
          .then(() => ({ message: isDeleted('Product Image') }))
          .catch((error) => error);
      })
      .catch((err) => err);
  }
}
