// Core
import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';
import { InsertResult } from 'typeorm';

// DTO
import { CreateProductGalleryDto } from '@/api/admin/product-gallery/dto';

// Entities
import { ProductEntity, ProductGalleryEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import { AddProductGalleryPipe } from '@/api/admin/product-gallery/pipes';

// Services
import { ProductGalleryService } from './product-gallery.service';

@Controller()
export class ProductGalleryController {
  @Inject(ProductGalleryService)
  private readonly productGalleryService: ProductGalleryService;

  @UsePipes(new AddProductGalleryPipe())
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'productgallery' }),
  )
  @Post()
  async create(
    @Body() createProductGalleryDto: CreateProductGalleryDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<InsertResult> {
    createProductGalleryDto.image = file?.filename;
    return this.productGalleryService.create(createProductGalleryDto);
  }

  @Get(':id')
  async findAll(
    @Param('id') id: ProductEntity['id'],
  ): Promise<ProductGalleryEntity> {
    return this.productGalleryService.findAll(id);
  }

  @Delete(':id')
  async remove(@Param('id') id: ProductGalleryEntity['id']) {
    return this.productGalleryService.remove(+id);
  }
}
