// Core
import { Module } from '@nestjs/common';

// Controllers
import { ProductGalleryController } from './product-gallery.controller';

// Entities
import { ProductEntity, ProductGalleryEntity } from '@/database/entities';

// Services
import { ProductGalleryService } from './product-gallery.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ProductGalleryEntity, ProductEntity])],
  controllers: [ProductGalleryController],
  providers: [ProductGalleryService],
})
export class ProductGalleryModule {}
