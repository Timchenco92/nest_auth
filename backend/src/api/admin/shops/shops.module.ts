// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { ShopsController } from './shops.controller';

// Entities
import { ShopEntity } from '@/database/entities';

// Services
import { ShopsService } from './shops.service';

@Module({
  imports: [TypeOrmModule.forFeature([ShopEntity])],
  controllers: [ShopsController],
  providers: [ShopsService],
})
export class ShopsModule {}
