// Core
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository, UpdateResult } from 'typeorm';
import { paginate, Paginated, PaginateQuery } from 'nestjs-paginate';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import { CreateShopDto, UpdateShopDto } from '@/api/admin/shops/dto';

// Entities
import { ShopEntity, UserEntity } from '@/database/entities';

const { isCreated, isDeleted, isUpdated, notFount } = RESPONSE_MESSAGES;

@Injectable()
export class ShopsService {
  @InjectRepository(ShopEntity)
  private readonly shopRepository: Repository<ShopEntity>;

  async create(createShopDto: CreateShopDto): Promise<InsertResult> {
    const shop = await this.shopRepository.create({ ...createShopDto });
    return this.shopRepository
      .save(shop)
      .then(() => ({ message: isCreated('Shop') }))
      .catch((error) => error);
  }

  async findAll(query: PaginateQuery): Promise<Paginated<UserEntity>> {
    const { limit: maxLimit } = query;
    return paginate(query, this.shopRepository, {
      maxLimit,
      sortableColumns: ['id'],
    })
      .then((response) => response)
      .catch((error) => error);
  }

  async findOne(id: ShopEntity['id']): Promise<ShopEntity | NotFoundException> {
    return this.shopRepository
      .findOne({ where: { id } })
      .then((response) => response)
      .catch(() => {
        throw new NotFoundException({ message: notFount('Shop') });
      });
  }

  async update(
    id: ShopEntity['id'],
    updateShopDto: UpdateShopDto,
  ): Promise<UpdateResult> {
    console.log(updateShopDto);
    return this.shopRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        const toSaveShop = await this.shopRepository.create({
          ...response,
          ...updateShopDto,
        });

        return this.shopRepository
          .save(toSaveShop)
          .then(() => ({ message: isUpdated('Shop') }))
          .catch((error) => error);
      })
      .catch((err) => err);
  }

  async remove(id: ShopEntity['id']): Promise<DeleteResult> {
    return this.shopRepository
      .findOneByOrFail({ id })
      .then(async (response) => {
        return this.shopRepository
          .remove(response)
          .then(() => ({ message: isDeleted('Shop') }))
          .catch((error) => error);
      })
      .catch((err) => err);
  }
}
