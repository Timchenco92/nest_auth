// Core
import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { EntityNotFoundError, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// Entities
import { ShopEntity } from '@/database/entities';

// Providers
import { FormatErrorsProvider } from '@/api/providers';

let imageObject;

const { errorValidation } = RESPONSE_MESSAGES;

@Injectable()
export class UpdateShopPipe implements PipeTransform {
  @Inject(REQUEST)
  private request: Request;
  @InjectRepository(ShopEntity)
  private readonly shopRepository: Repository<ShopEntity>;

  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype, type, data } = metadata;
    const { id: shopId } = this.request.params || undefined;

    const isExist = await this.shopRepository.exist({ where: { id: +shopId } });
    if (!isExist) {
      throw new HttpException(
        new EntityNotFoundError(ShopEntity, shopId),
        HttpStatus.NOT_FOUND,
      );
    }

    const errorsProvider = new FormatErrorsProvider();

    if (type === 'custom') {
      if (data) {
        return (imageObject = plainToInstance(metatype, value));
      }
    }

    const object = plainToInstance(metatype, {
      image: imageObject && { ...imageObject },
      id: shopId,
      ...value,
    });

    if (object) {
      const errors = await validate(object);

      if (errors.length > 0) {
        return errorsProvider.formatErrors({
          errors,
          message: errorValidation,
        });
      }
    }

    return value;
  }
}
