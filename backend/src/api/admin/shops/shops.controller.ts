// Core
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Inject,
  UseInterceptors,
  UploadedFile,
  UsePipes,
} from '@nestjs/common';
import { Paginate, PaginateQuery } from 'nestjs-paginate';

// DTO
import { CreateShopDto, UpdateShopDto } from '@/api/admin/shops/dto';

// Entities
import { ShopEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import { AddShopPipe, UpdateShopPipe } from '@/api/admin/shops/pipes';

// Services
import { ShopsService } from './shops.service';

@Controller()
export class ShopsController {
  @Inject(ShopsService)
  private readonly shopsService: ShopsService;

  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'shops' }),
  )
  @UsePipes(new AddShopPipe())
  @Post()
  create(
    @Body() createShopDto: CreateShopDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    createShopDto.image = file?.filename;
    return this.shopsService.create(createShopDto);
  }

  @Get()
  findAll(@Paginate() query: PaginateQuery) {
    return this.shopsService.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: ShopEntity['id']) {
    return this.shopsService.findOne(+id);
  }

  @UsePipes(UpdateShopPipe)
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'shops' }),
  )
  @Patch(':id')
  update(
    @Param('id') id: ShopEntity['id'],
    @Body() updateShopDto: UpdateShopDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) updateShopDto.image = file.filename;
    return this.shopsService.update(+id, updateShopDto);
  }

  @Delete(':id')
  remove(@Param('id') id: ShopEntity['id']) {
    return this.shopsService.remove(+id);
  }
}
