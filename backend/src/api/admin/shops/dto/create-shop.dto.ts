// Core
import { ValidateIf, IsNotEmpty, IsPhoneNumber } from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Decorators
import { IsCustomFile, IsUnique } from '@/api/decorators';

const { isNotEmpty, isPhoneNumber } = ERROR_MESSAGES;

export class CreateShopDto {
  @IsUnique('shops', 'title', 'Title')
  title: string;

  @IsCustomFile()
  image: string;

  @IsPhoneNumber('UA', { message: () => isPhoneNumber })
  @IsUnique('shops', 'firstPhone', 'Phone')
  firstPhone: string;

  @ValidateIf(
    (object, value) => object.hasOwnProperty('secondPhone') && value !== '',
  )
  @IsPhoneNumber('UA', { message: () => isPhoneNumber })
  @IsUnique('shops', 'secondPhone', 'Phone')
  secondPhone: string;

  @ValidateIf(
    (object, value) => object.hasOwnProperty('thirdPhone') && value !== '',
  )
  @IsPhoneNumber('UA', { message: () => isPhoneNumber })
  @IsUnique('shops', 'thirdPhone', 'Phone')
  thirdPhone: string;

  @IsNotEmpty({ message: () => isNotEmpty('Start Time Work') })
  startTimeWork: string;

  @IsNotEmpty({ message: () => isNotEmpty('End Time Work') })
  endTimeWork: string;
}
