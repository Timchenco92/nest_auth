// Core
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

// Middlewares
import { AuthMiddleware } from '@/api/middlewares';

// Modules
import { AdminModule as AdminAuthModule } from '@/api/admin/admin';
import { AdminRouterModule } from '@/api/admin/routes';
import { AuthModule } from '@/api/admin/auth';
import { BannersModule } from '@/api/admin/banners';
import { CategoriesModule } from '@/api/admin/categories';
import { OrderProductsModule } from '@/api/admin/order-products';
import { OrdersModule } from '@/api/admin/orders';
import { ProductGalleryModule } from '@/api/admin/product-gallery';
import { ProductsModule } from '@/api/admin/products';
import { ShopGalleryModule } from '@/api/admin/shop-gallery';
import { ShopsModule } from '@/api/admin/shops';
import { UsersModule } from '@/api/admin/users';

// Providers
import { SlugifyProvider } from '@/api/providers';
import { SettingsModule } from '@/api/admin/settings';
import { HomeModule } from '@/api/admin/home';

@Module({
  imports: [
    AdminAuthModule,
    AdminRouterModule,
    AuthModule,
    BannersModule,
    CategoriesModule,
    ProductGalleryModule,
    ProductsModule,
    UsersModule,
    OrdersModule,
    OrderProductsModule,
    ShopsModule,
    ShopGalleryModule,
    SettingsModule,
    HomeModule,
  ],
  providers: [SlugifyProvider],
})
export class AdminModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AuthMiddleware).forRoutes('*');
  }
}
