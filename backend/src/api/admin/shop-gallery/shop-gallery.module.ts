// Core
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

// Controllers
import { ShopGalleryController } from './shop-gallery.controller';

// Entities
import { ShopEntity, ShopGalleryEntity } from '@/database/entities';

// Services
import { ShopGalleryService } from './shop-gallery.service';

@Module({
  imports: [TypeOrmModule.forFeature([ShopEntity, ShopGalleryEntity])],
  controllers: [ShopGalleryController],
  providers: [ShopGalleryService],
})
export class ShopGalleryModule {}
