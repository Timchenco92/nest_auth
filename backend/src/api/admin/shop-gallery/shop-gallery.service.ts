// Core
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, InsertResult, Repository } from 'typeorm';

// Constants
import { RESPONSE_MESSAGES } from '@/constants';

// DTO
import { CreateShopGalleryDto } from '@/api/admin/shop-gallery/dto';

// Entities
import { ShopEntity, ShopGalleryEntity } from '@/database/entities';

const { isCreated, isDeleted, notFount } = RESPONSE_MESSAGES;

@Injectable()
export class ShopGalleryService {
  @InjectRepository(ShopGalleryEntity)
  private readonly shopGalleryRepository: Repository<ShopGalleryEntity>;
  @InjectRepository(ShopEntity)
  private readonly shopRepository: Repository<ShopEntity>;

  async create(
    createShopGalleryDto: CreateShopGalleryDto,
  ): Promise<InsertResult> {
    return this.shopRepository
      .findOneByOrFail({ id: createShopGalleryDto.shopId })
      .then(async () => {
        const shopGallery = await this.shopGalleryRepository.create({
          ...createShopGalleryDto,
        });
        return this.shopGalleryRepository
          .save(shopGallery)
          .then(() => ({ message: isCreated('Shop Gallery') }))
          .catch((error) => error);
      })
      .catch(() => {
        throw new NotFoundException({ message: notFount('Shop') });
      });
  }

  async findAll(shopId: ShopEntity['id']): Promise<ShopGalleryEntity> {
    return this.shopGalleryRepository
      .find({ where: { shopId } })
      .then((response) => response)
      .catch((error) => error);
  }

  async remove(id: ShopGalleryEntity['id']): Promise<DeleteResult> {
    return this.shopGalleryRepository
      .findOneByOrFail({ id })
      .then((response) => {
        return this.shopGalleryRepository
          .remove(response)
          .then(() => ({ message: isDeleted('Shop Gallery') }))
          .catch((error) => error);
      })
      .catch((error) => error);
  }
}
