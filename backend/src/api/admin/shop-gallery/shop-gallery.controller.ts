// Core
import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
  UsePipes,
} from '@nestjs/common';

// DTO
import { CreateShopGalleryDto } from '@/api/admin/shop-gallery/dto';

// Entities
import { ShopEntity, ShopGalleryEntity } from '@/database/entities';

// Interceptors
import { UploadFileInterceptor } from '@/api/interceptors';

// Pipes
import { AddShopGalleryPipe } from '@/api/admin/shop-gallery/pipes';

// Services
import { ShopGalleryService } from './shop-gallery.service';

@Controller()
export class ShopGalleryController {
  @Inject(ShopGalleryService)
  private readonly shopGalleryService: ShopGalleryService;

  @UsePipes(new AddShopGalleryPipe())
  @UseInterceptors(
    UploadFileInterceptor({ fieldName: 'image', tableName: 'shopgallery' }),
  )
  @Post()
  async create(
    @Body() createShopGalleryDto: CreateShopGalleryDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    createShopGalleryDto.image = file?.filename;
    return this.shopGalleryService.create(createShopGalleryDto);
  }

  @Get(':id')
  async findAll(@Param('id') id: ShopEntity['id']): Promise<ShopGalleryEntity> {
    return this.shopGalleryService.findAll(+id);
  }

  @Delete(':id')
  async remove(@Param('id') id: ShopGalleryEntity['id']) {
    return this.shopGalleryService.remove(+id);
  }
}
