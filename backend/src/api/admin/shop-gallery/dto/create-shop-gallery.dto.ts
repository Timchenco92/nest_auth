// Core
import { IsCustomFile, IsParentExist } from '@/api/decorators';

export class CreateShopGalleryDto {
  @IsParentExist('shops', 'id')
  shopId: number;

  @IsCustomFile()
  image: string;
}
