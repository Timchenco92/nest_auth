// Core
import {
  HttpException,
  HttpStatus,
  Injectable,
  ValidationError,
} from '@nestjs/common';

interface IFormatErrorsProvider {
  errors: ValidationError[];
  message: string;
}

@Injectable()
export class FormatErrorsProvider {
  public formatErrors({
    errors,
    message,
  }: IFormatErrorsProvider): HttpException {
    const displayErrors = errors.reduce(
      (acc, val) => ({
        ...acc,
        [val.property]: Object.values(val.constraints)[0],
      }),
      {},
    );
    throw new HttpException(
      {
        message,
        errors: displayErrors,
      },
      HttpStatus.UNPROCESSABLE_ENTITY,
    );
  }
}
