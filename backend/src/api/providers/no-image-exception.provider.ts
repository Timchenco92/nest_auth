// Core
import { Injectable, UnprocessableEntityException } from '@nestjs/common';

// Constants
import { ERROR_MESSAGES } from '@/constants';

@Injectable()
export class NoImageExceptionProvider {
  fileExceptionFactory() {
    const { isRequired } = ERROR_MESSAGES;
    throw new UnprocessableEntityException({
      message: 'The given data was invalid',
      errors: {
        image: isRequired('Image'),
      },
    });
  }
}
