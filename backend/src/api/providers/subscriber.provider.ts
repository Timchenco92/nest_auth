// Core
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { join } from 'path';

// Providers
import { SlugifyProvider } from '@/api/providers';

@Injectable()
export class SubscriberProvider {
  async getImageUrl({ tableName, image }) {
    const config = new ConfigService();
    const baseUrl = config.get('BASE_STATIC_ASSETS_URL');
    return `${baseUrl}/${tableName}/${image}`;
  }

  async getSlug({ id, title }) {
    const slug = new SlugifyProvider();
    return slug.generateSlug([String(id), title]);
  }

  getImagePathForRemove({ tableName, image }) {
    const assetsPath = join(
      __dirname,
      '../../../..',
      'backend',
      'public',
      'uploads',
      tableName,
    );
    return `${assetsPath}/${image}`;
  }
}
