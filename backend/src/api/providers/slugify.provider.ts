// Core
import { Injectable } from '@nestjs/common';
import slugify from 'slugify';

export interface IOptions {
  lower?: boolean;
  remove?: RegExp;
  replacement?: string;
  trim?: boolean;
}

@Injectable()
export class SlugifyProvider {
  public slugOptions: IOptions = {
    lower: true,
    remove: /[^a-zA-Z]/g,
    replacement: '-',
    trim: true,
  };

  public async generateSlug(value: string[], options?: IOptions) {
    this.slugOptions = options;
    const formattedValue = value.join(' ').toString().toLowerCase();
    return slugify(formattedValue, { ...options });
  }
}
