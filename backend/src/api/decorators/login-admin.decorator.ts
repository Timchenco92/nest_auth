// Core
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

// Entities
import { AdminEntity } from '@/database/entities';

export const LoginAdminDecorator = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): AdminEntity => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);
