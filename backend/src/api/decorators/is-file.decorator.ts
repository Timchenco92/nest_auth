// Core
import {
  IS_EMPTY,
  IS_MIME_TYPE,
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

const { isNotEmpty, isNotValidMimetype } = ERROR_MESSAGES;

@ValidatorConstraint({ name: 'customFileValidator', async: false })
export class CustomFileValidator implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    if (!value) {
      args.constraints[0] = IS_EMPTY;
      return false;
    }

    const fileTypes = ['image/jpg', 'image/png', 'image/jpeg'];
    if (!fileTypes.includes(value.mimetype)) {
      args.constraints[0] = IS_MIME_TYPE;
      return false;
    }

    return true;
  }

  defaultMessage(args: ValidationArguments) {
    const constraint = args.constraints[0];
    if (constraint === IS_EMPTY) {
      return isNotEmpty('Image');
    } else if (constraint === IS_MIME_TYPE) {
      return isNotValidMimetype('Image', [
        'image/jpg',
        'image/png',
        'image/jpeg',
      ]);
    }

    return 'Validation Error';
  }
}

export function IsCustomFile(validationOptions?: ValidationOptions) {
  return function (object: NonNullable<unknown>, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: CustomFileValidator,
    });
  };
}
