// Core
import { Injectable, SetMetadata } from '@nestjs/common';
import {
  IS_EMPTY,
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

// Constants
import { ERROR_MESSAGES } from '@/constants';

// Typeorm
import { connectionSource } from '@/typeorm';

const { isNotEmpty, isNotExist } = ERROR_MESSAGES;

@ValidatorConstraint({ name: 'isParentExist', async: true })
@Injectable()
export class IsParentExistValidator implements ValidatorConstraintInterface {
  async validate(value: any, args?: ValidationArguments): Promise<boolean> {
    SetMetadata('is-parent-exist', args);
    const [entityName, fieldName] = args.constraints;

    if (!value) {
      args.constraints[0] = IS_EMPTY;
      return false;
    }

    const manager = connectionSource;
    if (!manager.isInitialized) await connectionSource.initialize();
    const repository = await manager.getRepository(entityName);

    return await repository.exist({ where: { [fieldName]: value } });
  }
  defaultMessage(args?: ValidationArguments): string {
    const constraint = args.constraints[0];
    const [value, entityName, fieldName] = args.constraints;
    if (constraint === IS_EMPTY) {
      return isNotEmpty(value);
    }

    return isNotExist(fieldName, entityName);
  }
}

export function IsParentExist(
  entityName: string,
  fieldName: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: NonNullable<unknown>, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [entityName, fieldName],
      validator: IsParentExistValidator,
    });
  };
}
