// Core
import { MailerOptions, MailerOptionsFactory } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Inject, Injectable } from '@nestjs/common';
import { join } from 'path';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MailConfig implements MailerOptionsFactory {
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  public createMailerOptions(): Promise<MailerOptions> | MailerOptions {
    return {
      transport: {
        from: {
          name: this.configService.get<string>('MAIL_FROM_NAME'),
          address: this.configService.get<string>('MAIL_FROM_ADDRESS'),
        },
        host: this.configService.get<string>('MAIL_HOST'),
        port: this.configService.get<number>('MAIL_PORT'),
        auth: {
          user: this.configService.get<string>('MAIL_USER'),
          pass: this.configService.get<string>('MAIL_PASSWORD'),
        },
      },
      template: {
        dir: join(__dirname, '../', 'mail', 'templates'),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    };
  }
}
