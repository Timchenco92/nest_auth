// Core
import { Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { MailerService } from '@nestjs-modules/mailer';

// Constants
import { ADMIN_EVENT_TYPES } from '@/constants';

// Events
import { AdminCreatedUserEvent } from '@/events';

// Notifications
import { AdminCreatedUserNotification } from '@/notifications';

const { adminUserCreated } = ADMIN_EVENT_TYPES;

@Injectable()
export class AdminCreatedUserListener {
  @Inject(MailerService)
  private readonly mailerService: MailerService;
  @Inject(AdminCreatedUserNotification)
  private readonly adminCreatedUserNotification: AdminCreatedUserNotification;

  @OnEvent(adminUserCreated, { async: true })
  async handleAdminCreatedUserListener(payload: AdminCreatedUserEvent) {
    return this.adminCreatedUserNotification.handleClientRegisterUserNotification(
      payload,
    );
  }
}
