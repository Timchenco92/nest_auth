// Core
import { Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { MailerService } from '@nestjs-modules/mailer';

// Constants
import { CLIENT_EVENT_TYPES } from '@/constants';

// Events
import { ClientRegisterUserEvent } from '@/events';

// Notifications
import { ClientRegisterUserNotification } from '@/notifications';

const { clientRegisterUser } = CLIENT_EVENT_TYPES;

@Injectable()
export class ClientRegisterUserListener {
  @Inject(MailerService)
  private readonly mailerService: MailerService;
  @Inject(ClientRegisterUserNotification)
  private readonly clientRegisterUserNotification: ClientRegisterUserNotification;

  @OnEvent(clientRegisterUser, { async: true })
  async handleClientRegisterUserListener(payload: ClientRegisterUserEvent) {
    return this.clientRegisterUserNotification.handleClientRegisterUserNotification(
      payload,
    );
  }
}
