// Core
import { Inject, Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';

// Constants
import { ADMIN_EVENT_TYPES } from '@/constants';

// Events
import { AdminOrderCreateEvent } from '@/events';

// Notifications
import { AdminOrderCreateNotification } from '@/notifications';

const { adminOrderCreated } = ADMIN_EVENT_TYPES;

@Injectable()
export class AdminOrderCreateListener {
  @Inject(AdminOrderCreateNotification)
  private readonly adminOrderCreateNotification: AdminOrderCreateNotification;

  @OnEvent(adminOrderCreated, { async: true })
  async handleAdminCreatedOrderListener({ payload }: AdminOrderCreateEvent) {
    return this.adminOrderCreateNotification.handleAdminCreateOrderNotification(
      {
        payload,
      },
    );
  }
}
