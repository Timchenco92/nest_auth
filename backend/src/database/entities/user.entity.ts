// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Paginated } from 'nestjs-paginate';

// Entities
import { OrderEntity, ProductCommentEntity } from '@/database/entities';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({
    default: null,
    length: '255',
    name: 'avatar',
    nullable: true,
    type: 'varchar',
  })
  avatar: string;

  @Column({
    default: null,
    length: '255',
    name: 'avatarurl',
    nullable: true,
    type: 'varchar',
  })
  avatarUrl: string;

  @Column({
    length: '255',
    name: 'firstname',
    nullable: false,
    type: 'varchar',
  })
  firstName: string;

  @Column({
    length: '255',
    name: 'middlename',
    nullable: false,
    type: 'varchar',
  })
  middleName: string;

  @Column({
    length: '255',
    name: 'lastname',
    nullable: false,
    type: 'varchar',
  })
  lastName: string;

  @Column({
    length: '255',
    name: 'email',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  email: string;

  @Column({
    length: '255',
    name: 'phone',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  phone: string;

  @Column({
    default: null,
    length: '255',
    name: 'address',
    nullable: true,
    type: 'varchar',
  })
  address: string;

  @Column({
    length: '255',
    name: 'password',
    nullable: false,
    select: false,
    type: 'varchar',
  })
  password: string;

  fullName: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp' })
  updatedAt: Date;

  @OneToMany(() => OrderEntity, (order) => order.user, {
    nullable: false,
    lazy: true,
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  orders: OrderEntity[] | Paginated<OrderEntity>;

  @OneToMany(() => ProductCommentEntity, (comment) => comment.user)
  comments: ProductCommentEntity[];
}
