// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import { OrderProductEntity, UserEntity } from '@/database/entities';

export enum EOrderStatus {
  accepted = 'accepted',
  inProgress = 'in_progress',
  finished = 'finished',
}

@Entity('orders')
export class OrderEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'userid', nullable: false, type: 'bigint' })
  userId: number;

  @Column({
    name: 'ordernumber',
    nullable: false,
    unique: true,
    type: 'bigint',
  })
  orderNumber: number;

  @Column({
    length: '50',
    name: 'total',
    nullable: false,
    type: 'varchar',
  })
  total: string;

  @Column({ enum: EOrderStatus, name: 'status', nullable: false, type: 'enum' })
  status: EOrderStatus;

  @Column({
    default: null,
    length: '255',
    name: 'delivery',
    nullable: true,
    type: 'varchar',
  })
  delivery: string;

  @Column({
    default: null,
    length: '255',
    name: 'comment',
    nullable: true,
    type: 'varchar',
  })
  comment: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @ManyToOne(() => UserEntity, (user) => user.orders)
  @JoinColumn({ name: 'userid' })
  user: UserEntity;

  @OneToMany(() => OrderProductEntity, (product) => product.order, {
    nullable: false,
    lazy: true,
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  orderProducts: OrderProductEntity[];
}
