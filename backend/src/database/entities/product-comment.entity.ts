// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import { ProductEntity, UserEntity } from '@/database/entities';

@Entity('productcomments')
export class ProductCommentEntity {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint', unsigned: true })
  id: number;

  @Column({ name: 'productid', nullable: false, type: 'bigint' })
  productId: number;

  @Column({ name: 'userid', nullable: false, type: 'bigint' })
  userId: number;

  @Column({ name: 'title', type: 'varchar', nullable: false })
  title: string;

  @Column({ name: 'description', type: 'text', nullable: false })
  description: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @ManyToOne(() => ProductEntity, (product) => product.comments)
  @JoinColumn({ name: 'productid' })
  product: ProductEntity;

  @ManyToOne(() => UserEntity, (user) => user.comments)
  @JoinColumn({ name: 'userid' })
  user: UserEntity;
}
