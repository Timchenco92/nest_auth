// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Paginated } from 'nestjs-paginate';

// Entities
import { ProductEntity } from '@/database/entities';

@Entity('categories')
export class CategoryEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({
    length: '255',
    name: 'title',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  title: string;

  @Column({
    default: null,
    length: '255',
    name: 'slug',
    nullable: true,
    select: false,
    type: 'varchar',
    unique: true,
  })
  slug: string;

  @Column({
    length: '255',
    name: 'image',
    nullable: false,
    type: 'varchar',
  })
  image: string;

  @Column({
    default: null,
    length: '255',
    name: 'imageurl',
    nullable: true,
    type: 'varchar',
  })
  imageUrl: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @OneToMany(() => ProductEntity, (product) => product.category)
  products: ProductEntity[] | Paginated<ProductEntity>;
}
