// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import { OrderEntity, ProductEntity } from '@/database/entities';

@Entity('orderproducts')
export class OrderProductEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({
    name: 'orderid',
    nullable: false,
    type: 'bigint',
  })
  orderId: number;

  @Column({
    name: 'productid',
    nullable: false,
    type: 'bigint',
  })
  productId: number;

  @Column({
    name: 'price',
    nullable: false,
    type: 'varchar',
  })
  price: string;

  @Column({ name: 'total', nullable: false, type: 'varchar' })
  total: string;

  @Column({
    name: 'quantity',
    nullable: false,
    type: 'int',
  })
  quantity: number;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @OneToOne(() => ProductEntity, (product) => product.orderProduct)
  @JoinColumn({ name: 'productid' })
  product: ProductEntity;

  @ManyToOne(() => OrderEntity, (order) => order.orderProducts)
  @JoinColumn({ name: 'orderid' })
  order: OrderEntity;
}
