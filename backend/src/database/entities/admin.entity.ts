// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('admins')
export class AdminEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({
    length: '255',
    name: 'firstname',
    nullable: false,
    type: 'varchar',
  })
  firstName: string;

  @Column({
    length: '255',
    name: 'middlename',
    nullable: false,
    type: 'varchar',
  })
  middleName: string;

  @Column({
    length: '255',
    name: 'lastname',
    nullable: false,
    type: 'varchar',
  })
  lastName: string;

  @Column({
    length: '255',
    name: 'email',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  email: string;

  @Column({
    length: '255',
    name: 'phone',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  phone: string;

  @Column({
    default: null,
    length: '255',
    name: 'avatar',
    nullable: true,
    select: false,
    type: 'varchar',
  })
  avatar: string;

  @Column({
    default: null,
    length: '255',
    name: 'avatarurl',
    nullable: true,
    type: 'varchar',
  })
  avatarUrl: string;

  @Column({
    length: '255',
    name: 'password',
    nullable: false,
    select: false,
    type: 'varchar',
  })
  password: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;
}
