// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import { ShopGalleryEntity } from '@/database/entities';

@Entity('shops')
export class ShopEntity {
  @PrimaryGeneratedColumn({ name: 'id', type: 'bigint' })
  id: number;

  @Column({
    length: '255',
    name: 'title',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  title: string;

  @Column({
    default: null,
    length: '255',
    name: 'slug',
    nullable: true,
    type: 'varchar',
    unique: true,
  })
  slug: string;

  @Column({
    length: '255',
    name: 'address',
    nullable: false,
    select: true,
    type: 'varchar',
  })
  address: string;

  @Column({
    length: '255',
    name: 'image',
    nullable: false,
    select: true,
    type: 'varchar',
  })
  image: string;

  @Column({
    default: null,
    length: '255',
    name: 'imageurl',
    nullable: true,
    type: 'varchar',
  })
  imageUrl: string;

  @Column({
    length: '255',
    name: 'firstphone',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  firstPhone: string;

  @Column({
    default: null,
    length: '255',
    name: 'secondphone',
    nullable: true,
    type: 'varchar',
    unique: true,
  })
  secondPhone: string;

  @Column({
    default: null,
    length: '255',
    name: 'thirdphone',
    nullable: true,
    type: 'varchar',
    unique: true,
  })
  thirdPhone: string;

  @Column({ name: 'starttimework', nullable: false, type: 'time' })
  startTimeWork: string;

  @Column({ name: 'endtimework', nullable: false, type: 'time' })
  endTimeWork: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @OneToMany(() => ShopGalleryEntity, (galleries) => galleries.shop)
  galleries: ShopGalleryEntity[];
}
