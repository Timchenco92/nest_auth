// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import { ProductEntity } from '@/database/entities';

@Entity('productgallery')
export class ProductGalleryEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'productid', nullable: false, type: 'bigint' })
  productId: number;

  @Column({
    length: '255',
    name: 'image',
    nullable: false,
    type: 'varchar',
  })
  image: string;

  @Column({
    default: null,
    name: 'imageurl',
    nullable: false,
    length: '255',
    type: 'varchar',
  })
  imageUrl: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @ManyToOne(() => ProductEntity, (product) => product.galleries)
  @JoinColumn({ name: 'productid' })
  product: ProductEntity;
}
