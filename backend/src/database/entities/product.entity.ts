// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import {
  CategoryEntity,
  OrderProductEntity, ProductCommentEntity,
  ProductGalleryEntity,
} from '@/database/entities';

@Entity('products')
export class ProductEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'categoryid', nullable: false, type: 'bigint',  })
  categoryId: number;

  @Column({
    length: '255',
    name: 'title',
    nullable: false,
    type: 'varchar',
    unique: true,
  })
  title: string;

  @Column({
    length: '255',
    name: 'price',
    nullable: false,
    type: 'varchar',
  })
  price: string;

  @Column({
    default: null,
    name: 'slug',
    nullable: true,
    length: '255',
    type: 'varchar',
    unique: true,
  })
  slug: string;

  @Column({
    length: '255',
    name: 'image',
    nullable: true,
    type: 'varchar',
  })
  image: string;

  @Column({
    default: null,
    name: 'imageurl',
    nullable: true,
    length: '255',
    type: 'varchar',
  })
  imageUrl: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @ManyToOne(() => CategoryEntity, (category) => category.products)
  @JoinColumn({ name: 'categoryid' })
  category: CategoryEntity;

  @OneToMany(() => ProductGalleryEntity, (galleries) => galleries.product)
  galleries: ProductGalleryEntity[];

  @OneToOne(() => OrderProductEntity, (orderProduct) => orderProduct.product)
  orderProduct: OrderProductEntity;

  @OneToMany(() => ProductCommentEntity, (comments) => comments.product)
  comments: ProductCommentEntity[];
}
