// Core
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

// Entities
import { ShopEntity } from '@/database/entities';

@Entity('shopgallery')
export class ShopGalleryEntity {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ name: 'shopid', nullable: false, type: 'bigint' })
  shopId: number;

  @Column({
    length: '255',
    name: 'image',
    nullable: false,
    type: 'varchar',
  })
  image: string;

  @Column({
    default: null,
    name: 'imageurl',
    nullable: false,
    length: '255',
    type: 'varchar',
  })
  imageUrl: string;

  @CreateDateColumn({ name: 'createdat', type: 'timestamp with time zone' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'updatedat', type: 'timestamp with time zone' })
  updatedAt: Date;

  @ManyToOne(() => ShopEntity, (shop) => shop.galleries)
  @JoinColumn({ name: 'shopid' })
  shop: ShopEntity;
}
