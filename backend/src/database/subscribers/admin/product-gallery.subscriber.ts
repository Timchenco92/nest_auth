// Core
import {
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  RemoveEvent,
} from 'typeorm';
import { unlink } from 'fs';

// Entities
import { ProductGalleryEntity } from '@/database/entities';

// Providers
import { SubscriberProvider } from '@/api/providers';

@EventSubscriber()
export class ProductGallerySubscriber
  implements EntitySubscriberInterface<ProductGalleryEntity>
{
  subscriber: any;
  constructor() {
    this.subscriber = new SubscriberProvider();
  }

  listenTo(): CallableFunction | string {
    return ProductGalleryEntity;
  }

  async afterInsert(event: InsertEvent<ProductGalleryEntity>): Promise<any> {
    const { id, image } = event.entity;
    const { tableName } = event.metadata;

    const imageUrl = await this.subscriber.getImageUrl({ tableName, image });

    await event.manager.update(ProductGalleryEntity, { id }, { imageUrl });
  }

  async beforeRemove(event: RemoveEvent<ProductGalleryEntity>): Promise<any> {
    const { image } = event.entity;
    const { tableName } = event.metadata;

    const imagePath = this.subscriber.getImagePathForRemove({
      tableName,
      image,
    });
    await unlink(imagePath, () => ({}));
  }
}
