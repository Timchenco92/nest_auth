// Core
import {
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  RemoveEvent,
} from 'typeorm';
import { unlink } from 'fs';

// Entities
import { ShopGalleryEntity } from '@/database/entities';

// Providers
import { SubscriberProvider } from '@/api/providers';

@EventSubscriber()
export class ShopGallerySubscriber
  implements EntitySubscriberInterface<ShopGalleryEntity>
{
  subscriber: any;
  constructor() {
    this.subscriber = new SubscriberProvider();
  }
  listenTo(): CallableFunction | string {
    return ShopGalleryEntity;
  }

  async afterInsert(event: InsertEvent<ShopGalleryEntity>): Promise<void> {
    const { id, image } = event.entity;
    const { tableName } = event.metadata;

    const imageUrl = await this.subscriber.getImageUrl({ tableName, image });

    await event.manager.update(ShopGalleryEntity, { id }, { imageUrl });
  }

  async beforeRemove(event: RemoveEvent<ShopGalleryEntity>): Promise<void> {
    const { image } = event.entity;
    const { tableName } = event.metadata;

    const imagePath = this.subscriber.getImagePathForRemove({
      tableName,
      image,
    });
    await unlink(imagePath, () => ({}));
  }
}
