// Core
import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  RemoveEvent,
  UpdateEvent, TransactionCommitEvent,
} from 'typeorm';
import { unlink } from 'fs';

// Entities
import { OrderEntity } from '@/database/entities';

@EventSubscriber()
export class OrderSubscriber implements EntitySubscriberInterface<OrderEntity> {
  listenTo(): CallableFunction | string {
    return OrderEntity;
  }

  afterInsert(event: InsertEvent<OrderEntity>): Promise<any> | void {
    // console.log('after insert', event);
  }

  afterTransactionCommit(event: TransactionCommitEvent): Promise<any> | void {
    // console.log('afterTransactionCommit', event);
  }
}
