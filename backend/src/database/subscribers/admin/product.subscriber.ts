// Core
import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  RemoveEvent,
  UpdateEvent,
} from 'typeorm';
import { unlink } from 'fs';

// Entities
import { ProductEntity } from '@/database/entities';

// Providers
import { SubscriberProvider } from '@/api/providers';

@EventSubscriber()
export class ProductSubscriber
  implements EntitySubscriberInterface<ProductEntity>
{
  subscriber: any;
  constructor() {
    this.subscriber = new SubscriberProvider();
  }

  listenTo() {
    return ProductEntity;
  }

  async afterInsert(
    event: InsertEvent<ProductEntity>,
  ): Promise<Promise<any> | void> {
    const { id, image, title } = event.entity;
    const { tableName } = event.metadata;

    const slug = await this.subscriber.getSlug({ id, title });
    const imageUrl = await this.subscriber.getImageUrl({ tableName, image });

    await event.manager.update(ProductEntity, { id }, { imageUrl, slug });
  }

  async beforeRemove(
    event: RemoveEvent<ProductEntity>,
  ): Promise<Promise<any> | void> {
    const { image } = event.entity;
    const { tableName } = event.metadata;

    const imagePath = this.subscriber.getImagePathForRemove({
      tableName,
      image,
    });

    await unlink(imagePath, () => ({}));
  }

  async afterUpdate(
    event: UpdateEvent<ProductEntity>,
  ): Promise<Promise<any> | void> {
    const { metadata, entity, databaseEntity, manager } = event;
    const { tableName } = metadata;

    const { title: updatedTitle, image: updatedImage, id } = entity;

    if (databaseEntity?.title !== updatedTitle) {
      const slug = await this.subscriber.getSlug({ id, title: updatedTitle });
      await manager.update(ProductEntity, { id }, { slug });
    }

    if (databaseEntity?.image !== updatedImage) {
      const imageUrl = await this.subscriber.getImageUrl({
        tableName,
        image: updatedImage,
      });
      const imagePath = this.subscriber.getImagePathForRemove({
        tableName,
        image: databaseEntity?.image,
      });
      await unlink(imagePath, () => ({}));
      await manager.update(ProductEntity, { id }, { imageUrl });
    }
  }
}
