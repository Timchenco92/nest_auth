// Core
import {
  EventSubscriber,
  EntitySubscriberInterface,
  RemoveEvent,
  InsertEvent,
} from 'typeorm';
import { unlink } from 'fs';

// Entities
import { BannerEntity } from '@/database/entities';

// Providers
import { SubscriberProvider } from '@/api/providers';

@EventSubscriber()
export class BannerSubscriber
  implements EntitySubscriberInterface<BannerEntity>
{
  subscriber: any;
  constructor() {
    this.subscriber = new SubscriberProvider();
  }

  listenTo() {
    return BannerEntity;
  }

  async afterInsert(
    event: InsertEvent<BannerEntity>,
  ): Promise<Promise<any> | void> {
    const { id, image, title } = event.entity;
    const { tableName } = event.metadata;

    const slug = await this.subscriber.getSlug({ id, title });
    const imageUrl = await this.subscriber.getImageUrl({ tableName, image });

    await event.manager.update(BannerEntity, { id }, { slug, imageUrl });
  }

  async beforeRemove(
    event: RemoveEvent<BannerEntity>,
  ): Promise<Promise<any> | void> {
    const { image } = event.entity;
    const { tableName } = event.metadata;

    const imagePath = this.subscriber.getImagePathForRemove({
      tableName,
      image,
    });
    await unlink(imagePath, () => ({}));
  }
}
