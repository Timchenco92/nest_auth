// Core
import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  RemoveEvent,
  UpdateEvent,
} from 'typeorm';
import { unlink } from 'fs';
import { genSalt, hash } from 'bcrypt';

// Entities
import { UserEntity } from '@/database/entities';

// Providers
import { SubscriberProvider } from '@/api/providers';

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<UserEntity> {
  subscriber: any;
  constructor() {
    this.subscriber = new SubscriberProvider();
  }

  listenTo(): CallableFunction | string {
    return UserEntity;
  }

  afterLoad(entity: UserEntity): Promise<any> | void {
    entity.fullName = `${entity.firstName} ${entity.middleName} ${entity.lastName}`;
  }

  async beforeInsert(event: InsertEvent<UserEntity>): Promise<void> {
    const { entity } = event;
    const { tableName } = event.metadata;

    const salt = await genSalt(10);

    if (entity.avatar) {
      entity.avatarUrl = await this.subscriber.getImageUrl({
        tableName,
        image: entity.avatar,
      });
    }

    entity.password = await hash(entity.password, salt);
  }

  async beforeRemove(event: RemoveEvent<UserEntity>): Promise<void> {
    const { avatar } = event.entity;
    const { tableName } = event.metadata;

    const imagePath = this.subscriber.getImagePathForRemove({
      tableName,
      image: avatar,
    });

    await unlink(imagePath, () => ({}));
  }

  async beforeUpdate(event: UpdateEvent<UserEntity>): Promise<void> {
    const { entity, databaseEntity } = event;

    if (databaseEntity?.address !== entity?.address && entity?.address === '') {
      entity.address = null;
    }

    if (entity.password) {
      const salt = await genSalt(10);
      entity.password = await hash(entity.password, salt);
    }
  }

  async afterUpdate(event: UpdateEvent<UserEntity>): Promise<void> {
    const { metadata, entity, databaseEntity, manager } = event;
    const { tableName } = metadata;

    const { avatar: updatedAvatar, id } = entity;

    const imagePath = this.subscriber.getImagePathForRemove({
      tableName,
      image: databaseEntity?.avatar,
    });

    const avatarUrl = await this.subscriber.getImageUrl({
      tableName,
      image: updatedAvatar,
    });

    if (databaseEntity?.avatar !== updatedAvatar) {
      await unlink(imagePath, () => ({}));
      await manager.update(UserEntity, { id }, { avatarUrl });
    }

    if (databaseEntity?.avatar !== updatedAvatar && updatedAvatar === '') {
      await unlink(imagePath, () => ({}));
      await manager.update(
        UserEntity,
        { id },
        { avatar: null, avatarUrl: null },
      );
    }
  }
}
