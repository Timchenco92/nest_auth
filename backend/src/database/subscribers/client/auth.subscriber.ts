import { EventSubscriber, EntitySubscriberInterface, InsertEvent } from 'typeorm';
import { UserEntity } from '@/database/entities';

@EventSubscriber()
export class AuthSubscriber implements EntitySubscriberInterface<UserEntity> {
  listenTo(): CallableFunction | string {
    return UserEntity;
  }

  beforeInsert(event: InsertEvent<UserEntity>) {
    console.log(`Before insert of ExampleEntity: `, event.entity);
    // Your logic here
  }
}
