// Core
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
  TableUnique,
} from 'typeorm';

export class Ordersproducts1684781166011 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'orderproducts',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'orderid',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'productid',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            length: '255',
            name: 'total',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'price',
            type: 'varchar',
          },
          {
            isNullable: false,
            name: 'quantity',
            type: 'int',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );

    await queryRunner.createUniqueConstraints('orderproducts', [
      new TableUnique({
        columnNames: ['orderid', 'productid'],
      }),
    ]);

    await queryRunner.createForeignKey(
      'orderproducts',
      new TableForeignKey({
        columnNames: ['productid'],
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
        referencedTableName: 'products',
      }),
    );

    await queryRunner.createForeignKey(
      'orderproducts',
      new TableForeignKey({
        columnNames: ['orderid'],
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
        referencedTableName: 'orders',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('orderproducts', true);
  }
}
