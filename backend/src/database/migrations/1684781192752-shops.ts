// Core
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Shops1684781192752 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'shops',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            isUnique: true,
            length: '255',
            name: 'title',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'address',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            isUnique: true,
            length: '255',
            name: 'slug',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'image',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'imageurl',
            type: 'varchar',
          },
          {
            isNullable: false,
            isUnique: true,
            length: '255',
            name: 'firstphone',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            isUnique: true,
            length: '255',
            name: 'secondphone',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            isUnique: true,
            length: '255',
            name: 'thirdphone',
            type: 'varchar',
          },
          {
            isNullable: false,
            name: 'starttimework',
            type: 'time',
          },
          {
            isNullable: false,
            name: 'endtimework',
            type: 'time',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shops', true);
  }
}
