// Core
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Admins1684780811668 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'admins',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
          },
          {
            isNullable: false,
            length: '255',
            name: 'firstname',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'middlename',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'lastname',
            type: 'varchar',
          },
          {
            isNullable: false,
            isUnique: true,
            length: '255',
            name: 'email',
            type: 'varchar',
          },
          {
            isNullable: false,
            isUnique: true,
            length: '255',
            name: 'phone',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'avatar',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'avatarurl',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'password',
            type: 'varchar',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('admins', true);
  }
}
