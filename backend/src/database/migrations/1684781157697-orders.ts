// Core
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Orders1684781157697 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'orders',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'userid',
            type: 'bigint',
            unsigned: true,
          },
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isUnique: true,
            name: 'ordernumber',
            type: 'bigint',
          },
          {
            isNullable: false,
            length: '50',
            name: 'total',
            type: 'varchar',
          },
          {
            default: `'accepted'`,
            enum: ['accepted', 'in_progress', 'finished'],
            enumName: 'statusEnum',
            name: 'status',
            type: 'enum',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'delivery',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'comment',
            type: 'varchar',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'orders',
      new TableForeignKey({
        columnNames: ['userid'],
        referencedTableName: 'users',
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('orders', true);
  }
}
