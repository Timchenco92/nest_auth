// Core
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class Categories1684781136496 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'categories',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            isUnique: true,
            length: '255',
            name: 'title',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            isUnique: true,
            length: '255',
            name: 'slug',
            type: 'varchar',
          },
          {
            isNullable: false,
            length: '255',
            name: 'image',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'imageurl',
            type: 'varchar',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('categories', true);
  }
}
