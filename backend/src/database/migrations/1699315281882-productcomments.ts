// Core
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Productcomments1699315281882 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'productcomments',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'productid',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'userid',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'title',
            type: 'varchar',
          },
          {
            isNullable: false,
            name: 'description',
            type: 'text',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );

    await queryRunner.createForeignKey(
      'productcomments',
      new TableForeignKey({
        columnNames: ['productid'],
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
        referencedTableName: 'products',
      }),
    );

    await queryRunner.createForeignKey(
      'productcomments',
      new TableForeignKey({
        columnNames: ['userid'],
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('productcomments', true);
  }
}
