// Core
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Shopgallery1687958390745 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'shopgallery',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'shopid',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            length: '255',
            name: 'image',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'imageurl',
            type: 'varchar',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );
    await queryRunner.createForeignKey(
      'shopgallery',
      new TableForeignKey({
        columnNames: ['shopid'],
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
        referencedTableName: 'shops',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('shopgallery', true);
  }
}
