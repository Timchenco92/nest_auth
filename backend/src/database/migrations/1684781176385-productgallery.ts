// Core
import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class Productgallery1684781176385 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'productgallery',
        columns: [
          {
            generationStrategy: 'increment',
            isGenerated: true,
            isNullable: false,
            isPrimary: true,
            isUnique: true,
            name: 'id',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            name: 'productid',
            type: 'bigint',
            unsigned: true,
          },
          {
            isNullable: false,
            length: '255',
            name: 'image',
            type: 'varchar',
          },
          {
            default: 'NULL',
            isNullable: true,
            length: '255',
            name: 'imageurl',
            type: 'varchar',
          },
          {
            default: 'now()',
            name: 'createdat',
            type: 'timestamp with time zone',
          },
          {
            default: 'now()',
            name: 'updatedat',
            type: 'timestamp with time zone',
          },
        ],
      }),
      true,
    );
    await queryRunner.createForeignKey(
      'productgallery',
      new TableForeignKey({
        columnNames: ['productid'],
        onDelete: 'cascade',
        referencedColumnNames: ['id'],
        referencedTableName: 'products',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('productgallery', true);
  }
}
