// Core
import { Inject, Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { ConfigService } from '@nestjs/config';

// Constants
import { ADMIN_EMAIL_TEMPLATES } from '@/constants';

// Events
import { ClientRegisterUserEvent } from '@/events';

const {
  adminUserCreatedTemplate: { subject, template },
} = ADMIN_EMAIL_TEMPLATES;

@Injectable()
export class AdminCreatedUserNotification {
  @Inject(MailerService)
  private readonly mailerService: MailerService;
  @Inject(ConfigService)
  private readonly configService: ConfigService;

  async handleClientRegisterUserNotification(payload: ClientRegisterUserEvent) {
    const { user } = payload;
    const fullName = `${user.firstName} ${user.middleName} ${user.lastName}`;
    await this.mailerService
      .sendMail({
        to: user.email,
        // from: {
        //   name: this.configService.get<string>('MAIL_FROM_NAME'),
        //   address: this.configService.get<string>('MAIL_FROM_ADDRESS'),
        // },
        subject,
        template,
        context: {
          fullName,
          password: user.password,
        },
      })
      .then((res) => console.log('res', res))
      .catch((error) => console.log('err', error));
  }
}
