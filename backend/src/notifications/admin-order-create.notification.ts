// Core
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Telegraf } from 'telegraf';

// Events
import { AdminOrderCreateEvent } from '@/events';

@Injectable()
export class AdminOrderCreateNotification {
  @Inject(ConfigService)
  private readonly config: ConfigService;
  @Inject('TELEGRAM_API_CONNECTION')
  private readonly bot: Telegraf;

  async handleAdminCreateOrderNotification({ payload }: AdminOrderCreateEvent) {
    const {
      address,
      orderNumber,
      phone,
      email,
      delivery,
      createdAt,
      comment,
      total,
      fullName,
    } = payload;
    const formattedAddress = !address ? 'N/A' : address;
    const formattedDelivery = !delivery ? 'N/A' : delivery;
    const formattedComment = !comment ? 'N/A' : comment;
    const formattedCreatedAt = new Date(createdAt).toDateString();
    const text = `
          <b>Getting a new Order</b>
    <b>Address:</b> ${formattedAddress}
    <b>Delivery:</b> ${formattedDelivery}
    <b>Order Number:</b> ${orderNumber}
    <b>Phone:</b> ${phone}
    <b>Email:</b> ${email}
    <b>Created:</b> ${formattedCreatedAt}
    <b>Comment:</b> ${formattedComment}
    <b>Total:</b> ${total}$
    <b>Full Name:</b> ${fullName}
    `;

    const chatId = this.config.get('TELEGRAM_BOT_CHAT_ID');
    return this.bot.telegram.sendMessage(chatId, text, { parse_mode: 'HTML' });
  }
}
