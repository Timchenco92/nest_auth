export const ERROR_MESSAGES = {
  isEmail: 'Email is not valid',
  isExist: 'The record is already exist',
  isNotEmpty: (key: string): string => `${key} should not be empty`,
  isNotExist: (fieldName: string, entityName: string): string => `This ${fieldName} in ${entityName} is not exist`,
  isNotValidMimetype: (key: string, mimetypes: string[]): string => `The ${key} mimetype is not valid. The valid mimetypes are ${mimetypes.join(', ').toString()}`,
  isRequired: (key: string): string => `The ${key} is required`,
  isPhoneNumber: 'Phone must have a next format: 380999999999',
  isString: (key: string): string => `${key} must be a string`,
  isUnique: (key: string): string => `The record with this ${key} already exists.`,
  minLength: (key: string, length: number): string => `The ${key} must be more than ${length} characters`,
  maxLength: (key: string, length: number): string => `The ${key} must be less than ${length} characters`,
  arrayMinSize: (key: string, length: number): string => `The ${key} must contain at least ${length} elements`,
};

export const RESPONSE_MESSAGES = {
  errorValidation: 'The given data was invalid',
  isCreated: (key: string) => `The new ${key} created successfully`,
  isDeleted: (key: string) => `The ${key} delete successfully`,
  isUpdated: (key: string) => `The ${key} update successfully`,
  notFount: (key: string) => `Could not find any ${key}`,
};
