export const ADMIN_EMAIL_TEMPLATES = {
  adminUserCreatedTemplate: {
    subject: `Welcome to ${process.env.APP_NAME}!`,
    template: './admin-new-user-created',
  },
};
