export const ADMIN_EVENT_TYPES = {
  adminOrderCreated: 'admin.order.created',
  adminUserCreated: 'admin.user.created',
};

export const CLIENT_EVENT_TYPES = {
  clientRegisterUser: 'client.register.user',
};
