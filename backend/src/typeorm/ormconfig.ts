// Core
import { DataSource } from 'typeorm';
import { ConfigService } from '@nestjs/config';

const config = new ConfigService();

export const connectionSource = new DataSource({
  database: 'nest_auth',
  entities: ['dist/database/entities/*.entity.{ts,js}'],
  host: 'localhost',
  logging: 'all',
  logger: 'file',
  migrations: ['dist/database/migrations/*.{ts,js}'],
  migrationsTableName: 'migrations',
  name: 'nest_auth',
  password: 'nest_auth',
  port: 5432,
  schema: 'public',
  subscribers: ['dist/database/subscribers/**/*.{ts,js}'],
  synchronize: false,
  type: 'postgres',
  useUTC: true,
  username: 'nest_auth',
  metadataTableName: 'metadata',
});
