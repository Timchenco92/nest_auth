// Modules
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Screens
import { HomeScreen } from '@/screens/home/home-screen';

// Store
import { getBannersRequest, bannersPendingSelector, bannersErrorSelector, bannersSelector } from '@/store/banners';

export const HomeScreenContainer: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();
  const banners = useSelector(bannersSelector);
  const error = useSelector(bannersErrorSelector);
  const pending = useSelector(bannersPendingSelector);

  useEffect(() => {
    const action = getBannersRequest();
    dispatch(action);
  }, [dispatch]);

  return (
    <HomeScreen />
  );
};
