// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from './form-config';

// Interfaces
import type { IInitialValues } from '@/screens/auth/screens/register-screen/interfaces';

// Screens
import { RegisterScreen } from '@/screens/auth/screens/register-screen/register-screen';

// Store
import {
  registerErrorSelector,
  registerPendingSelector,
  registerSuccessSelector,
  registerRequest,
  resetRegisterError,
  resetRegisterSuccess,
} from '@/store/auth';

export const RegisterScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();

  const { errors, hasError } = useSelector(registerErrorSelector);
  const pending = useSelector(registerPendingSelector);
  const isRegisterSuccess = useSelector(registerSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/`, { replace: true });
  }, [navigate]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      await handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmit = useCallback((values: IInitialValues) => {
    const data = new FormData();
    if (values.avatar instanceof File) data.append('avatar', values.avatar);
    if (values.address && values.address.length) data.append('address', values.address);
    data.append('email', values.email);
    data.append('firstName', values.firstName);
    data.append('lastName', values.lastName);
    data.append('middleName', values.middleName);
    data.append('password', values.password);
    data.append('phone', values.phone);

    const action = registerRequest(data);
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isRegisterSuccess) return;
    handleClose();

    return() => {
      const resetAction = resetRegisterSuccess();
      dispatch(resetAction);
    };
  }, [dispatch, handleClose, isRegisterSuccess]);

  useEffect(() => () => {
    const action = resetRegisterError();
    dispatch(action);
  }, [dispatch]);

  return (
    <RegisterScreen
      errors={errors}
      formik={formik}
      handleClose={handleClose}
      pending={pending}
    />
  );
};
