export interface IInitialValues {
  address: string | undefined;
  avatar: File | null;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  password: string;
  phone: string;
}
