// Modules Types
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, IRegisterValidationErrors } from '@/screens/auth/screens/register-screen/interfaces'

export interface IRegisterScreenProps {
  errors: IRegisterValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose: () => void;
  pending: boolean;
}
