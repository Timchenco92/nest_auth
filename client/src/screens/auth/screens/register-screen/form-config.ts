// Modules
import { mixed, object, string } from 'yup';

// Constants
import { STATIC_VALIDATION_ERRORS } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/auth/screens/register-screen/interfaces';

const { isEmail, isRequired, max, min } = STATIC_VALIDATION_ERRORS;

export const validationSchema = object({
  address: string().notRequired(),
  avatar: mixed().notRequired(),
  email: string().email(isEmail).required(isRequired),
  firstName: string().required(isRequired),
  lastName: string().required(isRequired),
  middleName: string().required(isRequired),
  password: string()
    .min(8, min('Password', 8))
    .max(16, max('Password', 16))
    .required(isRequired),
  phone: string().required(isRequired),
});

export const initialValues: IInitialValues = {
  address: undefined,
  avatar: null,
  email: '',
  firstName: '',
  lastName: '',
  middleName: '',
  password: '',
  phone: '',
};
