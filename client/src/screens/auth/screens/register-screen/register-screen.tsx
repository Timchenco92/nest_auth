// Modules
import { useCallback, useEffect, useRef } from 'react';
import {
  MDBBtn,
  MDBCol,
  MDBFile,
  MDBInput,
  MDBModalFooter,
  MDBRow,
  MDBValidation,
  MDBValidationItem,
} from 'mdb-react-ui-kit';

// Modules Types
import type { BaseSyntheticEvent, FC } from 'react';

// Interfaces
import type { IRegisterScreenProps } from '@/screens/auth/screens/register-screen/interfaces';

export const RegisterScreen: FC<IRegisterScreenProps> = (props): JSX.Element => {
  const { errors: serverValidationErrors, formik, handleClose, pending } = props;

  const imageRef = useRef<HTMLInputElement>(null);

  const {
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    setFieldValue,
    touched,
    values,
  } = formik;

  const allowedDisabledConditions = [
    isValid,
    isSubmitting,
    pending,
  ];

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  console.log(imageRef.current);

  const handleChangeImage = useCallback(async (event: BaseSyntheticEvent) => {
    if (!event.target.files.length) return;
    const files = event.target.files[0];
    await setFieldValue('avatar', files);
  }, [setFieldValue]);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <form onSubmit={handleSubmit} encType="multipart/form-data">
      <MDBValidation className='row g-3'>
        <MDBValidationItem className='col-md-12' feedback={touched.firstName && errors.firstName} invalid>
          <MDBInput
            className={`mb-3 ${touched.firstName && Boolean(errors.firstName) ? 'is-invalid': '' }`}
            label="First Name"
            name="firstName"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.firstName}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.middleName && errors.middleName} invalid>
          <MDBInput
            className={`mb-3 ${touched.middleName && Boolean(errors.middleName) ? 'is-invalid': '' }`}
            label="Middle Name"
            name="middleName"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.middleName}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.lastName && errors.lastName} invalid>
          <MDBInput
            className={`mb-3 ${touched.lastName && Boolean(errors.lastName) ? 'is-invalid': '' }`}
            label="Last Name"
            name="lastName"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="text"
            value={values.lastName}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.email && errors.email} invalid>
          <MDBInput
            className={`mb-3 ${touched.email && Boolean(errors.email) ? 'is-invalid': '' }`}
            label="Email"
            name="email"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="email"
            value={values.email}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.phone && errors.phone} invalid>
          <MDBInput
            className={`mb-3 ${touched.phone && Boolean(errors.phone) ? 'is-invalid': '' }`}
            label="Phone"
            name="phone"
            maxLength={12}
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="tel"
            value={values.phone}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.address && errors.address} invalid>
          <MDBInput
            className={`mb-3 ${touched.address && Boolean(errors.address) ? 'is-invalid': '' }`}
            label="Address"
            name="address"
            onBlur={handleBlur}
            onChange={handleChange}
            type="text"
            value={values.address}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.avatar && errors.avatar} invalid>
          <MDBFile
            accept='image/*'
            className={`mb-3 ${touched.avatar && Boolean(errors.avatar) ? 'is-invalid': '' }`}
            onBlur={handleBlur}
            onChange={handleChangeImage}
            name="avatar"
            inputRef={imageRef}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.password && errors.password} invalid>
          <MDBInput
            className={`mb-3 ${touched.password && Boolean(errors.password) ? 'is-invalid': '' }`}
            label="Password"
            name="password"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="password"
            value={values.password}
          />
        </MDBValidationItem>
      </MDBValidation>
      <MDBModalFooter className="mt-4">
        <MDBRow className="w-100">
          <MDBCol md={6} sm={12}>
            <MDBBtn block color="secondary" outline onClick={handleClose}>
              Close
            </MDBBtn>
          </MDBCol>
          <MDBCol md={6} sm={12}>
            <MDBBtn block color="primary" outline type="submit">Sign In</MDBBtn>
          </MDBCol>
        </MDBRow>
      </MDBModalFooter>
    </form>
  );
};
