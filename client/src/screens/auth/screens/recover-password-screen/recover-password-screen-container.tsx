// Modules
import { useNavigate } from 'react-router-dom';
import { useCallback } from 'react';

// Modules Types
import type { FC } from 'react';

// Screens
import { RecoverPasswordScreen } from '@/screens/auth/screens/recover-password-screen/recover-password-screen';

export const RecoverPasswordScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();

  const handleClose = useCallback(() => {
    navigate(`/`, { replace: true });
  }, [navigate]);

  return (
    <RecoverPasswordScreen handleClose={handleClose} />
  );
};
