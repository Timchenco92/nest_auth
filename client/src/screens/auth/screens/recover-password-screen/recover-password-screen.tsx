// Modules
import { MDBBtn, MDBCol, MDBModalFooter, MDBRow } from 'mdb-react-ui-kit';

// Modules Types
import type { FC } from 'react';

// Interfaces
import type { IRecoverPasswordScreenProps } from '@/screens/auth/screens/recover-password-screen/interfaces';

export const RecoverPasswordScreen: FC<IRecoverPasswordScreenProps> = (props): JSX.Element => {
  const { handleClose } = props;

  return (
    <>
      <div>RecoverPasswordScreen</div>
      <MDBModalFooter>
        <MDBRow className="w-100">
          <MDBCol md={6} sm={12}>
            <MDBBtn block color="secondary" outline onClick={handleClose}>
              Close
            </MDBBtn>
          </MDBCol>
          <MDBCol md={6} sm={12}>
            <MDBBtn block color="primary" outline>Reset Password</MDBBtn>
          </MDBCol>
        </MDBRow>
      </MDBModalFooter>
    </>

  );
};
