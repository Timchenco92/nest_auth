// Modules
import { object, string } from 'yup';

// Constants
import { STATIC_VALIDATION_ERRORS } from '@/constants';

// Interfaces
import type { IInitialValues } from '@/screens/auth/screens/login-screen/interfaces';

const { isEmail, isRequired, max, min } = STATIC_VALIDATION_ERRORS;

export const validationSchema = object({
  email: string().email(isEmail).required(isRequired),
  password: string()
    .min(8, min('Password', 8))
    .max(16, max('Password', 16))
    .required(isRequired),
});

export const initialValues: IInitialValues = {
  email: '',
  password: '',
};
