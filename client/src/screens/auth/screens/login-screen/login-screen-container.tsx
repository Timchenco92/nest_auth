// Modules
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useFormik } from 'formik';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';
import type { FormikProps } from 'formik';

// Config
import { initialValues, validationSchema } from './form-config';

// Interfaces
import type { IInitialValues } from '@/screens/auth/screens/login-screen/interfaces';

// Screens
import { LoginScreen } from '@/screens/auth/screens/login-screen/login-screen';

// Store
import {
  loginErrorSelector,
  loginPendingSelector,
  loginSuccessSelector,
  loginRequest,
  resetLoginsError,
  resetLoginsSuccess,
} from '@/store/auth';

export const LoginScreenContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();
  const dispatch: Dispatch = useDispatch();

  const { errors: serverValidationErrors, hasError } = useSelector(loginErrorSelector);
  const pending = useSelector(loginPendingSelector);
  const isLoginSuccess = useSelector(loginSuccessSelector);

  const handleClose = useCallback(() => {
    navigate(`/`, { replace: true });
  }, [navigate]);

  const formik: FormikProps<IInitialValues> = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values, formikHelpers) => {
      const { setSubmitting } = formikHelpers;
      await handleSubmit(values);
      setSubmitting(false);
    },
    enableReinitialize: true,
    validateOnChange: !hasError,
    validateOnBlur: !hasError,
    validateOnMount: true,
  });

  const handleSubmit = useCallback(async (values: IInitialValues) => {
    const { email, password } = values;
    const action = loginRequest({ email, password });
    dispatch(action);
  }, [dispatch]);

  useEffect(() => {
    if (!isLoginSuccess) return;
    handleClose();

    return() => {
      const resetAction = resetLoginsSuccess();
      dispatch(resetAction);
    };
  }, [dispatch, handleClose, isLoginSuccess]);

  useEffect(() => () => {
    const action = resetLoginsError();
    dispatch(action);
  }, [dispatch]);

  return (
    <LoginScreen
      errors={serverValidationErrors}
      formik={formik}
      handleClose={handleClose}
      pending={pending}
    />
  );
};
