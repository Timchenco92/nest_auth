// Modules Types
import type { FormikProps } from 'formik';

// Interfaces
import type { IInitialValues, ILoginValidationErrors } from '@/screens/auth/screens/login-screen/interfaces';

export interface ILoginScreenProps {
  errors: ILoginValidationErrors;
  formik: FormikProps<IInitialValues>;
  handleClose: () => void;
  pending: boolean;
}
