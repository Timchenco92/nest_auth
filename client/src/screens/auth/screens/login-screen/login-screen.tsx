// Modules
import { useEffect } from 'react';
import {
  MDBBtn,
  MDBCol,
  MDBInput,
  MDBModalFooter,
  MDBRow,
  MDBValidation,
  MDBValidationItem,
} from 'mdb-react-ui-kit';

// Modules Types
import type { FC } from 'react';

// Interfaces
import type { ILoginScreenProps } from '@/screens/auth/screens/login-screen/interfaces';

export const LoginScreen: FC<ILoginScreenProps> = (props): JSX.Element => {
  const { errors: serverValidationErrors, formik, handleClose, pending } = props;

  const {
    errors,
    handleBlur,
    handleChange,
    handleSubmit,
    isSubmitting,
    isValid,
    setErrors,
    touched,
    values,
  } = formik;

  const allowedDisabledConditions = [
    isValid,
    isSubmitting,
    pending,
  ];

  const isSubmitDisabled: boolean = allowedDisabledConditions.every((item) => !item);

  useEffect(() => {
    if (!serverValidationErrors) return;
    setErrors(serverValidationErrors);
  }, [serverValidationErrors, setErrors]);

  return (
    <form onSubmit={handleSubmit}>
      <MDBValidation className='row g-3'>
        <MDBValidationItem className='col-md-12' feedback={touched.email && errors.email} invalid>
          <MDBInput
            className={`mb-3 ${touched.email && Boolean(errors.email) ? 'is-invalid': '' }`}
            label="Email"
            name="email"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="email"
            value={values.email}
          />
        </MDBValidationItem>
        <MDBValidationItem className='col-md-12' feedback={touched.password && errors.password} invalid>
          <MDBInput
            className={`mb-3 ${touched.password && Boolean(errors.password) ? 'is-invalid': '' }`}
            label="Password"
            name="password"
            onBlur={handleBlur}
            onChange={handleChange}
            required
            type="password"
            value={values.password}
          />
        </MDBValidationItem>
      </MDBValidation>
      <MDBModalFooter className="mt-4">
        <MDBRow className="w-100">
          <MDBCol md={6} sm={12}>
            <MDBBtn block color="secondary" outline onClick={handleClose}>
              Close
            </MDBBtn>
          </MDBCol>
          <MDBCol md={6} sm={12}>
            <MDBBtn block color="primary" disabled={isSubmitDisabled} outline type="submit">Sign In</MDBBtn>
          </MDBCol>
        </MDBRow>
      </MDBModalFooter>
    </form>
  );
};
