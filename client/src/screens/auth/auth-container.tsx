// Modules
import { useCallback } from 'react';
import { NavLink, Route, Routes, useNavigate } from 'react-router-dom';
import {
  MDBBtn, MDBCol,
  MDBModal,
  MDBModalBody,
  MDBModalContent,
  MDBModalDialog,
  MDBModalFooter, MDBRow,
  MDBTabs,
  MDBTabsContent,
  MDBTabsItem,
  MDBTabsLink,
} from 'mdb-react-ui-kit';

// Modules Types
import type { FC } from 'react';

// Screens
import { LoginScreenContainer } from '@/screens/auth/screens/login-screen';
import { RecoverPasswordScreenContainer } from '@/screens/auth/screens/recover-password-screen';
import { RegisterScreenContainer } from '@/screens/auth/screens/register-screen';

export const AuthContainer: FC = (): JSX.Element => {
  const navigate = useNavigate();

  const handleClose = useCallback(() => {
    navigate(`/`, { replace: true });
  }, [navigate]);

  return (
    <MDBModal
      open
      staticBackdrop
      tabIndex="-1"
      show
    >
      <MDBModalDialog centered size='xl'>
        <MDBModalContent>
          <MDBModalBody className="p-4">
            <MDBTabs pills justify className='mb-3'>
              <MDBTabsItem>
                <NavLink to="login">
                  {({ isActive }) => (
                    <MDBTabsLink active={isActive} role="presentation">
                      Login
                    </MDBTabsLink>
                  )}
                </NavLink>
              </MDBTabsItem>
              <MDBTabsItem>
                <NavLink to="register">
                  {({ isActive }) => (
                    <MDBTabsLink active={isActive} role="presentation">
                      Register
                    </MDBTabsLink>
                  )}
                </NavLink>
              </MDBTabsItem>
              <MDBTabsItem>
                <NavLink to="recover-password">
                  {({ isActive }) => (
                    <MDBTabsLink active={isActive} role="presentation">
                      Recover Password
                    </MDBTabsLink>
                  )}
                </NavLink>
              </MDBTabsItem>
            </MDBTabs>

            <MDBTabsContent>
              <Routes>
                <Route element={<LoginScreenContainer />} path="login" />
                <Route element={<RecoverPasswordScreenContainer />} path="recover-password" />
                <Route element={<RegisterScreenContainer />} path="register" />
              </Routes>
            </MDBTabsContent>
          </MDBModalBody>

          {/*<MDBModalFooter>*/}
          {/*  <MDBRow className="m-0 w-100">*/}
          {/*    <MDBCol md={6} sm={12}>*/}
          {/*      <MDBBtn block color="danger" outline onClick={handleClose}>*/}
          {/*        Close*/}
          {/*      </MDBBtn>*/}
          {/*    </MDBCol>*/}
          {/*    <MDBCol md={6} sm={12}>*/}
          {/*      <MDBBtn block color="success" outline>Save changes</MDBBtn>*/}
          {/*    </MDBCol>*/}
          {/*  </MDBRow>*/}
          {/*</MDBModalFooter>*/}
        </MDBModalContent>
      </MDBModalDialog>
    </MDBModal>
  );
};
