// Modules
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';

// Interfaces
import type { IProps } from '@/guards';

// Store
import { currentUserIsAuthSelector } from '@/store/auth';

export const UnAuthGuard: FC<IProps> = (props): JSX.Element => {
  const { component } = props;
  const navigate = useNavigate();
  const isAuth = useSelector(currentUserIsAuthSelector);

  useEffect(() => {
    if (isAuth) {
      navigate('/dashboard/home', { replace: true });
    }
  }, [isAuth, navigate]);

  return (
    <>{component}</>
  );
};
