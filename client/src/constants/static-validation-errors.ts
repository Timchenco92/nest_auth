export const STATIC_VALIDATION_ERRORS = {
  isRequired: 'This field is required',
  isEmail: 'The current Email is not valid',
  max: (fieldName: string, length: number) => `The ${fieldName} should be less then ${length} characters`,
  min: (fieldName: string, length: number) => `The ${fieldName} should be more then ${length} characters`,
};
