// Modules Types
import type { PathRouteProps } from 'react-router-dom';

export const DASHBOARD_ROUTES: PathRouteProps[] = [
  {
    element: null,
    path: '',
    index: false,
  },
];

export const MAIN_ROUTES: PathRouteProps[] = [
  {
    element: null,
    path: '',
    index: false,
  },
];
