// Interfaces
import type { IProduct } from '@/interfaces/models/IProduct';

export interface IOrderProduct {
  productId: IProduct['id'];
  total: string;
  quantity: number;
  price: string;
}
