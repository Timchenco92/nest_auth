export interface ICategory {
  createdAt: Date;
  id: string | number;
  imageUrl: string;
  slug: string;
  title: string;
  updatedAt: Date;
}
