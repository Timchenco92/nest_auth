export interface IUser {
  address: null | string;
  avatarUrl: null | string;
  createdAt: Date;
  email: string;
  firstName: string;
  id: number | string;
  lastName: string;
  middleName: string;
  phone: string;
  updatedAt: Date;
}
