export interface IShopGallery {
  createdAt: Date;
  id: string | number;
  imageUrl: string;
  shopId: string | number;
  updatedAt: Date;
}
