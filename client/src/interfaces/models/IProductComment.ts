// Interfaces
import type { IProduct, IUser } from '@/interfaces/models';

export interface IProductComment {
  createdAt: Date;
  description: string;
  id: string | number;
  productId: IProduct['id'];
  title: string;
  updatedAt: Date;
  userId: IUser['id'];
}
