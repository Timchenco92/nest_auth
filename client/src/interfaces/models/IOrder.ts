export interface IOrder {
  comment: string | null;
  createdAt: Date;
  delivery: string | null;
  id: string | number;
  orderNumber: string;
  orderProductsCount: number;
  updatedAt: Date;
  userId: string | number;
}
