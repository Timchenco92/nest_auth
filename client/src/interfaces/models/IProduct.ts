export interface IProduct {
  categoryId: string | number;
  createdAt: Date;
  id: string | number;
  imageUrl: string;
  price: string;
  slug: string;
  title: string;
  updatedAt: Date;
}
