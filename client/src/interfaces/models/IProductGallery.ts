export interface IProductGallery {
  createdAt: Date;
  id: string | number;
  imageUrl: string;
  productId: string | number;
  updatedAt: Date;
}
