export interface IShop {
  address: string;
  createdAt: Date;
  endTimeWork: Date;
  firstPhone: string;
  id: string | number;
  imageUrl: string;
  secondPhone: string | null;
  slug: string;
  startTimeWork: Date;
  thirdPhone: string | null;
  title: string;
  updatedAt: Date;
}
