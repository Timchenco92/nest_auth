// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IOrder, IOrderProduct, IUser } from '@/interfaces/models';

export interface ICreateOrderData {
  userId: string;
  total: string;
  delivery: string | null;
  comment: string | null;
  orderProducts: IOrderProduct[];
}

const createNewOrder = async (data: ICreateOrderData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/orders', data);
}

const getUserOrders = async (userId: IUser['id']): Promise<AxiosResponse<IOrder[]> | AxiosError> => {
  return API.get(`/orders/${userId}/all`);
}

const getUserOrder = async (orderId: IOrder['id']): Promise<AxiosResponse<IOrder> | AxiosError> => {
  return API.get(`/orders/${orderId}`);
}

export const ordersService = {
  createNewOrder,
  getUserOrders,
  getUserOrder,
};
