// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';

const getCurrentProductGallery = async (productId: IProduct['id']): Promise<AxiosResponse<IProductGallery[]> | AxiosError> => {
  return API.get(`/product-gallery/${productId}/all`);
}

export const productGalleryService = {
  getCurrentProductGallery,
};
