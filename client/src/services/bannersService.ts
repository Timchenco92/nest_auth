// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

const getBanners = async (): Promise<AxiosResponse | AxiosError> => {
  return API.get('banners');
};

export const bannersService = {
  getBanners,
};
