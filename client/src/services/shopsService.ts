// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IShop } from '@/interfaces/models';

const getAllShops = async (): Promise<AxiosResponse<IShop[]> | AxiosError> => {
  return API.get('shops');
}

const getMainShop = async (shopId: IShop['id']): Promise<AxiosResponse<IShop> | AxiosError> => {
  return API.get(`/shops/${shopId}`);
}

export const shopsService = {
  getAllShops,
  getMainShop,
};
