// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { ICategory, IProduct } from '@/interfaces/models';

const getProductsByCategory = async (categoryId: ICategory['id']): Promise<AxiosResponse<IProduct[]> | AxiosError> => {
  return API.get('/products', { params: { categoryId }});
}

const getMainProduct = async (productId: IProduct['id']): Promise<AxiosResponse<IProduct> | AxiosError> => {
  return API.get(`/products/${productId}`)
}

const getRandomProducts = async (): Promise<AxiosResponse<IProduct[]> | AxiosError> => {
  return API.get('/products/random-products');
}

export const productsService = {
  getProductsByCategory,
  getMainProduct,
  getRandomProducts,
};
