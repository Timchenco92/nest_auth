// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { ICategory } from '@/interfaces/models';

const getAllCategories = async (): Promise<AxiosResponse<ICategory[]> | AxiosError> => {
  return API.get('/categories');
}

const getMainCategory = async (categoryId: ICategory['id']): Promise<AxiosResponse<ICategory> | AxiosError>  => {
  return API.get(`/categories/${categoryId}`);
}

export const categoriesService = {
  getAllCategories,
  getMainCategory,
};
