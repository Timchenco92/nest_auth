// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IOrder, IOrderProduct } from '@/interfaces/models';

const getProductsFromCurrentOrder = async (orderId: IOrder['id']): Promise<AxiosResponse<IOrderProduct[]> | AxiosError> => {
  return API.get(`/order-products/${orderId}/all`);
}

export const orderProductsService = {
  getProductsFromCurrentOrder,
};
