// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IProduct, IProductComment, IUser } from '@/interfaces/models';

interface IStoredData {
  description: string;
  productId: IProduct['id'];
  title: string;
  userId: IUser['id'];
}

const addNewCommentToProduct = async (data: IStoredData): Promise<AxiosResponse | AxiosError> => {
  return API.post('/product-comments', data);
}

const getCurrentProductComments = async (productId: IProduct['id']): Promise<AxiosResponse<IProductComment[]> | AxiosError> => {
  return API.get(`/product-comments/${productId}/all`);
}

export const productCommentsService = {
  addNewCommentToProduct,
  getCurrentProductComments,
};
