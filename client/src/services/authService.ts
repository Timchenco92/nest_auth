// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

export interface ILoginData {
  email: string;
  password: string;
}

const getMe = async (): Promise<AxiosResponse | AxiosError> => {
  return API.get('auth/me');
};

const login = async (data: ILoginData): Promise<AxiosResponse | AxiosError> => {
  return API.post('auth/login', data);
};

const register = async (data: FormData): Promise<AxiosResponse | AxiosError> => {
  return API.post('auth/register', data, { headers: { 'Content-Type': 'multipart/form-data' } });
};

const restorePassword = async (email: string): Promise<AxiosResponse | AxiosError> => {
  return API.post('auth/restore-password', { email });
};

export const authService = {
  getMe,
  login,
  register,
  restorePassword,
};
