// Modules
import { AxiosResponse, AxiosError } from 'axios';

// API
import API from '@/api';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

const getGallery = async (shopId: IShop['id']): Promise<AxiosResponse<IShopGallery[]> | AxiosError> => {
  return API.get(`/shop-gallery/${shopId}`)
}

export const shopGalleryService = {
  getGallery,
};
