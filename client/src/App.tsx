// Modules
import { Routes } from 'react-router-dom';

// Modules Types
import type { FC } from 'react';

// Containers
import { MainContainer } from '@/containers/main-container';

// Routes
import { AuthRoutes, UnAuthRoutes } from '@/routes';

const App: FC = (): JSX.Element => {
  return (
    <div className="App">
      <Routes>
        {AuthRoutes}
        {UnAuthRoutes}
      </Routes>
    </div>
  );
}

export default App;
