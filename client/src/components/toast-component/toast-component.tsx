// Modules
import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Toast, ToastBody, ToastContainer, ToastHeader } from 'react-bootstrap';

// Modules Types
import type { FC } from 'react';
import type { Dispatch } from 'redux';

// Store
import { hideToast, isShowingSelector, messageSelector, typeSelector } from '@/store/toast';

interface IToastHeaderTitle {
  danger: string;
  success: string;
}

const TOAST_HEADER_TITLE: IToastHeaderTitle = {
  danger: 'Error',
  success: 'Success'
};

export const ToastComponent: FC = (): JSX.Element => {
  const dispatch: Dispatch = useDispatch();

  const isShow = useSelector(isShowingSelector);
  const message = useSelector(messageSelector);
  const type = useSelector(typeSelector);

  const headerTitle = TOAST_HEADER_TITLE[type as keyof IToastHeaderTitle];

  const handleClose = useCallback(() => {
    const action = hideToast({ isShowing: false, message: '', type: '' });
    dispatch(action);
  }, [dispatch]);

  return (
    <ToastContainer className="p-3" position="top-end" style={{ zIndex: 9999 }}>
      <Toast animation autohide bg={type} delay={4000} show={isShow} onClose={handleClose}>
        <ToastHeader style={{ backgroundColor: 'rgba(33, 37, 41, 0.65)'}}>
          <strong className="me-auto">{headerTitle}</strong>
        </ToastHeader>
        <ToastBody className="text-white">{message}</ToastBody>
      </Toast>
    </ToastContainer>

  );
};
