// Modules
import { Route } from 'react-router-dom';

// Containers
import { MainContainer } from '@/containers/main-container';

// Screens
import { AuthContainer } from '@/screens/auth';
import { HomeScreenContainer } from '@/screens/home';
import { LoginScreenContainer } from '@/screens/auth/screens/login-screen';

export const UnAuthRoutes = [
  <Route key="MainContainer" element={<MainContainer />}>
    <Route key="AuthContainer" path="account/*" element={<AuthContainer />}/>
    <Route index key="HomeScreenContainer" element={<HomeScreenContainer />}/>
  </Route>
];
