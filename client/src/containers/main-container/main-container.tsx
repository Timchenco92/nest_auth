// Modules
import { Outlet } from 'react-router-dom';
import { MDBContainer, } from 'mdb-react-ui-kit';

// Modules Types
import type { FC } from 'react';

// Components
import { HeaderComponent } from '@/containers/main-container/components/header-component';
import { ToastComponent } from '@/components/toast-component';

export const MainContainer: FC = (): JSX.Element => {

  return (
    <>
      <ToastComponent />
      <HeaderComponent />
      <MDBContainer>
        <Outlet />
      </MDBContainer>
    </>
  );
};
