// Modules
import { useState } from 'react';
import { useNavigate, NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import {
  MDBBtn,
  MDBCol,
  MDBCollapse,
  MDBContainer,
  MDBDropdown,
  MDBDropdownItem,
  MDBDropdownMenu,
  MDBDropdownToggle,
  MDBIcon,
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarItem,
  MDBNavbarLink,
  MDBNavbarNav,
  MDBNavbarToggler,
  MDBRow,
} from 'mdb-react-ui-kit';

// Modules Types
import { FC } from 'react';

// Store
import { currentUserIsAuthSelector } from '@/store/auth';

export const HeaderComponent: FC = (): JSX.Element => {
  const [navBarOpen, setNavBarOpen] = useState<boolean>(false);
  const isAuth = useSelector(currentUserIsAuthSelector);

  const handleToggleNavBar = () => setNavBarOpen((prevState) => !prevState);

  return (
    <MDBNavbar expand='lg' light bgColor='light' className="shadow-lg">
      <MDBContainer fluid>
        <MDBRow className="w-100">
          <MDBCol md="6" sm="6" xs="6" className="w-50">
            <MDBNavbarToggler
              aria-controls='navbarToggle'
              aria-expanded='false'
              aria-label='Toggle navigation'
              className="my-auto"
              data-target='#navbarToggle'
              onClick={handleToggleNavBar}
              type='button'
            >
              <MDBIcon icon='bars' fas size="2x" />
            </MDBNavbarToggler>
            <MDBCollapse navbar show={navBarOpen} className="h-100">
              <MDBNavbarBrand href='#'>Food APP</MDBNavbarBrand>
              <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
                <MDBNavbarItem>
                  <MDBNavbarLink active aria-current='page' href='#'>
                    Menu
                  </MDBNavbarLink>
                </MDBNavbarItem>
                <MDBNavbarItem>
                  <MDBNavbarLink active aria-current='page' href='#'>
                    Popular
                  </MDBNavbarLink>
                </MDBNavbarItem>
                <MDBNavbarItem>
                  <MDBNavbarLink active aria-current='page' href='#'>
                    Contacts
                  </MDBNavbarLink>
                </MDBNavbarItem>
              </MDBNavbarNav>
            </MDBCollapse>
          </MDBCol>
          <MDBCol md="6" sm="6" xs="6" className="text-end w-50">
            {!isAuth ? (
              <NavLink className="text-black" to="account/login">
                <MDBBtn outline className='mx-2' color='dark'>
                  Profile
                </MDBBtn>
              </NavLink>
            ) : (
              <MDBDropdown className="text-end">
                <MDBDropdownToggle tag='a' className="btn shadow-none p-0">
                  <img src="https://mdbcdn.b-cdn.net/img/new/avatars/2.webp" className="rounded-circle shadow-4"
                       style={{ width: '50px' }}
                       alt="Avatar" />
                </MDBDropdownToggle>
                <MDBDropdownMenu>
                  <MDBDropdownItem link>Dashboard</MDBDropdownItem>
                </MDBDropdownMenu>
              </MDBDropdown>
            )}
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </MDBNavbar>
  );
};
