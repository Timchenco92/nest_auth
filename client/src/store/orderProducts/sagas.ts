// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import { getProductsFromOrder, getProductsFromOrderError, getProductsFromOrderSuccess } from '@/store/orderProducts';

// Action Types
import { GET_PRODUCTS_FROM_ORDER } from '@/store/orderProducts';

// Interfaces
import type { IOrderProduct } from '@/interfaces/models';

// Services
import { orderProductsService } from '@/services';

const { getProductsFromCurrentOrder } = orderProductsService;

export function* getProductsFromOrderSaga({ payload }: ReturnType<typeof getProductsFromOrder>):
  Generator<CallEffect<AxiosResponse<IOrderProduct[]> | AxiosError> | PutEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { orderId } = payload;
    const { data }: AxiosResponse = yield call(() => getProductsFromCurrentOrder(orderId));
    yield put(getProductsFromOrderSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getProductsFromOrderError());
    return error;
  }
}

export function* orderProductsSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_PRODUCTS_FROM_ORDER, getProductsFromOrderSaga)]);
}
