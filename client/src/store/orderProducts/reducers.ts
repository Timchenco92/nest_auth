// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  GET_PRODUCTS_FROM_ORDER,
  GET_PRODUCTS_FROM_ORDER_ERROR,
  GET_PRODUCTS_FROM_ORDER_SUCCESS,
} from '@/store/orderProducts';

// Types
import type { IOrderProductsState, TOrderProductsTypes } from '@/store/orderProducts';

const initialState: IOrderProductsState = {
  orderProducts: {
    data: [],
    error: false,
    pending: false,
  },
};

export const orderProductsReducer: Reducer<IOrderProductsState, TOrderProductsTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS_FROM_ORDER: {
      return {
        ...state,
        orderProducts: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_PRODUCTS_FROM_ORDER_ERROR: {
      return {
        ...state,
        orderProducts: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_PRODUCTS_FROM_ORDER_SUCCESS: {
      return {
        ...state,
        orderProducts: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    default:
      return state;
  }
};
