// Action Types
import { GET_PRODUCTS_FROM_ORDER, GET_PRODUCTS_FROM_ORDER_ERROR, GET_PRODUCTS_FROM_ORDER_SUCCESS } from '@/store/orderProducts';

// Interfaces
import type { IOrder, IOrderProduct } from '@/interfaces/models';

// Types
import type { IGetProductsFromOrder, IGetProductsFromOrderError, IGetProductsFromOrderSuccess } from '@/store/orderProducts';

export const getProductsFromOrder = (orderId: IOrder['id']): IGetProductsFromOrder => ({
  type: GET_PRODUCTS_FROM_ORDER,
  payload: { orderId },
});

export const getProductsFromOrderError = (): IGetProductsFromOrderError => ({
  type: GET_PRODUCTS_FROM_ORDER_ERROR,
});

export const getProductsFromOrderSuccess = (payload: IOrderProduct[]): IGetProductsFromOrderSuccess => ({
  type: GET_PRODUCTS_FROM_ORDER_SUCCESS,
  payload,
});
