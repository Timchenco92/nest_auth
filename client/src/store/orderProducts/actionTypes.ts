export const GET_PRODUCTS_FROM_ORDER = 'GET_PRODUCTS_FROM_ORDER';
export const GET_PRODUCTS_FROM_ORDER_ERROR = 'GET_PRODUCTS_FROM_ORDER_ERROR';
export const GET_PRODUCTS_FROM_ORDER_SUCCESS = 'GET_PRODUCTS_FROM_ORDER_SUCCESS';
