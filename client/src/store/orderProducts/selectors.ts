// Modules
import { createSelector } from 'reselect';

// State
import type { OrderProductState } from '@/store/rootReducer';

const orderProducts = ({ orderProducts }: OrderProductState) => ({
  data: orderProducts.orderProducts.data,
  error: orderProducts.orderProducts.error,
  pending: orderProducts.orderProducts.pending,
});

export const orderProductsSelector = createSelector(orderProducts, ({ data }) => data);
export const orderProductsErrorSelector = createSelector(orderProducts, ({ error }) => error);
export const orderProductsPendingSelector = createSelector(orderProducts, ({ pending }) => pending);
