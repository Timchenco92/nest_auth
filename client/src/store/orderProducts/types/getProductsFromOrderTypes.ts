// Action Types
import { GET_PRODUCTS_FROM_ORDER, GET_PRODUCTS_FROM_ORDER_ERROR, GET_PRODUCTS_FROM_ORDER_SUCCESS } from '@/store/orderProducts';

// Interfaces
import type { IOrder, IOrderProduct } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetProductsFromOrderState extends IBaseState {
  data: IOrderProduct[];
}

export interface IGetProductsFromOrder {
  type: typeof GET_PRODUCTS_FROM_ORDER;
  payload: { orderId: IOrder['id'] };
}

export interface IGetProductsFromOrderError {
  type: typeof GET_PRODUCTS_FROM_ORDER_ERROR;
}

export interface IGetProductsFromOrderSuccess {
  type: typeof GET_PRODUCTS_FROM_ORDER_SUCCESS;
  payload: IOrderProduct[];
}
