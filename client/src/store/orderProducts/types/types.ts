// Types
import type {
  IGetProductsFromOrder,
  IGetProductsFromOrderError,
  IGetProductsFromOrderState,
  IGetProductsFromOrderSuccess,
} from '@/store/orderProducts';

export interface IOrderProductsState {
  orderProducts: IGetProductsFromOrderState;
}

export type TOrderProductsTypes = IGetProductsFromOrder |
  IGetProductsFromOrderError |
  IGetProductsFromOrderSuccess;
