// Action Types
import { RESET_ADD_ORDER_ERROR, RESET_ADD_ORDER_SUCCESS } from '@/store/orders';

export interface IResetAddOrderError {
  type: typeof RESET_ADD_ORDER_ERROR;
}

export interface IResetAddOrderSuccess {
  type: typeof RESET_ADD_ORDER_SUCCESS;
}
