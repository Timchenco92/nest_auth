// Action Types
import { ADD_ORDER, ADD_ORDER_ERROR, ADD_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrderProduct } from '@/interfaces/models';

export interface IAddOrderState {
  error: IAddOrderErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddOrder {
  type: typeof ADD_ORDER;
  payload: IAddOrderPayload;
}

export interface IAddOrderPayload {
  userId: string;
  total: string;
  delivery: string | null;
  comment: string | null;
  orderProducts: IOrderProduct[];
}

export interface IAddOrderError {
  type: typeof ADD_ORDER_ERROR;
  payload: IAddOrderErrorStateAndPayload;
}

export interface IAddOrderErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
  message: string;
}

interface IErrors {
  total: string;
  userId: string;
  delivery: string;
  comment: string;
  orderProducts: string;
}

export interface IAddOrderSuccess {
  type: typeof ADD_ORDER_SUCCESS;
}
