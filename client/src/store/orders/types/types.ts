// Types
import type {
  IAddOrder,
  IAddOrderError,
  IAddOrderState,
  IAddOrderSuccess,
  IGetCurrentUserOrder,
  IGetCurrentUserOrderError,
  IGetCurrentUserOrderState,
  IGetCurrentUserOrderSuccess,
  IGetCurrentUserOrders,
  IGetCurrentUserOrdersError,
  IGetCurrentUserOrdersState,
  IGetCurrentUserOrdersSuccess,
  IResetAddOrderError,
  IResetAddOrderSuccess,
} from '@/store/orders';

export interface IOrdersState {
  addOrder: IAddOrderState;
  order: IGetCurrentUserOrderState;
  orders: IGetCurrentUserOrdersState;
}

export type TOrdersTypes = IAddOrder |
  IAddOrderError |
  IAddOrderSuccess |
  IGetCurrentUserOrder |
  IGetCurrentUserOrderError |
  IGetCurrentUserOrderSuccess |
  IGetCurrentUserOrders |
  IGetCurrentUserOrdersError |
  IGetCurrentUserOrdersSuccess |
  IResetAddOrderError |
  IResetAddOrderSuccess;
