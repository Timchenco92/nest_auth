// Action Types
import { GET_CURRENT_USER_ORDER, GET_CURRENT_USER_ORDER_ERROR, GET_CURRENT_USER_ORDER_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetCurrentUserOrderState extends IBaseState {
  data: IOrder;
}

export interface IGetCurrentUserOrder {
  type: typeof GET_CURRENT_USER_ORDER;
  payload: { orderId: IOrder['id'] }
}

export interface IGetCurrentUserOrderError {
  type: typeof GET_CURRENT_USER_ORDER_ERROR;
}

export interface IGetCurrentUserOrderSuccess {
  type: typeof GET_CURRENT_USER_ORDER_SUCCESS;
  payload: IOrder;
}
