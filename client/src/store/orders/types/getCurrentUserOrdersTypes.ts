// Action Types
import { GET_CURRENT_USER_ORDERS, GET_CURRENT_USER_ORDERS_ERROR, GET_CURRENT_USER_ORDERS_SUCCESS } from '@/store/orders';

// Interfaces
import type { IOrder, IUser } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetCurrentUserOrdersState extends IBaseState {
  data: IOrder[];
}

export interface IGetCurrentUserOrders {
  type: typeof GET_CURRENT_USER_ORDERS;
  payload: { userId: IUser['id'] };
}

export interface IGetCurrentUserOrdersError {
  type: typeof GET_CURRENT_USER_ORDERS_ERROR;
}

export interface IGetCurrentUserOrdersSuccess {
  type: typeof GET_CURRENT_USER_ORDERS_SUCCESS;
  payload: IOrder[];
}
