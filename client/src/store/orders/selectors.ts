// Modules
import { createSelector } from 'reselect';

// State
import type { OrderState } from '@/store/rootReducer';

const addOrder = ({ orders }: OrderState) => ({
  error: orders.addOrder.error,
  pending: orders.addOrder.pending,
  success: orders.addOrder.success,
});

const order = ({ orders }: OrderState) => ({
  data: orders.order.data,
  error: orders.order.error,
  pending: orders.order.pending,
});

const orders = ({ orders }: OrderState) => ({
  data: orders.orders.data,
  error: orders.orders.error,
  pending: orders.orders.pending,
});

export const addOrderErrorSelector = createSelector(addOrder, ({ error }) => error);
export const addOrderPendingSelector = createSelector(addOrder, ({ pending }) => pending);
export const addOrderSuccessSelector = createSelector(addOrder, ({ success }) => success);

export const orderSelector = createSelector(order, ({ data }) => data);
export const orderErrorSelector = createSelector(order, ({ error }) => error);
export const orderPendingSelector = createSelector(order, ({ pending }) => pending);

export const ordersSelector = createSelector(orders, ({ data }) => data);
export const ordersErrorSelector = createSelector(orders, ({ error }) => error);
export const ordersPendingSelector = createSelector(orders, ({ pending }) => pending);
