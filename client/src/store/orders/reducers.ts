// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_ORDER,
  ADD_ORDER_ERROR,
  ADD_ORDER_SUCCESS,
  GET_CURRENT_USER_ORDER,
  GET_CURRENT_USER_ORDERS,
  GET_CURRENT_USER_ORDERS_ERROR,
  GET_CURRENT_USER_ORDERS_SUCCESS,
  GET_CURRENT_USER_ORDER_ERROR,
  GET_CURRENT_USER_ORDER_SUCCESS,
  RESET_ADD_ORDER_ERROR,
  RESET_ADD_ORDER_SUCCESS,
} from '@/store/orders';

// Types
import type { IOrdersState, TOrdersTypes } from '@/store/orders';

const initialState: IOrdersState = {
  addOrder: {
    error: {
      errors: {
        comment: '',
        delivery: '',
        orderProducts: '',
        total: '',
        userId: '',
      },
      hasError: false,
      message: '',
    },
    pending: false,
    success: false,
  },
  order: {
    data: {
      comment: null,
      createdAt: new Date(),
      delivery: null,
      id: '',
      orderNumber: '',
      orderProductsCount: 0,
      updatedAt: new Date(),
      userId: '',
    },
    error: false,
    pending: false,
  },
  orders: {
    data: [],
    error: false,
    pending: false,
  },
};

export const ordersReducer: Reducer<IOrdersState, TOrdersTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ORDER: {
      return {
        ...state,
        addOrder: {
          error: { ...state.addOrder.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_ORDER_ERROR: {
      return {
        ...state,
        addOrder: {
          error: { ...action.payload },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_ORDER_SUCCESS: {
      return {
        ...state,
        addOrder: {
          error: { ...state.addOrder.error },
          pending: false,
          success: true,
        },
      };
    }
    case GET_CURRENT_USER_ORDER: {
      return {
        ...state,
        order: {
          data: { ...state.order.data },
          error: false,
          pending: false,
        },
      };
    }
    case GET_CURRENT_USER_ORDER_ERROR: {
      return {
        ...state,
        order: {
          data: { ...state.order.data },
          error: false,
          pending: false,
        },
      };
    }
    case GET_CURRENT_USER_ORDER_SUCCESS: {
      return {
        ...state,
        order: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_CURRENT_USER_ORDERS: {
      return {
        ...state,
        orders: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_CURRENT_USER_ORDERS_ERROR: {
      return {
        ...state,
        orders: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_CURRENT_USER_ORDERS_SUCCESS: {
      return {
        ...state,
        orders: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case RESET_ADD_ORDER_ERROR: {
      return {
        ...state,
        addOrder: {
          error: {
            errors: { comment: '', delivery: '', orderProducts: '', total: '', userId: '' },
            hasError: false,
            message: '',
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_ADD_ORDER_SUCCESS: {
      return {
        ...state,
        addOrder: {
          error: {
            errors: { comment: '', delivery: '', orderProducts: '', total: '', userId: '' },
            hasError: false,
            message: '',
          },
          pending: false,
          success: false,
        },
      };
    }
    default:
      return state;
  }
}
