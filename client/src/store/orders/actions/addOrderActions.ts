// Action Types
import { ADD_ORDER, ADD_ORDER_ERROR, ADD_ORDER_SUCCESS } from '@/store/orders';

// Types
import type {
  IAddOrder,
  IAddOrderError,
  IAddOrderErrorStateAndPayload,
  IAddOrderPayload,
  IAddOrderSuccess,
} from '@/store/orders';

export const addOrder = (payload: IAddOrderPayload): IAddOrder => ({
  type: ADD_ORDER,
  payload,
});

export const addOrderError = (payload: IAddOrderErrorStateAndPayload): IAddOrderError => ({
  type: ADD_ORDER_ERROR,
  payload,
});

export const addOrderSuccess = (): IAddOrderSuccess => ({
  type: ADD_ORDER_SUCCESS,
});
