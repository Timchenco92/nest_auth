// Action Types
import { RESET_ADD_ORDER_ERROR, RESET_ADD_ORDER_SUCCESS } from '@/store/orders';

// Types
import { IResetAddOrderError, IResetAddOrderSuccess } from '@/store/orders';

export const resetAddOrderError = (): IResetAddOrderError => ({
  type: RESET_ADD_ORDER_ERROR,
});

export const resetAddOrderSuccess = (): IResetAddOrderSuccess => ({
  type: RESET_ADD_ORDER_SUCCESS
});
