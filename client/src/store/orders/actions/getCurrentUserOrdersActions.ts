// Action Types
import {
  GET_CURRENT_USER_ORDERS,
  GET_CURRENT_USER_ORDERS_ERROR,
  GET_CURRENT_USER_ORDERS_SUCCESS,
} from '@/store/orders';

// Interfaces
import type { IOrder, IUser } from '@/interfaces/models';

// Types
import type { IGetCurrentUserOrders, IGetCurrentUserOrdersError, IGetCurrentUserOrdersSuccess } from '@/store/orders';

export const getCurrentUserOrders = (userId: IUser['id']): IGetCurrentUserOrders => ({
  type: GET_CURRENT_USER_ORDERS,
  payload: { userId },
});

export const getCurrentUserOrdersError = (): IGetCurrentUserOrdersError => ({
  type: GET_CURRENT_USER_ORDERS_ERROR,
});

export const getCurrentUserOrdersSuccess = (payload: IOrder[]): IGetCurrentUserOrdersSuccess => ({
  type: GET_CURRENT_USER_ORDERS_SUCCESS,
  payload,
});
