// Action Types
import {
  GET_CURRENT_USER_ORDER,
  GET_CURRENT_USER_ORDER_ERROR,
  GET_CURRENT_USER_ORDER_SUCCESS,
} from '@/store/orders';

// Interfaces
import type { IOrder } from '@/interfaces/models';

// Types
import type { IGetCurrentUserOrder, IGetCurrentUserOrderError, IGetCurrentUserOrderSuccess } from '@/store/orders';

export const getCurrentUserOrder = (orderId: IOrder['id']): IGetCurrentUserOrder => ({
  type: GET_CURRENT_USER_ORDER,
  payload: { orderId },
});

export const getCurrentUserOrderError = (): IGetCurrentUserOrderError => ({
  type: GET_CURRENT_USER_ORDER_ERROR,
});

export const getCurrentUserOrderSuccess = (payload: IOrder): IGetCurrentUserOrderSuccess => ({
  type: GET_CURRENT_USER_ORDER_SUCCESS,
  payload,
});
