// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import {
  addOrder,
  addOrderError,
  addOrderSuccess,
  getCurrentUserOrder,
  getCurrentUserOrderError,
  getCurrentUserOrderSuccess,
  getCurrentUserOrders,
  getCurrentUserOrdersError,
  getCurrentUserOrdersSuccess,
} from '@/store/orders';

import { showToast } from '@/store/toast';

// Action Types
import { ADD_ORDER, GET_CURRENT_USER_ORDER, GET_CURRENT_USER_ORDERS } from '@/store/orders';
import { GET_PRODUCTS_FROM_ORDER } from '@/store/orderProducts';

// Interfaces
import { IOrder } from '@/interfaces/models';

// Sagas
import { getProductsFromOrderSaga } from '@/store/orderProducts';

// Services
import { ordersService } from '@/services';

const { createNewOrder, getUserOrder, getUserOrders } = ordersService;

function* addOrderSaga({ payload }: ReturnType<typeof addOrder>):
  Generator<CallEffect<AxiosResponse | AxiosError> | PutEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => createNewOrder(payload));
    yield put(addOrderSuccess());
    yield put(showToast({ isShowing: true, type: 'success', message: data?.message }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addOrderError({ hasError: true, errors, message }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

function* getCurrentUserOrdersSaga({ payload }: ReturnType<typeof getCurrentUserOrders>):
  Generator<CallEffect<AxiosResponse<IOrder[]> | AxiosError> | PutEffect,
  AxiosResponse | AxiosError,
  never> {
  try {
    const { userId } = payload;
    const { data }: AxiosResponse = yield call(() => getUserOrders(userId));
    yield put(getCurrentUserOrdersSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getCurrentUserOrdersError());
    return error;
  }
}

function* getCurrentUserOrderSaga({ payload }: ReturnType<typeof getCurrentUserOrder>):
  Generator<CallEffect<AxiosResponse<IOrder> | AxiosError> | PutEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { orderId } = payload;
    const { data: orderData }: AxiosResponse = yield call(() => getUserOrder(orderId));
    yield put(getCurrentUserOrderSuccess(orderData));
    yield call(getProductsFromOrderSaga, { payload: { orderId }, type: GET_PRODUCTS_FROM_ORDER});
    return orderData;
  } catch (error: any) {
    yield put(getCurrentUserOrderError());
    return error;
  }
}

export function* ordersSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_ORDER, addOrderSaga)]);
  yield all([takeLatest(GET_CURRENT_USER_ORDERS, getCurrentUserOrdersSaga)]);
  yield all([takeLatest(GET_CURRENT_USER_ORDER, getCurrentUserOrderSaga)]);
}
