// Modules
import { all, call, CallEffect, takeLatest } from 'redux-saga/effects';

// Actions
import { hideToast, showToast } from '@/store/toast';

// Action Types
import { HIDE_TOAST, SHOW_TOAST } from '@/store/toast';

// Types
import type { IToastPayload } from '@/store/toast';

function* hideToastSaga(payload: IToastPayload): Generator<CallEffect> {
  yield call(() => hideToast(payload));
}

function* showToastSaga(payload: IToastPayload): Generator<CallEffect> {
  yield call(() => showToast(payload));
}

export function* toastSaga() {
  yield all([takeLatest(HIDE_TOAST, hideToastSaga)]);
  yield all([takeLatest(SHOW_TOAST, showToastSaga)]);
}
