// Modules
import type { Reducer } from 'redux';

// Action Types
import { HIDE_TOAST, SHOW_TOAST } from '@/store/toast';

// Types
import type { IToastState, TToastActions } from '@/store/toast';

const initialState: IToastState = { isShowing: false, message: '', type: 'success' };

export const toastReducer: Reducer<IToastState, TToastActions> = (state = initialState, action) => {
  switch (action.type) {
    case HIDE_TOAST:
      return { ...state, isShowing: false, message: '', type: action.payload.type };
    case SHOW_TOAST:
      return { ...state, isShowing: true, message: action.payload.message, type: action.payload.type };
    default:
      return state;
  }
};
