// Action Types
import { HIDE_TOAST, SHOW_TOAST } from '@/store/toast';

// Types
import type { IToastHideAction, IToastShowAction, IToastPayload } from '@/store/toast';

export const showToast = (payload: IToastPayload): IToastShowAction => ({
  type: SHOW_TOAST,
  payload,
});

export const hideToast = (payload: IToastPayload): IToastHideAction => ({
  type: HIDE_TOAST,
  payload,
});
