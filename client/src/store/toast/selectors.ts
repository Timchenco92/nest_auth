// Modules
import { createSelector } from 'reselect';

// State
import type { ToastState } from '@/store/rootReducer';

const toast = ({ toast }: ToastState) => ({
  isShowing: toast.isShowing,
  message: toast.message,
  type: toast.type,
});

export const isShowingSelector = createSelector(toast, ({ isShowing }) => isShowing);
export const messageSelector = createSelector(toast, ({ message }) => message);
export const typeSelector = createSelector(toast, ({ type }) => type);
