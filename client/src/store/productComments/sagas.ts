// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import {
  addCommentToProduct,
  addCommentToProductError,
  addCommentToProductSuccess,
  getCurrentProductComments,
  getCurrentProductCommentsError,
  getCurrentProductCommentsSuccess,
} from '@/store/productComments';

import { showToast } from '@/store/toast';

// Action Types
import { ADD_COMMENT_TO_PRODUCT, GET_CURRENT_PRODUCT_COMMENTS } from '@/store/productComments';

// Interfaces
import type { IProductComment } from '@/interfaces/models';

// Services
import { productCommentsService } from '@/services';

const { addNewCommentToProduct, getCurrentProductComments: getComments } = productCommentsService;

function* addNewCommentToProductSaga({ payload }: ReturnType<typeof addCommentToProduct>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => addNewCommentToProduct(payload));
    yield put(addCommentToProductSuccess());
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const { errors, message } = error.response?.data!;
      yield put(addCommentToProductError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'error' }));
    }
    return error;
  }
}

export function* getCurrentProductCommentsSaga({ payload }: ReturnType<typeof getCurrentProductComments>):
  Generator<CallEffect<AxiosResponse<IProductComment[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { productId } = payload;
    const { data }: AxiosResponse = yield call(() => getComments(productId));
    yield put(getCurrentProductCommentsSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getCurrentProductCommentsError());
    return error;
  }
}

export function* productCommentsSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(ADD_COMMENT_TO_PRODUCT, addNewCommentToProductSaga)]);
  yield all([takeLatest(GET_CURRENT_PRODUCT_COMMENTS, getCurrentProductCommentsSaga)]);
}
