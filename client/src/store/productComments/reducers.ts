// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  ADD_COMMENT_TO_PRODUCT,
  ADD_COMMENT_TO_PRODUCT_ERROR,
  ADD_COMMENT_TO_PRODUCT_SUCCESS,
  GET_CURRENT_PRODUCT_COMMENTS,
  GET_CURRENT_PRODUCT_COMMENTS_ERROR,
  GET_CURRENT_PRODUCT_COMMENTS_SUCCESS,
  RESET_ADD_COMMENT_TO_PRODUCT_ERROR,
  RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS,
} from '@/store/productComments';

// Types
import type { IProductCommentsState, TProductCommentsTypes } from '@/store/productComments';

const initialState: IProductCommentsState = {
  add: {
    error: {
      errors: {
        description: '',
        productId: '',
        title: '',
        userId: '',
      },
      hasError: false,
    },
    pending: false,
    success: false,
  },
  comments: {
    data: [],
    error: false,
    pending: false,
  },
};

export const productCommentsReducer: Reducer<IProductCommentsState, TProductCommentsTypes> = (state = initialState, action) => {
  switch (action.type) {
    case ADD_COMMENT_TO_PRODUCT: {
      return {
        ...state,
        add: {
          error: { ...state.add.error },
          pending: true,
          success: false,
        },
      };
    }
    case ADD_COMMENT_TO_PRODUCT_ERROR: {
      return {
        ...state,
        add: {
          error: {
            errors: action.payload.errors,
            hasError: action.payload.hasError,
          },
          pending: false,
          success: false,
        },
      };
    }
    case ADD_COMMENT_TO_PRODUCT_SUCCESS: {
      return {
        ...state,
        add: {
          error: { ...state.add.error },
          pending: false,
          success: true,
        },
      };
    }
    case GET_CURRENT_PRODUCT_COMMENTS: {
      return {
        ...state,
        comments: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_CURRENT_PRODUCT_COMMENTS_ERROR: {
      return {
        ...state,
        comments: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_CURRENT_PRODUCT_COMMENTS_SUCCESS: {
      return {
        ...state,
        comments: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case RESET_ADD_COMMENT_TO_PRODUCT_ERROR: {
      return {
        ...state,
        add: {
          error: {
            errors: {
              description: '',
              productId: '',
              title: '',
              userId: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS: {
      return {
        ...state,
        add: {
          error: {
            errors: {
              description: '',
              productId: '',
              title: '',
              userId: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    default:
      return state;
  }
}
