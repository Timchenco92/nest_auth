// Actions
export {
  addCommentToProduct, addCommentToProductError, addCommentToProductSuccess,
} from './actions/addCommentToProductActions';
export {
  getCurrentProductComments, getCurrentProductCommentsError, getCurrentProductCommentsSuccess,
} from './actions/getCurrentProductCommentsActions';
export { resetAddCommentToProductError, resetAddCommentToProductSuccess } from './actions/resetActions';

// Action Types
export {
  ADD_COMMENT_TO_PRODUCT,
  ADD_COMMENT_TO_PRODUCT_ERROR,
  ADD_COMMENT_TO_PRODUCT_SUCCESS,
  GET_CURRENT_PRODUCT_COMMENTS,
  GET_CURRENT_PRODUCT_COMMENTS_ERROR,
  GET_CURRENT_PRODUCT_COMMENTS_SUCCESS,
  RESET_ADD_COMMENT_TO_PRODUCT_ERROR,
  RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS,
} from './actionTypes';

// Reducers
export { productCommentsReducer } from './reducers';

// Sagas
export { getCurrentProductCommentsSaga, productCommentsSaga } from './sagas';

// Selectors
export {
  addCommentErrorSelector,
  addCommentPendingSelector,
  addCommentSuccessSelector,
  commentsSelector,
  commentsErrorSelector,
  commentsPendingSelector,
} from './selectors';

// Types
export type {
  IAddCommentToProduct,
  IAddCommentToProductError,
  IAddCommentToProductErrorStateAndPayload,
  IAddCommentToProductPayload,
  IAddCommentToProductSuccess,
  IAddCommentToProductState,
} from './types/addCommentToProductTypes';

export type {
  IGetCurrentProductComments,
  IGetCurrentProductCommentsError,
  IGetCurrentProductCommentsState,
  IGetCurrentProductCommentsSuccess,
} from './types/getCurrentProductCommentsTypes';

export type { IResetAddCommentToProductError, IResetAddCommentToProductSuccess } from './types/resetTypes';
export type { IProductCommentsState, TProductCommentsTypes } from './types/types';
