// Modules
import { createSelector } from 'reselect';

// State
import type { ProductCommentState } from '@/store/rootReducer';

const addComment = ({ productComments }: ProductCommentState) => ({
  error: productComments.add.error,
  pending: productComments.add.pending,
  success: productComments.add.success,
});

const comments = ({ productComments }: ProductCommentState) => ({
  data: productComments.comments.data,
  error: productComments.comments.error,
  pending: productComments.comments.pending,
});

export const addCommentErrorSelector = createSelector(addComment, ({ error }) => error);
export const addCommentPendingSelector = createSelector(addComment, ({ pending }) => pending);
export const addCommentSuccessSelector = createSelector(addComment, ({ success }) => success);

export const commentsSelector = createSelector(comments, ({ data }) => data);
export const commentsErrorSelector = createSelector(comments, ({ error }) => error);
export const commentsPendingSelector = createSelector(comments, ({ pending }) => pending);
