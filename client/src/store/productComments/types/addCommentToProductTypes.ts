// Action Types
import { ADD_COMMENT_TO_PRODUCT, ADD_COMMENT_TO_PRODUCT_ERROR, ADD_COMMENT_TO_PRODUCT_SUCCESS } from '@/store/productComments';

// Interfaces
import type { IProduct, IUser } from '@/interfaces/models';

export interface IAddCommentToProductState {
  error: IAddCommentToProductErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IAddCommentToProduct {
  type: typeof ADD_COMMENT_TO_PRODUCT;
  payload: IAddCommentToProductPayload;
}

export interface IAddCommentToProductPayload {
  description: string;
  productId: IProduct['id'];
  title: string;
  userId: IUser['id'];
}

export interface IAddCommentToProductError {
  type: typeof ADD_COMMENT_TO_PRODUCT_ERROR;
  payload: IAddCommentToProductErrorStateAndPayload;
}

export interface IAddCommentToProductErrorStateAndPayload {
  errors: IErrors;
  hasError: boolean;
}

interface IErrors {
  description: string;
  productId: string;
  title: string;
  userId: string
}

export interface IAddCommentToProductSuccess {
  type: typeof ADD_COMMENT_TO_PRODUCT_SUCCESS;
}
