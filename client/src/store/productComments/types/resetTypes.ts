// Action Types
import { RESET_ADD_COMMENT_TO_PRODUCT_ERROR, RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS } from '@/store/productComments';

export interface IResetAddCommentToProductError {
  type: typeof RESET_ADD_COMMENT_TO_PRODUCT_ERROR;
}

export interface IResetAddCommentToProductSuccess {
  type: typeof RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS;
}
