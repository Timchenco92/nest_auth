// Types
import type {
  IAddCommentToProduct,
  IAddCommentToProductError,
  IAddCommentToProductState,
  IAddCommentToProductSuccess,
  IGetCurrentProductComments,
  IGetCurrentProductCommentsError,
  IGetCurrentProductCommentsState,
  IGetCurrentProductCommentsSuccess,
  IResetAddCommentToProductError,
  IResetAddCommentToProductSuccess,
} from '@/store/productComments';

export interface IProductCommentsState {
  add: IAddCommentToProductState;
  comments: IGetCurrentProductCommentsState;
}

export type TProductCommentsTypes = IAddCommentToProduct |
  IAddCommentToProductError |
  IAddCommentToProductSuccess |
  IGetCurrentProductComments |
  IGetCurrentProductCommentsError |
  IGetCurrentProductCommentsSuccess |
  IResetAddCommentToProductError |
  IResetAddCommentToProductSuccess;
