// Action Types
import { GET_CURRENT_PRODUCT_COMMENTS, GET_CURRENT_PRODUCT_COMMENTS_ERROR, GET_CURRENT_PRODUCT_COMMENTS_SUCCESS } from '@/store/productComments';

// Interfaces
import type { IProduct, IProductComment } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetCurrentProductCommentsState extends IBaseState{
  data: IProductComment[];
}

export interface IGetCurrentProductComments {
  type: typeof GET_CURRENT_PRODUCT_COMMENTS;
  payload: { productId: IProduct['id'] };
}

export interface IGetCurrentProductCommentsError {
  type: typeof GET_CURRENT_PRODUCT_COMMENTS_ERROR;
}

export interface IGetCurrentProductCommentsSuccess {
  type: typeof GET_CURRENT_PRODUCT_COMMENTS_SUCCESS;
  payload: IProductComment[];
}
