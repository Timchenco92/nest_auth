// Action Types
import {
  GET_CURRENT_PRODUCT_COMMENTS,
  GET_CURRENT_PRODUCT_COMMENTS_ERROR,
  GET_CURRENT_PRODUCT_COMMENTS_SUCCESS,
} from '@/store/productComments';

// Interfaces
import type { IProduct, IProductComment } from '@/interfaces/models';

// Types
import type {
  IGetCurrentProductComments,
  IGetCurrentProductCommentsError,
  IGetCurrentProductCommentsSuccess,
} from '@/store/productComments';

export const getCurrentProductComments = (productId: IProduct['id']): IGetCurrentProductComments => ({
  type: GET_CURRENT_PRODUCT_COMMENTS,
  payload: { productId },
});

export const getCurrentProductCommentsError = (): IGetCurrentProductCommentsError => ({
  type: GET_CURRENT_PRODUCT_COMMENTS_ERROR,
});

export const getCurrentProductCommentsSuccess = (payload: IProductComment[]): IGetCurrentProductCommentsSuccess => ({
  type: GET_CURRENT_PRODUCT_COMMENTS_SUCCESS,
  payload,
});
