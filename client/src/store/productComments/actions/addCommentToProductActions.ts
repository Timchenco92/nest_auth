// Action Types
import {
  ADD_COMMENT_TO_PRODUCT,
  ADD_COMMENT_TO_PRODUCT_ERROR,
  ADD_COMMENT_TO_PRODUCT_SUCCESS,
} from '@/store/productComments';

// Types
import type {
  IAddCommentToProduct,
  IAddCommentToProductError,
  IAddCommentToProductErrorStateAndPayload,
  IAddCommentToProductPayload,
  IAddCommentToProductSuccess,
} from '@/store/productComments';

export const addCommentToProduct = (payload: IAddCommentToProductPayload): IAddCommentToProduct => ({
  type: ADD_COMMENT_TO_PRODUCT,
  payload,
});

export const addCommentToProductError = (payload: IAddCommentToProductErrorStateAndPayload): IAddCommentToProductError => ({
  type: ADD_COMMENT_TO_PRODUCT_ERROR,
  payload,
});

export const addCommentToProductSuccess = (): IAddCommentToProductSuccess => ({
  type: ADD_COMMENT_TO_PRODUCT_SUCCESS,
});
