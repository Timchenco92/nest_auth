// Action Types
import { RESET_ADD_COMMENT_TO_PRODUCT_ERROR, RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS } from '@/store/productComments';

// Types
import { IResetAddCommentToProductError, IResetAddCommentToProductSuccess } from '@/store/productComments';

export const resetAddCommentToProductError = (): IResetAddCommentToProductError => ({
  type: RESET_ADD_COMMENT_TO_PRODUCT_ERROR,
});

export const resetAddCommentToProductSuccess = (): IResetAddCommentToProductSuccess => ({
  type: RESET_ADD_COMMENT_TO_PRODUCT_SUCCESS,
});
