// Modules
import { createSelector } from 'reselect';

// State
import type { ShopGalleryState } from '@/store/rootReducer';

const gallery = ({ shopGallery }: ShopGalleryState) => ({
  data: shopGallery.gallery.data,
  error: shopGallery.gallery.error,
  pending: shopGallery.gallery.pending,
});

export const shopGallerySelector = createSelector(gallery, ({ data }) => data);
export const shopGalleryErrorSelector = createSelector(gallery, ({ error }) => error);
export const shopGalleryPendingSelector = createSelector(gallery, ({ pending }) => pending);
