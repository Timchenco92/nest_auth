// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import { getShopGallery, getShopGalleryError, getShopGallerySuccess } from '@/store/shopGallery';

// Action Types
import { GET_SHOP_GALLERY } from '@/store/shopGallery';

// Interfaces
import type { IShopGallery } from '@/interfaces/models';

// Services
import { shopGalleryService } from '@/services';

const { getGallery } = shopGalleryService;

export function* getShopGallerySaga({ payload }: ReturnType<typeof getShopGallery>):
  Generator<CallEffect<AxiosResponse<IShopGallery[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { shopId } = payload;
    const { data }: AxiosResponse = yield call(() => getGallery(shopId));
    yield put(getShopGallerySuccess(data));
    return data;
  } catch (error: any) {
    yield put(getShopGalleryError());
    return error;
  }
}

export function* shopGallerySaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_SHOP_GALLERY, getShopGallerySaga)]);
}
