// Modules Types
import type { Reducer } from 'redux';

// Action Types
import { GET_SHOP_GALLERY, GET_SHOP_GALLERY_ERROR, GET_SHOP_GALLERY_SUCCESS } from '@/store/shopGallery';

// Types
import type { IShopGalleryState, TShopGalleryTypes } from '@/store/shopGallery';

const initialState: IShopGalleryState = {
  gallery: {
    data: [],
    error: false,
    pending: false,
  },
};

export const shopGalleryReducer: Reducer<IShopGalleryState, TShopGalleryTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_SHOP_GALLERY: {
      return {
        ...state,
        gallery: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_SHOP_GALLERY_ERROR: {
      return {
        ...state,
        gallery: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_SHOP_GALLERY_SUCCESS: {
      return {
        ...state,
        gallery: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    default: return state;
  }
}
