// Action Types
import { GET_SHOP_GALLERY, GET_SHOP_GALLERY_ERROR, GET_SHOP_GALLERY_SUCCESS } from '@/store/shopGallery';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';

// Types
import type { IGetShopGallery, IGetShopGalleryError, IGetShopGallerySuccess } from '@/store/shopGallery';

export const getShopGallery = (shopId: IShop['id']): IGetShopGallery => ({
  type: GET_SHOP_GALLERY,
  payload: { shopId },
});

export const getShopGalleryError = (): IGetShopGalleryError => ({
  type: GET_SHOP_GALLERY_ERROR,
});

export const getShopGallerySuccess = (payload: IShopGallery[]): IGetShopGallerySuccess => ({
  type: GET_SHOP_GALLERY_SUCCESS,
  payload,
});
