// Action Types
import { GET_SHOP_GALLERY, GET_SHOP_GALLERY_ERROR, GET_SHOP_GALLERY_SUCCESS } from '@/store/shopGallery';

// Interfaces
import type { IShop, IShopGallery } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetShopGalleryState extends IBaseState {
  data: IShopGallery[];
}

export interface IGetShopGallery {
  type: typeof GET_SHOP_GALLERY;
  payload: { shopId: IShop['id'] };
}

export interface IGetShopGalleryError {
  type: typeof GET_SHOP_GALLERY_ERROR;
}

export interface IGetShopGallerySuccess {
  type: typeof GET_SHOP_GALLERY_SUCCESS;
  payload: IShopGallery[];
}
