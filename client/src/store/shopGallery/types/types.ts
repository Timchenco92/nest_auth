// Types
import type { IGetShopGallery, IGetShopGalleryError, IGetShopGalleryState, IGetShopGallerySuccess } from '@/store/shopGallery';

export interface IShopGalleryState {
  gallery: IGetShopGalleryState;
}

export type TShopGalleryTypes = IGetShopGallery |
  IGetShopGalleryError |
  IGetShopGallerySuccess;
