// Modules
import { createSelector } from 'reselect';

// State
import type { CategoryState } from '@/store/rootReducer';

const categories = ({ categories }: CategoryState) => ({
  data: categories.categories.data,
  error: categories.categories.error,
  pending: categories.categories.pending,
});

const category = ({ categories }: CategoryState) => ({
  data: categories.category.data,
  error: categories.category.error,
  pending: categories.category.pending,
});

export const categoriesSelector = createSelector(categories, ({ data }) => data);
export const categoriesErrorSelector = createSelector(categories, ({ error }) => error);
export const categoriesPendingSelector = createSelector(categories, ({ pending }) => pending);

export const categorySelector = createSelector(category, ({ data }) => data);
export const categoryErrorSelector = createSelector(category, ({ error }) => error);
export const categoryPendingSelector = createSelector(category, ({ pending }) => pending);
