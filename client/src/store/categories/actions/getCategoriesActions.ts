// Action Types
import { GET_CATEGORIES, GET_CATEGORIES_ERROR, GET_CATEGORIES_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';

// Types
import type { IGetCategories, IGetCategoriesError, IGetCategoriesSuccess } from '@/store/categories';

export const getCategories = (): IGetCategories => ({
  type: GET_CATEGORIES,
});

export const getCategoriesError = (): IGetCategoriesError => ({
  type: GET_CATEGORIES_ERROR,
});

export const getCategoriesSuccess = (payload: ICategory[]): IGetCategoriesSuccess => ({
  type: GET_CATEGORIES_SUCCESS,
  payload,
});
