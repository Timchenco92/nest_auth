// Action Types
import { GET_CATEGORY, GET_CATEGORY_ERROR, GET_CATEGORY_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';

// Types
import type { IGetCategory, IGetCategoryError, IGetCategorySuccess } from '@/store/categories';

export const getCategory = (categoryId: ICategory['id']): IGetCategory => ({
  type: GET_CATEGORY,
  payload: { categoryId },
});

export const getCategoryError = (): IGetCategoryError => ({
  type: GET_CATEGORY_ERROR,
});

export const getCategorySuccess = (payload: ICategory): IGetCategorySuccess => ({
  type: GET_CATEGORY_SUCCESS,
  payload,
});
