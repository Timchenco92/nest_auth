// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import { getCategory, getCategorySuccess, getCategoryError, getCategoriesSuccess, getCategoriesError } from '@/store/categories';

// Action Types
import { GET_CATEGORIES, GET_CATEGORY } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';

// Services
import { categoriesService } from '@/services';

const { getAllCategories, getMainCategory } = categoriesService;


function* getCategoriesSaga():
  Generator<CallEffect<AxiosResponse<ICategory[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getAllCategories());
    yield put(getCategoriesSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getCategoriesError());
    return error;
  }
}

function* getCategorySaga({ payload }: ReturnType<typeof getCategory>):
  Generator<CallEffect<AxiosResponse<ICategory> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { categoryId } = payload;
    const { data }: AxiosResponse = yield call(() => getMainCategory(categoryId));
    yield put(getCategorySuccess(data));
    return data;
  } catch (error: any) {
    yield put(getCategoryError());
    return error;
  }
}

export function* categoriesSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_CATEGORIES, getCategoriesSaga)]);
  yield all([takeLatest(GET_CATEGORY, getCategorySaga)]);
}
