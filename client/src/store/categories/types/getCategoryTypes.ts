// Action Types
import { GET_CATEGORY, GET_CATEGORY_ERROR, GET_CATEGORY_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetCategoryState extends IBaseState {
  data: ICategory;
}

export interface IGetCategory {
  type: typeof GET_CATEGORY;
  payload: {
    categoryId: ICategory['id'],
  };
}

export interface IGetCategoryError {
  type: typeof GET_CATEGORY_ERROR;
}

export interface IGetCategorySuccess {
  type: typeof GET_CATEGORY_SUCCESS;
  payload: ICategory;
}
