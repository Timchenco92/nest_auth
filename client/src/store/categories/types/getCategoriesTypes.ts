// Action Types
import { GET_CATEGORIES, GET_CATEGORIES_ERROR, GET_CATEGORIES_SUCCESS } from '@/store/categories';

// Interfaces
import type { ICategory } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetCategoriesState extends IBaseState {
  data: ICategory[];
}

export interface IGetCategories {
  type: typeof GET_CATEGORIES;
}

export interface IGetCategoriesError {
  type: typeof GET_CATEGORIES_ERROR;
}

export interface IGetCategoriesSuccess {
  type: typeof GET_CATEGORIES_SUCCESS;
  payload: ICategory[];
}
