// Types
import type {
  IGetCategories,
  IGetCategoriesError,
  IGetCategoriesState,
  IGetCategoriesSuccess,
  IGetCategory,
  IGetCategoryError,
  IGetCategoryState,
  IGetCategorySuccess,
} from '@/store/categories';

export interface ICategoriesState {
  categories: IGetCategoriesState;
  category: IGetCategoryState;
}

export type TCategoriesTypes = IGetCategories |
  IGetCategoriesError |
  IGetCategoriesSuccess |
  IGetCategory |
  IGetCategoryError |
  IGetCategorySuccess;
