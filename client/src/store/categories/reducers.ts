// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  GET_CATEGORIES,
  GET_CATEGORIES_ERROR,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORY,
  GET_CATEGORY_ERROR,
  GET_CATEGORY_SUCCESS,
} from '@/store/categories';

// Types
import type { ICategoriesState, TCategoriesTypes } from '@/store/categories';

const initialState: ICategoriesState = {
  categories: {
    data: [],
    error: false,
    pending: false,
  },
  category: {
    data: {
      id: '',
      createdAt: new Date(),
      imageUrl: '',
      slug: '',
      title: '',
      updatedAt: new Date(),
    },
    error: false,
    pending: false,
  },
};

export const categoriesReducer: Reducer<ICategoriesState, TCategoriesTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES: {
      return {
        ...state,
        categories: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_CATEGORIES_ERROR: {
      return {
        ...state,
        categories: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_CATEGORIES_SUCCESS: {
      return {
        ...state,
        categories: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_CATEGORY: {
      return {
        ...state,
        category: {
          data: { ...state.category.data },
          error: false,
          pending: true,
        },
      };
    }
    case GET_CATEGORY_ERROR: {
      return {
        ...state,
        category: {
          data: { ...state.category.data },
          error: true,
          pending: false,
        },
      };
    }
    case GET_CATEGORY_SUCCESS: {
      return {
        ...state,
        category: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    default:
      return state;
  }
}
