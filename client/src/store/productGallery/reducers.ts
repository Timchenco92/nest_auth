// Modules Types
import type { Reducer } from 'redux';

// Action Types
import { GET_PRODUCT_GALLERY, GET_PRODUCT_GALLERY_ERROR, GET_PRODUCT_GALLERY_SUCCESS } from '@/store/productGallery';

// Types
import type { IProductGalleryState, TProductGalleryTypes } from '@/store/productGallery';

const initialState: IProductGalleryState = {
  gallery: {
    data: [],
    error: false,
    pending: false,
  }
};

export const productGalleryReducer: Reducer<IProductGalleryState, TProductGalleryTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT_GALLERY: {
      return {
        ...state,
        gallery: {
          data: [],
          error: false,
          pending: true,
        },
      }
    }
    case GET_PRODUCT_GALLERY_ERROR: {
      return {
        ...state,
        gallery: {
          data: [],
          error: true,
          pending: false,
        },
      }
    }
    case GET_PRODUCT_GALLERY_SUCCESS: {
      return {
        ...state,
        gallery: {
          data: action.payload,
          error: false,
          pending: false,
        },
      }
    }
    default:
      return state;
  }
}
