// Types
import type {
  IGetProductGallery,
  IGetProductGalleryError,
  IGetProductGalleryState,
  IGetProductGallerySuccess,
} from '@/store/productGallery';

export interface IProductGalleryState {
  gallery: IGetProductGalleryState;
}

export type TProductGalleryTypes = IGetProductGallery |
  IGetProductGalleryError |
  IGetProductGallerySuccess;
