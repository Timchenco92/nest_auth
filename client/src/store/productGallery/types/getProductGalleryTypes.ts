// Action Types
import { GET_PRODUCT_GALLERY, GET_PRODUCT_GALLERY_ERROR, GET_PRODUCT_GALLERY_SUCCESS } from '@/store/productGallery';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetProductGalleryState extends IBaseState{
  data: IProductGallery[];
}

export interface IGetProductGallery {
  type: typeof GET_PRODUCT_GALLERY;
  payload: { productId: IProduct['id'] };
}

export interface IGetProductGalleryError {
  type: typeof GET_PRODUCT_GALLERY_ERROR;
}

export interface IGetProductGallerySuccess {
  type: typeof GET_PRODUCT_GALLERY_SUCCESS;
  payload: IProductGallery[];
}
