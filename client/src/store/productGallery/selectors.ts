// Modules
import { createSelector } from 'reselect';

// State
import type { ProductGalleryState } from '@/store/rootReducer';

const gallery = ({ productGallery }: ProductGalleryState) => ({
  data: productGallery.gallery.data,
  error: productGallery.gallery.error,
  pending: productGallery.gallery.pending,
});

export const productGallerySelector = createSelector(gallery, ({ data }) => data);
export const productGalleryErrorSelector = createSelector(gallery, ({ error }) => error);
export const productGalleryPendingSelector = createSelector(gallery, ({ pending }) => pending);
