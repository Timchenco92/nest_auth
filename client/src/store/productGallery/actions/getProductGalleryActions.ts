// Action Types
import { GET_PRODUCT_GALLERY, GET_PRODUCT_GALLERY_ERROR, GET_PRODUCT_GALLERY_SUCCESS } from '@/store/productGallery';

// Interfaces
import type { IProduct, IProductGallery } from '@/interfaces/models';

// Types
import type { IGetProductGallery, IGetProductGalleryError, IGetProductGallerySuccess } from '@/store/productGallery';

export const getProductGallery = (productId: IProduct['id']): IGetProductGallery => ({
  type: GET_PRODUCT_GALLERY,
  payload: { productId }
});

export const getProductGalleryError = (): IGetProductGalleryError => ({
  type: GET_PRODUCT_GALLERY_ERROR,
});

export const getProductGallerySuccess = (payload: IProductGallery[]): IGetProductGallerySuccess => ({
  type: GET_PRODUCT_GALLERY_SUCCESS,
  payload,
});
