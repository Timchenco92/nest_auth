// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import { getProductGallery, getProductGalleryError, getProductGallerySuccess } from '@/store/productGallery';

// Action Types
import { GET_PRODUCT_GALLERY } from '@/store/productGallery';

// Interfaces
import type { IProductGallery } from '@/interfaces/models';

// Services
import { productGalleryService } from '@/services';

const { getCurrentProductGallery } = productGalleryService;

export function* getProductGallerySaga({ payload }: ReturnType<typeof getProductGallery>):
  Generator<CallEffect<AxiosResponse<IProductGallery[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { productId } = payload;
    const { data }: AxiosResponse = yield call(() =>getCurrentProductGallery(productId));
    yield put(getProductGallerySuccess(data));
    return data;
  } catch (error: any) {
    yield put(getProductGalleryError());
    return error;
  }
}

export function* productGallerySaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_PRODUCT_GALLERY, getProductGallerySaga)]);
}
