// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  GET_BANNERS_ERROR,
  GET_BANNERS_REQUEST,
  GET_BANNERS_SUCCESS,
} from '@/store/banners';

// Types
import type { IBannerState, TBannersTypes } from '@/store/banners';

const initialState: IBannerState = {
  banners: {
    data: [],
    error: false,
    pending: false,
  },
};

export const bannerReducer: Reducer<IBannerState, TBannersTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_BANNERS_REQUEST: {
      return {
        ...state,
        banners: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_BANNERS_ERROR: {
      return {
        ...state,
        banners: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_BANNERS_SUCCESS: {
      return {
        ...state,
        banners: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    default:
      return state;
  }
};
