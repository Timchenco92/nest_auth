// Modules
import { createSelector } from 'reselect';

// State
import type { BannerState } from '@/store/rootReducer';

const getBanners = ({ banners }: BannerState) => ({
  data: banners.banners.data,
  error: banners.banners.error,
  pending: banners.banners.pending,
});

export const bannersSelector = createSelector(getBanners, ({ data }) => data);
export const bannersErrorSelector = createSelector(getBanners, ({ error }) => error);
export const bannersPendingSelector = createSelector(getBanners, ({ pending }) => pending);
