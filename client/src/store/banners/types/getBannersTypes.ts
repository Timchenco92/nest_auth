// Action Types
import { GET_BANNERS_ERROR, GET_BANNERS_REQUEST, GET_BANNERS_SUCCESS } from '@/store/banners';

// Interfaces
import type { IBanner } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetBannersState extends IBaseState {
  data: IBanner[];
}

export interface IGetBannersError {
  type: typeof GET_BANNERS_ERROR;
  payload?: any;
}

export interface IGetBannersRequest {
  type: typeof GET_BANNERS_REQUEST;
}

export interface IGetBannersSuccess {
  type: typeof GET_BANNERS_SUCCESS;
  payload: IBanner[];
}
