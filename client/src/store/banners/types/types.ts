// Types
import type { IGetBannersError, IGetBannersRequest, IGetBannersState, IGetBannersSuccess } from '@/store/banners';

export interface IBannerState {
  banners: IGetBannersState;
}

export type TBannersTypes = IGetBannersError |
  IGetBannersRequest |
  IGetBannersSuccess;
