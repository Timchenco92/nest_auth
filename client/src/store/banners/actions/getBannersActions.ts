// Action Types
import { GET_BANNERS_ERROR, GET_BANNERS_REQUEST, GET_BANNERS_SUCCESS } from '@/store/banners';

// Interfaces
import type { IBanner } from '@/interfaces/models';

// Types
import type { IGetBannersError, IGetBannersRequest, IGetBannersSuccess } from '@/store/banners';

export const getBannersError = (): IGetBannersError => ({
  type: GET_BANNERS_ERROR,
});

export const getBannersRequest = (): IGetBannersRequest => ({
  type: GET_BANNERS_REQUEST,
});

export const getBannersSuccess = (payload: IBanner[]): IGetBannersSuccess => ({
  type: GET_BANNERS_SUCCESS,
  payload,
});
