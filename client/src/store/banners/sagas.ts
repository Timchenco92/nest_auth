// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import { getBannersError, getBannersSuccess } from '@/store/banners';

// Action Types
import { GET_BANNERS_REQUEST } from '@/store/banners';

// Interfaces
import type { IBanner } from '@/interfaces/models';

// Services
import { bannersService } from '@/services';

const { getBanners } = bannersService;

function* getBannersSaga(): Generator<CallEffect<AxiosResponse<IBanner[]> | AxiosError> | PutEffect,
  AxiosResponse | AxiosError,
  never> {
  try {
    const { data }: AxiosResponse = yield call(() => getBanners());
    yield put(getBannersSuccess(data));
    return data;
  } catch (error: any) {
    put(getBannersError());
    return error;
  }
}

export function* bannerSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_BANNERS_REQUEST, getBannersSaga)]);
}
