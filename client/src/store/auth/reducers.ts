// Modules Types
import { Reducer } from 'redux';

// Action Types
import {
  GET_ME_ERROR,
  GET_ME_REQUEST,
  GET_ME_SUCCESS,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  RECOVER_PASSWORD_ERROR,
  RECOVER_PASSWORD_REQUEST,
  RECOVER_PASSWORD_SUCCESS,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  RESET_LOGIN_ERROR,
  RESET_LOGIN_SUCCESS,
  RESET_RECOVER_PASSWORD_ERROR,
  RESET_RECOVER_PASSWORD_SUCCESS,
  RESET_REGISTER_ERROR,
  RESET_REGISTER_SUCCESS,
} from '@/store/auth';

// Types
import type { IAuthState, TAuthTypes } from '@/store/auth';

const initialState: IAuthState = {
  login: {
    error: {
      hasError: false,
      errors: {
        email: '',
        password: '',
      },
    },
    pending: false,
    success: false,
  },
  me: {
    data: {
      email: '',
      lastName: '',
      firstName: '',
      updatedAt: new Date(),
      phone: '',
      avatarUrl: null,
      id: '',
      middleName: '',
      createdAt: new Date(),
      address: null,
    },
    error: false,
    isAuth: false,
    pending: false,
  },
  recoverPassword: {
    error: {
      errors: {
        email: '',
      },
      hasError: false,
    },
    pending: false,
    success: false,
  },
  register: {
    error: {
      errors: {
        email: '',
        password: '',
        avatar: '',
        firstName: '',
        middleName: '',
        lastName: '',
        phone: '',
        address: '',
      },
      hasError: false,
    },
    pending: false,
    success: false,
  },
};

export const authReducer: Reducer<IAuthState, TAuthTypes> = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST: {
      return {
        ...state,
        login: {
          error: { ...state.login.error },
          pending: true,
          success: false,
        },
      };
    }
    case LOGIN_ERROR: {
      return {
        ...state,
        login: {
          error: {
            errors: { ...action.payload.errors },
            hasError: action.payload.hasError,
          },
          pending: false,
          success: false,
        },
      };
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        login: {
          error: { ...state.login.error },
          pending: false,
          success: true,
        },
      };
    }
    case GET_ME_REQUEST: {
      return {
        ...state,
        me: {
          data: { ...state.me.data },
          error: false,
          isAuth: false,
          pending: true,
        },
      };
    }
    case GET_ME_ERROR: {
      return {
        ...state,
        me: {
          data: { ...state.me.data },
          error: true,
          isAuth: false,
          pending: false,
        },
      };
    }
    case GET_ME_SUCCESS: {
      return {
        ...state,
        me: {
          data: action.payload.data,
          error: false,
          isAuth: action.payload.isAuth,
          pending: false,
        },
      };
    }
    case RECOVER_PASSWORD_REQUEST: {
      return {
        ...state,
        recoverPassword: {
          error: { ...state.recoverPassword.error },
          pending: true,
          success: false,
        },
      };
    }
    case RECOVER_PASSWORD_ERROR: {
      return {
        ...state,
        recoverPassword: {
          error: {
            errors: { ...action.payload.errors },
            hasError: action.payload.hasError,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RECOVER_PASSWORD_SUCCESS: {
      return {
        ...state,
        recoverPassword: {
          error: { ...state.recoverPassword.error },
          pending: false,
          success: true,
        },
      };
    }
    case REGISTER_REQUEST: {
      return {
        ...state,
        register: {
          error: { ...state.register.error },
          pending: true,
          success: false,
        },
      };
    }
    case REGISTER_ERROR: {
      return {
        ...state,
        register: {
          error: { ...action.payload },
          pending: false,
          success: false,
        },
      };
    }
    case REGISTER_SUCCESS: {
      return {
        ...state,
        register: {
          error: { ...state.register.error },
          pending: false,
          success: true,
        },
      };
    }
    case RESET_LOGIN_ERROR: {
      return {
        ...state,
        login: {
          error: {
            errors: {
              email: '',
              password: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_LOGIN_SUCCESS: {
      return {
        ...state,
        login: {
          error: {
            errors: {
              email: '',
              password: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_RECOVER_PASSWORD_ERROR: {
      return {
        ...state,
        recoverPassword: {
          error: {
            errors: { email: '' },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_RECOVER_PASSWORD_SUCCESS: {
      return {
        ...state,
        recoverPassword: {
          error: {
            errors: { email: '' },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_REGISTER_ERROR: {
      return {
        ...state,
        register: {
          error: {
            errors: {
              address: '',
              avatar: '',
              email: '',
              firstName: '',
              lastName: '',
              middleName: '',
              password: '',
              phone: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    case RESET_REGISTER_SUCCESS: {
      return {
        ...state,
        register: {
          error: {
            errors: {
              address: '',
              avatar: '',
              email: '',
              firstName: '',
              lastName: '',
              middleName: '',
              password: '',
              phone: '',
            },
            hasError: false,
          },
          pending: false,
          success: false,
        },
      };
    }
    default:
      return state;
  }
};
