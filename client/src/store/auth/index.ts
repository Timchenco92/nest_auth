// Actions
export { getMeError, getMeRequest, getMeSuccess } from './actions/getMeActions';
export { loginError, loginRequest, loginSuccess } from './actions/loginActions';
export { recoverPasswordError, recoverPasswordRequest, recoverPasswordSuccess } from './actions/recoverPasswordActions';
export { registerError, registerRequest, registerSuccess } from './actions/registerActions';
export {
  resetLoginsError,
  resetLoginsSuccess,
  resetRecoverPasswordError,
  resetRecoverPasswordSuccess,
  resetRegisterError,
  resetRegisterSuccess,
} from './actions/resetActions';

// Action Types
export {
  GET_ME_ERROR,
  GET_ME_REQUEST,
  GET_ME_SUCCESS,
  LOGIN_ERROR,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  RECOVER_PASSWORD_ERROR,
  RECOVER_PASSWORD_REQUEST,
  RECOVER_PASSWORD_SUCCESS,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  RESET_LOGIN_ERROR,
  RESET_LOGIN_SUCCESS,
  RESET_RECOVER_PASSWORD_ERROR,
  RESET_RECOVER_PASSWORD_SUCCESS,
  RESET_REGISTER_ERROR,
  RESET_REGISTER_SUCCESS,
} from './actionTypes';

// Reducers
export { authReducer } from './reducers';

// Sagas
export { authSaga } from './sagas';

// Selectors
export {
  currentUserDataSelector,
  currentUserErrorSelector,
  currentUserIsAuthSelector,
  currentUserPendingSelector,
  loginErrorSelector,
  loginPendingSelector,
  loginSuccessSelector,
  recoverPasswordErrorSelector,
  recoverPasswordPendingSelector,
  recoverPasswordSuccessSelector,
  registerErrorSelector,
  registerPendingSelector,
  registerSuccessSelector,
} from './selectors';

// Types
export type { IGetMeError, IGetMeRequest, IGetMeState, IGetMeSuccess, IGetMeSuccessPayload } from './types/getMeTypes';
export type {
  ILoginError,
  ILoginErrorStateAndPayload,
  ILoginRequest,
  ILoginState,
  ILoginSuccess,
} from './types/loginTypes';

export type {
  IRecoverPasswordError,
  IRecoverPasswordErrorStateAndPayload,
  IRecoverPasswordRequest,
  IRecoverPasswordState,
  IRecoverPasswordSuccess,
} from './types/recoverPasswordTypes';

export type {
  IRegisterError,
  IRegisterErrorStateAndPayload,
  IRegisterRequest,
  IRegisterState,
  IRegisterSuccess,
} from './types/registerTypes';

export type {
  IResetLoginError,
  IResetLoginSuccess,
  IResetRecoverPasswordError,
  IResetRecoverPasswordSuccess,
  IResetRegisterError,
  IResetRegisterSuccess,
} from './types/resetTypes';

export type { IAuthState, TAuthTypes } from './types/types';
