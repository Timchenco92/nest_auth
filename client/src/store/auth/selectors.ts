// Modules
import { createSelector } from 'reselect';

// State
import type { AuthState } from '@/store/rootReducer';

const login = ({ auth }: AuthState) => ({
  error: auth.login.error,
  pending: auth.login.pending,
  success: auth.login.success,
});

const getMe = ({ auth }: AuthState) => ({
  data: auth.me.data,
  error: auth.me.error,
  isAuth: auth.me.isAuth,
  pending: auth.me.pending,
});

const recoverPassword = ({ auth }: AuthState) => ({
  error: auth.recoverPassword.error,
  pending: auth.recoverPassword.pending,
  success: auth.recoverPassword.success,
});

const register = ({ auth }: AuthState) => ({
  error: auth.register.error,
  pending: auth.register.pending,
  success: auth.register.success,
});

export const loginErrorSelector = createSelector(login, ({ error }) => error);
export const loginPendingSelector = createSelector(login, ({ pending }) => pending);
export const loginSuccessSelector = createSelector(login, ({ success }) => success);

export const currentUserDataSelector = createSelector(getMe, ({ data }) => data);
export const currentUserErrorSelector = createSelector(getMe, ({ error }) => error);
export const currentUserIsAuthSelector = createSelector(getMe, ({ isAuth }) => isAuth);
export const currentUserPendingSelector = createSelector(getMe, ({ pending }) => pending);

export const recoverPasswordErrorSelector = createSelector(recoverPassword, ({ error }) => error);
export const recoverPasswordPendingSelector = createSelector(recoverPassword, ({ pending }) => pending);
export const recoverPasswordSuccessSelector = createSelector(recoverPassword, ({ success }) => success);

export const registerErrorSelector = createSelector(register, ({ error }) => error);
export const registerPendingSelector = createSelector(register, ({ pending }) => pending);
export const registerSuccessSelector = createSelector(register, ({ success }) => success);
