// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Action
import {
  getMeError,
  getMeSuccess,
  loginError,
  loginRequest,
  loginSuccess,
  recoverPasswordError,
  recoverPasswordRequest,
  recoverPasswordSuccess,
  registerError,
  registerRequest,
  registerSuccess,
} from '@/store/auth';

import { showToast } from '@/store/toast';

// Action Types
import { GET_ME_REQUEST, LOGIN_REQUEST, RECOVER_PASSWORD_REQUEST, REGISTER_REQUEST } from '@/store/auth';

// Constants
import { TOAST_TYPES } from '@/constants';

// Services
import { authService } from '@/services';

const { getMe, login, restorePassword, register } = authService;

function* getMeSaga(): Generator<CallEffect<AxiosResponse | AxiosError>
  | PutEffect
  | ForkEffect
  | CallEffect,
  AxiosResponse | AxiosError,
  never> {
  try {
    const { data }: AxiosResponse = yield call(() => getMe());
    yield put(getMeSuccess({ data, isAuth: true }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance && error?.response?.status === 401) {
      yield call(() => localStorage.removeItem('access_token'));
      yield put(getMeError({ isAuth: false }));
    }
    return error;
  }
}

function* loginSaga({ payload }: ReturnType<typeof loginRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => login(payload));
    yield put(loginSuccess(data));
    yield call(() => localStorage.setItem('access_token', data?.accessToken));
    yield call(getMeSaga);
    yield put(showToast({ isShowing: true, message: data?.message, type: TOAST_TYPES.success }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const message = error.response?.data?.message;
      const errors = error.response?.data?.errors;
      yield put(loginError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: TOAST_TYPES.danger }));
    }
    return error;
  }
}

function* recoverPasswordSaga({ payload }: ReturnType<typeof recoverPasswordRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { email } = payload;
    const { data }: AxiosResponse = yield call(() => restorePassword(email));
    yield put(recoverPasswordSuccess());
    yield put(showToast({ isShowing: true, message: data?.message, type: TOAST_TYPES.success }));
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const message = error.response?.data?.message;
      const errors = error.response?.data?.errors;
      yield put(recoverPasswordError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: 'danger' }));
    }
    return error;
  }
}

function* registerSaga({ payload }: ReturnType<typeof registerRequest>):
  Generator<CallEffect<AxiosResponse | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => register(payload));
    yield put(registerSuccess(data));
    yield call(() => localStorage.setItem('access_token', data?.accessToken));
    yield put(showToast({ isShowing: true, message: data?.message, type: TOAST_TYPES.success }));
    yield call(getMeSaga);
    return data;
  } catch (error: any) {
    const isInstance = error instanceof AxiosError;
    if (isInstance) {
      const message = error.response?.data?.message;
      const errors = error.response?.data?.errors;
      yield put(registerError({ errors, hasError: true }));
      yield put(showToast({ isShowing: true, message, type: TOAST_TYPES.danger }));
    }
    return error;
  }
}

export function* authSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_ME_REQUEST, getMeSaga)]);
  yield all([takeLatest(LOGIN_REQUEST, loginSaga)]);
  yield all([takeLatest(RECOVER_PASSWORD_REQUEST, recoverPasswordSaga)]);
  yield all([takeLatest(REGISTER_REQUEST, registerSaga)]);
}
