// Action Types
import { GET_ME_ERROR, GET_ME_REQUEST, GET_ME_SUCCESS } from '@/store/auth';

// Types
import type { IGetMeError, IGetMeRequest, IGetMeSuccess, IGetMeSuccessPayload } from '@/store/auth';

export const getMeError = ({ isAuth = false }): IGetMeError => ({
  type: GET_ME_ERROR,
  isAuth,
});

export const getMeRequest = (): IGetMeRequest => ({
  type: GET_ME_REQUEST,
});

export const getMeSuccess = (payload: IGetMeSuccessPayload): IGetMeSuccess => ({
  type: GET_ME_SUCCESS,
  payload,
});
