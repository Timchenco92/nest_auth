// Action Types
import { LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS } from '@/store/auth';

// Types
import type { ILoginError, ILoginErrorStateAndPayload, ILoginRequest, ILoginSuccess } from '@/store/auth';

export const loginError = (payload: ILoginErrorStateAndPayload): ILoginError => ({
  type: LOGIN_ERROR,
  payload,
});

export const loginRequest = ({ email = '', password = ''}): ILoginRequest => ({
  type: LOGIN_REQUEST,
  payload: {
    email,
    password,
  }
});

export const loginSuccess = (payload: object): ILoginSuccess => ({
  type: LOGIN_SUCCESS,
  payload
});
