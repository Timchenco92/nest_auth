// Action Types
import { REGISTER_ERROR, REGISTER_REQUEST, REGISTER_SUCCESS } from '@/store/auth';

// Types
import type {
  IRegisterError,
  IRegisterErrorStateAndPayload,
  IRegisterRequest,
  IRegisterSuccess,
} from '@/store/auth';

export const registerError = (payload: IRegisterErrorStateAndPayload): IRegisterError => ({
  type: REGISTER_ERROR,
  payload,
});

export const registerRequest = (payload: FormData): IRegisterRequest => ({
  type: REGISTER_REQUEST,
  payload,
});

export const registerSuccess = (payload: string): IRegisterSuccess => ({
  type: REGISTER_SUCCESS,
  payload
});
