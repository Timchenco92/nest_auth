// Action Types
import { RECOVER_PASSWORD_ERROR, RECOVER_PASSWORD_REQUEST, RECOVER_PASSWORD_SUCCESS } from '@/store/auth';

// Types
import type {
  IRecoverPasswordError,
  IRecoverPasswordErrorStateAndPayload,
  IRecoverPasswordRequest,
  IRecoverPasswordSuccess,
} from '@/store/auth';

export const recoverPasswordError = (payload: IRecoverPasswordErrorStateAndPayload): IRecoverPasswordError => ({
  type: RECOVER_PASSWORD_ERROR,
  payload,
});

export const recoverPasswordRequest = ({ email = '' }): IRecoverPasswordRequest => ({
  type: RECOVER_PASSWORD_REQUEST,
  payload: {
    email,
  },
});

export const recoverPasswordSuccess = (): IRecoverPasswordSuccess => ({
  type: RECOVER_PASSWORD_SUCCESS,
});
