// Action Types
import {
  RESET_LOGIN_ERROR,
  RESET_LOGIN_SUCCESS,
  RESET_RECOVER_PASSWORD_ERROR,
  RESET_RECOVER_PASSWORD_SUCCESS,
  RESET_REGISTER_ERROR,
  RESET_REGISTER_SUCCESS,
} from '@/store/auth';

// Types
import type {
  IResetLoginError,
  IResetLoginSuccess,
  IResetRecoverPasswordError,
  IResetRecoverPasswordSuccess,
  IResetRegisterError,
  IResetRegisterSuccess,
} from '@/store/auth';

export const resetLoginsError = (): IResetLoginError => ({
  type: RESET_LOGIN_ERROR,
});

export const resetLoginsSuccess = (): IResetLoginSuccess => ({
  type: RESET_LOGIN_SUCCESS,
});

export const resetRecoverPasswordError = (): IResetRecoverPasswordError => ({
  type: RESET_RECOVER_PASSWORD_ERROR,
});

export const resetRecoverPasswordSuccess = (): IResetRecoverPasswordSuccess => ({
  type: RESET_RECOVER_PASSWORD_SUCCESS,
});

export const resetRegisterError = (): IResetRegisterError => ({
  type: RESET_REGISTER_ERROR,
});

export const resetRegisterSuccess = (): IResetRegisterSuccess => ({
  type: RESET_REGISTER_SUCCESS,
});
