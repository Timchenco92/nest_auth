// Action Types
import { LOGIN_ERROR, LOGIN_REQUEST, LOGIN_SUCCESS } from '@/store/auth';

export interface ILoginState {
  error: ILoginErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface ILoginError {
  type: typeof LOGIN_ERROR;
  payload: ILoginErrorStateAndPayload;
}

export interface ILoginErrorStateAndPayload {
  errors: IErrors;
  hasError: boolean;
}

interface IErrors {
  email: string;
  password: string;
}

export interface ILoginRequest {
  type: typeof LOGIN_REQUEST;
  payload: {
    email: string,
    password: string,
  };
}

export interface ILoginSuccess {
  type: typeof LOGIN_SUCCESS;
  payload: object;
}
