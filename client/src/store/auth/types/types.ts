// Types
import type {
  IGetMeError,
  IGetMeRequest,
  IGetMeState,
  IGetMeSuccess,
  ILoginError,
  ILoginRequest,
  ILoginState,
  ILoginSuccess,
  IRecoverPasswordError,
  IRecoverPasswordRequest,
  IRecoverPasswordState,
  IRecoverPasswordSuccess,
  IRegisterError,
  IRegisterRequest,
  IRegisterState,
  IRegisterSuccess,
  IResetLoginError,
  IResetLoginSuccess,
  IResetRecoverPasswordError,
  IResetRecoverPasswordSuccess,
  IResetRegisterError,
  IResetRegisterSuccess,
} from '@/store/auth';

export interface IAuthState {
  login: ILoginState;
  me: IGetMeState;
  recoverPassword: IRecoverPasswordState;
  register: IRegisterState;
}

export type TAuthTypes = IGetMeError |
  IGetMeRequest |
  IGetMeSuccess |
  ILoginError |
  ILoginRequest |
  ILoginSuccess |
  IRecoverPasswordError |
  IRecoverPasswordRequest |
  IRecoverPasswordSuccess |
  IRegisterError |
  IRegisterRequest |
  IRegisterSuccess |
  IResetLoginError |
  IResetLoginSuccess |
  IResetRecoverPasswordError |
  IResetRecoverPasswordSuccess |
  IResetRegisterError |
  IResetRegisterSuccess;
