// Action Types
import { GET_ME_ERROR, GET_ME_REQUEST, GET_ME_SUCCESS } from '@/store/auth';

// Interfaces
import type { IUser } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetMeState extends IBaseState {
  data: IUser;
  isAuth: boolean;
}

export interface IGetMeError {
  type: typeof GET_ME_ERROR;
  isAuth: boolean;
}

export interface IGetMeRequest {
  type: typeof GET_ME_REQUEST;
}

export interface IGetMeSuccess {
  payload: IGetMeSuccessPayload
  type: typeof GET_ME_SUCCESS;
}

export interface IGetMeSuccessPayload {
  data: IUser;
  isAuth: boolean;
}
