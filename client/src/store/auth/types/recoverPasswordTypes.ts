// Action Types
import { RECOVER_PASSWORD_ERROR, RECOVER_PASSWORD_REQUEST, RECOVER_PASSWORD_SUCCESS } from '@/store/auth';

export interface IRecoverPasswordState {
  error: IRecoverPasswordErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IRecoverPasswordError {
  type: typeof RECOVER_PASSWORD_ERROR;
  payload: IRecoverPasswordErrorStateAndPayload;
}

export interface IRecoverPasswordErrorStateAndPayload {
  hasError: boolean;
  errors: {
    email: string;
  };
}

export interface IRecoverPasswordRequest {
  type: typeof RECOVER_PASSWORD_REQUEST;
  payload: {
    email: string,
  };
}

export interface IRecoverPasswordSuccess {
  type: typeof RECOVER_PASSWORD_SUCCESS;
}
