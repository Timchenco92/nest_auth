// Action Types
import {
  RESET_LOGIN_ERROR,
  RESET_LOGIN_SUCCESS,
  RESET_RECOVER_PASSWORD_ERROR,
  RESET_RECOVER_PASSWORD_SUCCESS,
  RESET_REGISTER_ERROR,
  RESET_REGISTER_SUCCESS,
} from '@/store/auth';

export interface IResetLoginError {
  type: typeof RESET_LOGIN_ERROR;
}

export interface IResetLoginSuccess {
  type: typeof RESET_LOGIN_SUCCESS;
}

export interface IResetRecoverPasswordError {
  type: typeof RESET_RECOVER_PASSWORD_ERROR;
}

export interface IResetRecoverPasswordSuccess {
  type: typeof RESET_RECOVER_PASSWORD_SUCCESS;
}

export interface IResetRegisterError {
  type: typeof RESET_REGISTER_ERROR;
}

export interface IResetRegisterSuccess {
  type: typeof RESET_REGISTER_SUCCESS;
}
