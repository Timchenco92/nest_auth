// Action Types
import { REGISTER_ERROR, REGISTER_REQUEST, REGISTER_SUCCESS } from '@/store/auth';

export interface IRegisterState {
  error: IRegisterErrorStateAndPayload;
  pending: boolean;
  success: boolean;
}

export interface IRegisterError {
  type: typeof REGISTER_ERROR;
  payload: IRegisterErrorStateAndPayload;
}

export interface IRegisterErrorStateAndPayload {
  hasError: boolean;
  errors: IErrors;
}

interface IErrors {
  address: string;
  avatar: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  password: string;
  phone: string;
}

export interface IRegisterRequest {
  type: typeof REGISTER_REQUEST;
  payload: FormData
}

export interface IRegisterSuccess {
  type: typeof REGISTER_SUCCESS;
  payload: string;
}
