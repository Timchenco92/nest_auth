// Action Types
import { GET_PRODUCTS, GET_PRODUCTS_ERROR, GET_PRODUCTS_SUCCESS } from '@/store/products';

// Interfaces
import type { ICategory, IProduct } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetProductsState extends IBaseState {
  data: IProduct[];
}

export interface IGetProducts {
  type: typeof GET_PRODUCTS;
  payload: { categoryId: ICategory['id'] };
}

export interface IGetProductsError {
  type: typeof GET_PRODUCTS_ERROR;
}

export interface IGetProductsSuccess {
  type: typeof GET_PRODUCTS_SUCCESS;
  payload: IProduct[];
}
