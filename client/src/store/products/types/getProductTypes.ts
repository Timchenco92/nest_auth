// Action Types
import { GET_PRODUCT, GET_PRODUCT_ERROR, GET_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetProductState extends IBaseState {
  data: IProduct;
}

export interface IGetProduct {
  type: typeof GET_PRODUCT;
  payload: { productId: IProduct['id'] };
}

export interface IGetProductError {
  type: typeof GET_PRODUCT_ERROR;
}

export interface IGetProductSuccess {
  type: typeof GET_PRODUCT_SUCCESS;
  payload: IProduct;
}
