// Types
import type {
  IGetProduct,
  IGetProductError,
  IGetProductState,
  IGetProductSuccess,
  IGetProducts,
  IGetProductsError,
  IGetProductsState,
  IGetProductsSuccess,
  IGetRandomProducts,
  IGetRandomProductsError,
  IGetRandomProductsState,
  IGetRandomProductsSuccess,
} from '@/store/products';

export interface IProductsState {
  product: IGetProductState;
  products: IGetProductsState;
  randomProducts: IGetRandomProductsState;
}

export type TProductsTypes = IGetProduct |
  IGetProductError |
  IGetProductSuccess |
  IGetProducts |
  IGetProductsError |
  IGetProductsSuccess |
  IGetRandomProducts |
  IGetRandomProductsError |
  IGetRandomProductsSuccess;
