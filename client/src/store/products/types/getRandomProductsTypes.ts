// Action Types
import { GET_RANDOM_PRODUCTS, GET_RANDOM_PRODUCTS_ERROR, GET_RANDOM_PRODUCTS_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IGetRandomProductsState extends IBaseState {
  data: IProduct[];
}

export interface IGetRandomProducts {
  type: typeof GET_RANDOM_PRODUCTS;
}

export interface IGetRandomProductsError {
  type: typeof GET_RANDOM_PRODUCTS_ERROR;
}

export interface IGetRandomProductsSuccess {
  type: typeof GET_RANDOM_PRODUCTS_SUCCESS;
  payload: IProduct[];
}
