// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_ERROR,
  GET_PRODUCT_SUCCESS,
  GET_RANDOM_PRODUCTS,
  GET_RANDOM_PRODUCTS_ERROR,
  GET_RANDOM_PRODUCTS_SUCCESS,
} from '@/store/products';

// Types
import type { IProductsState, TProductsTypes } from '@/store/products';

const initialState: IProductsState = {
  product: {
    data: {
      categoryId: '',
      createdAt: new Date(),
      id: '',
      imageUrl: '',
      price: '',
      slug: '',
      title: '',
      updatedAt: new Date(),
    },
    error: false,
    pending: false,
  },
  products: {
    data: [],
    error: false,
    pending: false,
  },
  randomProducts: {
    data: [],
    error: false,
    pending: false,
  },
};

export const productsReducer: Reducer<IProductsState, TProductsTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCT: {
      return {
        ...state,
        product: {
          data: { ...state.product.data },
          error: false,
          pending: true,
        },
      };
    }
    case GET_PRODUCT_ERROR: {
      return {
        ...state,
        product: {
          data: { ...state.product.data },
          error: true,
          pending: false,
        },
      };
    }
    case GET_PRODUCT_SUCCESS: {
      return {
        ...state,
        product: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_PRODUCTS: {
      return {
        ...state,
        products: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_PRODUCTS_ERROR: {
      return {
        ...state,
        products: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_PRODUCTS_SUCCESS: {
      return {
        ...state,
        products: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_RANDOM_PRODUCTS: {
      return {
        ...state,
        randomProducts: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_RANDOM_PRODUCTS_ERROR: {
      return {
        ...state,
        randomProducts: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_RANDOM_PRODUCTS_SUCCESS: {
      return {
        ...state,
        randomProducts: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    default:
      return state;
  }
}
