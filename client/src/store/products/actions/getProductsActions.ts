// Action Types
import { GET_PRODUCTS, GET_PRODUCTS_ERROR, GET_PRODUCTS_SUCCESS } from '@/store/products';

// Interfaces
import type { ICategory, IProduct } from '@/interfaces/models';

// Types
import type { IGetProducts, IGetProductsError, IGetProductsSuccess } from '@/store/products';

export const getProducts = (categoryId: ICategory['id']): IGetProducts => ({
  type: GET_PRODUCTS,
  payload: { categoryId },
});

export const getProductsError = (): IGetProductsError => ({
  type: GET_PRODUCTS_ERROR,
});

export const getProductsSuccess = (payload: IProduct[]): IGetProductsSuccess => ({
  type: GET_PRODUCTS_SUCCESS,
  payload,
});
