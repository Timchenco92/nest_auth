// Action Types
import { GET_RANDOM_PRODUCTS, GET_RANDOM_PRODUCTS_ERROR, GET_RANDOM_PRODUCTS_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

// Types
import type { IGetRandomProducts, IGetRandomProductsError, IGetRandomProductsSuccess } from '@/store/products';

export const getRandomProducts = (): IGetRandomProducts => ({
  type: GET_RANDOM_PRODUCTS,
});

export const getRandomProductsError = (): IGetRandomProductsError => ({
  type: GET_RANDOM_PRODUCTS_ERROR,
});

export const getRandomProductsSuccess = (payload: IProduct[]): IGetRandomProductsSuccess => ({
  type: GET_RANDOM_PRODUCTS_SUCCESS,
  payload,
});
