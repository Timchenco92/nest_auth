// Action Types
import { GET_PRODUCT, GET_PRODUCT_ERROR, GET_PRODUCT_SUCCESS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

// Types
import type { IGetProduct, IGetProductError, IGetProductSuccess } from '@/store/products';

export const getProduct = (productId: IProduct['id']): IGetProduct => ({
  type: GET_PRODUCT,
  payload: { productId },
});

export const getProductError = (): IGetProductError => ({
  type: GET_PRODUCT_ERROR,
});

export const getProductSuccess = (payload: IProduct): IGetProductSuccess => ({
  type: GET_PRODUCT_SUCCESS,
  payload,
});
