// Actions
export { getProduct, getProductError, getProductSuccess } from './actions/getProductActions';
export { getProducts, getProductsError, getProductsSuccess } from './actions/getProductsActions';
export {
  getRandomProducts, getRandomProductsError, getRandomProductsSuccess,
} from './actions/getRandomProductsActions';

// Action Types
export {
  GET_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCTS_ERROR,
  GET_PRODUCTS_SUCCESS,
  GET_PRODUCT_ERROR,
  GET_PRODUCT_SUCCESS,
  GET_RANDOM_PRODUCTS,
  GET_RANDOM_PRODUCTS_ERROR,
  GET_RANDOM_PRODUCTS_SUCCESS,
} from './actionTypes';

// Reducers
export { productsReducer } from './reducers';

// Sagas
export { getProductsSaga, productsSaga } from './sagas';

// Selectors
export {
  productErrorSelector,
  productPendingSelector,
  productSelector,
  productsErrorSelector,
  productsPendingSelector,
  productsSelector,
  randomProductsErrorSelector,
  randomProductsPendingSelector,
  randomProductsSelector,
} from './selectors';

// Types
export type { IGetProducts, IGetProductsError, IGetProductsState, IGetProductsSuccess } from './types/getProductsTypes';
export type { IGetProduct, IGetProductError, IGetProductState, IGetProductSuccess } from './types/getProductTypes';
export type {
  IGetRandomProducts, IGetRandomProductsError, IGetRandomProductsState, IGetRandomProductsSuccess,
} from './types/getRandomProductsTypes';
export type { IProductsState, TProductsTypes } from './types/types';
