// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import {
  getProduct,
  getProductError,
  getProductSuccess,
  getProducts,
  getProductsError,
  getProductsSuccess,
  getRandomProductsError,
  getRandomProductsSuccess,
} from '@/store/products';

// Action Types
import { GET_PRODUCT, GET_PRODUCTS, GET_RANDOM_PRODUCTS } from '@/store/products';

// Interfaces
import type { IProduct } from '@/interfaces/models';

// Services
import { productsService } from '@/services';

const { getMainProduct, getProductsByCategory, getRandomProducts } = productsService;

export function* getProductsSaga({ payload }: ReturnType<typeof getProducts>):
  Generator<CallEffect<AxiosResponse<IProduct[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { categoryId } = payload;
    const { data }: AxiosResponse = yield call(() => getProductsByCategory(categoryId));
    yield put(getProductsSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getProductsError());
    return error;
  }
}

function* getProductSaga({ payload }: ReturnType<typeof getProduct>):
  Generator<CallEffect<AxiosResponse<IProduct> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { productId } = payload;
    const { data }: AxiosResponse = yield call(() => getMainProduct(productId));
    yield put(getProductSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getProductError());
    return error;
  }
}

function* getRandomProductsSaga():
  Generator<CallEffect<AxiosResponse<IProduct[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getRandomProducts());
    yield put(getRandomProductsSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getRandomProductsError());
    return error;
  }
}

export function* productsSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_PRODUCT, getProductSaga)]);
  yield all([takeLatest(GET_PRODUCTS, getProductsSaga)]);
  yield all([takeLatest(GET_RANDOM_PRODUCTS, getRandomProductsSaga)]);
}
