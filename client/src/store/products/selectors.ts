// Modules
import { createSelector } from 'reselect';

// State
import type { ProductState } from '@/store/rootReducer';

const product = ({ products }: ProductState) => ({
  data: products.product.data,
  error: products.product.error,
  pending: products.product.pending,
});

const products = ({ products }: ProductState) => ({
  data: products.products.data,
  error: products.products.error,
  pending: products.products.pending,
});

const randomProducts = ({ products }: ProductState) => ({
  data: products.randomProducts.data,
  error: products.randomProducts.error,
  pending: products.randomProducts.pending,
});

export const productSelector = createSelector(product, ({ data }) => data);
export const productErrorSelector = createSelector(product, ({ error }) => error);
export const productPendingSelector = createSelector(product, ({ pending }) => pending);

export const productsSelector = createSelector(products, ({ data }) => data);
export const productsErrorSelector = createSelector(products, ({ error }) => error);
export const productsPendingSelector = createSelector(products, ({ pending }) => pending);

export const randomProductsSelector = createSelector(randomProducts, ({ data }) => data);
export const randomProductsErrorSelector = createSelector(randomProducts, ({ error }) => error);
export const randomProductsPendingSelector = createSelector(randomProducts, ({ pending }) => pending);
