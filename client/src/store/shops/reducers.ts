// Modules Types
import type { Reducer } from 'redux';

// Action Types
import {
  GET_ALL_SHOPS,
  GET_ALL_SHOPS_ERROR,
  GET_ALL_SHOPS_SUCCESS,
  GET_MAIN_SHOP,
  GET_MAIN_SHOP_ERROR,
  GET_MAIN_SHOP_SUCCESS,
} from '@/store/shops';

// Types
import type { IShopsState, TShopsTypes } from '@/store/shops';

const initialState: IShopsState = {
  shop: {
    data: {
      address: '',
      createdAt: new Date(),
      endTimeWork: new Date(),
      firstPhone: '',
      id: '',
      imageUrl: '',
      secondPhone: null,
      slug: '',
      startTimeWork: new Date(),
      thirdPhone: null,
      title: '',
      updatedAt: new Date(),
    },
    error: false,
    pending: false,
  },
  shops: {
    data: [],
    error: false,
    pending: false,
  },
};

export const shopsReducer: Reducer<IShopsState, TShopsTypes> = (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_SHOPS: {
      return {
        ...state,
        shops: {
          data: [],
          error: false,
          pending: true,
        },
      };
    }
    case GET_ALL_SHOPS_ERROR: {
      return {
        ...state,
        shops: {
          data: [],
          error: true,
          pending: false,
        },
      };
    }
    case GET_ALL_SHOPS_SUCCESS: {
      return {
        ...state,
        shops: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    case GET_MAIN_SHOP: {
      return {
        ...state,
        shop: {
          data: { ...state.shop.data },
          error: false,
          pending: true,
        },
      };
    }
    case GET_MAIN_SHOP_ERROR: {
      return {
        ...state,
        shop: {
          data: { ...state.shop.data },
          error: true,
          pending: false,
        },
      };
    }
    case GET_MAIN_SHOP_SUCCESS: {
      return {
        ...state,
        shop: {
          data: action.payload,
          error: false,
          pending: false,
        },
      };
    }
    default: return state;
  }
}
