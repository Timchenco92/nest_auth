// Modules
import { createSelector } from 'reselect';

// State
import type { ShopState } from '@/store/rootReducer';

const shop = ({ shops }: ShopState) => ({
  data: shops.shop.data,
  error: shops.shop.error,
  pending: shops.shop.pending,
});

const shops = ({ shops }: ShopState) => ({
  data: shops.shops.data,
  error: shops.shops.error,
  pending: shops.shops.pending,
});

export const shopSelector = createSelector(shop, ({ data }) => data);
export const shopErrorSelector = createSelector(shop, ({ error }) => error);
export const shopPendingSelector = createSelector(shop, ({ pending }) => pending);

export const shopsSelector = createSelector(shops, ({ data }) => data);
export const shopsErrorSelector = createSelector(shops, ({ error }) => error);
export const shopsPendingSelector = createSelector(shops, ({ pending }) => pending);
