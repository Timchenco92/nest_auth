// Modules Types
import type { CallEffect, PutEffect, AllEffect, ForkEffect } from 'redux-saga/effects';

// Modules
import { call, put, all, takeLatest } from 'redux-saga/effects';
import { AxiosResponse, AxiosError } from 'axios';

// Actions
import { getShop, getShopError, getShopSuccess, getShopsError, getShopsSuccess } from '@/store/shops';

// Action Types
import { GET_ALL_SHOPS, GET_MAIN_SHOP } from '@/store/shops';
import { GET_SHOP_GALLERY } from '@/store/shopGallery';

// Interfaces
import type { IShop } from '@/interfaces/models';

// Sagas
import { getShopGallerySaga } from '@/store/shopGallery';

// Services
import { shopsService } from '@/services';

const { getAllShops, getMainShop } = shopsService;

function* getAllShopsSaga():
  Generator<CallEffect<AxiosResponse<IShop[]> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { data }: AxiosResponse = yield call(() => getAllShops());
    yield put(getShopsSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getShopsError());
    return error;
  }
}

function* getMainShopSaga({ payload }: ReturnType<typeof getShop>):
  Generator<CallEffect<AxiosResponse<IShop> | AxiosError>
    | PutEffect
    | CallEffect,
    AxiosResponse | AxiosError,
    never> {
  try {
    const { shopId } = payload;
    const { data }: AxiosResponse = yield call(() => getMainShop(shopId));
    yield call(getShopGallerySaga, { payload: { shopId }, type: GET_SHOP_GALLERY });
    yield put(getShopSuccess(data));
    return data;
  } catch (error: any) {
    yield put(getShopError())
    return error;
  }
}

export function* shopsSaga(): Generator<AllEffect<ForkEffect<never>>> {
  yield all([takeLatest(GET_ALL_SHOPS, getAllShopsSaga)]);
  yield all([takeLatest(GET_MAIN_SHOP, getMainShopSaga)]);
}
