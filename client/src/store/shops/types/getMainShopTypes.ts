// Action Types
import { GET_MAIN_SHOP, GET_MAIN_SHOP_ERROR, GET_MAIN_SHOP_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IMainShopState extends IBaseState {
  data: IShop;
}

export interface IGetMainShop {
  type: typeof GET_MAIN_SHOP;
  payload: { shopId: IShop['id'] };
}

export interface IGetMainShopError {
  type: typeof GET_MAIN_SHOP_ERROR;
}

export interface IGetMainShopSuccess {
  type: typeof GET_MAIN_SHOP_SUCCESS;
  payload: IShop;
}
