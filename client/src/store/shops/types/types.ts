// Types
import type {
  IAllShopsState,
  IGetAllShops,
  IGetAllShopsError,
  IGetAllShopsSuccess,
  IGetMainShop,
  IGetMainShopError,
  IGetMainShopSuccess,
  IMainShopState,
} from '@/store/shops';

export interface IShopsState {
  shop: IMainShopState;
  shops: IAllShopsState;
}

export type TShopsTypes = IGetAllShops |
  IGetAllShopsError |
  IGetAllShopsSuccess |
  IGetMainShop |
  IGetMainShopError |
  IGetMainShopSuccess;
