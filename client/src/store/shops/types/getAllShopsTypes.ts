// Action Types
import { GET_ALL_SHOPS, GET_ALL_SHOPS_ERROR, GET_ALL_SHOPS_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';
import type { IBaseState } from '@/store/interfaces';

export interface IAllShopsState extends IBaseState{
  data: IShop[];
}

export interface IGetAllShops {
  type: typeof GET_ALL_SHOPS;
}

export interface IGetAllShopsError {
  type: typeof GET_ALL_SHOPS_ERROR;
}

export interface IGetAllShopsSuccess {
  type: typeof GET_ALL_SHOPS_SUCCESS;
  payload: IShop[];
}
