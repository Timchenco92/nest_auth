// Action Types
import { GET_ALL_SHOPS, GET_ALL_SHOPS_ERROR, GET_ALL_SHOPS_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

// Types
import type { IGetAllShops, IGetAllShopsError, IGetAllShopsSuccess } from '@/store/shops';

export const getShops = (): IGetAllShops => ({
  type: GET_ALL_SHOPS,
});

export const getShopsError = (): IGetAllShopsError => ({
  type: GET_ALL_SHOPS_ERROR,
});

export const getShopsSuccess = (payload: IShop[]): IGetAllShopsSuccess => ({
  type: GET_ALL_SHOPS_SUCCESS,
  payload,
});
