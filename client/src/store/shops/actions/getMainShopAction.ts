// Action Types
import { GET_MAIN_SHOP, GET_MAIN_SHOP_ERROR, GET_MAIN_SHOP_SUCCESS } from '@/store/shops';

// Interfaces
import type { IShop } from '@/interfaces/models';

// Types
import type { IGetMainShop, IGetMainShopError, IGetMainShopSuccess } from '@/store/shops';

export const getShop = (shopId: IShop['id']): IGetMainShop => ({
  type: GET_MAIN_SHOP,
  payload: { shopId },
});

export const getShopError = (): IGetMainShopError => ({
  type: GET_MAIN_SHOP_ERROR,
});

export const getShopSuccess = (payload: IShop): IGetMainShopSuccess => ({
  type: GET_MAIN_SHOP_SUCCESS,
  payload,
});
