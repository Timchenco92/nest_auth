// Modules
import { combineReducers } from 'redux';

// Reducers
import { authReducer } from '@/store/auth';
import { bannerReducer } from '@/store/banners';
import { categoriesReducer } from '@/store/categories';
import { orderProductsReducer } from '@/store/orderProducts';
import { ordersReducer } from '@/store/orders';
import { productCommentsReducer } from '@/store/productComments';
import { productGalleryReducer } from '@/store/productGallery';
import { productsReducer } from '@/store/products';
import { shopGalleryReducer } from '@/store/shopGallery';
import { shopsReducer } from '@/store/shops';
import { toastReducer } from '@/store/toast';

const rootReducer = combineReducers({
  auth: authReducer,
  banners: bannerReducer,
  categories: categoriesReducer,
  orderProducts: orderProductsReducer,
  orders: ordersReducer,
  productComments: productCommentsReducer,
  productGallery: productGalleryReducer,
  products: productsReducer,
  shopGallery: shopGalleryReducer,
  shops: shopsReducer,
  toast: toastReducer,
});

export type AuthState = ReturnType<typeof rootReducer>;
export type BannerState = ReturnType<typeof rootReducer>;
export type CategoryState = ReturnType<typeof rootReducer>;
export type OrderProductState = ReturnType<typeof rootReducer>;
export type OrderState = ReturnType<typeof rootReducer>;
export type ProductCommentState = ReturnType<typeof rootReducer>;
export type ProductGalleryState = ReturnType<typeof rootReducer>;
export type ProductState = ReturnType<typeof rootReducer>;
export type ShopGalleryState = ReturnType<typeof rootReducer>;
export type ShopState = ReturnType<typeof rootReducer>;
export type ToastState = ReturnType<typeof rootReducer>;


export default rootReducer;
