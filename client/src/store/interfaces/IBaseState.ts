export interface IBaseState {
  error: boolean;
  pending: boolean;
}
