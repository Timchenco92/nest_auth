// Modules
import { all, fork } from 'redux-saga/effects';

// Sagas
import { authSaga } from '@/store/auth';
import { bannerSaga } from '@/store/banners';
import { categoriesSaga } from '@/store/categories';
import { orderProductsSaga } from '@/store/orderProducts';
import { ordersSaga } from '@/store/orders';
import { productCommentsSaga } from '@/store/productComments'
import { productGallerySaga } from '@/store/productGallery';
import { productsSaga } from '@/store/products';
import { shopGallerySaga } from '@/store/shopGallery';
import { shopsSaga } from '@/store/shops';
import { toastSaga } from '@/store/toast';

function* rootSaga() {
  yield all([fork(authSaga)]);
  yield all([fork(bannerSaga)]);
  yield all([fork(categoriesSaga)]);
  yield all([fork(orderProductsSaga)]);
  yield all([fork(ordersSaga)]);
  yield all([fork(productCommentsSaga)]);
  yield all([fork(productGallerySaga)]);
  yield all([fork(productsSaga)]);
  yield all([fork(shopGallerySaga)]);
  yield all([fork(shopsSaga)]);
  yield all([fork(toastSaga)]);
}

export default rootSaga;
